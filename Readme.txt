IceCube game engine documentation and usage


LICENSING
=========
See "Copying.txt" for licensing information and copyright notices.


USAGE
=====
The IceCube engine binary is a drop-in replacement for the Cube engine binary.

You will need a copy of the Cube binary distribution for the maps and media.

You will also need updated SDL libraries and the latest zlib library. For
Windows, SDL libraries can be downloaded from the SDL website, and you may need
to build zlib1.dll by compiling zlib from source.

IceCube is not compatible with old savegames and demos. Furthermore, IceCube
uses the latest ENet version which results in network protocol differences from
the open source Cube client. You can revert to the original ENet library to
workaround the issue.


BUILD STEPS
===========
Compiling the sources should be straight forward.

Unix users need to make sure to have the development version of all libs
installed (OpenGL, SDL, SDL_Mixer, SDL_Image, zlib, libpng). You will need
to compile ENet separately before building IceCube. In the enet folder, run
configure to generate the Makefile, then run make to build ENet. Then to build
IceCube, change to the src folder for IceCube and run make. Object files and
the executables are placed in the src directory.

Windows users can use the included Visual Studio project files, which
references the lib/include directories for the external libraries and should
thus be self contained. Builds will place executables in the bin or bin64
directory.

Linux builds may run into an issue where there are some missing OpenGL
functions. To fix this, you can comment out those occurences without losing
core gameplay/editing functionality.

macOS builds may run into an issue with undefined OpenGL constants. To fix
this, you can comment out those occurences without losing functionality.

More information is available on the IceCube wiki:
https://bitbucket.org/wildly/icecube/wiki/Home


DESIGN
======
The readme from the Cube engine ("LICENSE-Cube.txt") has a note on the design
of the Cube engine.

The goals for IceCube are to add updated gameplay and other enhancements while
maintaining compatibility with maps included with Cube.


OPEN SOURCE
===========
The IceCube game engine is open source (see "Copying.txt").

Our git repository is located at: https://bitbucket.org/wildly/icecube


AUTHORS
=======

Cube
----

Wouter "Aardappel" van Oortmerssen
http://strlen.com

For additional authors/contributors, see the cube binary distribution readme.html

Sauerbraten
-----------

Wouter "Aardappel" van Oortmerssen
http://strlen.com

Lee "eihrul" Salzman
http://lee.fov120.com

Mike "Gilt" Dysart

Robert "baby-rabbit" Pointon
http://www.fernlightning.com

Quinton "Quin" Reeves
http://bloodfrontier.com

For additional authors/contributors, see the Sauerbraten binary distribution readme.

Tesseract
---------

Wouter "Aardappel" van Oortmerssen
http://strlen.com

Lee "eihrul" Salzman
http://sauerbraten.org/lee/

Mike "Gilt" Dysart

Robert "baby-rabbit" Pointon
http://www.fernlightning.com

Quinton "Quin" Reeves
http://www.redeclipse.net

Benjamin Segovia

For additional authors/contributors, see the Tesseract distribution readme.

IceCube
-------

Willy "wildly" Deng