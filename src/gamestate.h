// Shared things between game server and game client

//#include "cube.h"

// SDL_GetTicks() or custom milliseconds counter.
// Please implement this in client.cpp or serverutil.cpp
extern uint millisecondCtr(void);

// BaseClock runs in discrete milliseconds
struct BaseClock
{
protected:
    int elapsedMsec;
    // timestamp of the most recent call to this object's update function
    uint lastUpdated;

public:
    BaseClock()                       { reset(); };
    BaseClock(uint tick, int elapsed) { reset(tick, elapsed); };

    void update() { update(millisecondCtr()); };

    void update(uint currentTick)
    {
        const int elapsedMsec_delta = currentTick - lastUpdated;
        lastUpdated = currentTick;
        elapsedMsec += elapsedMsec_delta;
    };

    void reset(uint currentTick, int newElapsedMsec)
    {
        lastUpdated = currentTick;
        elapsedMsec = newElapsedMsec;
    };

    void reset() { reset(millisecondCtr(), 0); };

    int getSeconds() { return elapsedMsec / 1000; };
    int getMillis() { return elapsedMsec; };
};

// supports delta-millis, pause, and time-scaling!
struct WarpableClock : BaseClock
{
private:
    // clock_error accumulates the fractional part that would be discarded on updates in conversion from double to int
    double clockError;  // milliseconds
    double scaleRate;   // percent
    int deltaTicks;     // milliseconds
    BaseClock *refClock;

public:
    WarpableClock() { warpableReset(); };
    WarpableClock(BaseClock &source)
    {
        warpableReset();
        refClock = &source;
        lastUpdated = (uint)refClock->getMillis();
    };

    void warpableReset()
    {
        reset();
        lastUpdated = 0;
        clockError = 0;
        scaleRate = 100;
        deltaTicks = 0;
        refClock = NULL;
    };

    void update()
    {
        if(refClock) update((uint)refClock->getMillis());
        else fatal("WarpableClock: Can't update clock! When refClock is NULL, a timestamp is required.");
    };

    void update(uint currentTick)
    {
        const double deltaTimeFraction = scaleRate/100.0f * (double)(currentTick - lastUpdated);
        lastUpdated = currentTick;
        deltaTicks = (int)(deltaTimeFraction + clockError);

        if(deltaTicks > 0)
        {
            elapsedMsec += deltaTicks;
        };
        clockError = clockError + deltaTimeFraction - deltaTicks;
    };

    void pauseAt(uint pauseStartTime, uint currentTime)
    {
        update(pauseStartTime);
        lastUpdated = currentTime;
    };

    void unpauseAt(uint pauseEndTime, uint currentTime)
    {
        lastUpdated = max(pauseEndTime, lastUpdated);
        update(currentTime);
    };

    void setScaleRate(double newScaleRate)
    {
        scaleRate = newScaleRate;
    };

    void rebase(int msecChange, double newClockError)
    {
        elapsedMsec += msecChange;
        clockError = newClockError;
    };

    int getDeltaMillis() { return deltaTicks; };
};

extern BaseClock *InternalClockPtr;
extern WarpableClock *GameClockPtr;
extern WarpableClock *PhysicsClockPtr;
