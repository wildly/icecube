// weapon.cpp: all shooting and effects code

#include "cube.h"

VAR(logbattledata,0,0,1);

// Monster damage factors
// Monsters deal "base damage"/"damage factor" as follows
const int MONSTERDAMAGEFACTOR = 4;
const int NIGHTMAREMVPDAMAGEFACTOR = 1; // Monster vs Player damage factor during Nightmare mode

const int MAXRAYS = 20;
const int SGRAYS = 20;
vec gunrays[MAXRAYS];

struct guninfo
{
    short sound, attackdelay, damage, spread, projspeed, part, kickamount, muzzledist;
    const char *name, *abbrev;
};

// placeholder sounds
#define S_GRENADE S_FLAUNCH
#define S_PISTOL S_SLIMEBALL
#define S_LASER S_SLIMEBALL
#define S_LASERHIT S_ICEBALL

guninfo guns[NUMGUNS] =
{
    { S_PUNCH1,     250,   50,    0,    0,              0,   5,  0, "fist",            "FI" },
    { S_SG,        1400,   10,  200,    0,              0, 100, 20, "shotgun",         "SG" },  // *SGRAYS
    { S_CG,         100,   30,  100,    0,              0,  35, 10, "chaingun",        "CG" },
    { S_RLFIRE,     800,  120,    0,   80,              0,  50, 10, "rocketlauncher",  "RL" },
    { S_RIFLE,     1500,  100,    0,    0,              0, 150,  0, "rifle",           "RI" },
    { S_FLAUNCH,    200,   20,    0,   25, PART_FIREBALL1,   5,  0, "fireball",        "FB" },
    { S_ICEBALL,    200,   40,    0,   15, PART_FIREBALL2,   5,  0, "iceball",         "IB" },
    { S_SLIMEBALL,  200,   30,    0,   80, PART_FIREBALL3,   5,  0, "slimeball",       "SB" },
    { S_PIGR1,      250,   50,    0,    0,              0,   5,  0, "bite",            "BI" },  // monster's version of fist
#ifdef NEWGUNS
    { S_GRENADE,    400,   60,    0,   80,              0,  25, 10, "grenadelauncher", "GL" },
    { S_PISTOL,     200,   20,  100,    0,              0,  20, 10, "pistol",          "PI" },
    { S_LASER,      500,   35,    0,  200, PART_FIREBALL1,   5, 10, "laser",           "LA" },
    { S_RLFIRE,    1000,    0,    0,   80, PART_FIREBALL2,   5,  0, "hook",            "HK" },
    { S_RLFIRE,     100,    0,    0,    0,              0,-100,  0, "thrusters",       "TH" },
    { S_PUNCH1,     500,   50,    0,    0,              0,   5,  0, "stomp",           "ST" },  // 50 damage downward fist
    { S_PUNCH1,     250,    0,    0,    0,              0,   0,  0, "grapple",         "GR" },  // 0 damage fist
    { S_RIFLE,     1500,    0,    0,    0,              0, 150,  0, "vorpal",          "VO" },  // 0 damage, debuffs movement speed
    { S_LASER,     1500,    0,    0,    0,              0,   0,  0, "portal",          "PO" }   // Teleport
#endif
};

const char *getWeaponName(int gun)
{
    if(gun < 0 || gun >= NUMGUNS)
        return guns[0].name; // fist
    return guns[gun].name;
};

const char *getWeaponNameAbbrev(int gun)
{
    if(gun < 0 || gun >= NUMGUNS)
        return guns[0].abbrev; // fist
    return guns[gun].abbrev;
};

// Returns guns[gun].damage, except for Shotgun which returns SGRAYS*guns[gun].damage
int getWeaponBaseDamage(int gun)
{
    if(gun < 0 || gun >= NUMGUNS)
        return 0;
    int damage = guns[gun].damage;
    if(gun == GUN_SG) damage *= SGRAYS;
    return damage;
};

bool hasRecoilPitchAdjustment(int gun)
{
    bool recoil = true;
#ifdef NEWGUNS
    if(gun == GUN_THRUSTERS) recoil = false;
#endif
    return recoil;
};

bool isMelee(int gun)
{
    bool melee = false;
    if(gun==GUN_FIST || gun==GUN_BITE) melee = true;
#ifdef NEWGUNS
    if(gun == GUN_GRAPPLE) melee = true;
    if(gun == GUN_STOMP) melee = true; // Stomp is a special case of downward melee attack instead of forward melee attack
#endif
    return melee;
};

bool isSplashDamage(int gun)
{
    bool splash = false;
    if(gun==GUN_RL) splash = true;
#ifdef NEWGUNS
    if(gun == GUN_GRENADE) splash = true;
#endif
    return splash;
}

bool consumesAmmo(int gun)
{
    bool ammo = true;
    if(gun == GUN_FIST || gun == GUN_BITE) ammo = false;
#ifdef NEWGUNS
    if(gun == GUN_STOMP || gun == GUN_GRAPPLE) ammo = false;
#endif
    return ammo;
};

// Converts ammo for certain original guns into ammo for new guns
// RL -> Grenade
// SG -> Pistol
// Rifle -> Laser
void testammo()
{
#ifdef NEWGUNS
    if(player1->ammo[GUN_RL])    { player1->ammo[GUN_GRENADE] += player1->ammo[GUN_RL];    player1->ammo[GUN_RL] = 0; };
    if(player1->ammo[GUN_SG])    { player1->ammo[GUN_PISTOL]  += player1->ammo[GUN_SG];    player1->ammo[GUN_SG] = 0; };
    if(player1->ammo[GUN_RIFLE]) { player1->ammo[GUN_LASER]   += player1->ammo[GUN_RIFLE]; player1->ammo[GUN_RIFLE] = 0; };
#endif
};

// TODO: For guns with ammo clips or reloadable ammo or overheat
// For now, this function bootstraps Hook and Thrusters testing by giving 1 of that ammo if you have none.
// Hook and Thrusters do no damage, so this should be fine.
// It does cause pain animation to the target, we may need to set Thrusters gunray to 0 distance.
void reload()
{
#ifdef NEWGUNS
    const int armory[] = { GUN_HOOK, GUN_THRUSTERS, GUN_STOMP, GUN_GRAPPLE, GUN_VORPAL, GUN_PORTAL };
    const int count = ARRLEN(armory);
    loopi(count)
    {
        if(!player1->ammo[armory[i]])
            ++player1->ammo[armory[i]];
        if(armory[i] == GUN_THRUSTERS)
            player1->ammo[GUN_THRUSTERS] = 100;
    };
#endif
};

COMMAND(testammo, ARG_NONE);
COMMAND(reload, ARG_NONE);

bool hasspread(const int gun)
{
    if(guns[gun].spread)
        return true;
    else
        return false;
};

// Only works in single player because network play doesn't sync mutator selection to other clients yet.
// Therefore, shot visual will appear to have normal accuracy to other clients.
short getRealisticGunspread(dynent *d)
{
    short gunspread = guns[d->gunselect].spread;

    if(d->gunselect == GUN_SG) gunspread = 50;              // 50 spread = 50 milliarcradian = 172 MOA
    else if(d->gunselect == GUN_CG) gunspread = 18;         // 18 spread = 18 milliarcradian =  62 MOA
#ifdef NEWGUNS
    else if(d->gunselect == GUN_PISTOL) gunspread = 18;     // 18 spread =  18 milliarcradian = 62 MOA
#endif

    return gunspread;
};

// TODO: 3-shot burst for pistol and/or chaingun?
int getnumrays(const int gun)
{
    assert(SGRAYS<=MAXRAYS);
    int numrays = guns[gun].projspeed > 0 ? 0 : 1; // projectiles have 0 gunrays
    switch(gun)
    {
        case GUN_SG:
            return SGRAYS;
        default:
            return numrays;
    };
};

// Used to swap rules from Cube to IceCube
void accurateChainGun(bool enable)
{
    static short origSpreadAmount = guns[GUN_CG].spread;
    if(enable) guns[GUN_CG].spread = 0; // Chain Gun has 0 spread in Cube
    else guns[GUN_CG].spread = origSpreadAmount;
};

// Used to enable/disable Shotgun's blast effect
bool ShotgunBlastEnabled = false;
void enableShotgunBlast(bool enable)
{
    ShotgunBlastEnabled = enable;
};

void gunselect(int gun)
{
    if(gun!=player1->gunselect)
    {
        playsoundc(S_WEAPLOAD);
    };
    player1->gunselect = gun;
    //conoutf("%s selected", guns[gun].name);
};

void selectgun(int a, int b, int c)
{
    if(a<-1 || b<-1 || c<-1 || a>=NUMGUNS || b>=NUMGUNS || c>=NUMGUNS) return;
    int s = player1->gunselect;
    if(a>=0 && s!=a && player1->ammo[a]) s = a;
    else if(b>=0 && s!=b && player1->ammo[b]) s = b;
    else if(c>=0 && s!=c && player1->ammo[c]) s = c;
    else if(s!=GUN_RL && player1->ammo[GUN_RL]) s = GUN_RL;
    else if(s!=GUN_CG && player1->ammo[GUN_CG]) s = GUN_CG;
    else if(s!=GUN_SG && player1->ammo[GUN_SG]) s = GUN_SG;
    else if(s!=GUN_RIFLE && player1->ammo[GUN_RIFLE]) s = GUN_RIFLE;
    else s = GUN_FIST;
    gunselect(s);
};

int reloadtime(int gun) { return guns[gun].attackdelay; };

void weapon(char *a1, char *a2, char *a3)
{
    selectgun(a1[0] ? atoi(a1) : -1,
              a2[0] ? atoi(a2) : -1,
              a3[0] ? atoi(a3) : -1);
};

COMMAND(weapon, ARG_3STR);

// create an offset ray using dest as storage
void offsetray(const vec &from, const vec &to, const int spread, const float dist, vec &dest)
{
    float f = dist*spread/1000.0f;
    const int MaxTries = 10;
    vec offset = rndVecInSpace(UNIFORM_SPHERE, MaxTries, 0.5f);
    vmul(offset, f);
    dest = to;
    vadd(dest, offset);
};

// create random spread of ray(s) for the shotgun and chaingun
void createrays(const int gun, const int numrays, const vec &from, const vec &to)
{
    const int rays = clamp(numrays, 1, MAXRAYS);
    vdist(dist, dvec, from, to);
    loopi(rays) offsetray(from, to, guns[gun].spread, dist, gunrays[i]);
};

// create a single, precise ray for all other raydamage weapons
void createsimpleray(const int gun, const vec &from, const vec &to)
{
    gunrays[0] = to;
};

int setupgunrays(const int gun, const vec &from, const vec &to)
{
    int numrays = getnumrays(gun);
    if(hasspread(gun) || numrays>1)
        createrays(gun, numrays, from, to);
    else if(numrays == 1)
        createsimpleray(gun, from, to);

    loopi(numrays) { raycheck(from, gunrays[i]); };

    return numrays;
};

bool intersect(dynent *d, vec &from, vec &to)   // if lineseg hits entity bounding box
{
    vec v = to, w = d->o, *p;
    vsub(v, from);
    vsub(w, from);
    float c1 = dotprod(w, v);

    if(c1<=0) p = &from;
    else
    {
        float c2 = dotprod(v, v);
        if(c2<=c1) p = &to;
        else
        {
            float f = c1/c2;
            vmul(v, f);
            vadd(v, from);
            p = &v;
        };
    };

    return p->x <= d->o.x+d->radius
        && p->x >= d->o.x-d->radius
        && p->y <= d->o.y+d->radius
        && p->y >= d->o.y-d->radius
        && p->z <= d->o.z+d->aboveeye
        && p->z >= d->o.z-d->eyeheight;
};

char *playerincrosshair()
{
    if(demoplayback) return NULL;
    loopv(players)
    {
        dynent *o = players[i];
        if(!o) continue; 
        if(intersect(o, player1->o, worldpos)) return o->name;
    };
    return NULL;
};

dynent *dynentincrosshair()
{
    if(demoplayback) return NULL;
    dvector v = getcollidable();
    loopv(v)
    {
        dynent *o = v[i];
        if(!o || o == player1 || o->state == CS_DEAD) continue;
        if(intersect(o, player1->o, worldpos)) return o;
    };
    return NULL;
};

struct projectile { vec o, to, motion; float speed; dynent *owner; int gun; bool local, expired, bounced; int smokechance, countdown; };
vector<projectile> projs;

void clearprojectiles() { projs.setsize(0); clearspheres(); };

void newprojectile(vec &from, vec &to, float speed, bool local, dynent *owner, int gun)
{
    const vec zero = vec(0);
    projectile *p = &projs.add();
    p->o = from;
    p->to = to;
    p->motion = zero;
    p->speed = speed;
    p->local = local;
    p->owner = owner;
    p->gun = gun;
    p->smokechance = 0;
    p->expired = false;
    p->bounced = false;
#ifdef NEWGUNS
    p->countdown = gun == GUN_GRENADE ? 2000 : 0;
    if(p->gun == GUN_GRENADE)
    {
        vdist(dist, motion, from, to);
        vdiv(motion, dist);
        vmul(motion, p->speed);
        p->motion = motion;
        vec omotion = getPhysVelocity(owner); // add 0.5x owner's motion to initial motion
        vmul(omotion, 0.5f);
        vadd(p->motion, omotion);
    };
#else
    p->countdown = 0;
#endif
};

// Look for and remove expired projectiles
// We will swap the expired projectiles to the back of the vector, then
// trim the vector to complete the removal.
void cleanupprojectiles()
{
    if(projs.length() < 1) return;

    // initialize and find the location of backmost unexpired projectile
    int numExpired = 0;
    int backIndex = projs.length() - 1;
    while(projs[backIndex].expired) { --backIndex; ++numExpired; if(backIndex == -1) break; };

    // Case "backIndex == -1: all projectiles are expired
    if(!(backIndex == -1))
    {
        loopv(projs)
        {
            // Case "i == backIndex": we've reached the backmost unexpired projectile
            if(i == backIndex) break;

            // projs[i] is not expired. Continue.
            if(!projs[i].expired) continue;

            // projs[i] is expired. Swap it with projs[backIndex].
            projectile tmp = projs[i];
            projs[i] = projs[backIndex];
            projs[backIndex] = tmp;

            // find the next backmost unexpired projectile
            while(projs[backIndex].expired) { --backIndex; ++numExpired; if(backIndex == i) break; };

            // Case "i == backIndex": we've reached the backmost unexpired projectile
            if(i == backIndex) break;
        };
    };

    projs.setsize(projs.length() - numExpired);
};

// Accuracy Hit Sound
VARP(hitsound, 0, 0, 1);

// Placeholder hit sound
#define S_ACCURACY_HIT S_ITEMARMOUR

void playhitsound()
{
    if(hitsound) playsoundc(S_ACCURACY_HIT);
};

// int target: NetID for target player/monster hit by damage
// int damage: damage amount
// dynent * d: pointer to dynent with NetID referred to by var "target"
// dynent * at: pointer to dynent that dealt the damage
void hit(int target, int damage, dynent *d, dynent *at, vec &damagekick, bool useEnergyPush)
{
    int ignoredamage = 0;
    if(d==player1)
    {
        ignoredamage = selfdamage(damage, at==player1 ? -1 : -2, at);
    }
    else if(d->monsterstate)
    {
        ignoredamage = monsterpain(d, damage, at);
    }
    else
    {
        addmsg(1, 4, SV_DAMAGE, target, damage, d->lifesequence);
        playsound(S_PAIN1+rnd(5), &d->o);
    };

    if(ignoredamage == 0)
    {
        if(useEnergyPush) energypush(d, damagekick);
        else vadd(d->vel, damagekick);
        particle_jet(PART_BLOOD, damage, 1000, d->o);
        demodamage(damage, d->o);
    };
};

// Calculate the nominal damage amount for a shot
int calcNominalDamage(const int baseDamage, dynent *src)
{
    int damage = baseDamage;
    if(src->monsterstate) damage /= currentmutator.nightmare ? NIGHTMAREMVPDAMAGEFACTOR : MONSTERDAMAGEFACTOR;
    return damage;
};

// Calculate the final damage amount to apply to the victim
int calcFinalDamage(const int nominalDamage, dynent *d, dynent *at)
{
    int damage = nominalDamage;
    // Monsters don't deal nightmarish damage to other monsters during Nightmare mode
    if(currentmutator.nightmare && d->monsterstate && at->monsterstate)
    {
        damage *= NIGHTMAREMVPDAMAGEFACTOR; // un-apply NIGHTMAREMVPDAMAGEFACTOR
        damage /= MONSTERDAMAGEFACTOR;  // and apply MONSTERDAMAGEFACTOR instead
    };
    if(at->buffs[BUFF_QUAD]) damage *= 4;
    return damage;
};

float calcPushEffectMagnitude(dynent *d, int damage)
{
    return damage/50.0f*4*1.21f*3.9f/(4*d->radius*d->radius*(d->eyeheight+d->aboveeye));
};

float calcExplosionPushMagnitude(dynent *d, int damage)
{
    return 3.112f*sqrt(damage/60.0f)/pow(4*d->radius*d->radius*(d->eyeheight+d->aboveeye), 1.0f/6.0f);
};

float convPEMagToVelMag(dynent *d, float pushMag)
{
    float vm = pushMag*22.0f/d->maxspeed;
    if(d->inwater) vm /= 2; // 0.5x physics push when in water
    return vm;
};

const float RL_RADIUS = 5;
const float RL_DAMRAD = 7;   // hack

bool radialeffect(dynent *o, vec &v, int cn, int qdam, dynent *at, int gun, bool local, int &totalNomDamage, int &totalNomSelfDamage)
{
    if(o->state != CS_ALIVE)
        return false;

    vdist(dist, kickv, v, o->o);
    vdiv(kickv, dist);
    const float tdist = dist;
    dist -= 2; // account for eye distance imprecision
    if(dist<RL_DAMRAD)
    {
        if(dist<0) dist = 0;
        int damage = (int)(qdam*(1-(dist/RL_DAMRAD)));
        if(o != at) totalNomDamage += damage;
        else totalNomSelfDamage += damage;
        damage = calcFinalDamage(damage, o, at);
        if(at == player1) // damage tracking
        {
            extern int damagedealt;
            damagedealt += damage;
        }
        float pushEffectMag = calcExplosionPushMagnitude(o, damage);
#ifdef NEWGUNS
        if(gun == GUN_GRENADE) pushEffectMag *= -1;
#endif
        vmul(kickv, convPEMagToVelMag(o, pushEffectMag));
        if(local) hit(cn, damage, o, at, kickv, false);
        else virtualpush(o, kickv, false);
        if(logbattledata) printf("weapon,%d,radialeffect,damage:%d,pushmag:%.2f,effectiveDistance:%.2f,trueDistance:%.2f\n", gun, damage, pushEffectMag, dist, tdist);
        return true;
    };

    return false;
};

VAR(debugparticles, 0, 0, 1);

void splash(projectile *p, vec &v, vec &vold, dvector &targets, int qdam)
{
    const int ExtraParticles = (debugparticles && p->owner==player1) ? 200 : 0;
    const int ExtraLifetime = (debugparticles && p->owner==player1) ? 2000 : 0;

    // Create some particles for the projectile splash
    const int SparksCount = (p->gun==GUN_RL ? 100 : 50) + ExtraParticles;
    const int SparksLifetime = 300 + ExtraLifetime;
    particle_jet(PART_SPARKS, SparksCount, SparksLifetime, v);

    // Play a sound for the projectile splash
    int explosionSound = S_FEXPLODE;
    if(p->gun==GUN_RL) explosionSound = S_RLHIT;
#ifdef NEWGUNS
    if(p->gun==GUN_LASER) explosionSound = S_LASERHIT;
    if(p->gun==GUN_GRENADE) explosionSound = S_RLHIT;
#endif
    playsound(explosionSound, &v);

    // Projectile explodes and deals splash damage
#ifdef NEWGUNS
    if(p->gun==GUN_RL || p->gun==GUN_GRENADE)
#else
    if(p->gun==GUN_RL)
#endif
    {
        newsphere(v, RL_RADIUS, p->gun==GUN_RL ? 0 : 2, p->owner);
        int totalNomDamage = 0; // for accuracy tracking
        int totalNomSelfDamage = 0; // for accuracy tracking
        loopv(targets)
        {
            dynent *o = targets[i];
            if(!o) continue;
            radialeffect(o, v, i, qdam, p->owner, p->gun, p->local, totalNomDamage, totalNomSelfDamage);
        };
        if(p->owner==player1) accuracyAddResult(p->gun, totalNomDamage, totalNomSelfDamage, accuracyCreateResultFlags(p->gun, totalNomDamage, totalNomSelfDamage));
    };
};

bool projdamage(dynent *o, projectile *p, vec &v, dvector &t, int i, int qdam)
{
    if(o->state != CS_ALIVE) return false;
    if(!intersect(o, p->o, v)) return false;

    // splash damage all within area including the directly hit target
    // this also creates the visual for the exploding projectile
    splash(p, v, p->o, t, qdam);

    // rockets and grenades can't direct hit, no need to process further
#ifdef NEWGUNS
    if(p->gun==GUN_RL || p->gun==GUN_GRENADE)
#else
    if(p->gun==GUN_RL)
#endif
        return true;

    // hit and knockback the directly hit target
    int totalNomDamage = qdam; // for accuracy tracking
    qdam = calcFinalDamage(qdam, o, p->owner);
#ifdef NEWGUNS
    int extraPushMultiplier = 1;
    if(p->gun == GUN_LASER) extraPushMultiplier *= 4;
    float pushEffectMag = calcPushEffectMagnitude(o, extraPushMultiplier*qdam);
#else
    float pushEffectMag = calcPushEffectMagnitude(o, qdam);
#endif
    vdist(dist, kickv, v, o->o);
    vmul(kickv, convPEMagToVelMag(o, pushEffectMag)/dist);
    if(p->local) hit(i, qdam, o, p->owner, kickv, false);
    else virtualpush(o, kickv, false);
    if(logbattledata) printf("weapon,%d,projdamage,damage:%d,pushmag:%.2f,effectiveDistance:%.2f,trueDistance:%.2f\n", p->gun, qdam, pushEffectMag, dist, dist);
    if(p->owner==player1)
    {
        accuracyAddResult(p->gun, totalNomDamage, 0, accuracyCreateResultFlags(p->gun, totalNomDamage, 0));
        extern int damagedealt;
        damagedealt += qdam;
    }
    return true;
};

// Emit one quantity of smoke per interval per emitter on average.
// Projectiles are updated with the same granularity as movement (MINFRAMETIME in physics.cpp or better)
// If not enough smoke is created, can try to catch up in subsequent frames.
const int SMOKEINTERVAL = 25; // 40 fps = 25 msec (1000 msec/40 frames)
const int createsmoke(projectile *p, const int time)
{
    int smokecreated = 0;
    const int threshold = SMOKEINTERVAL * (p->owner->monsterstate && p->owner->mtype == M_BAUUL ? 2 : 1); // Bauul's rocket emits 50% less smoke

    p->smokechance += time;
    if(p->smokechance + threshold/2 >= threshold) // when checking, boost with 50% of "threshold"; smokechance should have an expected value of "0" on random sampling of projectiles
    {
        p->smokechance -= threshold;
        ++smokecreated;
    };
    return smokecreated;
};

// Return value is whether the projectile exploded
bool moveprojectile_path(projectile *p, int time, vec &nextpos)
{
    vdist(dist, v, p->o, p->to);
    float dtime = dist*1000/p->speed;
    // if time>dtime, projectile has reached the destination (p->to)
    bool explode = (time>dtime) ? true : false;
    if(dist <= 0) { nextpos = p->to; raycheck(p->o, nextpos); return true; };
    if(dist < 0.3f) explode = true; // force the projectile to explode, but still try to move the projectile
    vmul(v, time/dtime);
    vadd(v, p->o);
    nextpos = v;
    raycheck(p->o, nextpos);
    if(iscollided(nextpos)) explode = true;
    return explode;
};

void applyprojectilegravity(projectile *p, int time);
bool moveprojectile_motion(projectile *p, int time, vec &nextpos)
{
    applyprojectilegravity(p, time);    // update physent's velocity
    nextpos = p->motion;                // determine physent's new position
    vmul(nextpos, time/1000.0f);
    vadd(nextpos, p->o);
    raycheck(p->o, nextpos);
    return false;
};

bool checkprojectilebounce(projectile *p, vec &v)
{
    bool explode = false;
    // Check whether the projectile exploded due to world collision
    int bounce = trybounce_physent(v, p->motion);
    if(bounce != COL_RESULT_FREESPACE)
    {
#ifdef NEWGUNS
        if(p->gun == GUN_GRENADE)
        {
            // hack: After bouncing once, the projectile:
            // 1. Is no longer affected by tractor component of dynent gravity, and
            // 2. Can collide with the projectile's owner
            p->bounced = true;
            vmul(p->motion, 1/sqrt(2.0f));
        }
        else
#endif
        {
            explode = true;
        };
    };
    return explode;
};

void moveprojectiles(int time, bool doEffects)
{
    loopv(projs)
    {
        projectile *p = &projs[i];
        int qdam = calcNominalDamage(guns[p->gun].damage, p->owner);
        bool explode = false;
        vec v;

        // Calculate the projectile's next position
#ifdef NEWGUNS
        if(p->gun == GUN_GRENADE)
            explode = moveprojectile_motion(p, time, v);
        else
            explode = moveprojectile_path(p, time, v);
#else
        explode = moveprojectile_path(p, time, v);
#endif

        // vector of dynents for projdamage and splash/radialeffect
        dvector collidable = getcollidable();

        // Check whether the projectile collided with a dynent (direct hit)
        loopv(collidable)
        {
            // check each dynent for collision (unless that dynent is the proj owner)
            dynent *o = collidable[i];
            if(!o || vreject(o->o, v, 10.0f) || (p->owner == o && !p->bounced)) continue;
            if(projdamage(o, p, v, collidable, i, qdam)) { p->expired = true; break; };
        };

        // As a fallback to detect a path-type projectile that can't make further progress (e.g. collided with something in raycheck), check whether the projectile could move but didn't.
        // Do this check after trying to collide with a dynent.
        // Don't do this check if projectile explodes via countdown.
        if(p->o.x == v.x && p->o.y == v.y && p->o.z == v.z && time && !p->countdown)
            explode = true;

        // Check whether the projectile exploded due to countdown
        if(p->countdown && (p->countdown -= time) <= 0)
            explode = true;

#ifdef NEWGUNS
        if(p->gun == GUN_GRENADE) { // hack: Only needed by GUN_GRENADE. Bounce/collision isn't 100% accurate, so avoid this section for projectiles which don't need it.
            if(checkprojectilebounce(p, v)) explode = true;
        }; // end bounce/collision avoidance hack
#endif

        // Check whether the projectile exploded without colliding with a dynent (e.g. countdown, world collision, reaching the attack point)
        // If the projectile is local, splash will use collidable vector for determining radialeffects
        if(!p->expired)
        {
            if(explode)
            {
                splash(p, v, p->o, collidable, qdam);
                p->expired = true;
            }
            else
            {
                if(p->gun==GUN_RL)
                {
                    if(doEffects) { newsphere(p->o, 0.3f, 1, p->owner); };
                    loopi(createsmoke(p, time)) { particle_splash(PART_BIGSMOKE, 2, 200, v); };
                }
#ifdef NEWGUNS
                else if(p->gun==GUN_GRENADE)
                {
                    if(doEffects) { newsphere(p->o, 0.3f, 3, p->owner); };
                }
#endif
                else
                {
                    loopi(createsmoke(p, time)) { particle_splash(PART_SMALLSMOKE, 1, 200, v); };
                    if(doEffects) particle_splash(guns[p->gun].part, 1, -1, v);
                };
            };
        };
        // move the projectile to the updated location (regardless of whether it has already exploded)
        p->o = v;
    };

    // finally remove the expired projectiles
    // it is best to delay removal of exploded projectiles until outside loopv(projs)
    cleanupprojectiles();

    // Glowing spherical projectiles and explosion dynlights
    if(doEffects) dodynlightforspheres();
};

extern int gravitypercent;
void applyprojectilegravity(projectile *p, int time)
{
    // apply physical component of gravity (applied to velocity)
    float localgravity = -1000/30.0f*gravitypercent/100.0f;
    p->motion.z += localgravity*time/1000.0f;

    // apply dynent type tractor component of gravity (applied to position)
    if(!p->bounced) p->o.z += -9.5f*time/1000.0f*gravitypercent/100.0f;
};

// Derived from moveplayer() driver function in "physics.cpp"
void updateprojectiles(int time, int movesteps)
{
    const int iterations = movesteps;

    int completedtime = 0;
    // Move projectiles also does projectile lighting effects and draws particle sprites
    // Ensure those effects are only done in the final frame
    bool doEffects = false;
    loopi(iterations)
    {
        int timeslice = time*(i+1)/iterations - completedtime;
        if(completedtime + timeslice >= time) doEffects = true;
        moveprojectiles(timeslice, doEffects);
        completedtime += timeslice;
    };
};

// This step for projectile attacks corresponds to processgunrays for hitscan attacks.
// Local projectiles can collide, deal damage, and push.
// Non-local projectiles can collide and push; they cannot deal damage.
void projectileattack(int gun, vec &from, vec &to, dynent *d, bool local)
{
    int pspeed = 25;
    pspeed = guns[gun].projspeed;
    if(d->monsterstate && d->mtype == M_BAUUL) pspeed /= 2; // 50% speed for Bauul's rocket
    newprojectile(from, to, (float)pspeed, local, d, gun);
};

const float SG_BLASTRANGE = 3.0f;
const int SG_BLASTPOWER = 100;
const float FIST_HITRANGE = 3.0f; // shortened by 1.1 due to player's radius
const float STOMP_HITRANGE = 5.0f; // shortened by 3.0 due to player's eyeheight (3.2) and beloweye adjustment (-0.2)

bool pusheffect(dynent *o, vec &from, vec &to, const int power, dynent *at)
{
    // push anyway?
    //if(o->state != CS_ALIVE)
    //    return false;

    if(o == player1 && editmode)
        return false;

    const int fakedamage = calcFinalDamage(power, o, at);

    vdist(dist, kickv, from, to);
    float pushEffectMag = calcPushEffectMagnitude(o, fakedamage);
    vmul(kickv, convPEMagToVelMag(o, pushEffectMag)/dist);
    vadd(o->vel, kickv);
    vdist(tdist, unused, at->o, o->o);
    if(logbattledata) printf("weapon,%d,pusheffect,damage:%d,pushmag:%.2f,effectiveDistance:%.2f,trueDistance:%.2f\n", GUN_SG, 0, pushEffectMag, dist, tdist);
    return true;
};

void blast(dynent *at, vec &from, vec &to, const int power)
{
    dvector v = getcollidable();
    loopv(v)
    {
        if(!v[i] || v[i]==at || vreject(v[i]->o, from, 10.0f) || !intersect(v[i], from, to)) continue;
        pusheffect(v[i], from, to, power, at);
    };
};

// shotgun blast effect
void processblast(dynent *at, vec &from, vec &to)
{
    if(!ShotgunBlastEnabled) return;
    // close range shotgun pushback
    vdist(dist, unitv, from, to);
    vmul(unitv, SG_BLASTRANGE/dist); // same as punch range
    vec blastTo = from;
    vadd(blastTo, unitv);
    blast(at, from, blastTo, calcNominalDamage(SG_BLASTPOWER, at));
};

// shotgun smoke effect
void muzzle_smoke(vec &from, vec &to, const int count)
{
    // muzzle smoke origin (smokeOrig), directional velocity (unitv)
    vdist(dist, unitv, from, to);
    vmul(unitv, SG_BLASTRANGE/dist); // same as punch range
    vec smokeOrig = from;
    vadd(smokeOrig, unitv);

    particle_cloud(PART_BIGSMOKE, count, 250, 500, smokeOrig, 0.75f, 1.5f, true);
    on_created_particles_add_velocity(unitv);
};

// thrusters smoke particle effect
void thrusters_smoke(vec &from, vec &to)
{
    // create smoke in opposite direction
    vdist(dist, unitv, from, to);
    vec to_reverse = from;
    vsub(to_reverse, unitv);

    // to_reverse is not a raycheck validated location, but that's fine because it doesn't need to be
    muzzle_smoke(from, to_reverse, 5);
};

VAR(debugweapon, 0, 0, 1);

// create a trace hinting where a hypothetical projectile would go
void projectile_trace(dynent *o, int gun)
{
    if(guns[gun].projspeed == 0) return;
#ifdef NEWGUNS
    if(gun != GUN_RL && gun != GUN_GRENADE) return;
#else
    if(gun != GUN_RL) return;
#endif

    vec from = o->o;
    from.z -= 0.2f;    // below eye

    newprojectile(from, worldpos, (float)guns[gun].projspeed, true, o, gun);
    projectile *p = &projs.last();
    // simulate 1 second @ 10 msec steps
    loopi(1000/10)
    {
        vec nextpos;
        bool explode;
#ifdef NEWGUNS
        if(gun == GUN_GRENADE)
        {
            moveprojectile_motion(p, 10, nextpos);
            particle_splash(PART_DEMOTRACK, 1, -1, nextpos);
            checkprojectilebounce(p, nextpos);
            p->o = nextpos;
        };
#endif
        if(gun == GUN_RL)
        {
            explode = moveprojectile_path(p, 10, nextpos);
            particle_splash(PART_DEMOTRACK, 1, -1, nextpos);
            p->o = nextpos;
            if(explode) break;
        };
    };
    p->expired = true;
    cleanupprojectiles();
};

void bulletv(vec &from, vec &to, float shorten_amount)              // create a glowing bullet trace visual
{
    const int TraceLifetime = debugweapon ? 1500 : 300;
    // Shorten the weapon trace by moving the trace origin from "from" to "muzzle"
    vec muzzle = from;
    vdist(trace_dist, unitv, from, to);
    vmul(unitv, shorten_amount/trace_dist);
    vadd(muzzle, unitv);
    particle_trace(PART_SPARKTRAIL, TraceLifetime, muzzle, to);
};

void shootv(int gun, vec &from, vec &to, dynent *d)                 // create visual effect from a shot
{
    playsound(guns[gun].sound, d==player1 ? NULL : &d->o);
    switch(gun)
    {
        case GUN_FIST:
        case GUN_BITE:
#ifdef NEWGUNS
        case GUN_STOMP:
        case GUN_GRAPPLE:
#endif
            if(debugweapon) bulletv(from, to, 0);
            break;

        case GUN_SG:
        {
            loopi(SGRAYS)
            {
                float muzzledist = d->monsterstate ? 0 : guns[gun].muzzledist/10.0f;
                bulletv(from, gunrays[i], muzzledist);
                particle_jet(PART_SPARKS, 5, 200, gunrays[i]);
            };
            muzzle_smoke(from, to, 5);
            break;
        };

        case GUN_CG:
#ifdef NEWGUNS
        case GUN_PISTOL:
#endif
        {
            float muzzledist = d->monsterstate ? 0 : guns[gun].muzzledist/10.0f;
            bulletv(from, gunrays[0], muzzledist);
            // 15 sparks is logical, but causes shot to appear weak. X2 sparks multiplier (30 sparks) is a compromise.
            particle_jet(PART_SPARKS, 30, 200, gunrays[0]);
            //muzzle_smoke(from, to, 3);
            break;
        };

#ifdef NEWGUNS
        case GUN_THRUSTERS:
            thrusters_smoke(from, to);
            break;
#endif

        // No additional shot visual effects are needed for projectiles
        case GUN_RL:
        case GUN_FIREBALL:
        case GUN_ICEBALL:
        case GUN_SLIMEBALL:
#ifdef NEWGUNS
        case GUN_GRENADE:
        case GUN_LASER:
        case GUN_HOOK: // in-flight hook projectile
#endif
            break;


        case GUN_RIFLE:
#ifdef NEWGUNS
        case GUN_VORPAL:
        case GUN_PORTAL: // teleport effect realized in hitpush function
#endif
            particle_jet(PART_SPARKS, 50, 200, gunrays[0]);
            particle_trail(PART_SMALLSMOKE, 500, from, gunrays[0]);
            if(debugparticles && d == player1) particle_trail(PART_SPARKS, 2000, from, gunrays[0]);
            break;
    };
};

struct HitEvent { dynent *d; int cn; } hits[MAXRAYS];

// For local attacks, hit and push
// For non-local attacks, only push
void hitpush(int target, int damage, dynent *d, int gun, dynent *at, vec &from, vec &to, bool local)
{
    vdist(dist, v, from, to);
    float pushEffectMag = calcPushEffectMagnitude(d, damage);
    vmul(v, convPEMagToVelMag(d, pushEffectMag)/dist);
    if(local) hit(target, damage, d, at, v, false);
    else virtualpush(d, v, false);
#ifdef NEWGUNS
    if(gun == GUN_PORTAL) teleport(at, d); // teleport attacker to target
    if(gun == GUN_STOMP) // stomp bounce
    {
        vec bounce = vec( 0, 0, 1.7f );
        energypush(at, bounce);
    };
#endif
    vdist(tdist, unused, at->o, d->o);
    if(logbattledata) printf("weapon,%d,hitpush,damage:%d,pushmag:%.2f,effectiveDistance:%.2f,trueDistance:%.2f\n", gun, damage, pushEffectMag, dist, tdist);
};

void rayshorten(vec &from, vec &to, const int numrays)
{
    loopi(numrays)
    {
        // if hits[i] is NULL, this gunray did not hit a dynent
        if(hits[i].d == NULL) continue;

        vdist(dist, hitloc, from, gunrays[i]);
        vdist(dist_proxy, tmp, from, hits[i].d->o);
        vmul(hitloc, dist_proxy/dist);
        vadd(hitloc, from);
        gunrays[i] = hitloc;
    };
};

void checkraydamage(dynent *o, vec &from, vec &to, int i, int numrays)
{
    if(o->state!=CS_ALIVE) return;

    loop(r, numrays)
    {
        if(!intersect(o, from, gunrays[r])) continue;

        if(hits[r].d != NULL)
        {
            vdist(curDist, curDiff, o->o, from);
            vdist(bestDist, bestDiff, hits[r].d->o, from);
            if(curDist > bestDist)
                continue;
        };
        // Update hits[r] because we found a closer hit for this ray.
        // Target met one of the criteria: "curDist <= bestDist" or "hits[r].d == NULL"
        hits[r].d = o;
        hits[r].cn = i;
    };
};

void processgunrays(dynent *at, vec &from, vec &to, int numrays)
{
    loopi(MAXRAYS) { hits[i].d = NULL; hits[i].cn = -1; };

    dvector targets = getcollidable();
    loopv(targets)
    {
        dynent *o = targets[i];
        if(!o || o==at) continue;
        checkraydamage(o, from, to, i, numrays);
    };
    rayshorten(from, to, numrays);
};

void doraydamage(int gun, dynent *at, vec &from, vec &to, bool local)
{
    int totalNomDamage = 0; // for accuracy tracking
    int totalFinalDamage = 0; // for damage tracking
    loopi(MAXRAYS)
    {
        if(hits[i].d == NULL) continue;
        int addlHits = 0;
        for(int r = i+1; r < MAXRAYS; ++r)
        {
            // Consider the situation where 2+ rays hit the same dynent.
            // Search the remainder of hits array for additional rays that hit this dynent (hits[i].d).
            // For any additional ray found at a hits[r], treat that hit as an additional hit for hits[i] instead.
            if(hits[r].d == hits[i].d)
            {
                hits[r].d = NULL;
                hits[r].cn = -1;
                ++addlHits;
            };
        };
        int damage = guns[gun].damage;
        damage += addlHits*damage;
        damage = calcNominalDamage(damage, at);
        totalNomDamage += damage;
        damage = calcFinalDamage(damage, hits[i].d, at);
        totalFinalDamage += damage;
        hitpush(hits[i].cn, damage, hits[i].d, gun, at, from, to, local);
    };
    if(at==player1)
    {
        accuracyAddResult(gun, totalNomDamage, 0, accuracyCreateResultFlags(gun, totalNomDamage, 0));
        extern int damagedealt;
        damagedealt += totalFinalDamage;
    };
};

void shoot(dynent *d, vec &targ)
{
    if(debugweapon && d==player1) projectile_trace(d, d->gunselect);
    int attacktime = lastmillis-d->lastaction;
    if(attacktime<d->gunwait) return;
    d->gunwait = 0;
    if(!d->attacking) return;
    d->lastaction = lastmillis;
    d->lastattackgun = d->gunselect;
    if(!d->ammo[d->gunselect]) { playsoundc(S_NOAMMO); d->gunwait = 250; d->lastattackgun = -1; return; };
    if(consumesAmmo(d->gunselect)) d->ammo[d->gunselect]--;
    vec from = d->o;
    vec to = targ;
    from.z -= 0.2f;    // below eye

    vdist(dist, unitv, from, to);
    vdiv(unitv, dist);
    vec kickback = unitv;
    float kickEffectMag = calcPushEffectMagnitude(d, guns[d->gunselect].kickamount)/-10.0f; // assumes calcPushEffectMagnitude is linear in "damage"
    vmul(kickback, convPEMagToVelMag(d, kickEffectMag));
    vadd(d->vel, kickback);
    if(logbattledata) printf("weaponkick,%d,kickmag:%.2f\n", d->gunselect, kickEffectMag);
    if(d->pitch<80.0f && hasRecoilPitchAdjustment(d->gunselect)) d->pitch = min(d->pitch+guns[d->gunselect].kickamount*0.01f, 80.0f);

    if(isMelee(d->gunselect))
    {
        vmul(unitv, FIST_HITRANGE); // punch range
        to = from;
        vadd(to, unitv);
    };

#ifdef NEWGUNS
    if(d->gunselect==GUN_STOMP)
    {
        to = from;
        to.z -= STOMP_HITRANGE;
    };

    // reduce thruster's range so it won't stun enemies (pain animation)
    // it still needs to create a gunray to avoid being processed as a projectile
    // and it needs valid "from" and "to" vectors for the thruster's smoke effect
    if(d->gunselect==GUN_THRUSTERS)
    {
        vmul(unitv, 0.1f);
        to = from;
        vadd(to, unitv);
    };
#endif

    int numrays = setupgunrays(d->gunselect, from, to);

    if(d->buffs[BUFF_QUAD] && attacktime>200) playsoundc(S_ITEMPUP);
    if(!d->monsterstate) addmsg(1, 8, SV_SHOT, d->gunselect, (int)(from.x*DMF), (int)(from.y*DMF), (int)(from.z*DMF), (int)(to.x*DMF), (int)(to.y*DMF), (int)(to.z*DMF));
    d->gunwait = guns[d->gunselect].attackdelay;

    executeshot(numrays, d->gunselect, from, to, d, true);
};

void executeshot(int numrays, int gun, vec &from, vec &to, dynent *at, bool local)
{
    if(at==player1) accuracyAddShot(at->gunselect);
    raycheck(from, to);
    if(numrays == 0)
    {
        projectileattack(gun, from, to, at, local);
        shootv(gun, from, to, at);
    }
    else
    {
        processgunrays(at, from, to, numrays);
        if(gun == GUN_SG) processblast(at, from, to);
        shootv(gun, from, to, at);
        doraydamage(gun, at, from, to, local);
    };
};
