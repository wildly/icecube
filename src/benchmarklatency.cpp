// Latency testing tools/functions
// Copyright (C) 2020 Willy Deng
// zlib License

#include "cube.h"
#include "gamestate.h"

// RGB
unsigned char colors[12][3] =
{
    {   0,   0,   0 },
    { 160,  82,  45 },
    { 255,   0,   0 },
    { 255, 140,   0 },
    { 255, 255,   0 },
    {  60, 179, 113 },
    { 100, 149, 237 },
    { 238, 130, 238 },
    { 128, 128, 128 },
    { 255, 255, 255 },
    { 255, 215,   0 },
    { 192, 192, 192 }
};

VAR(showlatency, 0, 0, 1);

unsigned int counter = 0;

void do_latencystrip_background(int &textcolor) // and set textcolor
{
    ++counter;
    if(counter>=10) counter = 0;
    // default text color is "7: white"; when background is yellow or white, we set textcolor to "8: black".
    if(counter == 4 || counter == 9) textcolor = 8;
    else textcolor = 7;

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    glColor3ub(colors[counter][0],colors[counter][1],colors[counter][2]);

    // option 1: render with a triangle strip (like crosshair rendering in Sauerbraten)
    glBegin(GL_TRIANGLE_STRIP);
    glVertex2f(  0,         0);
    glVertex2f(100,         0);
    glVertex2f(  0, VIRTH*3/2);
    glVertex2f(100, VIRTH*3/2);
    glEnd();

    // option 2: render with a quad (like crosshair rendering in Cube)
    /*
    glBegin(GL_QUADS);
    glVertex2f(  0,         0);
    glVertex2f(100,         0);
    glVertex2f(100, VIRTH*3/2);
    glVertex2f(  0, VIRTH*3/2);
    glEnd();
    */

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
}

int reaction_state = 0;
void latency_set_reaction_state(bool state)
{
    reaction_state = state ? 1 : 0;
}
bool latency_get_reaction_state()
{
    return reaction_state;
}

void do_latencystrip_reaction()
{
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    if(reaction_state == 1) glColor3ub(0,0,255); // new input: electric blue
    else glColor3ub(0,255,0); // held input: lime green

    // option 1: render with a triangle strip
    glBegin(GL_TRIANGLE_STRIP);
    glVertex2f( 0+100,         0);
    glVertex2f(40+100,         0);
    glVertex2f( 0+100, VIRTH*3/2);
    glVertex2f(40+100, VIRTH*3/2);
    glEnd();

    // option 2: render with a quad
    /*
    glBegin(GL_QUADS);
    glVertex2f( 0+100,         0);
    glVertex2f(40+100,         0);
    glVertex2f(40+100, VIRTH*3/2);
    glVertex2f( 0+100, VIRTH*3/2);
    glEnd();
    */

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
}

const int layoutLineSize = 70; // same size as stats lines in src/renderextras.cpp
void do_latencystrip()
{
    // engine latency tool: frame labeler for latency/delivery/jitter analysis
    int textcolor;
    do_latencystrip_background(textcolor);
    defformatstring(etime)("\fs\f%d%02d\fr", textcolor, InternalClockPtr->getMillis()%100);
    const int len = (VIRTH*3/2)/layoutLineSize + 1;
    loopi(len)
    {
        draw_text(etime, 0, i*layoutLineSize, 2, 1);
    }

    // latency test: input device to monitor's scan out
    if(latency_get_reaction_state()) { do_latencystrip_reaction(); reaction_state = 2; };
}

void latency_play_reaction_sound()
{
    if(!showlatency) return;

    // sound latency test: play accuracy hitsound
    #define S_ACCURACY_HIT S_ITEMARMOUR
    playsoundc(S_ACCURACY_HIT);
}
