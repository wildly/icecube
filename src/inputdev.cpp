// Joystick support
// Copyright (C) 2016, 2020 Willy Deng
// zlib License

#include "cube.h"

// For compatibility purposes, left and right triggers were paired together in a single axis.
// SDL2 exposes separate left and right trigger axes.
// See joyaxismotion function below.
void checktriggers(const uint joy, const uint axis);
void checktriggerspaired(const uint joy, const uint axis);
void check2Dstick(const uint joy, const uint axis);

vector<SDL_Joystick *> controllers;

vector<int> joyaxisvalues;
void joyaxismotion(const uint joy, const uint axis, const int value)
{
    while((uint)joyaxisvalues.length() < axis+1)
    {
        int &v = joyaxisvalues.add();
        v = 0;
    };
    joyaxisvalues[axis] = value;

#if SDL_VERSION_ATLEAST(2, 0, 0)
    checktriggers(joy, axis);
#else
    checktriggerspaired(joy, axis);
#endif
    check2Dstick(joy, axis);
};

void joyaxisinfo()
{
    loopv(joyaxisvalues) conoutf("jaxis %d: %d", i, joyaxisvalues[i]);
};
COMMAND(joyaxisinfo, ARG_NONE);
VAR(debugjoystick,0,0,1);

VARP(joyaxismovementx,-1,1,10);
VARP(joyaxismovementy,-1,0,10);
VARP(joyaxislookx,-1,3,10);
VARP(joyaxislooky,-1,4,10);
VARP(joyaxistrigger,-1,2,10);
VARP(joyaxistrigger2,-1,5,10);

const short JoyAxisMinValue = SHRT_MIN;
const short JoyAxisMaxValue = SHRT_MAX;
const int JoyAxisAbsMaxValue = (-int(SHRT_MIN));

// process and filter the stick input
// "cooked" values are to be read by mousemove and moveplayer functions
VARP(joyaxisstickdeadzone,0,20,80);
VARP(joyaxissticksensitivity,0,100,1000);
VARP(invertstickmovementaxes,0,1,1);
VARP(invertstickfreelookaxes,0,0,1);

struct stick2 { float x; float y; };
stick2 jmovement = { 0, 0 };
stick2 jlook = { 0, 0 };

float cook2Dstickaxis(const int value)
{
    float result = float(value)/JoyAxisAbsMaxValue;
    float deadzone = joyaxisstickdeadzone/100.0f;
    if(result > deadzone)
        result -= deadzone;
    else if(result < -deadzone)
        result += deadzone;
    else
        result = 0;

    // expand to full range and add in sensitivity
    result = result * (1.0f/(joyaxisstickdeadzone/100.0f)) * (joyaxissticksensitivity/100.0f);

    // clamp result to [-1.0f,1.0f]
    return (result > 1.0f ? 1.0f : result < -1.0f ? -1.0f : result);
};

void check2Dstick(const uint joy, const uint axis)
{
    float value = cook2Dstickaxis(joyaxisvalues[axis]);
    if(axis == joyaxismovementx)      jmovement.x = value;
    else if(axis == joyaxismovementy) jmovement.y = value;
    else if(axis == joyaxislookx)     jlook.x = value;
    else if(axis == joyaxislooky)     jlook.y = value;
};

float getjoymovementmove() { return jmovement.x * (invertstickmovementaxes ? -1 : 1); };
float getjoymovementstrafe() { return jmovement.y * (invertstickmovementaxes ? -1 : 1); };
float getjoylookx() { return jlook.x * (invertstickfreelookaxes ? -1 : 1); };
float getjoylooky() { return jlook.y * (invertstickfreelookaxes ? -1 : 1); };

VARP(joyaxistriggerthreshold,0,80,100);
VARP(inverttriggeraxis,0,0,1);

struct trigger2 { uint left:1; uint right:1; } t2 = { 0, 0 };
void checktriggerspaired(const uint joy, const uint axis)
{
    if(joyaxistrigger+1 > joyaxisvalues.length() || joyaxistrigger == -1 || axis != joyaxistrigger)
        return;

    int value = inverttriggeraxis == 0? joyaxisvalues[joyaxistrigger] : -joyaxisvalues[joyaxistrigger];

    trigger2 last_t2 = t2;
    if(value >= JoyAxisAbsMaxValue*joyaxistriggerthreshold/100) // above positive threshold: left trigger on
    {
        t2.left = 1;
        t2.right = 0;
    }
    else if(value < -JoyAxisAbsMaxValue*joyaxistriggerthreshold/100) // below negative threshold: right trigger on
    {
        t2.right = 1;
        t2.left = 0;
    }
    else // triggers centered
    {
        t2.left = 0;
        t2.right = 0;
    };

    // trigger(s) changed state
    if(last_t2.left != t2.left)
    {
        if(debugjoystick) conoutf("checktrigger: left trigger changed to %s", t2.left ? "on" : "off");
        keypress(-300, t2.left == 1, 0);
    };
    if(last_t2.right != t2.right)
    {
        if(debugjoystick) conoutf("checktrigger: right trigger changed to %s", t2.right ? "on" : "off");
        keypress(-301, t2.right == 1, 0);
    };
};

void checktriggers(const uint joy, const uint axis)
{
    if(joyaxistrigger+1 > joyaxisvalues.length() || joyaxistrigger == -1 || !(axis == joyaxistrigger || axis == joyaxistrigger2))
        return;

    int value = axis == joyaxistrigger? joyaxisvalues[joyaxistrigger] : joyaxisvalues[joyaxistrigger2];
    if(inverttriggeraxis) value *= -1;

    trigger2 last_t2 = t2;

    if(axis == joyaxistrigger)
    {
    if(value >= JoyAxisAbsMaxValue*joyaxistriggerthreshold/100) // above positive threshold: left trigger on
        t2.left = 1;
    else
        t2.left = 0;
    };

    if(axis == joyaxistrigger2)
    {
    if(value >= JoyAxisAbsMaxValue*joyaxistriggerthreshold/100) // above positive threshold: right trigger on
        t2.right = 1;
    else
        t2.right = 0;
    };

    // trigger(s) changed state
    if(axis == joyaxistrigger && last_t2.left != t2.left)
    {
        if(debugjoystick) conoutf("checktrigger: left trigger changed to %s", t2.left ? "on" : "off");
        keypress(-300, t2.left == 1, 0);
    };
    if(axis == joyaxistrigger2 && last_t2.right != t2.right)
    {
        if(debugjoystick) conoutf("checktrigger: right trigger changed to %s", t2.right ? "on" : "off");
        keypress(-301, t2.right == 1, 0);
    };
};

struct hat4 { uint up:1; uint down:1; uint left:1; uint right:1; };
vector<hat4> joyhatstate;
void joyhatmotion(const uint joy, const uint hat, const uint value)
{
    while((uint)joyhatstate.length() < hat+1)
    {
        hat4 &h = joyhatstate.add();
        h.up = h.down = h.left = h.right = 0;
    };
    if(value&SDL_HAT_CENTERED)
        joyhatstate[hat].up = joyhatstate[hat].down = joyhatstate[hat].left = joyhatstate[hat].right = 0;
    if(value&SDL_HAT_UP)    { joyhatstate[hat].down  = 0; if(!joyhatstate[hat].up)    { joyhatstate[hat].up    = 1; keypress(-200, true, 0); }; };
    if(value&SDL_HAT_DOWN)  { joyhatstate[hat].up    = 0; if(!joyhatstate[hat].down)  { joyhatstate[hat].down  = 1; keypress(-201, true, 0); }; };
    if(value&SDL_HAT_LEFT)  { joyhatstate[hat].right = 0; if(!joyhatstate[hat].left)  { joyhatstate[hat].left  = 1; keypress(-202, true, 0); }; };
    if(value&SDL_HAT_RIGHT) { joyhatstate[hat].left  = 0; if(!joyhatstate[hat].right) { joyhatstate[hat].right = 1; keypress(-203, true, 0); }; };
};
