// renderglloader.cpp: opengl feature detction and extension loading
// moved here from rendergl.cpp

// Portions of this file are from rendergl.cpp included in Tesseract (Rev 2471)

// Portions from Tesseract are: Copyright (C) 2001-2020 Wouter van Oortmerssen, Lee Salzman, Mike Dysart, Robert Pointon, Quinton Reeves, and Benjamin Segovia
// See LICENSE-Tesseract.txt for the Tesseract source code license

// Portions of this file are from rendergl.cpp included in Sauerbraten (2020 Release)

// Portions from Sauerbraten are: Copyright (C) 2001-2020 Wouter van Oortmerssen, Lee Salzman, Mike Dysart, Robert Pointon, Quinton Reeves, and Benjamin Segovia
// See LICENSE-Sauerbraten.txt for the Sauerbraten source code license


#include "cube.h"

// features
bool hasVAO = false, hasTR = false, hasTSW = false, hasPBO = false, hasFBO = false, hasAFBO = false, hasDS = false, hasTF = false, hasCBF = false, hasS3TC = false, hasFXT1 = false, hasLATC = false, hasRGTC = false, hasAF = false, hasFBB = false, hasFBMS = false, hasTMS = false, hasMSS = false, hasFBMSBS = false, hasUBO = false, hasMBR = false, hasDB2 = false, hasDBB = false, hasTG = false, hasTQ = false, hasPF = false, hasTRG = false, hasTI = false, hasHFV = false, hasHFP = false, hasDBT = false, hasDC = false, hasDBGO = false, hasEGPU4 = false, hasGPU4 = false, hasGPU5 = false, hasBFE = false, hasEAL = false, hasCR = false, hasOQ2 = false, hasES2 = false, hasES3 = false, hasCB = false, hasCI = false, hasTS = false;

// renderer
bool mesa = false, intel = false, amd = false, nvidia = false;

int hasstencil = 0;

// rendergl
VAR(useubo, 1, 0, 0);
VAR(usetexgather, 1, 0, 0);
VAR(usetexcompress, 1, 0, 0);
VAR(maxdrawbufs, 1, 0, 0);
VAR(maxdualdrawbufs, 1, 0, 0);

// extensions
// For this VAR to take effect, it is necessary to load cfg before calling gl_init
VAR(dbgexts, 0, 0, 1);

// hashtable for glexts (Cube doesn't have hashset)
// hashtable key = "const char *" meaning name of the extension
// hashtable data = "const char *" meaning name of the extension to prefer if different from the key
// If updating this hashtable's data, it is recommended to free the existing string pointed to by data to avoid leaking strings.
hashtable<char *> glexts;

void parseglexts()
{
/*
    if(glversion >= 300)
    {
        GLint numexts = 0;
        glGetIntegerv(GL_NUM_EXTENSIONS, &numexts);
        loopi(numexts)
        {
            const char *ext = (const char *)glGetStringi_(GL_EXTENSIONS, i);
            glexts.add(newstring(ext));
        }
    }
    else
*/
    {
        const char *exts = (const char *)glGetString(GL_EXTENSIONS);
        for(;;)
        {
            while(*exts == ' ') exts++;
            if(!*exts) break;
            const char *ext = exts;
            while(*exts && *exts != ' ') exts++;
            char *data = newstring(ext, size_t(exts-ext));
            if(exts > ext) glexts.access(newstring(ext, size_t(exts-ext)), &data);
        }
    }
}

bool hasext(const char *ext)
{
    return glexts.access(ext)!=NULL;
}

#ifdef LOAD_OPENGL_EXTENSIONS
// Windows:
// - Features above OpenGL 1.1 will need to be loaded.
// Linux:
// - Comment or uncomment these lines depending on your OpenGL headers and/or implementation.
// - It seems these functions are available except for glBlendEquationSeparate. (It is used in rendertextcolor.cpp.)
#ifdef _WINDOWS
PFNGLBLENDEQUATIONPROC glBlendEquation = NULL;
#endif

PFNGLBLENDEQUATIONSEPARATEPROC glBlendEquationSeparate = NULL;

#ifdef _WINDOWS
PFNGLTEXIMAGE3DPROC glTexImage3D = NULL;
PFNGLTEXSUBIMAGE3DPROC glTexSubImage3D = NULL;

PFNGLCOMPRESSEDTEXIMAGE3DPROC glCompressedTexImage3D = NULL;
PFNGLCOMPRESSEDTEXIMAGE2DPROC glCompressedTexImage2D = NULL;
PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC glCompressedTexSubImage3D = NULL;
PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC glCompressedTexSubImage2D = NULL;
#endif

PFNGLTEXSTORAGE2DPROC glTexStorage2D = NULL;

// The function below is called on certain platforms to load additional GL functions (see gl_init below).
// wglGetProcAddress works fine on Windows, but we'll use SDL_GL_GetProcAddress to be cross-platform.
void gl_load_extensions()
{
#ifdef _WINDOWS
    // OpenGL 1.4: glBlendEquation
    glBlendEquation = (PFNGLBLENDEQUATIONPROC)SDL_GL_GetProcAddress("glBlendEquation");
    if(glBlendEquation) conoutf("gl_init: glBlendEquation");
#endif

    // OpenGL 2.0: glBlendEquationSeparate
    glBlendEquationSeparate = (PFNGLBLENDEQUATIONSEPARATEPROC)SDL_GL_GetProcAddress("glBlendEquationSeparate");
    if(glBlendEquationSeparate) conoutf("gl_init: glBlendEquationSeparate");

#ifdef _WINDOWS
    // OpenGL 1.2: 3D Textures
    glTexImage3D = (PFNGLTEXIMAGE3DPROC)SDL_GL_GetProcAddress("glTexImage3D");
    if(glTexImage3D) conoutf("gl_init: glTexImage3D");
    glTexSubImage3D = (PFNGLTEXSUBIMAGE3DPROC)SDL_GL_GetProcAddress("glTexSubImage3D");
    if(glTexSubImage3D) conoutf("gl_init: glTexSubImage3D");

    // OpenGL 1.3: Compressed Textures
    glCompressedTexImage3D = (PFNGLCOMPRESSEDTEXIMAGE3DPROC)SDL_GL_GetProcAddress("glCompressedTexImage3D");
    if(glCompressedTexImage3D) conoutf("gl_init: glCompressedTexImage3D");
    glCompressedTexImage2D = (PFNGLCOMPRESSEDTEXIMAGE2DPROC)SDL_GL_GetProcAddress("glCompressedTexImage2D");
    if(glCompressedTexImage2D) conoutf("gl_init: glCompressedTexImage2D");
    glCompressedTexSubImage3D = (PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC)SDL_GL_GetProcAddress("glCompressedTexSubImage3D");
    if(glCompressedTexSubImage3D) conoutf("gl_init: glCompressedTexSubImage3D");
    glCompressedTexSubImage2D = (PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC)SDL_GL_GetProcAddress("glCompressedTexSubImage2D");
    if(glCompressedTexSubImage2D) conoutf("gl_init: glCompressedTexSubImage2D");
#endif

    // OpenGL 4.2: Immutable "Texture Storage" (ARB_texture_storage)
    glTexStorage2D = (PFNGLTEXSTORAGE2DPROC)SDL_GL_GetProcAddress("glTexStorage2D");
    if(glTexStorage2D) conoutf("gl_init: glTexStorage2D");
}
#endif /* LOAD_OPENGL_EXTENSIONS */

void gl_print_version()
{
    const char *vendor = (const char *)glGetString(GL_VENDOR);
    conoutf("GL Vendor: %s", vendor);
    const char *renderer = (const char *)glGetString(GL_RENDERER);
    conoutf("GL Renderer: %s", renderer);
    const char *version = (const char *)glGetString(GL_VERSION);
    conoutf("GL Version: %s", version);

    // OpenGL 2.0+: GLSL Version (GL_SHADING_LANGUAGE_VERSION)
    const char *glslstr = (const char *)glGetString(GL_SHADING_LANGUAGE_VERSION);
    conoutf("GLSL Version: %s", glslstr);
}

// engine.h (Tesseract Rev 2471) - texture
extern int hwtexsize, hwcubetexsize, hwmaxaniso, maxtexsize, hwtexunits, hwvtexunits;

void gl_detect_features()
{
    const char *vendor = (const char *)glGetString(GL_VENDOR);
    const char *renderer = (const char *)glGetString(GL_RENDERER);
    const char *version = (const char *)glGetString(GL_VERSION);

    if(strstr(renderer, "Mesa") || strstr(version, "Mesa"))
    {
        mesa = true;
        if(strstr(renderer, "Intel")) intel = true;
    }
    else if(strstr(vendor, "NVIDIA"))
        nvidia = true;
    else if(strstr(vendor, "ATI") || strstr(vendor, "Advanced Micro Devices"))
        amd = true;
    else if(strstr(vendor, "Intel"))
        intel = true;

    int glversion;
    uint glmajorversion, glminorversion;
    if(sscanf(version, " %u.%u", &glmajorversion, &glminorversion) != 2) glversion = 100;
    else glversion = glmajorversion*100 + glminorversion*10;

    parseglexts();

    GLint texsize = 0, texunits = 0, vtexunits = 0, cubetexsize = 0, drawbufs = 0;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &texsize);
    hwtexsize = texsize;
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &texunits);
    hwtexunits = texunits;
    glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &vtexunits);
    hwvtexunits = vtexunits;
    glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE, &cubetexsize);
    hwcubetexsize = cubetexsize;
    glGetIntegerv(GL_MAX_DRAW_BUFFERS, &drawbufs);
    maxdrawbufs = drawbufs;

    if(glversion >= 300)
    {
        hasTF = hasTRG = hasRGTC = hasPF = hasHFV = hasHFP = true;
    }
    else
    {
        if(hasext("GL_ARB_texture_float"))
        {
            hasTF = true;
            if(dbgexts) conoutf(CON_INIT, "Using GL_ARB_texture_float extension.");
        }
        if(hasext("GL_ARB_texture_rg"))
        {
            hasTRG = true;
            if(dbgexts) conoutf(CON_INIT, "Using GL_ARB_texture_rg extension.");
        }
        if(hasext("GL_ARB_texture_compression_rgtc") || hasext("GL_EXT_texture_compression_rgtc"))
        {
            hasRGTC = true;
            if(dbgexts) conoutf(CON_INIT, "Using GL_ARB_texture_compression_rgtc extension.");
        }
        if(hasext("GL_EXT_packed_float"))
        {
            hasPF = true;
            if(dbgexts) conoutf(CON_INIT, "Using GL_EXT_packed_float extension.");
        }
    }

    // Framebuffer object
    if(glversion >= 300 || hasext("GL_ARB_framebuffer_object"))
    {
        hasAFBO = hasFBO = hasFBB = hasFBMS = hasDS = true;
        if(glversion < 300 && dbgexts) conoutf(CON_INIT, "Using GL_ARB_framebuffer_object extension.");
    }
    else if(hasext("GL_EXT_framebuffer_object"))
    {
        if(hasext("GL_EXT_packed_depth_stencil") || hasext("GL_NV_packed_depth_stencil"))
        {
            hasDS = true;
            if(dbgexts) conoutf(CON_INIT, "Using GL_EXT_packed_depth_stencil extension.");
        }
    }

    if(glversion >= 310 || hasext("GL_ARB_texture_rectangle"))
    {
        hasTR = true;
        if(glversion < 310 && dbgexts) conoutf(CON_INIT, "Using GL_ARB_texture_rectangle extension.");
    }

    // Compressed texture formats
    if(hasext("GL_EXT_texture_compression_s3tc"))
    {
        hasS3TC = true;
#ifdef __APPLE__
        usetexcompress = 1;
#else
        if(!mesa) usetexcompress = 2;
#endif
        if(dbgexts) conoutf(CON_INIT, "Using GL_EXT_texture_compression_s3tc extension.");
    }
    else if(hasext("GL_EXT_texture_compression_dxt1") && hasext("GL_ANGLE_texture_compression_dxt3") && hasext("GL_ANGLE_texture_compression_dxt5"))
    {
        hasS3TC = true;
        if(dbgexts) conoutf(CON_INIT, "Using GL_EXT_texture_compression_dxt1 extension.");
    }
    if(hasext("GL_3DFX_texture_compression_FXT1"))
    {
        hasFXT1 = true;
        if(mesa) usetexcompress = max(usetexcompress, 1);
        if(dbgexts) conoutf(CON_INIT, "Using GL_3DFX_texture_compression_FXT1.");
    }
    if(hasext("GL_EXT_texture_compression_latc"))
    {
        hasLATC = true;
        if(dbgexts) conoutf(CON_INIT, "Using GL_EXT_texture_compression_latc extension.");
    }
    if(glversion >= 300 || hasext("GL_ARB_texture_compression_rgtc") || hasext("GL_EXT_texture_compression_rgtc"))
    {
        hasRGTC = true;
        if(glversion < 300 && dbgexts) conoutf(CON_INIT, "Using GL_ARB_texture_compression_rgtc extension.");
    }

    // Anisotropic filtering
    if(hasext("GL_EXT_texture_filter_anisotropic"))
    {
        GLint val;
        glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &val);
        hwmaxaniso = val;
        hasAF = true;
        if(dbgexts) conoutf(CON_INIT, "Using GL_EXT_texture_filter_anisotropic extension.");
    }

    if(glversion >= 330)
    {
        hasTSW = hasEAL = hasOQ2 = true;
    }
    else
    {
        if(hasext("GL_ARB_texture_swizzle") || hasext("GL_EXT_texture_swizzle"))
        {
            hasTSW = true;
            if(dbgexts) conoutf(CON_INIT, "Using GL_ARB_texture_swizzle extension.");
        }
        if(hasext("GL_ARB_explicit_attrib_location"))
        {
            hasEAL = true;
            if(dbgexts) conoutf(CON_INIT, "Using GL_ARB_explicit_attrib_location extension.");
        }
        if(hasext("GL_ARB_occlusion_query2"))
        {
            hasOQ2 = true;
            if(dbgexts) conoutf(CON_INIT, "Using GL_ARB_occlusion_query2 extension.");
        }
    }

    if(glversion >= 420 || hasext("GL_ARB_texture_storage"))
    {
        hasTS = true;
        if(glversion < 420 && dbgexts) conoutf(CON_INIT, "Using GL_ARB_texture_storage extension.");
    }
    else if(hasext("GL_EXT_texture_storage"))
    {
        hasTS = true;
        if(dbgexts) conoutf(CON_INIT, "Using GL_EXT_texture_storage extension.");
    }
}
