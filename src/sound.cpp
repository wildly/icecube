// sound.cpp: basic positional sound using sdl_mixer

#include "cube.h"
#include <SDL_mixer.h>

#define MAXVOL MIX_MAX_VOLUME

bool nosound = true;

VARP(soundvol, 0, 255, 255);
VARP(musicvol, 0, 128, 255);
VARP(videovol, 0, 255, 255);

const int NUMFXCHAN = 32;
const int NUMSOUNDSTREAM = 2;
const int NUM_RESERVED_CHANNELS = NUMSOUNDSTREAM;
const int MAXCHAN = NUMFXCHAN + NUM_RESERVED_CHANNELS;

VARP(soundfreq, 22050, MIX_DEFAULT_FREQUENCY, 48000);

struct soundloc { vec loc; bool inuse; } soundlocs[MAXCHAN];

Mix_Music *music = NULL;

void stopmusic()
{
    if(nosound) return;
    if(music)
    {
        Mix_HaltMusic();
        Mix_FreeMusic(music);
        music = NULL;
    };
};

VAR(soundbufferlen, 128, 1024, 4096);

void initsound()
{
    memset(soundlocs, 0, sizeof(soundloc)*MAXCHAN);
    if(Mix_OpenAudio(soundfreq, MIX_DEFAULT_FORMAT, 2, soundbufferlen)<0)
    {
        conoutf("sound init failed (SDL_mixer): %s", (size_t)Mix_GetError());
        nosound = true;
        return;
    };
    Mix_AllocateChannels(MAXCHAN);
    soundstream_init();
    nosound = false;
};

void startmusic(char *name)
{
    if(nosound) return;
    stopmusic();
    if(musicvol) // since setting soundvol to 0 doesn't stop the currently playing music track, let's make music independent of soundvol (i.e. let's not check soundvol here)
    {
        defformatstring(sn)("packages/%s", name);
        if(music = Mix_LoadMUS(path(sn)))
        {
            Mix_PlayMusic(music, -1);
            Mix_VolumeMusic((musicvol*MAXVOL)/255);
        }
        else
        {
            conoutf("could not play music: %s", sn);
        };
    };
};

COMMANDN(music, startmusic, ARG_1STR);

vector<Mix_Chunk *> samples;
cvector snames;

int registersound(char *name)
{
    loopv(snames) if(strcmp(snames[i], name)==0) return i;
    snames.add(newstring(name));
    samples.add(NULL);
    return samples.length()-1;
};

COMMAND(registersound, ARG_1EST);

void cleansound()
{
    if(nosound) return;
    stopmusic();
    soundstream_teardown();
    Mix_CloseAudio();
};

VAR(stereo, 0, 1, 1);

int lastsoundl = 0, lastsoundr = 0, lastsoundv = 0;

// Note: Panning causes sounds to be half as loud (average 127 between both channels).
void updatechanvol(int chan, vec *loc)
{
    float vol = (float)soundvol;
    if(chan < NUMSOUNDSTREAM) vol = (float)videovol;
    int pan = 127;
    if(loc)
    {
        vdist(dist, v, *loc, player1->o);
        vol -= dist*3.0f*vol/255.0f;                        // simple mono distance attenuation
        if(vol < 0) vol = 0;
        if(stereo && (v.x != 0 || v.y != 0))
        {
            float yaw = -atan2(v.x, v.y) - player1->yaw*(PI / 180.0f); // relative angle of sound along X-Y axis
            pan = (int)(254*(0.5f*sin(yaw)+0.5f)+0.5f);     // range is from 0 (left) to 254 (right)
        };
    };
    vol = vol*MAXVOL/255.0f;
    Mix_Volume(chan, (int)vol);
    Mix_SetPanning(chan, 254-pan, pan);
    lastsoundl = 254-pan;
    lastsoundr = pan;
    lastsoundv = (int)vol;
};

void newsoundloc(int chan, vec *loc)
{
    assert(chan>=0 && chan<MAXCHAN);
    soundlocs[chan].loc = *loc;
    soundlocs[chan].inuse = true;
};

void updatevol()
{
    if(nosound) return;
    loopi(MAXCHAN) if(soundlocs[i].inuse)
    {
        if(Mix_Playing(i))
            updatechanvol(i, &soundlocs[i].loc);
        else soundlocs[i].inuse = false;
    };
    // Update volume for non-positional soundstream and music because videovol or musicvol may have changed.
    // Skip updating volume for non-panning sound fx that are already playing?
    loopi(NUMSOUNDSTREAM) if(!soundlocs[i].inuse)
    {
        if(Mix_Playing(i))
            updatechanvol(i, NULL);
    };
    Mix_VolumeMusic((musicvol*MAXVOL)/255);
};

void playsoundc(int n) { addmsg(0, 2, SV_SOUND, n); playsound(n); };

void playsound(int n, vec *loc)
{
    if(nosound || !soundvol) return;
    if(n<0 || n>=samples.length()) { conoutf("unregistered sound: %d", n); return; };

    if(!samples[n])
    {
        defformatstring(buf)("packages/sounds/%s.wav", snames[n]);
        samples[n] = Mix_LoadWAV(path(buf));
        if(!samples[n]) { conoutf("failed to load sample: %s", buf); return; };
    };

    // avoid bursts of sounds with heavy packetloss and in sp
    static int soundsatonce = 0, lastsoundmillis = 0;
    soundsatonce = (lastmillis==lastsoundmillis) ? soundsatonce+1 : 1;
    lastsoundmillis = lastmillis;
    if(soundsatonce>5) return;

    int chan = Mix_PlayChannel(-1, samples[n], 0);
    if(chan<0) return;
    if(loc) newsoundloc(chan, loc);
    updatechanvol(chan, loc);
};

void sound(int n) { playsound(n, NULL); };
COMMAND(sound, ARG_1INT);

// soundstream
SDL_mutex *soundstream_lock = NULL;
const int soundstream_chunk_count = 8;
VARP(soundstreamaudiodelay, 0, 50, 1000);

struct soundstream_s
{
    vector<Mix_Chunk *> chunks;
    vector<Uint8 *> chunk_backing_memory;
    SDL_AudioStream *source;
    Uint32 audio_start_timestamp;
    // If a soundstream is halting, source is NULL; do not enqueue any more chunks.
    // A soundstream starts as not playing, and automatically starts playing.
    // If a soundstream stops due to exhausting the Mix_Chunk queue, playing is set to false.
    bool inuse, halting, playing;

    void set_audio_delay(int msec)
    {
        audio_start_timestamp = SDL_GetTicks() + msec;
        // Workaround the situation when SDL_GetTicks + msec equals 0.
        if(audio_start_timestamp == 0) audio_start_timestamp = 1;
    }

    void init()
    {
        source = NULL;
        inuse = false;
        halting = false;
        playing = false;
        chunks.reserve(soundstream_chunk_count);
        chunk_backing_memory.reserve(soundstream_chunk_count);
        set_audio_delay(soundstreamaudiodelay);
    }

    // Pre-req: Called from soundstream_check_sources which holds the soundstream_lock mutex
    void update()
    {
        if(inuse && !halting)
        {
            while(1)
            {
            const int chunk_preferred_size_bytes = 1024*2*4*1; // 1024 stereo F32 samples

            int availableLen = SDL_AudioStreamAvailable(source);
            if(availableLen < chunk_preferred_size_bytes) return;

            // Create and enqueue the Mix_Chunk
            float *buffer = (float *)malloc(chunk_preferred_size_bytes);
            SDL_AudioStreamGet(source, buffer, chunk_preferred_size_bytes);
            chunk_backing_memory.add((Uint8 *)buffer);
            Mix_Chunk *chunk = Mix_QuickLoad_RAW((Uint8 *)buffer, chunk_preferred_size_bytes);
            chunks.add(chunk);
            }
        }
    }

    void chunk_finished()
    {
        // Cleanup this chunk
        Mix_FreeChunk(chunks[0]);
        chunks.remove(0);
        free(chunk_backing_memory[0]);
        chunk_backing_memory.remove(0);
    }

    void cleanup()
    {
        loopv(chunks)
        {
            Mix_FreeChunk(chunks[i]);
        }
        chunks.setsize(0);

        loopv(chunk_backing_memory)
        {
            free(chunk_backing_memory[i]);
        }
        chunk_backing_memory.setsize(0);

        source = NULL;
        inuse = false;
        halting = false;
    }
} soundstream[NUMSOUNDSTREAM];

int createsoundstream(SDL_AudioStream *stream)
{
    loopi(NUMSOUNDSTREAM)
    {
        if(soundstream[i].inuse == false)
        {
            soundstream[i].source = stream;
            soundstream[i].inuse = true;
            return i;
        }
        // else try next channel
    }
    // return -1 if no available channels
    return -1;
}

// Pre-req: Channel isn't currently playing
void soundstream_free(int chan)
{
    if(chan >= NUMSOUNDSTREAM) return;

    soundstream[chan].cleanup();
}

// Doesn't halt right away if currently playing because we allow the chunk to finish playing.
void haltsoundstream(int chan)
{
    if(chan >= NUMSOUNDSTREAM) return;

    // If playing, set halting to true and let soundstream_chunk_finished free the soundstream.
    if(Mix_Playing(chan))
    {
        soundstream[chan].halting = true;
    }
    else
    {
        soundstream_free(chan);
    }
}

// TODO: flushsoundstream
// If no further audio will be buffered into source (e.g. finished playing or became paused), call SDL_AudioStreamFlush() and create a Mix_Chunk.

void setsoundstreamloc(int chan, vec *loc)
{
    if(loc)
        newsoundloc(chan, loc);
    else
    {
        soundlocs[chan].inuse = false;
    }
    updatechanvol(chan, loc);
}

// Ask each soundstream to create a Mix_Chunk if possible and add it to the
// queue. Then, if the soundstream channel isn't playing and there is a
// Mix_Chunk in the queue, start playback.
void soundstream_check_sources()
{
    if(nosound) return;
    if(SDL_TryLockMutex(soundstream_lock) == 0)
    {
        loopi(NUMSOUNDSTREAM)
        {
            soundstream[i].update();

            // Check whether audio should start due to completing audio delay time.
            // If audio_start_timestamp is 0, assume we have passed the audio delay time (set it to true).
            bool reached_audio_playback_start_time = soundstream[i].audio_start_timestamp ? false : true;
            if(soundstream[i].audio_start_timestamp &&
                ((int)(SDL_GetTicks() - soundstream[i].audio_start_timestamp) >= 0))
            {
                reached_audio_playback_start_time = true;
                soundstream[i].audio_start_timestamp = 0;
            }

            // If channel isn't playing and soundstream.playing indicates it isn't playing, start playback.
            if(!Mix_Playing(i) && !soundstream[i].playing && soundstream[i].chunks.length() > 0 && reached_audio_playback_start_time)
            {
                Mix_PlayChannel(i, soundstream[i].chunks[0], 0);
                updatechanvol(i, NULL);
                soundstream[i].playing = true;
                //conoutf("soundstream_check_sources: chan %d: playback started (SDL tick: %d)", i, SDL_GetTicks());
            }
        }

        SDL_UnlockMutex(soundstream_lock);
    }
}

// This is the callback function for Mix_ChannelFinished.
// When a Mix_Chunk on a "texture video" audio channel finishes playing, try to play the next queued Mix_Chunk.
void soundstream_chunk_finished(int chan)
{
    if(chan >= NUMSOUNDSTREAM) return;

    if(soundstream[chan].halting)
    {
        haltsoundstream(chan);
        return;
    }

    if(SDL_LockMutex(soundstream_lock) == 0)
    {
        // Important: Queue up and start playing the next Mix_Chunk before cleaning up the finished chunk.

        // Try to play the next queued chunk
        if(soundstream[chan].chunks.length() > 1)
        {
            if(Mix_Playing(chan))
            {
                //conoutf("soundstream_chunk_finished: chan %d: channel is still playing?! (SDL tick: %d)", chan, SDL_GetTicks());
            }
            else
            {
            Mix_PlayChannel(chan, soundstream[chan].chunks[1], 0);
            updatechanvol(chan, NULL);
            //conoutf("soundstream_chunk_finished: chan %d: playing next chunk", chan); // Normal behavior so this message can be commented out.
            }
        }
        else
        {
            //conoutf("soundstream_chunk_finished: chan %d: audio buffer empty (SDL tick: %d)", chan, SDL_GetTicks());
            soundstream[chan].playing = false;
            soundstream[chan].set_audio_delay(soundstreamaudiodelay);
        }

        // Clean up the finished Mix_Chunk
        soundstream[chan].chunk_finished();

        SDL_UnlockMutex(soundstream_lock);
    }
}

void soundstream_init()
{
    Mix_ChannelFinished(soundstream_chunk_finished);
    Mix_ReserveChannels(NUM_RESERVED_CHANNELS);
    loopi(NUMSOUNDSTREAM)
    {
        soundstream[i].init();
    }
    soundstream_lock = SDL_CreateMutex();
}

void soundstream_teardown()
{
    Mix_ChannelFinished(NULL);

    // Additional soundstream cleanup is complicated because we need to stop audio channels that are playing.
    // But we don't need to cleanup because we are exiting.
    // If not exiting, we need to stop soundstream channels, then call soundstream::cleanup for each soundstream channel.
}

int soundstream_get_chunkcount()
{
    SDL_LockMutex(soundstream_lock);
    int total_chunks = 0;
    loopi(NUMSOUNDSTREAM)
        total_chunks += soundstream[i].chunks.length();
    SDL_UnlockMutex(soundstream_lock);
    return total_chunks;
}
