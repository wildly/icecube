// protos for ALL external functions in cube... 

// command
extern int variable(const char *name, int min, int cur, int max, int *storage, void (*fun)(), bool persist);
extern void setvar(const char *name, int i);
extern int getvar(const char *name);
extern bool identexists(const char *name);
extern bool addcommand(const char *name, void (*fun)(), int narg);
extern int execute(char *p, bool down = true);
extern void exec(const char *cfgfile);
extern bool execfile(const char *cfgfile);
extern void resetcomplete();
extern void complete(char *s);
extern void alias(const char *name, char *action);
extern char *getalias(const char *name);
extern void writecfg();

// console
extern void initconsole();
extern void textinput(const char* composition);
extern void keypress(int code, bool isdown, int cooked);
extern void keypress(int code, bool isdown, bool isrepeat, int cooked);
extern void renderconsole();
extern void conoutf(const char *s, ...);
extern void conoutf(int type, const char *s, ...);
extern void conouts(const char *s, int fmt_ver);
extern char *getcurcommand();
extern void writebinds(FILE *f);

// menus
extern void initmenus();
extern bool rendermenu();
extern void menuset(int menu);
extern void menumanual(int m, int n, char *text);
extern void menuclean(int m);
extern void sortmenu(int start, int num);
extern bool menukey(int code, bool isdown);
extern void newmenu(const char *name);

// serverbrowser
extern void addserver(char *servername);
extern char *getservername(int n);
extern void writeservercfg();

// camera
extern float getzoompower();
extern float getzoomfov(float nominalfov);
extern float getCameraOffsetZ(dynent *d);   // in physics.cpp
extern void resetcamera(dynent *d);         // in physics.cpp

// texturevideo
bool loadvideo(int tnum, const char *filename);
void updatevideotexture(int deltamsec);
void uploadvideotex(int tnum, uchar *videoTexture);
void closevideo();

// rendergl
extern void gl_init(int w, int h);
extern void cleangl();
extern void gl_drawframe(int w, int h);
extern bool installtex(int tnum, char *texname, int &xs, int &ys, bool clamp = false, bool mipmap = true);
extern bool installtex_dds(int tnum, char *texname, int &xs, int &ys, bool clamp = false, bool mipmap = true);
extern bool installtex_crn(int tnum, char *texname, int &xs, int &ys, bool clamp = false, bool mipmap = true);
extern void mipstats(int a, int b, int c);
extern void vertf(float v1, float v2, float v3, sqr *ls, float t1, float t2);
extern void addstrip(int tex, int start, int n);
extern int lookuptexture(int tex, int &xs, int &ys);
extern int gethudgunalias(int gun);

// rendercubes
extern void resetcubes();
extern void render_flat(int tex, int x, int y, int size, int h, sqr *l1, sqr *l2, sqr *l3, sqr *l4, bool isceil);
extern void render_flatdelta(int wtex, int x, int y, int size, float h1, float h2, float h3, float h4, sqr *l1, sqr *l2, sqr *l3, sqr *l4, bool isceil);
extern void render_square(int wtex, float floor1, float floor2, float ceil1, float ceil2, int x1, int y1, int x2, int y2, int size, sqr *l1, sqr *l2, bool topleft);
extern void render_tris(int x, int y, int size, bool topleft, sqr *h1, sqr *h2, sqr *s, sqr *t, sqr *u, sqr *v);
extern void addwaterquad(int x, int y, int size);
extern int renderwater(float hf);
extern void finishstrips();
extern void setarraypointers();

// client
extern void localservertoclient(uchar *buf, int len);
extern void connects(char *servername);
extern void disconnect(int onlyclean = 0, int async = 0);
extern void toserver(char *text);
extern void addmsg(int rel, int num, int type, ...);
extern bool multiplayer();
extern bool allowedittoggle();
extern void sendpackettoserv(void *packet);
extern void gets2c();
extern void c2sinfo(dynent *d);
extern void neterr(char *s);
extern void initclientnet();
extern bool netmapstart();
extern int getclientnum();
extern void changemapserv(char *name, int mode);
extern void writeclientinfo(FILE *f);

// clientgame
extern void checksleepcmd();
extern void mousemove(int dx, int dy);
extern void freelook_periodic_update(int cur_realtime);
extern int getgamespeed();
extern void updateworld(int millis);
extern void startmap(char *name);
extern void changemap(char *name);
extern void initclient();
extern void spawnplayer(dynent *d);
extern void virtualpush(dynent *d, vec &damagekick, bool useEnergyPush);
extern int selfdamage(int damage, int actor, dynent *act);
extern dynent *newdynent(dynent *const d);
extern char *getclientmap();
extern void zapdynent(dynent *&d);
extern dynent *getclient(int cn);
extern void timeupdate(int timeremain);
extern void resetmovement(dynent *d);
extern void fixplayer1range();
extern void resetmutators();
extern void applymutators();
extern void startslowmo();

// clientextras
extern void renderclients();
extern void renderclient(dynent *d, bool team, const char *mdlname, float scale);
void showscores(bool on);
extern void renderscores();
void showaccuracy(bool on);
extern void renderaccuracy();

// inputdev
extern vector<SDL_Joystick *> controllers;
extern void joyaxismotion(const uint joy, const uint axis, const int value);
extern void joyhatmotion(const uint joy, const uint hat, const uint value);
extern float getjoymovementmove();
extern float getjoymovementstrafe();
extern float getjoylookx();
extern float getjoylooky();

// gameinfo
extern const char *modename(int gamemode);
extern const char *modeinfo(int gamemode);

// gamestate
extern void constructClocks();

// world
extern void setupworld(int factor);
extern void empty_world(int factor, bool force);
extern void remip(block &b, int level = 0);
extern void remipmore(block &b, int level = 0);
extern int closestent();
extern int findentity(int type, int index = 0);
extern void trigger(int tag, int type, bool savegame);
extern void resettagareas();
extern void settagareas();
extern extentity *newentity(int x, int y, int z, char *what, int v1, int v2, int v3, int v4);

// worldlight
extern void calclight();
extern void adddynlight(vec &v, int reach, int strength, dynent *owner, int colorindex);
extern void dodynlights();
extern void cleardynlights();
extern void dynlightstats(int &numlights, int &numsources);
extern void add_dynlightsource(vec &v, int reach, int strength, int colorindex, int duration, int fadeout);
extern void check_dynlightsources(int time);
extern void clear_dynlightsources();
extern block *blockcopy(block &b);
extern void blockpaste(block &b);

// worldrender
extern void render_world(float vx, float vy, float vh, int yaw, int pitch, float widef, int w, int h);

// worldocull
extern void computeraytable(const float vx, const float vy);
extern int isoccluded(const float vx, const float vy, const float cx, const float cy, const float csize);

// main
extern void fatal(const char *s, const char *o = "");
extern void *alloc(int s);
extern void keyrepeat(bool on);
extern void mainloop_mapchanged_nodelay();

// rendertext
extern void draw_text(const char *str, int left, int top, int gl_num, int fmt_ver = 0);
extern void draw_textf(const char *fstr, int left, int top, int gl_num, ...);
extern int text_width(const char *str, int fmt_ver = 0);
extern void draw_envbox(int t, int fogdist);

// rendertextcolor
extern void text_color(char c);

// editing
extern void cursorupdate();
extern void toggleedit();
extern void editdrag(bool isdown);
extern void setvdeltaxy(int delta, block &sel);
extern void editequalisexy(bool isfloor, block &sel);
extern void edittypexy(int type, block &sel);
extern void edittexxy(int type, int t, block &sel);
extern void editheightxy(bool isfloor, int amount, block &sel);
extern bool noteditmode();
extern void pruneundos(int maxremain = 0);

// renderboundingbox
extern void drawboundingbox(dynent *d, extentity *e, bool editmode);

// renderextras
extern void line(int x1, int y1, float z1, int x2, int y2, float z2);
extern void box(block &b, float z1, float z2, float z3, float z4);
extern void dot(int x, int y, float z);
extern void linestyle(float width, int r, int g, int b);
extern void newsphere(vec &o, float max, int type, dynent *owner);
extern void renderspheres(int time);
extern void gl_drawhud(int w, int h, bool underwater);
extern void readdepth(int w, int h);
extern void reset_orient_and_worldpos();
extern void blendbox(int x1, int y1, int x2, int y2, bool border);
extern void damageblend(int n);
extern void dodynlightforspheres();
extern void clearspheres();
extern float getGUIAspectRatio();
extern float getGUIPointScaling();

// renderparticles
extern void setorient(vec &r, vec &u);
extern void particle_splash(int type, int num, int fade, vec &p);
extern void particle_jet(int type, int num, int fade, vec &p, const vec &groupv);
extern void particle_jet(int type, int num, int fade, vec &p);
extern void particle_trail(int type, int fade, vec &from, vec &to);
extern void particle_trace(int type, int fade, vec &from, vec &to);
extern void particle_cloud(const int type, const int num, const uint fade, const uint fadeRnd, vec &source, const float radius, const float speed, bool grouped = false);
extern void particle_bubble(const int type, const int num, const uint fade, const uint fadeRnd, vec &source, const float radius, const float speed, bool grouped = false);
extern void on_created_particles_add_velocity(vec &v);
extern void render_particles(int time);
extern void reset_particles();
extern void raycheck(const vec &o, vec &to);
extern bool iscollided(vec &o);
extern int trybounce_physent(vec &o, vec &d);

// worldio
extern void save_world(char *fname);
extern bool load_world(char *mname);
extern void writemap(char *mname, int msize, uchar *mdata);
extern uchar *readmap(char *mname, int *msize);
extern void loadgamerest();
extern void incomingdemodata(uchar *buf, int len, bool extras = false);
extern void demoplaybackstep();
extern void stop();
extern void stopifrecording();
extern void demodamage(int damage, vec &o);
extern void demoblend(int damage);

// physics
extern dvector &getcollidable();
extern void moveplayer(dynent *pl, int moveres, bool local);
extern vec estimatevelocity(dynent *d);
extern vec getPhysVelocity(dynent *d);
extern void timeinair_end(dynent *d);
extern void moveplayer_timeinair_end(dynent *d);
extern bool collide(dynent *d, bool spawn, float drop, float rise);
extern bool mmcollidephysent(vec &o, vec &out);
extern void entinmap(dynent *d);
extern void setentphysics(int mml, int mmr);
extern void physicsframe(int deltamillis);
extern int getphysicsrepeat();
extern void energypush(dynent *d, vec push);
extern void energypush(dynent *d, vec push, int type);
extern void addCameraRoll(dynent *d, float amount, float limit);
extern void enterDeathCamera();
extern void prettyprintvec(char *buf, const vec &v, const char *separatorChars, const float threshold);

// sound
extern void playsound(int n, vec *loc = 0);
extern void playsoundc(int n);
extern void initsound();
extern void cleansound();
extern void updatevol();

// soundstream (in sound.cpp)
extern void soundstream_init();
extern void soundstream_teardown();
extern void soundstream_check_sources();
extern int soundstream_get_chunkcount();
extern int createsoundstream(SDL_AudioStream *stream);
extern void haltsoundstream(int chan);
extern void setsoundstreamloc(int chan, vec *loc);

// capture, movie
extern void checkmovie();
extern void stopmovie();

// rendermd2
extern void rendermodel(const char *mdl, int frame, int range, int tex, float rad, float x, float y, float z, float yaw, float pitch, bool teammate, float scale, float speed, int snap = 0, int basetime = 0);
extern bool mmi_valid(mapmodelinfo &mmi);
extern mapmodelinfo &getmminfo(int i);

// server
extern void initserver(bool dedicated, int uprate, char *sdesc, char *ip, char *master, char *passwd, int maxcl);
extern void cleanupserver();
extern void localconnect();
extern void localdisconnect();
extern void localclienttoserver(struct _ENetPacket *);
extern void serverslice(unsigned int mseconds, unsigned int timeout);
extern void putint(uchar *&p, int n);
extern int getint(uchar *&p);
extern void sendstring(char *t, uchar *&p);
extern void startintermission();
extern void restoreserverstate(vector<extentity> &ents);
extern uchar *retrieveservers(uchar *buf, int buflen);
extern signed char msgsizelookup(int msg);
extern void serverms(int mode, int numplayers, int minremain, char *smapname, int seconds, bool isfull);
extern void servermsinit(const char *master, char *sdesc, bool listen);
extern void sendmaps(int n, string mapname, int mapsize, uchar *mapdata);
extern ENetPacket *recvmap(int n);

// weapon
extern void selectgun(int a = -1, int b = -1, int c =-1);
extern void shoot(dynent *d, vec &to);
extern void executeshot(int numrays, int gun, vec &from, vec &to, dynent *d, bool local);
extern int setupgunrays(const int gun, const vec &from, const vec &to);
extern void updateprojectiles(int time, int movesteps);
extern void clearprojectiles();
extern char *playerincrosshair();
extern dynent *dynentincrosshair();
extern int reloadtime(int gun);
extern void accurateChainGun(bool enable);
extern void enableShotgunBlast(bool enable);
extern const char *getWeaponName(int gun);
extern const char *getWeaponNameAbbrev(int gun);
extern int getWeaponBaseDamage(int gun);
extern bool isMelee(int gun);
extern bool isSplashDamage(int gun);
extern void playhitsound();

// health
extern int armourdamagecalc(int damage, int *armourloss = NULL);
extern void givearmour(dynent *d, const int type, int addVal, int maxVal = 0);
extern void updatearmourtype(dynent *d);
extern void checkregen(int time);
extern void resetexpregen(dynent *d);
extern void inhibitexpregen(int incomingdamage, int healthdamage);
extern void loghealth();

// monster
extern void monsterstartmap();
extern void monsterclear();
extern void restoremonsterstate();
extern void monsterthink();
extern void monsterrender();
extern dvector &getmonsters();
extern int monsterpain(dynent *m, int damage, dynent *d);
extern void endsp(bool allkilled);
extern int monsterbasehealth(int mtype);

// accuracy
extern void initAccuracy();
extern void accuracyAddShot(int gun);
extern void accuracyAddResult(int gun, int totalNomDamage, int totalNomSelfDamage, unsigned int flags);
extern unsigned int accuracyCreateResultFlags(int gun, int totalNomDamage, int totalNomSelfDamage);
extern float getWeaponAccuracy(int gun);
extern int getWeaponAccuracyWeight(int gun);
extern float getTotalAccuracy();
extern int getWeaponAccuracyHitsNumerator(const int gun);
extern int getWeaponAccuracyHitsDenominator(const int gun);

extern void initFinalScore(int skill);
extern void updateFinalScoreTime(int seconds);
extern void updateFinalScoreSlowmo(int seconds);
extern void updateFinalScoreMapCleared(bool allkilled);
extern int getFinalScore();

// entities
extern void renderents();
extern void putitems(uchar *&p);
extern void checkbuffs(int time);
extern void checkitems();
extern void realpickup(int n, dynent *d);
extern void renderentities();
extern void resetspawns();
extern void setspawn(uint i, bool on);
extern void teleport(int n, dynent *d);
extern void teleport(dynent *s, dynent *dest);
extern void teleportEffect(dynent *d, vec &effectLoc, bool arriving = false);
extern void jumppad(int n, dynent *d);
extern bool jumppadlockout(int n, dynent *d, int &lastjumppad);
extern void baseammo(int gun);
extern void classicItems(bool enable);

// rndmap
extern void perlinarea(block &b, int scale, int seed, int psize);

// speedy
extern void init_speedy_client();
extern void init_speedy_dedicated_server();

// latency test
extern void latency_set_reaction_state(bool state);
extern void do_latencystrip();
extern void latency_play_reaction_sound();
