// texturemanager.cpp: texture slot management
// texture slot management moved here from texture.cpp

// Portions from Tesseract are: Copyright (C) 2001-2020 Wouter van Oortmerssen, Lee Salzman, Mike Dysart, Robert Pointon, Quinton Reeves, and Benjamin Segovia
// See LICENSE-Tesseract.txt for the Tesseract source code license

#include "cube.h"
#include "texture.h"
#include <SDL_image.h>

#ifdef LOAD_OPENGL_EXTENSIONS
#ifdef _WINDOWS
extern PFNGLTEXIMAGE3DPROC glTexImage3D;
extern PFNGLTEXSUBIMAGE3DPROC glTexSubImage3D;
extern PFNGLCOMPRESSEDTEXIMAGE3DPROC glCompressedTexImage3D;
extern PFNGLCOMPRESSEDTEXIMAGE2DPROC glCompressedTexImage2D;
extern PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC glCompressedTexSubImage3D;
extern PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC glCompressedTexSubImage2D;
#endif
extern PFNGLTEXSTORAGE2DPROC glTexStorage2D;
#endif

// TODO (IceCube): Port initwarning. For now, we'll just print a message.
#define INIT_LOAD 0
void initwarning(const char *msg, int level) { conoutf("initwarning: %s (INIT_LOAD)", msg); }

// engine.h (Tesseract Rev 2471) - rendergl
extern bool hasVAO, hasTR, hasTSW, hasPBO, hasFBO, hasAFBO, hasDS, hasTF, hasCBF, hasS3TC, hasFXT1, hasLATC, hasRGTC, hasAF, hasFBB, hasFBMS, hasTMS, hasMSS, hasFBMSBS, hasUBO, hasMBR, hasDB2, hasDBB, hasTG, hasTQ, hasPF, hasTRG, hasTI, hasHFV, hasHFP, hasDBT, hasDC, hasDBGO, hasEGPU4, hasGPU4, hasGPU5, hasBFE, hasEAL, hasCR, hasOQ2, hasES2, hasES3, hasCB, hasCI, hasTS;

// pre-declaration in engine.h - function is in this file
void setuptexcompress();

VAR(hwtexsize, 1, 0, 0);
VAR(hwcubetexsize, 1, 0, 0);
VAR(hwmaxaniso, 1, 0, 0);
VAR(hwtexunits, 1, 0, 0);
VAR(hwvtexunits, 1, 0, 0);
VARFP(maxtexsize, 0, 0, 1<<12, initwarning("texture quality", INIT_LOAD));
VARFP(reducefilter, 0, 1, 1, initwarning("texture quality", INIT_LOAD));
VARFP(texreduce, 0, 0, 12, initwarning("texture quality", INIT_LOAD));
VARFP(texcompress, 0, 1536, 1<<12, initwarning("texture quality", INIT_LOAD));
VARFP(texcompressquality, -1, -1, 1, setuptexcompress());
VARF(trilinear, 0, 1, 1, initwarning("texture filtering", INIT_LOAD));
VARF(bilinear, 0, 1, 1, initwarning("texture filtering", INIT_LOAD));
VARFP(aniso, 0, 0, 16, initwarning("texture filtering", INIT_LOAD));

extern int usetexcompress;

void setuptexcompress()
{
    if(!usetexcompress) return;

    GLenum hint = GL_DONT_CARE;
    switch(texcompressquality)
    {
        case 1: hint = GL_NICEST; break;
        case 0: hint = GL_FASTEST; break;
    }
    glHint(GL_TEXTURE_COMPRESSION_HINT, hint);
}

GLenum compressedformat(GLenum format, int w, int h, int force = 0)
{
    if(usetexcompress && texcompress && force >= 0 && (force || max(w, h) >= texcompress)) switch(format)
    {
        case GL_R3_G3_B2:
        case GL_RGB5:
        case GL_RGB565:
        case GL_RGB8:
        case GL_RGB: return usetexcompress > 1 ? GL_COMPRESSED_RGB_S3TC_DXT1_EXT : GL_COMPRESSED_RGB;
        case GL_RGB5_A1: return usetexcompress > 1 ? GL_COMPRESSED_RGBA_S3TC_DXT1_EXT : GL_COMPRESSED_RGBA;
        case GL_RGBA: return usetexcompress > 1 ? GL_COMPRESSED_RGBA_S3TC_DXT5_EXT : GL_COMPRESSED_RGBA;
        case GL_RED:
        case GL_R8: return hasRGTC ? (usetexcompress > 1 ? GL_COMPRESSED_RED_RGTC1 : GL_COMPRESSED_RED) : (usetexcompress > 1 ? GL_COMPRESSED_RGB_S3TC_DXT1_EXT : GL_COMPRESSED_RGB);
        case GL_RG:
        case GL_RG8: return hasRGTC ? (usetexcompress > 1 ? GL_COMPRESSED_RG_RGTC2 : GL_COMPRESSED_RG) : (usetexcompress > 1 ? GL_COMPRESSED_RGBA_S3TC_DXT5_EXT : GL_COMPRESSED_RGBA);
        case GL_LUMINANCE:
        case GL_LUMINANCE8: return hasLATC ? (usetexcompress > 1 ? GL_COMPRESSED_LUMINANCE_LATC1_EXT : GL_COMPRESSED_LUMINANCE) : (usetexcompress > 1 ? GL_COMPRESSED_RGB_S3TC_DXT1_EXT : GL_COMPRESSED_RGB);
        case GL_LUMINANCE_ALPHA:
        case GL_LUMINANCE8_ALPHA8: return hasLATC ? (usetexcompress > 1 ? GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT : GL_COMPRESSED_LUMINANCE_ALPHA) : (usetexcompress > 1 ? GL_COMPRESSED_RGBA_S3TC_DXT5_EXT : GL_COMPRESSED_RGBA);
    }
    return format;
}

GLenum uncompressedformat(GLenum format)
{
    switch(format)
    {
        case GL_COMPRESSED_ALPHA:
            return GL_ALPHA;
        case GL_COMPRESSED_LUMINANCE:
        case GL_COMPRESSED_LUMINANCE_LATC1_EXT:
            return GL_LUMINANCE;
        case GL_COMPRESSED_LUMINANCE_ALPHA:
        case GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT:
            return GL_LUMINANCE_ALPHA;
        case GL_COMPRESSED_RED:
        case GL_COMPRESSED_RED_RGTC1:
            return GL_RED;
        case GL_COMPRESSED_RG:
        case GL_COMPRESSED_RG_RGTC2:
            return GL_RG;
        case GL_COMPRESSED_RGB:
        case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
            return GL_RGB;
        case GL_COMPRESSED_RGBA:
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
            return GL_RGBA;
    }
    return GL_FALSE;
}

int formatsize(GLenum format)
{
    switch(format)
    {
        case GL_RED:
        case GL_LUMINANCE:
        case GL_ALPHA: return 1;
        case GL_RG:
        case GL_LUMINANCE_ALPHA: return 2;
        case GL_RGB: return 3;
        case GL_RGBA: return 4;
        default: return 4;
    }
}

GLenum sizedformat(GLenum format)
{
    switch(format)
    {
        case GL_RED: return GL_R8;
        case GL_RG: return GL_RG8;
        case GL_RGB: return GL_RGB8;
        case GL_RGBA: return GL_RGBA8;
    }
    return format;
}

VARFP(usenp2, 0, 1, 1, initwarning("texture quality", INIT_LOAD));

void resizetexture(int w, int h, bool mipmap, bool canreduce, GLenum target, int compress, int &tw, int &th)
{
    int hwlimit = target==GL_TEXTURE_CUBE_MAP ? hwcubetexsize : hwtexsize,
        sizelimit = mipmap && maxtexsize ? min(maxtexsize, hwlimit) : hwlimit;
    if(compress > 0 && !usetexcompress)
    {
        w = max(w/compress, 1);
        h = max(h/compress, 1);
    }
    if(canreduce && texreduce)
    {
        w = max(w>>texreduce, 1);
        h = max(h>>texreduce, 1);
    }
    w = min(w, sizelimit);
    h = min(h, sizelimit);
    if(!usenp2 && target!=GL_TEXTURE_RECTANGLE && (w&(w-1) || h&(h-1)))
    {
        tw = th = 1;
        while(tw < w) tw *= 2;
        while(th < h) th *= 2;
        if(w < tw - tw/4) tw /= 2;
        if(h < th - th/4) th /= 2;
    }
    else
    {
        tw = w;
        th = h;
    }
}

// dbgdds verbosity
// 0: off, 1: dds, 2: mipmaps (subimages)
VAR(dbgdds, 0, 0, 2);

// [IceCube] Disable the gpumipmap function
/*
static GLuint mipmapfbo[2] = { 0, 0 };

void cleanupmipmaps()
{
    if(mipmapfbo[0]) { glDeleteFramebuffers_(2, mipmapfbo); memset(mipmapfbo, 0, sizeof(mipmapfbo)); }
}

VARFP(gpumipmap, 0, 1, 1, cleanupmipmaps());
*/

void uploadtexture(int tnum, GLenum target, GLenum internal, int tw, int th, GLenum format, GLenum type, const void *pixels, int pw, int ph, int pitch, bool mipmap, bool prealloc)
{
    int bpp = formatsize(format), row = 0, rowalign = 0;
    if(!pitch) pitch = pw*bpp;
    uchar *buf = NULL;
    if(pw!=tw || ph!=th)
    {
        buf = new uchar[tw*th*bpp];
        scaletexture((uchar *)pixels, pw, ph, bpp, pitch, buf, tw, th);
    }
    else if(tw*bpp != pitch)
    {
        row = pitch/bpp;
        rowalign = texalign(pixels, pitch, 1);
        while(rowalign > 0 && ((row*bpp + rowalign - 1)/rowalign)*rowalign != pitch) rowalign >>= 1;
        if(!rowalign)
        {
            row = 0;
            buf = new uchar[tw*th*bpp];
            loopi(th) memcpy(&buf[i*tw*bpp], &((uchar *)pixels)[i*pitch], tw*bpp);
        }
    }
    //bool shouldgpumipmap = pixels && mipmap && max(tw, th) > 1 && gpumipmap && hasFBB && !uncompressedformat(internal);
    bool shouldgpumipmap = false;
    for(int level = 0, align = 0, mw = tw, mh = th;; level++)
    {
        uchar *src = buf ? buf : (uchar *)pixels;
        if(buf) pitch = mw*bpp;
        int srcalign = row > 0 ? rowalign : texalign(src, pitch, 1);
        if(align != srcalign) glPixelStorei(GL_UNPACK_ALIGNMENT, align = srcalign);
        if(row > 0) glPixelStorei(GL_UNPACK_ROW_LENGTH, row);
        if(!prealloc) glTexImage2D(target, level, internal, mw, mh, 0, format, type, src);
        else if(src) glTexSubImage2D(target, level, 0, 0, mw, mh, format, type, src);
        if(dbgdds >= 2) conoutf(CON_DEBUG, "subimage %d: %dx%d", level+1, mw, mh);
        if(row > 0) glPixelStorei(GL_UNPACK_ROW_LENGTH, row = 0);
        if(!mipmap || shouldgpumipmap || max(mw, mh) <= 1) break;
        int srcw = mw, srch = mh;
        if(mw > 1) mw /= 2;
        if(mh > 1) mh /= 2;
        if(src)
        {
            if(!buf) buf = new uchar[mw*mh*bpp];
            scaletexture(src, srcw, srch, bpp, pitch, buf, mw, mh);
        }
    }
    if(buf) delete[] buf;
/*
    if(shouldgpumipmap)
    {
        GLint fbo = 0;
        if(!inbetweenframes || drawtex) glGetIntegerv(GL_FRAMEBUFFER_BINDING, &fbo);
        if(!prealloc) for(int level = 1, mw = tw, mh = th; max(mw, mh) > 1; level++)
        {
            if(mw > 1) mw /= 2;
            if(mh > 1) mh /= 2;
            glTexImage2D(target, level, internal, mw, mh, 0, format, type, NULL);
        }
        if(!mipmapfbo[0]) glGenFramebuffers_(2, mipmapfbo);
        glBindFramebuffer_(GL_READ_FRAMEBUFFER, mipmapfbo[0]);
        glBindFramebuffer_(GL_DRAW_FRAMEBUFFER, mipmapfbo[1]);
        for(int level = 1, mw = tw, mh = th; max(mw, mh) > 1; level++)
        {
            int srcw = mw, srch = mh;
            if(mw > 1) mw /= 2;
            if(mh > 1) mh /= 2;
            glFramebufferTexture2D_(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target, tnum, level - 1);
            glFramebufferTexture2D_(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target, tnum, level);
            glBlitFramebuffer_(0, 0, srcw, srch, 0, 0, mw, mh, GL_COLOR_BUFFER_BIT, GL_LINEAR);
        }
        glFramebufferTexture2D_(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target, 0, 0);
        glFramebufferTexture2D_(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target, 0, 0);
        glBindFramebuffer_(GL_FRAMEBUFFER, fbo);
    }
*/
}

void uploadcompressedtexture(GLenum target, GLenum subtarget, GLenum format, int w, int h, const uchar *data, int align, int blocksize, int levels, bool mipmap, bool prealloc)
{
    int hwlimit = target==GL_TEXTURE_CUBE_MAP ? hwcubetexsize : hwtexsize,
        sizelimit = levels > 1 && maxtexsize ? min(maxtexsize, hwlimit) : hwlimit;
    int level = 0;
    if(dbgdds >= 2) conoutf(CON_DEBUG, "uploadcompressedtexture: format=%x align=%d blocksize=%d", format, align, blocksize);
    loopi(levels)
    {
        int size = ((w + align-1)/align) * ((h + align-1)/align) * blocksize;
        if(w <= sizelimit && h <= sizelimit)
        {
            if(dbgdds >= 2) conoutf(CON_DEBUG, "subimage %d: %dx%d size=%d", level+1, w, h, size);
            if(prealloc) glCompressedTexSubImage2D(subtarget, level, 0, 0, w, h, format, size, data);
            else glCompressedTexImage2D(subtarget, level, format, w, h, 0, size, data);
            level++;
            if(!mipmap) break;
        }
        if(max(w, h) <= 1) break;
        if(w > 1) w /= 2;
        if(h > 1) h /= 2;
        data += size;
    }
}

GLenum textarget(GLenum subtarget)
{
    switch(subtarget)
    {
        case GL_TEXTURE_CUBE_MAP_POSITIVE_X:
        case GL_TEXTURE_CUBE_MAP_NEGATIVE_X:
        case GL_TEXTURE_CUBE_MAP_POSITIVE_Y:
        case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y:
        case GL_TEXTURE_CUBE_MAP_POSITIVE_Z:
        case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z:
            return GL_TEXTURE_CUBE_MAP;
    }
    return subtarget;
}

const GLint *swizzlemask(GLenum format)
{
    static const GLint luminance[4] = { GL_RED, GL_RED, GL_RED, GL_ONE };
    static const GLint luminancealpha[4] = { GL_RED, GL_RED, GL_RED, GL_GREEN };
    switch(format)
    {
        case GL_RED: return luminance;
        case GL_RG: return luminancealpha;
    }
    return NULL;
}

void setuptexparameters(int tnum, const void *pixels, int clamp, int filter, GLenum format, GLenum target, bool swizzle)
{
    glBindTexture(target, tnum);
    glTexParameteri(target, GL_TEXTURE_WRAP_S, clamp&1 ? GL_CLAMP_TO_EDGE : (clamp&0x100 ? GL_MIRRORED_REPEAT : GL_REPEAT));
    glTexParameteri(target, GL_TEXTURE_WRAP_T, clamp&2 ? GL_CLAMP_TO_EDGE : (clamp&0x200 ? GL_MIRRORED_REPEAT : GL_REPEAT));
    if(target==GL_TEXTURE_3D) glTexParameteri(target, GL_TEXTURE_WRAP_R, clamp&4 ? GL_CLAMP_TO_EDGE : (clamp&0x400 ? GL_MIRRORED_REPEAT : GL_REPEAT));
    if(target==GL_TEXTURE_2D && hasAF && min(aniso, hwmaxaniso) > 0 && filter > 1) glTexParameteri(target, GL_TEXTURE_MAX_ANISOTROPY_EXT, min(aniso, hwmaxaniso));
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter && bilinear ? GL_LINEAR : GL_NEAREST);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER,
        filter > 1 ?
            (trilinear ?
                (bilinear ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR) :
                (bilinear ? GL_LINEAR_MIPMAP_NEAREST : GL_NEAREST_MIPMAP_NEAREST)) :
            (filter && bilinear ? GL_LINEAR : GL_NEAREST));
    if(swizzle && hasTRG && hasTSW)
    {
        const GLint *mask = swizzlemask(format);
        if(mask) glTexParameteriv(target, GL_TEXTURE_SWIZZLE_RGBA, mask);
    }
}

static GLenum textype(GLenum &component, GLenum &format)
{
    GLenum type = GL_UNSIGNED_BYTE;
    switch(component)
    {
        case GL_R16F:
        case GL_R32F:
            if(!format) format = GL_RED;
            type = GL_FLOAT;
            break;

        case GL_RG16F:
        case GL_RG32F:
            if(!format) format = GL_RG;
            type = GL_FLOAT;
            break;

        case GL_RGB16F:
        case GL_RGB32F:
        case GL_R11F_G11F_B10F:
            if(!format) format = GL_RGB;
            type = GL_FLOAT;
            break;

        case GL_RGBA16F:
        case GL_RGBA32F:
            if(!format) format = GL_RGBA;
            type = GL_FLOAT;
            break;

        case GL_DEPTH_COMPONENT16:
        case GL_DEPTH_COMPONENT24:
        case GL_DEPTH_COMPONENT32:
            if(!format) format = GL_DEPTH_COMPONENT;
            break;

        case GL_DEPTH_STENCIL:
        case GL_DEPTH24_STENCIL8:
            if(!format) format = GL_DEPTH_STENCIL;
            type = GL_UNSIGNED_INT_24_8;
            break;

        case GL_R8:
        case GL_R16:
        case GL_COMPRESSED_RED:
        case GL_COMPRESSED_RED_RGTC1:
            if(!format) format = GL_RED;
            break;

        case GL_RG8:
        case GL_RG16:
        case GL_COMPRESSED_RG:
        case GL_COMPRESSED_RG_RGTC2:
            if(!format) format = GL_RG;
            break;

        case GL_R3_G3_B2:
        case GL_RGB5:
        case GL_RGB565:
        case GL_RGB8:
        case GL_RGB16:
        case GL_RGB10:
        case GL_COMPRESSED_RGB:
        case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
            if(!format) format = GL_RGB;
            break;

        case GL_RGBA4:
        case GL_RGB5_A1:
        case GL_RGBA8:
        case GL_RGBA16:
        case GL_RGB10_A2:
        case GL_COMPRESSED_RGBA:
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
            if(!format) format = GL_RGBA;
            break;

        case GL_LUMINANCE8:
        case GL_LUMINANCE16:
        case GL_COMPRESSED_LUMINANCE:
        case GL_COMPRESSED_LUMINANCE_LATC1_EXT:
            if(!format) format = GL_LUMINANCE;
            break;

        case GL_LUMINANCE8_ALPHA8:
        case GL_LUMINANCE16_ALPHA16:
        case GL_COMPRESSED_LUMINANCE_ALPHA:
        case GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT:
            if(!format) format = GL_LUMINANCE_ALPHA;
            break;

        case GL_ALPHA8:
        case GL_ALPHA16:
        case GL_COMPRESSED_ALPHA:
            if(!format) format = GL_ALPHA;
            break;

        case GL_RGB8UI:
        case GL_RGB16UI:
        case GL_RGB32UI:
        case GL_RGB8I:
        case GL_RGB16I:
        case GL_RGB32I:
            if(!format) format = GL_RGB_INTEGER;
            break;

        case GL_RGBA8UI:
        case GL_RGBA16UI:
        case GL_RGBA32UI:
        case GL_RGBA8I:
        case GL_RGBA16I:
        case GL_RGBA32I:
            if(!format) format = GL_RGBA_INTEGER;
            break;

        case GL_R8UI:
        case GL_R16UI:
        case GL_R32UI:
        case GL_R8I:
        case GL_R16I:
        case GL_R32I:
            if(!format) format = GL_RED_INTEGER;
            break;

        case GL_RG8UI:
        case GL_RG16UI:
        case GL_RG32UI:
        case GL_RG8I:
        case GL_RG16I:
        case GL_RG32I:
            if(!format) format = GL_RG_INTEGER;
            break;
    }
    if(!format) format = component;
    return type;
}

static int miplevels(int n)
{
    int levels = 1;
    for(; n > 1; n /= 2) levels++;
    return levels;
}

// Turn off filtering when there is no mipchain.
void createtexture(int tnum, int w, int h, const void *pixels, int clamp, int filter, GLenum component, GLenum subtarget, int pw, int ph, int pitch, bool resize, GLenum format, bool swizzle)
{
    GLenum target = textarget(subtarget), type = textype(component, format);
    if(!pw) pw = w;
    if(!ph) ph = h;
    int tw = w, th = h;
    bool mipmap = filter > 1;
    if(resize && pixels)
    {
        resizetexture(w, h, mipmap, false, target, 0, tw, th);
        if(mipmap) component = compressedformat(component, tw, th);
    }
    bool prealloc = !resize && hasTS && hasTRG && hasTSW && !uncompressedformat(component);
    if(filter >= 0 && clamp >= 0)
    {
        int levels = mipmap ? miplevels(max(tw, th)) : 1;
        setuptexparameters(tnum, pixels, clamp, levels == 1 ? min(1, filter) : filter, format, target, swizzle);
        if(prealloc)
        {
            glTexStorage2D(target, levels, sizedformat(component), tw, th);
            if(dbgdds >= 2) conoutf(CON_DEBUG, "createtexture: prealloc texture storage with miplevels: %d", levels);
        }
    }
    uploadtexture(tnum, subtarget, component, tw, th, format, type, pixels, pw, ph, pitch, mipmap, prealloc);
}

// Turn off filtering when there is no mipchain.
void createcompressedtexture(int tnum, int w, int h, const uchar *data, int align, int blocksize, int levels, int clamp, int filter, GLenum format, GLenum subtarget, bool swizzle = false)
{
    GLenum target = textarget(subtarget);
    bool mipmap = filter > 1;
    bool prealloc = hasTS && hasTRG && hasTSW;
    if(filter >= 0 && clamp >= 0)
    {
        levels = mipmap ? min(miplevels(max(w, h)), levels) : 1;
        setuptexparameters(tnum, data, clamp, levels == 1 ? min(1, filter) : filter, format, target, swizzle);
        if(prealloc)
        {
            glTexStorage2D(target, levels, format, w, h);
            if(dbgdds >= 2) conoutf(CON_DEBUG, "createcompressedtexture: prealloc texture storage with miplevels: %d", levels);
        }
    }
    uploadcompressedtexture(target, subtarget, format, w, h, data, align, blocksize, levels, mipmap, prealloc);
}

void create3dtexture(int tnum, int w, int h, int d, const void *pixels, int clamp, int filter, GLenum component, GLenum target, bool swizzle)
{
    GLenum format = GL_FALSE, type = textype(component, format);
    if(filter >= 0 && clamp >= 0) setuptexparameters(tnum, pixels, clamp, filter, format, target, swizzle);
    glTexImage3D(target, 0, component, w, h, d, 0, format, type, pixels);
}

#ifdef USE_TEXTURE_MAN
hashnameset<Texture> textures;

Texture *notexture = NULL; // used as default, ensured to be loaded

static GLenum texformat(int bpp, bool swizzle = false)
{
    switch(bpp)
    {
        case 1: return hasTRG && (hasTSW || !glcompat || !swizzle) ? GL_RED : GL_LUMINANCE;
        case 2: return hasTRG && (hasTSW || !glcompat || !swizzle) ? GL_RG : GL_LUMINANCE_ALPHA;
        case 3: return GL_RGB;
        case 4: return GL_RGBA;
        default: return 0;
    }
}

static Texture *newtexture(Texture *t, const char *rname, ImageData &s, int clamp = 0, bool mipit = true, bool canreduce = false, bool transient = false, int compress = 0)
{
    if(!t)
    {
        char *key = newstring(rname);
        t = &textures[key];
        t->name = key;
    }

    t->clamp = clamp;
    t->mipmap = mipit;
    t->type = Texture::IMAGE;
    if(transient) t->type |= Texture::TRANSIENT;
    if(clamp&0x300) t->type |= Texture::MIRROR;
    if(!s.data)
    {
        t->type |= Texture::STUB;
        t->w = t->h = t->xs = t->ys = t->bpp = 0;
        return t;
    }

    bool swizzle = !(clamp&0x10000);
    GLenum format;
    if(s.compressed)
    {
        format = uncompressedformat(s.compressed);
        t->bpp = formatsize(format);
        t->type |= Texture::COMPRESSED;
    }
    else
    {
        format = texformat(s.bpp, swizzle);
        t->bpp = s.bpp;
        if(swizzle && hasTRG && !hasTSW && swizzlemask(format))
        {
            swizzleimage(s);
            format = texformat(s.bpp, swizzle);
            t->bpp = s.bpp;
        }
    }
    if(alphaformat(format)) t->type |= Texture::ALPHA;
    t->w = t->xs = s.w;
    t->h = t->ys = s.h;

    int filter = !canreduce || reducefilter ? (mipit ? 2 : 1) : 0;
    glGenTextures(1, &t->id);
    if(s.compressed)
    {
        uchar *data = s.data;
        int levels = s.levels, level = 0;
        if(canreduce && texreduce) loopi(min(texreduce, s.levels-1))
        {
            data += s.calclevelsize(level++);
            levels--;
            if(t->w > 1) t->w /= 2;
            if(t->h > 1) t->h /= 2;
        }
        int sizelimit = mipit && maxtexsize ? min(maxtexsize, hwtexsize) : hwtexsize;
        while(t->w > sizelimit || t->h > sizelimit)
        {
            data += s.calclevelsize(level++);
            levels--;
            if(t->w > 1) t->w /= 2;
            if(t->h > 1) t->h /= 2;
        }
        createcompressedtexture(t->id, t->w, t->h, data, s.align, s.bpp, levels, clamp, filter, s.compressed, GL_TEXTURE_2D, swizzle);
    }
    else
    {
        resizetexture(t->w, t->h, mipit, canreduce, GL_TEXTURE_2D, compress, t->w, t->h);
        GLenum component = compressedformat(format, t->w, t->h, compress);
        createtexture(t->id, t->w, t->h, s.data, clamp, filter, component, GL_TEXTURE_2D, t->xs, t->ys, s.pitch, false, format, swizzle);
    }
    return t;
}

bool canloadsurface(const char *name)
{
    stream *f = openfile(name, "rb");
    if(!f) return false;
    delete f;
    return true;
}

SDL_Surface *loadsurface(const char *name)
{
    SDL_Surface *s = NULL;
    stream *z = openzipfile(name, "rb");
    if(z)
    {
        SDL_RWops *rw = z->rwops();
        if(rw)
        {
            const char *ext = strrchr(name, '.');
            if(ext) ++ext; 
            s = IMG_LoadTyped_RW(rw, 0, ext);
            SDL_FreeRW(rw);
        }
        delete z;
    }
    if(!s) s = IMG_Load(findfile(name, "rb"));
    return fixsurfaceformat(s);
}

static vec parsevec(const char *arg)
{
    vec v(0, 0, 0);
    int i = 0;
    for(; arg[0] && (!i || arg[0]=='/') && i<3; arg += strcspn(arg, "/,><"), i++)
    {
        if(i) arg++;
        v[i] = atof(arg);
    }
    if(i==1) v.y = v.z = v.x;
    return v;
}
#endif

VAR(usedds, 0, 1, 1);
VAR(scaledds, 0, 2, 4);

#ifdef USE_TEXTURE_MAN
static bool texturedata(ImageData &d, const char *tname, bool msg = true, int *compress = NULL, int *wrap = NULL, const char *tdir = NULL, int ttype = TEX_DIFFUSE)
{
    const char *cmds = NULL, *file = tname;
    if(tname[0]=='<')
    {
        cmds = tname;
        file = strrchr(tname, '>');
        if(!file) { if(msg) conoutf(CON_ERROR, "could not load texture %s", tname); return false; }
        file++;
    }
    string pname;
    if(tdir)
    {
        formatstring(pname, "%s/%s", tdir, file);
        file = path(pname);
    }

    int flen = strlen(file);
    bool raw = !usedds || !compress, dds = false, guess = false;
    for(const char *pcmds = cmds; pcmds;)
    {
        #define PARSETEXCOMMANDS(cmds) \
            const char *cmd = NULL, *end = NULL, *arg[4] = { NULL, NULL, NULL, NULL }; \
            cmd = &cmds[1]; \
            end = strchr(cmd, '>'); \
            if(!end) break; \
            cmds = strchr(cmd, '<'); \
            size_t len = strcspn(cmd, ":,><"); \
            loopi(4) \
            { \
                arg[i] = strchr(i ? arg[i-1] : cmd, i ? ',' : ':'); \
                if(!arg[i] || arg[i] >= end) arg[i] = ""; \
                else arg[i]++; \
            }
        #define COPYTEXARG(dst, src) copystring(dst, stringslice(src, strcspn(src, ":,><")))
        PARSETEXCOMMANDS(pcmds);
        if(matchstring(cmd, len, "dds")) dds = true;
        else if(matchstring(cmd, len, "thumbnail"))
        {
            raw = true;
            guess = flen >= 4 && !strchr(file+flen-4, '.');
        }
        else if(matchstring(cmd, len, "stub")) return canloadsurface(file);
    }

    if(msg) renderprogress(loadprogress, file);

    if(flen >= 4 && (!strcasecmp(file + flen - 4, ".dds") || (dds && !raw)))
    {
        string dfile;
        copystring(dfile, file);
        memcpy(dfile + flen - 4, ".dds", 4);
        if(!loaddds(dfile, d, raw ? 1 : (dds ? 0 : -1)) && (!dds || raw))
        {
            if(msg) conoutf(CON_ERROR, "could not load texture %s", dfile);
            return false;
        }
        if(d.data && !d.compressed && !dds && compress) *compress = scaledds;
    }

    if(!d.data)
    {
        SDL_Surface *s = NULL;
        if(guess)
        {
            static const char *exts[] = {".jpg", ".png"};
            string ext;
            loopi(sizeof(exts)/sizeof(exts[0]))
            {
                copystring(ext, file);
                concatstring(ext, exts[i]);
                s = loadsurface(ext);
                if(s) break;
            }
        }
        else s = loadsurface(file);
        if(!s) { if(msg) conoutf(CON_ERROR, "could not load texture %s", file); return false; }
        int bpp = s->format->BitsPerPixel;
        if(bpp%8 || !texformat(bpp/8)) { SDL_FreeSurface(s); conoutf(CON_ERROR, "texture must be 8, 16, 24, or 32 bpp: %s", file); return false; }
        if(max(s->w, s->h) > (1<<12)) { SDL_FreeSurface(s); conoutf(CON_ERROR, "texture size exceeded %dx%d pixels: %s", 1<<12, 1<<12, file); return false; }
        d.wrap(s);
    }

    while(cmds)
    {
        PARSETEXCOMMANDS(cmds);
        if(d.compressed) goto compressed;
        if(matchstring(cmd, len, "mad")) texmad(d, parsevec(arg[0]), parsevec(arg[1]));
        else if(matchstring(cmd, len, "colorify")) texcolorify(d, parsevec(arg[0]), parsevec(arg[1]));
        else if(matchstring(cmd, len, "colormask")) texcolormask(d, parsevec(arg[0]), *arg[1] ? parsevec(arg[1]) : vec(1, 1, 1));
        else if(matchstring(cmd, len, "normal"))
        {
            int emphasis = atoi(arg[0]);
            texnormal(d, emphasis > 0 ? emphasis : 3);
        }
        else if(matchstring(cmd, len, "dup")) texdup(d, atoi(arg[0]), atoi(arg[1]));
        else if(matchstring(cmd, len, "offset")) texoffset(d, atoi(arg[0]), atoi(arg[1]));
        else if(matchstring(cmd, len, "rotate")) texrotate(d, atoi(arg[0]), ttype);
        else if(matchstring(cmd, len, "reorient")) texreorient(d, atoi(arg[0])>0, atoi(arg[1])>0, atoi(arg[2])>0, ttype);
        else if(matchstring(cmd, len, "crop")) texcrop(d, atoi(arg[0]), atoi(arg[1]), *arg[2] ? atoi(arg[2]) : -1, *arg[3] ? atoi(arg[3]) : -1);
        else if(matchstring(cmd, len, "mix")) texmix(d, *arg[0] ? atoi(arg[0]) : -1, *arg[1] ? atoi(arg[1]) : -1, *arg[2] ? atoi(arg[2]) : -1, *arg[3] ? atoi(arg[3]) : -1);
        else if(matchstring(cmd, len, "grey")) texgrey(d);
        else if(matchstring(cmd, len, "blur"))
        {
            int emphasis = atoi(arg[0]), repeat = atoi(arg[1]);
            texblur(d, emphasis > 0 ? clamp(emphasis, 1, 2) : 1, repeat > 0 ? repeat : 1);
        }
        else if(matchstring(cmd, len, "premul")) texpremul(d);
        else if(matchstring(cmd, len, "agrad")) texagrad(d, atof(arg[0]), atof(arg[1]), atof(arg[2]), atof(arg[3]));
        else if(matchstring(cmd, len, "blend"))
        {
            ImageData src, mask;
            string srcname, maskname;
            COPYTEXARG(srcname, arg[0]);
            COPYTEXARG(maskname, arg[1]);
            if(srcname[0] && texturedata(src, srcname, false, NULL, NULL, tdir, ttype) && (!maskname[0] || texturedata(mask, maskname, false, NULL, NULL, tdir, ttype)))
                texblend(d, src, maskname[0] ? mask : src);
        }
        else if(matchstring(cmd, len, "thumbnail"))
        {
            int w = atoi(arg[0]), h = atoi(arg[1]);
            if(w <= 0 || w > (1<<12)) w = 64;
            if(h <= 0 || h > (1<<12)) h = w;
            if(d.w > w || d.h > h) scaleimage(d, w, h);
        }
        else if(matchstring(cmd, len, "compress") || matchstring(cmd, len, "dds"))
        {
            int scale = atoi(arg[0]);
            if(scale <= 0) scale = scaledds;
            if(compress) *compress = scale;
        }
        else if(matchstring(cmd, len, "nocompress"))
        {
            if(compress) *compress = -1;
        }
        else
    compressed:
        if(matchstring(cmd, len, "mirror"))
        {
            if(wrap) *wrap |= 0x300;
        }
        else if(matchstring(cmd, len, "noswizzle"))
        {
            if(wrap) *wrap |= 0x10000;
        }
    }

    return true;
}

static inline bool texturedata(ImageData &d, Slot &slot, Slot::Tex &tex, bool msg = true, int *compress = NULL, int *wrap = NULL)
{
    return texturedata(d, tex.name, msg, compress, wrap, slot.texturedir(), tex.type);
}

uchar *loadalphamask(Texture *t)
{
    if(t->alphamask) return t->alphamask;
    if(!(t->type&Texture::ALPHA)) return NULL;
    ImageData s;
    if(!texturedata(s, t->name, false) || !s.data || s.compressed) return NULL;
    t->alphamask = new uchar[s.h * ((s.w+7)/8)];
    uchar *srcrow = s.data, *dst = t->alphamask-1;
    loop(y, s.h)
    {
        uchar *src = srcrow+s.bpp-1;
        loop(x, s.w)
        {
            int offset = x%8;
            if(!offset) *++dst = 0;
            if(*src) *dst |= 1<<offset;
            src += s.bpp;
        }
        srcrow += s.pitch;
    }
    return t->alphamask;
}

Texture *textureload(const char *name, int clamp, bool mipit, bool msg)
{
    string tname;
    copystring(tname, name);
    Texture *t = textures.access(path(tname));
    if(t) return t;
    int compress = 0;
    ImageData s;
    if(texturedata(s, tname, msg, &compress, &clamp)) return newtexture(NULL, tname, s, clamp, mipit, false, false, compress);
    return notexture;
}

bool settexture(const char *name, int clamp)
{
    Texture *t = textureload(name, clamp, true, false);
    glBindTexture(GL_TEXTURE_2D, t->id);
    return t != notexture;
}

vector<VSlot *> vslots;
vector<Slot *> slots;
MatSlot materialslots[(MATF_VOLUME|MATF_INDEX)+1];
Slot dummyslot;
VSlot dummyvslot(&dummyslot);
vector<DecalSlot *> decalslots;
DecalSlot dummydecalslot;
Slot *defslot = NULL;

const char *Slot::name() const { return tempformatstring("slot %d", index); }

MatSlot::MatSlot() : Slot(int(this - materialslots)), VSlot(this) {}
const char *MatSlot::name() const { return tempformatstring("material slot %s", findmaterialname(Slot::index)); }

const char *DecalSlot::name() const { return tempformatstring("decal slot %d", Slot::index); }

void texturereset(int *n)
{
    if(!(identflags&IDF_OVERRIDDEN) && !game::allowedittoggle()) return;
    defslot = NULL;
    resetslotshader();
    int limit = clamp(*n, 0, slots.length());
    for(int i = limit; i < slots.length(); i++)
    {
        Slot *s = slots[i];
        for(VSlot *vs = s->variants; vs; vs = vs->next) vs->slot = &dummyslot;
        delete s;
    }
    slots.setsize(limit);
    while(vslots.length())
    {
        VSlot *vs = vslots.last();
        if(vs->slot != &dummyslot || vs->changed) break;
        delete vslots.pop();
    }
}

COMMAND(texturereset, "i");

void materialreset()
{
    if(!(identflags&IDF_OVERRIDDEN) && !game::allowedittoggle()) return;
    defslot = NULL;
    loopi((MATF_VOLUME|MATF_INDEX)+1) materialslots[i].reset();
}

COMMAND(materialreset, "");

void decalreset(int *n)
{
    if(!(identflags&IDF_OVERRIDDEN) && !game::allowedittoggle()) return;
    defslot = NULL;
    resetslotshader();
    decalslots.deletecontents(*n);
}

COMMAND(decalreset, "i");

static int compactedvslots = 0, compactvslotsprogress = 0, clonedvslots = 0;
static bool markingvslots = false;

void clearslots()
{
    defslot = NULL;
    resetslotshader();
    slots.deletecontents();
    vslots.deletecontents();
    loopi((MATF_VOLUME|MATF_INDEX)+1) materialslots[i].reset();
    decalslots.deletecontents();
    clonedvslots = 0;
}

static void assignvslot(VSlot &vs);

static inline void assignvslotlayer(VSlot &vs)
{
    if(vs.layer && vslots.inrange(vs.layer) && vslots[vs.layer]->index < 0) assignvslot(*vslots[vs.layer]);
    if(vs.detail && vslots.inrange(vs.detail) && vslots[vs.detail]->index < 0) assignvslot(*vslots[vs.detail]);
}

static void assignvslot(VSlot &vs)
{
    vs.index = compactedvslots++;
    assignvslotlayer(vs);
}

void compactvslot(int &index)
{
    if(vslots.inrange(index))
    {
        VSlot &vs = *vslots[index];
        if(vs.index < 0) assignvslot(vs);
        if(!markingvslots) index = vs.index;
    }
}

void compactvslot(VSlot &vs)
{
    if(vs.index < 0) assignvslot(vs);
}

void compactvslots(cube *c, int n)
{
    if((compactvslotsprogress++&0xFFF)==0) renderprogress(min(float(compactvslotsprogress)/allocnodes, 1.0f), markingvslots ? "marking slots..." : "compacting slots...");
    loopi(n)
    {
        if(c[i].children) compactvslots(c[i].children);
        else loopj(6) if(vslots.inrange(c[i].texture[j]))
        {
            VSlot &vs = *vslots[c[i].texture[j]];
            if(vs.index < 0) assignvslot(vs);
            if(!markingvslots) c[i].texture[j] = vs.index;
        }
    }
}

int compactvslots(bool cull)
{
    defslot = NULL;
    clonedvslots = 0;
    markingvslots = cull;
    compactedvslots = 0;
    compactvslotsprogress = 0;
    loopv(vslots) vslots[i]->index = -1;
    if(cull)
    {
        int numdefaults = min(int(NUMDEFAULTSLOTS), slots.length());
        loopi(numdefaults) slots[i]->variants->index = compactedvslots++;
        loopi(numdefaults) assignvslotlayer(*slots[i]->variants);
    }
    else
    {
        loopv(slots) slots[i]->variants->index = compactedvslots++;
        loopv(slots) assignvslotlayer(*slots[i]->variants);
        loopv(vslots)
        {
            VSlot &vs = *vslots[i];
            if(!vs.changed && vs.index < 0) { markingvslots = true; break; }
        }
    }
    compactvslots(worldroot);
    int total = compactedvslots;
    compacteditvslots();
    loopv(vslots)
    {
        VSlot *vs = vslots[i];
        if(vs->changed) continue;
        while(vs->next)
        {
            if(vs->next->index < 0) vs->next = vs->next->next;
            else vs = vs->next;
        }
    }
    if(markingvslots)
    {
        markingvslots = false;
        compactedvslots = 0;
        compactvslotsprogress = 0;
        int lastdiscard = 0;
        loopv(vslots)
        {
            VSlot &vs = *vslots[i];
            if(vs.changed || (vs.index < 0 && !vs.next)) vs.index = -1;
            else
            {
                if(!cull) while(lastdiscard < i)
                {
                    VSlot &ds = *vslots[lastdiscard++];
                    if(!ds.changed && ds.index < 0) ds.index = compactedvslots++;
                }
                vs.index = compactedvslots++;
            }
        }
        compactvslots(worldroot);
        total = compactedvslots;
        compacteditvslots();
    }
    compactmruvslots();
    loopv(vslots)
    {
        VSlot &vs = *vslots[i];
        if(vs.index >= 0)
        {
            if(vs.layer && vslots.inrange(vs.layer)) vs.layer = vslots[vs.layer]->index;
            if(vs.detail && vslots.inrange(vs.detail)) vs.detail = vslots[vs.detail]->index;
        }
    }
    if(cull)
    {
        loopvrev(slots) if(slots[i]->variants->index < 0) delete slots.remove(i);
        loopv(slots) slots[i]->index = i;
    }
    loopv(vslots)
    {
        while(vslots[i]->index >= 0 && vslots[i]->index != i)
            swap(vslots[i], vslots[vslots[i]->index]);
    }
    for(int i = compactedvslots; i < vslots.length(); i++) delete vslots[i];
    vslots.setsize(compactedvslots);
    return total;
}

ICOMMAND(compactvslots, "i", (int *cull),
{
    extern int nompedit;
    if(nompedit && multiplayer()) return;
    compactvslots(*cull!=0);
    allchanged();
});

static void clampvslotoffset(VSlot &dst, Slot *slot = NULL)
{
    if(!slot) slot = dst.slot;
    if(slot && slot->sts.inrange(0))
    {
        if(!slot->loaded) slot->load();
        Texture *t = slot->sts[0].t;
        int xs = t->xs, ys = t->ys;
        if(t->type & Texture::MIRROR) { xs *= 2; ys *= 2; }
        if(texrotations[dst.rotation].swapxy) swap(xs, ys);
        dst.offset.x %= xs; if(dst.offset.x < 0) dst.offset.x += xs;
        dst.offset.y %= ys; if(dst.offset.y < 0) dst.offset.y += ys;
    }
    else dst.offset.max(0);
}

static void propagatevslot(VSlot &dst, const VSlot &src, int diff, bool edit = false)
{
    if(diff & (1<<VSLOT_SHPARAM)) loopv(src.params) dst.params.add(src.params[i]);
    if(diff & (1<<VSLOT_SCALE)) dst.scale = src.scale;
    if(diff & (1<<VSLOT_ROTATION))
    {
        dst.rotation = src.rotation;
        if(edit && !dst.offset.iszero()) clampvslotoffset(dst);
    }
    if(diff & (1<<VSLOT_OFFSET))
    {
        dst.offset = src.offset;
        if(edit) clampvslotoffset(dst);
    }
    if(diff & (1<<VSLOT_SCROLL)) dst.scroll = src.scroll;
    if(diff & (1<<VSLOT_LAYER)) dst.layer = src.layer;
    if(diff & (1<<VSLOT_ALPHA))
    {
        dst.alphafront = src.alphafront;
        dst.alphaback = src.alphaback;
    }
    if(diff & (1<<VSLOT_COLOR)) dst.colorscale = src.colorscale;
    if(diff & (1<<VSLOT_REFRACT))
    {
        dst.refractscale = src.refractscale;
        dst.refractcolor = src.refractcolor;
    }
    if(diff & (1<<VSLOT_DETAIL)) dst.detail = src.detail;
}

static void propagatevslot(VSlot *root, int changed)
{
    for(VSlot *vs = root->next; vs; vs = vs->next)
    {
        int diff = changed & ~vs->changed;
        if(diff) propagatevslot(*vs, *root, diff);
    }
}

static void mergevslot(VSlot &dst, const VSlot &src, int diff, Slot *slot = NULL)
{
    if(diff & (1<<VSLOT_SHPARAM)) loopv(src.params)
    {
        const SlotShaderParam &sp = src.params[i];
        loopvj(dst.params)
        {
            SlotShaderParam &dp = dst.params[j];
            if(sp.name == dp.name)
            {
                memcpy(dp.val, sp.val, sizeof(dp.val));
                goto nextparam;
            }
        }
        dst.params.add(sp);
    nextparam:;
    }
    if(diff & (1<<VSLOT_SCALE))
    {
        dst.scale = clamp(dst.scale*src.scale, 1/8.0f, 8.0f);
    }
    if(diff & (1<<VSLOT_ROTATION))
    {
        dst.rotation = clamp(dst.rotation + src.rotation, 0, 7);
        if(!dst.offset.iszero()) clampvslotoffset(dst, slot);
    }
    if(diff & (1<<VSLOT_OFFSET))
    {
        dst.offset.add(src.offset);
        clampvslotoffset(dst, slot);
    }
    if(diff & (1<<VSLOT_SCROLL)) dst.scroll.add(src.scroll);
    if(diff & (1<<VSLOT_LAYER)) dst.layer = src.layer;
    if(diff & (1<<VSLOT_ALPHA))
    {
        dst.alphafront = src.alphafront;
        dst.alphaback = src.alphaback;
    }
    if(diff & (1<<VSLOT_COLOR)) dst.colorscale.mul(src.colorscale);
    if(diff & (1<<VSLOT_REFRACT))
    {
        dst.refractscale *= src.refractscale;
        dst.refractcolor.mul(src.refractcolor);
    }
    if(diff & (1<<VSLOT_DETAIL)) dst.detail = src.detail;
}

void mergevslot(VSlot &dst, const VSlot &src, const VSlot &delta)
{
    dst.changed = src.changed | delta.changed;
    propagatevslot(dst, src, (1<<VSLOT_NUM)-1);
    mergevslot(dst, delta, delta.changed, src.slot);
}

static VSlot *reassignvslot(Slot &owner, VSlot *vs)
{
    vs->reset();
    owner.variants = vs;
    while(vs)
    {
        vs->slot = &owner;
        vs->linked = false;
        vs = vs->next;
    }
    return owner.variants;
}

static VSlot *emptyvslot(Slot &owner)
{
    int offset = 0;
    loopvrev(slots) if(slots[i]->variants) { offset = slots[i]->variants->index + 1; break; }
    for(int i = offset; i < vslots.length(); i++) if(!vslots[i]->changed) return reassignvslot(owner, vslots[i]);
    return vslots.add(new VSlot(&owner, vslots.length()));
}

VSlot &Slot::emptyvslot()
{
    return *::emptyvslot(*this);
}

static bool comparevslot(const VSlot &dst, const VSlot &src, int diff)
{
    if(diff & (1<<VSLOT_SHPARAM))
    {
        if(src.params.length() != dst.params.length()) return false;
        loopv(src.params)
        {
            const SlotShaderParam &sp = src.params[i], &dp = dst.params[i];
            if(sp.name != dp.name || memcmp(sp.val, dp.val, sizeof(sp.val))) return false;
        }
    }
    if(diff & (1<<VSLOT_SCALE) && dst.scale != src.scale) return false;
    if(diff & (1<<VSLOT_ROTATION) && dst.rotation != src.rotation) return false;
    if(diff & (1<<VSLOT_OFFSET) && dst.offset != src.offset) return false;
    if(diff & (1<<VSLOT_SCROLL) && dst.scroll != src.scroll) return false;
    if(diff & (1<<VSLOT_LAYER) && dst.layer != src.layer) return false;
    if(diff & (1<<VSLOT_ALPHA) && (dst.alphafront != src.alphafront || dst.alphaback != src.alphaback)) return false;
    if(diff & (1<<VSLOT_COLOR) && dst.colorscale != src.colorscale) return false;
    if(diff & (1<<VSLOT_REFRACT) && (dst.refractscale != src.refractscale || dst.refractcolor != src.refractcolor)) return false;
    if(diff & (1<<VSLOT_DETAIL) && dst.detail != src.detail) return false;
    return true;
}

void packvslot(vector<uchar> &buf, const VSlot &src)
{
    if(src.changed & (1<<VSLOT_SHPARAM))
    {
        loopv(src.params)
        {
            const SlotShaderParam &p = src.params[i];
            buf.put(VSLOT_SHPARAM);
            sendstring(p.name, buf);
            loopj(4) putfloat(buf, p.val[j]);
        }
    }
    if(src.changed & (1<<VSLOT_SCALE))
    {
        buf.put(VSLOT_SCALE);
        putfloat(buf, src.scale);
    }
    if(src.changed & (1<<VSLOT_ROTATION))
    {
        buf.put(VSLOT_ROTATION);
        putint(buf, src.rotation);
    }
    if(src.changed & (1<<VSLOT_OFFSET))
    {
        buf.put(VSLOT_OFFSET);
        putint(buf, src.offset.x);
        putint(buf, src.offset.y);
    }
    if(src.changed & (1<<VSLOT_SCROLL))
    {
        buf.put(VSLOT_SCROLL);
        putfloat(buf, src.scroll.x);
        putfloat(buf, src.scroll.y);
    }
    if(src.changed & (1<<VSLOT_LAYER))
    {
        buf.put(VSLOT_LAYER);
        putuint(buf, vslots.inrange(src.layer) && !vslots[src.layer]->changed ? src.layer : 0);
    }
    if(src.changed & (1<<VSLOT_ALPHA))
    {
        buf.put(VSLOT_ALPHA);
        putfloat(buf, src.alphafront);
        putfloat(buf, src.alphaback);
    }
    if(src.changed & (1<<VSLOT_COLOR))
    {
        buf.put(VSLOT_COLOR);
        putfloat(buf, src.colorscale.r);
        putfloat(buf, src.colorscale.g);
        putfloat(buf, src.colorscale.b);
    }
    if(src.changed & (1<<VSLOT_REFRACT))
    {
        buf.put(VSLOT_REFRACT);
        putfloat(buf, src.refractscale);
        putfloat(buf, src.refractcolor.r);
        putfloat(buf, src.refractcolor.g);
        putfloat(buf, src.refractcolor.b);
    }
    if(src.changed & (1<<VSLOT_DETAIL))
    {
        buf.put(VSLOT_DETAIL);
        putuint(buf, vslots.inrange(src.detail) && !vslots[src.detail]->changed ? src.detail : 0);
    }
    buf.put(0xFF);
}

void packvslot(vector<uchar> &buf, int index)
{
    if(vslots.inrange(index)) packvslot(buf, *vslots[index]);
    else buf.put(0xFF);
}

void packvslot(vector<uchar> &buf, const VSlot *vs)
{
    if(vs) packvslot(buf, *vs);
    else buf.put(0xFF);
}

bool unpackvslot(ucharbuf &buf, VSlot &dst, bool delta)
{
    while(buf.remaining())
    {
        int changed = buf.get();
        if(changed >= 0x80) break;
        switch(changed)
        {
            case VSLOT_SHPARAM:
            {
                string name;
                getstring(name, buf);
                SlotShaderParam p = { name[0] ? getshaderparamname(name) : NULL, -1, 0, { 0, 0, 0, 0 } };
                loopi(4) p.val[i] = getfloat(buf);
                if(p.name) dst.params.add(p);
                break;
            }
            case VSLOT_SCALE:
                dst.scale = getfloat(buf);
                if(dst.scale <= 0) dst.scale = 1;
                else if(!delta) dst.scale = clamp(dst.scale, 1/8.0f, 8.0f);
                break;
            case VSLOT_ROTATION:
                dst.rotation = getint(buf);
                if(!delta) dst.rotation = clamp(dst.rotation, 0, 7);
                break;
            case VSLOT_OFFSET:
                dst.offset.x = getint(buf);
                dst.offset.y = getint(buf);
                if(!delta) dst.offset.max(0);
                break;
            case VSLOT_SCROLL:
                dst.scroll.x = getfloat(buf);
                dst.scroll.y = getfloat(buf);
                break;
            case VSLOT_LAYER:
            {
                int tex = getuint(buf);
                dst.layer = vslots.inrange(tex) ? tex : 0;
                break;
            }
            case VSLOT_ALPHA:
                dst.alphafront = clamp(getfloat(buf), 0.0f, 1.0f);
                dst.alphaback = clamp(getfloat(buf), 0.0f, 1.0f);
                break;
            case VSLOT_COLOR:
                dst.colorscale.r = clamp(getfloat(buf), 0.0f, 2.0f);
                dst.colorscale.g = clamp(getfloat(buf), 0.0f, 2.0f);
                dst.colorscale.b = clamp(getfloat(buf), 0.0f, 2.0f);
                break;
            case VSLOT_REFRACT:
                dst.refractscale = clamp(getfloat(buf), 0.0f, 1.0f);
                dst.refractcolor.r = clamp(getfloat(buf), 0.0f, 1.0f);
                dst.refractcolor.g = clamp(getfloat(buf), 0.0f, 1.0f);
                dst.refractcolor.b = clamp(getfloat(buf), 0.0f, 1.0f);
                break;
            case VSLOT_DETAIL:
            {
                int tex = getuint(buf);
                dst.detail = vslots.inrange(tex) ? tex : 0;
                break;
            }
            default:
                return false;
        } 
        dst.changed |= 1<<changed;
    }
    if(buf.overread()) return false;
    return true; 
}
 
VSlot *findvslot(Slot &slot, const VSlot &src, const VSlot &delta)
{
    for(VSlot *dst = slot.variants; dst; dst = dst->next)
    {
        if((!dst->changed || dst->changed == (src.changed | delta.changed)) &&
           comparevslot(*dst, src, src.changed & ~delta.changed) &&
           comparevslot(*dst, delta, delta.changed))
            return dst;
    }
    return NULL;
}

static VSlot *clonevslot(const VSlot &src, const VSlot &delta)
{
    VSlot *dst = vslots.add(new VSlot(src.slot, vslots.length()));
    dst->changed = src.changed | delta.changed;
    propagatevslot(*dst, src, ((1<<VSLOT_NUM)-1) & ~delta.changed);
    propagatevslot(*dst, delta, delta.changed, true);
    return dst;
}

VARP(autocompactvslots, 0, 256, 0x10000);

VSlot *editvslot(const VSlot &src, const VSlot &delta)
{
    VSlot *exists = findvslot(*src.slot, src, delta);
    if(exists) return exists;
    if(vslots.length()>=0x10000)
    {
        compactvslots();
        allchanged();
        if(vslots.length()>=0x10000) return NULL;
    }
    if(autocompactvslots && ++clonedvslots >= autocompactvslots)
    {
        compactvslots();
        allchanged();
    }
    return clonevslot(src, delta);
}

static void fixinsidefaces(cube *c, const ivec &o, int size, int tex)
{
    loopi(8)
    {
        ivec co(i, o, size);
        if(c[i].children) fixinsidefaces(c[i].children, co, size>>1, tex);
        else loopj(6) if(!visibletris(c[i], j, co, size))
            c[i].texture[j] = tex;
    }
}

ICOMMAND(fixinsidefaces, "i", (int *tex),
{
    extern int nompedit;
    if(noedit(true) || (nompedit && multiplayer())) return;
    fixinsidefaces(worldroot, ivec(0, 0, 0), worldsize>>1, *tex && vslots.inrange(*tex) ? *tex : DEFAULT_GEOM);
    allchanged();
});

const struct slottex
{
    const char *name;
    int id;
} slottexs[] =
{
    {"0", TEX_DIFFUSE},
    {"1", TEX_UNKNOWN},

    {"c", TEX_DIFFUSE},
    {"u", TEX_UNKNOWN},
    {"n", TEX_NORMAL},
    {"g", TEX_GLOW},
    {"s", TEX_SPEC},
    {"z", TEX_DEPTH},
    {"a", TEX_ALPHA},
    {"e", TEX_ENVMAP}
};

int findslottex(const char *name)
{
    loopi(sizeof(slottexs)/sizeof(slottex))
    {
        if(!strcmp(slottexs[i].name, name)) return slottexs[i].id;
    }
    return -1;
}

void texture(char *type, char *name, int *rot, int *xoffset, int *yoffset, float *scale)
{
    int tnum = findslottex(type), matslot;
    if(tnum==TEX_DIFFUSE)
    {
        if(slots.length() >= 0x10000) return;
        defslot = slots.add(new Slot(slots.length()));
    }
    else if(!strcmp(type, "decal"))
    {
        if(decalslots.length() >= 0x10000) return;
        tnum = TEX_DIFFUSE;
        defslot = decalslots.add(new DecalSlot(decalslots.length()));
    }
    else if((matslot = findmaterial(type)) >= 0)
    {
        tnum = TEX_DIFFUSE;
        defslot = &materialslots[matslot];
        defslot->reset(); 
    }
    else if(!defslot) return;
    else if(tnum < 0) tnum = TEX_UNKNOWN;
    Slot &s = *defslot;
    s.loaded = false;
    s.texmask |= 1<<tnum;
    if(s.sts.length()>=8) conoutf(CON_WARN, "warning: too many textures in %s", s.name());
    Slot::Tex &st = s.sts.add();
    st.type = tnum;
    copystring(st.name, name);
    path(st.name);
    if(tnum==TEX_DIFFUSE)
    {
        setslotshader(s);
        VSlot &vs = s.emptyvslot();
        vs.rotation = clamp(*rot, 0, 7);
        vs.offset = ivec2(*xoffset, *yoffset).max(0);
        vs.scale = *scale <= 0 ? 1 : *scale;
        propagatevslot(&vs, (1<<VSLOT_NUM)-1);
    }
}

COMMAND(texture, "ssiiif");

void texgrass(char *name)
{
    if(!defslot) return;
    Slot &s = *defslot;
    DELETEA(s.grass);
    s.grass = name[0] ? newstring(makerelpath("media/texture", name)) : NULL;
}
COMMAND(texgrass, "s");

void texscroll(float *scrollS, float *scrollT)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->scroll = vec2(*scrollS/1000.0f, *scrollT/1000.0f);
    propagatevslot(s.variants, 1<<VSLOT_SCROLL);
}
COMMAND(texscroll, "ff");

void texoffset_(int *xoffset, int *yoffset)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->offset = ivec2(*xoffset, *yoffset).max(0);
    propagatevslot(s.variants, 1<<VSLOT_OFFSET);
}
COMMANDN(texoffset, texoffset_, "ii");

void texrotate_(int *rot)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->rotation = clamp(*rot, 0, 7);
    propagatevslot(s.variants, 1<<VSLOT_ROTATION);
}
COMMANDN(texrotate, texrotate_, "i");

void texscale(float *scale)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->scale = *scale <= 0 ? 1 : *scale;
    propagatevslot(s.variants, 1<<VSLOT_SCALE);
}
COMMAND(texscale, "f");

void texlayer(int *layer)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->layer = *layer < 0 ? max(slots.length()-1+*layer, 0) : *layer;
    propagatevslot(s.variants, 1<<VSLOT_LAYER);
}
COMMAND(texlayer, "i");

void texdetail(int *detail)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->detail = *detail < 0 ? max(slots.length()-1+*detail, 0) : *detail;
    propagatevslot(s.variants, 1<<VSLOT_DETAIL);
}
COMMAND(texdetail, "i");

void texalpha(float *front, float *back)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->alphafront = clamp(*front, 0.0f, 1.0f);
    s.variants->alphaback = clamp(*back, 0.0f, 1.0f);
    propagatevslot(s.variants, 1<<VSLOT_ALPHA);
}
COMMAND(texalpha, "ff");

void texcolor(float *r, float *g, float *b)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->colorscale = vec(clamp(*r, 0.0f, 2.0f), clamp(*g, 0.0f, 2.0f), clamp(*b, 0.0f, 2.0f));
    propagatevslot(s.variants, 1<<VSLOT_COLOR);
}
COMMAND(texcolor, "fff");

void texrefract(float *k, float *r, float *g, float *b)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.variants->refractscale = clamp(*k, 0.0f, 1.0f);
    if(s.variants->refractscale > 0 && (*r > 0 || *g > 0 || *b > 0))
        s.variants->refractcolor = vec(clamp(*r, 0.0f, 1.0f), clamp(*g, 0.0f, 1.0f), clamp(*b, 0.0f, 1.0f));
    else
        s.variants->refractcolor = vec(1, 1, 1);
    propagatevslot(s.variants, 1<<VSLOT_REFRACT);
}
COMMAND(texrefract, "ffff");

void texsmooth(int *id, int *angle)
{
    if(!defslot) return;
    Slot &s = *defslot;
    s.smooth = smoothangle(*id, *angle);
}
COMMAND(texsmooth, "ib");

void decaldepth(float *depth, float *fade)
{
    if(!defslot || defslot->type() != Slot::DECAL) return;
    DecalSlot &s = *(DecalSlot *)defslot;
    s.depth = clamp(*depth, 1e-3f, 1e3f);
    s.fade = clamp(*fade, 0.0f, s.depth);
}
COMMAND(decaldepth, "ff");

static void addglow(ImageData &c, ImageData &g, const vec &glowcolor)
{
    if(g.bpp < 3)
    {
        readwritergbtex(c, g,
            loopk(3) dst[k] = clamp(int(dst[k]) + int(src[0]*glowcolor[k]), 0, 255);
        );
    }
    else
    {
        readwritergbtex(c, g,
            loopk(3) dst[k] = clamp(int(dst[k]) + int(src[k]*glowcolor[k]), 0, 255);
        );
    }
}

static void mergespec(ImageData &c, ImageData &s)
{
    if(s.bpp < 3)
    {
        readwritergbatex(c, s,
            dst[3] = src[0];
        );
    }
    else
    {
        readwritergbatex(c, s,
            dst[3] = (int(src[0]) + int(src[1]) + int(src[2]))/3;
        );
    }
}

static void mergedepth(ImageData &c, ImageData &z)
{
    readwritergbatex(c, z,
        dst[3] = src[0];
    );
}

static void mergealpha(ImageData &c, ImageData &s)
{
    if(s.bpp < 3)
    {
        readwritergbatex(c, s,
            dst[3] = src[0];
        );
    }
    else if(s.bpp == 3)
    {
        readwritergbatex(c, s,
            dst[3] = (int(src[0]) + int(src[1]) + int(src[2]))/3;
        );
    }
    else
    {
        readwritergbatex(c, s,
            dst[3] = src[3];
        );
    }
}

static void collapsespec(ImageData &s)
{
    ImageData d(s.w, s.h, 1);
    if(s.bpp >= 3) readwritetex(d, s, { dst[0] = (int(src[0]) + int(src[1]) + int(src[2]))/3; });
    else readwritetex(d, s, { dst[0] = src[0]; });
    s.replace(d);
}

int Slot::findtextype(int type, int last) const
{
    for(int i = last+1; i<sts.length(); i++) if((type&(1<<sts[i].type)) && sts[i].combined<0) return i;
    return -1;
}

int Slot::cancombine(int type) const
{
    switch(type)
    {
        case TEX_DIFFUSE: return texmask&((1<<TEX_SPEC)|(1<<TEX_NORMAL)) ? TEX_SPEC : TEX_ALPHA;
        case TEX_NORMAL: return texmask&(1<<TEX_DEPTH) ? TEX_DEPTH : TEX_ALPHA;
        default: return -1;
    }
}

int DecalSlot::cancombine(int type) const
{
    switch(type)
    {
        case TEX_GLOW: return TEX_SPEC;
        case TEX_NORMAL: return texmask&(1<<TEX_DEPTH) ? TEX_DEPTH : (texmask&(1<<TEX_GLOW) ? -1 : TEX_SPEC);
        default: return -1;
    }
}

bool DecalSlot::shouldpremul(int type) const
{
    switch(type)
    {
        case TEX_DIFFUSE: return true;
        default: return false;
    }
}

static void addname(vector<char> &key, Slot &slot, Slot::Tex &t, bool combined = false, const char *prefix = NULL)
{
    if(combined) key.add('&');
    if(prefix) { while(*prefix) key.add(*prefix++); }
    defformatstring(tname, "%s/%s", slot.texturedir(), t.name);
    for(const char *s = path(tname); *s; key.add(*s++));
}

void Slot::load(int index, Slot::Tex &t)
{
    vector<char> key;
    addname(key, *this, t, false, shouldpremul(t.type) ? "<premul>" : NULL);
    Slot::Tex *combine = NULL;
    loopv(sts)
    {
        Slot::Tex &c = sts[i];
        if(c.combined == index)
        {
            combine = &c;
            addname(key, *this, c, true);
            break;
        }
    }
    key.add('\0');
    t.t = textures.access(key.getbuf());
    if(t.t) return;
    int compress = 0, wrap = 0;
    ImageData ts;
    if(!texturedata(ts, *this, t, true, &compress, &wrap)) { t.t = notexture; return; }
    if(!ts.compressed) switch(t.type)
    {
        case TEX_SPEC:
            if(ts.bpp > 1) collapsespec(ts);
            break;
        case TEX_GLOW:
        case TEX_DIFFUSE:
        case TEX_NORMAL:
            if(combine)
            {
                ImageData cs;
                if(texturedata(cs, *this, *combine))
                {
                    if(cs.w!=ts.w || cs.h!=ts.h) scaleimage(cs, ts.w, ts.h);
                    switch(combine->type)
                    {
                        case TEX_SPEC: mergespec(ts, cs); break;
                        case TEX_DEPTH: mergedepth(ts, cs); break;
                        case TEX_ALPHA: mergealpha(ts, cs); break;
                    }
                }
            }
            if(ts.bpp < 3) swizzleimage(ts);
            break;
    }
    if(!ts.compressed && shouldpremul(t.type)) texpremul(ts);
    t.t = newtexture(NULL, key.getbuf(), ts, wrap, true, true, true, compress);
}

void Slot::load()
{
    linkslotshader(*this);
    loopv(sts)
    {
        Slot::Tex &t = sts[i];
        if(t.combined >= 0) continue;
        int combine = cancombine(t.type);
        if(combine >= 0 && (combine = findtextype(1<<combine)) >= 0)
        {
            Slot::Tex &c = sts[combine];
            c.combined = i;
        }
    }
    loopv(sts)
    {
        Slot::Tex &t = sts[i];
        if(t.combined >= 0) continue;
        switch(t.type)
        {
            case TEX_ENVMAP:
                t.t = cubemapload(t.name);
                break;

            default:
                load(i, t);
                break;
        }
    }
    loaded = true;
}

MatSlot &lookupmaterialslot(int index, bool load)
{
    if(materialslots[index].sts.empty() && index&MATF_INDEX) index &= ~MATF_INDEX;
    MatSlot &s = materialslots[index];
    if(load && !s.linked)
    {
        if(!s.loaded) s.load();
        linkvslotshader(s);
        s.linked = true;
    }
    return s;
}

Slot &lookupslot(int index, bool load)
{
    Slot &s = slots.inrange(index) ? *slots[index] : (slots.inrange(DEFAULT_GEOM) ? *slots[DEFAULT_GEOM] : dummyslot);
    if(!s.loaded && load) s.load();
    return s;
}

VSlot &lookupvslot(int index, bool load)
{
    VSlot &s = vslots.inrange(index) && vslots[index]->slot ? *vslots[index] : (slots.inrange(DEFAULT_GEOM) && slots[DEFAULT_GEOM]->variants ? *slots[DEFAULT_GEOM]->variants : dummyvslot);
    if(load && !s.linked)
    {
        if(!s.slot->loaded) s.slot->load();
        linkvslotshader(s);
        s.linked = true;
    }
    return s;
}

DecalSlot &lookupdecalslot(int index, bool load)
{
    DecalSlot &s = decalslots.inrange(index) ? *decalslots[index] : dummydecalslot;
    if(load && !s.linked)
    {
        if(!s.loaded) s.load();
        linkvslotshader(s);
        s.linked = true;
    }
    return s;
}

void linkslotshaders()
{
    loopv(slots) if(slots[i]->loaded) linkslotshader(*slots[i]);
    loopv(vslots) if(vslots[i]->linked) linkvslotshader(*vslots[i]);
    loopi((MATF_VOLUME|MATF_INDEX)+1) if(materialslots[i].loaded)
    {
        linkslotshader(materialslots[i]);
        linkvslotshader(materialslots[i]);
    }
    loopv(decalslots) if(decalslots[i]->loaded)
    {
        linkslotshader(*decalslots[i]);
        linkvslotshader(*decalslots[i]);
    }
}

static void blitthumbnail(ImageData &d, ImageData &s, int x, int y)
{
    forcergbimage(d);
    forcergbimage(s);
    uchar *dstrow = &d.data[d.pitch*y + d.bpp*x], *srcrow = s.data;
    loop(y, s.h)
    {
        for(uchar *dst = dstrow, *src = srcrow, *end = &srcrow[s.w*s.bpp]; src < end; dst += d.bpp, src += s.bpp)
        loopk(3) dst[k] = src[k];
        dstrow += d.pitch;
        srcrow += s.pitch;
    }
}

Texture *Slot::loadthumbnail()
{
    if(thumbnail) return thumbnail;
    if(!variants)
    {
        thumbnail = notexture;
        return thumbnail;
    }
    VSlot &vslot = *variants;
    linkslotshader(*this, false);
    linkvslotshader(vslot, false);
    vector<char> name;
    if(vslot.colorscale == vec(1, 1, 1)) addname(name, *this, sts[0], false, "<thumbnail>");
    else
    {
        defformatstring(prefix, "<thumbnail:%.2f/%.2f/%.2f>", vslot.colorscale.x, vslot.colorscale.y, vslot.colorscale.z);
        addname(name, *this, sts[0], false, prefix);
    }
    int glow = -1;
    if(texmask&(1<<TEX_GLOW))
    {
        loopvj(sts) if(sts[j].type==TEX_GLOW) { glow = j; break; }
        if(glow >= 0)
        {
            defformatstring(prefix, "<glow:%.2f/%.2f/%.2f>", vslot.glowcolor.x, vslot.glowcolor.y, vslot.glowcolor.z);
            addname(name, *this, sts[glow], true, prefix);
        }
    }
    VSlot *layer = vslot.layer ? &lookupvslot(vslot.layer, false) : NULL;
    if(layer)
    {
        if(layer->colorscale == vec(1, 1, 1)) addname(name, *layer->slot, layer->slot->sts[0], true, "<layer>");
        else
        {
            defformatstring(prefix, "<layer:%.2f/%.2f/%.2f>", layer->colorscale.x, layer->colorscale.y, layer->colorscale.z);
            addname(name, *layer->slot, layer->slot->sts[0], true, prefix);
        }
    }
    VSlot *detail = vslot.detail ? &lookupvslot(vslot.detail, false) : NULL;
    if(detail) addname(name, *detail->slot, detail->slot->sts[0], true, "<detail>");
    name.add('\0');
    Texture *t = textures.access(path(name.getbuf()));
    if(t) thumbnail = t;
    else
    {
        ImageData s, g, l, d;
        texturedata(s, *this, sts[0], false);
        if(glow >= 0) texturedata(g, *this, sts[glow], false);
        if(layer) texturedata(l, *layer->slot, layer->slot->sts[0], false);
        if(detail) texturedata(d, *detail->slot, detail->slot->sts[0], false);
        if(!s.data) t = thumbnail = notexture;
        else
        {
            if(vslot.colorscale != vec(1, 1, 1)) texmad(s, vslot.colorscale, vec(0, 0, 0));
            int xs = s.w, ys = s.h;
            if(s.w > 128 || s.h > 128) scaleimage(s, min(s.w, 128), min(s.h, 128));
            if(g.data)
            {
                if(g.w != s.w || g.h != s.h) scaleimage(g, s.w, s.h);
                addglow(s, g, vslot.glowcolor);
            }
            if(l.data)
            {
                if(layer->colorscale != vec(1, 1, 1)) texmad(l, layer->colorscale, vec(0, 0, 0));
                if(l.w != s.w/2 || l.h != s.h/2) scaleimage(l, s.w/2, s.h/2);
                blitthumbnail(s, l, s.w-l.w, s.h-l.h);
            }
            if(d.data)
            {
                if(vslot.colorscale != vec(1, 1, 1)) texmad(d, vslot.colorscale, vec(0, 0, 0));
                if(d.w != s.w/2 || d.h != s.h/2) scaleimage(d, s.w/2, s.h/2);
                blitthumbnail(s, d, 0, 0);
            }
            if(s.bpp < 3) forcergbimage(s);
            t = newtexture(NULL, name.getbuf(), s, 0, false, false, true);
            t->xs = xs;
            t->ys = ys;
            thumbnail = t;
        }
    }
    return t;
}

// environment mapped reflections

extern const cubemapside cubemapsides[6] =
{
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_X, "lf", false, true,  true  },
    { GL_TEXTURE_CUBE_MAP_POSITIVE_X, "rt", true,  false, true  },
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, "bk", false, false, false },
    { GL_TEXTURE_CUBE_MAP_POSITIVE_Y, "ft", true,  true,  false },
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, "dn", true,  false, true  },
    { GL_TEXTURE_CUBE_MAP_POSITIVE_Z, "up", true,  false, true  },
};

VARFP(envmapsize, 4, 7, 10, setupmaterials());

Texture *cubemaploadwildcard(Texture *t, const char *name, bool mipit, bool msg, bool transient = false)
{
    string tname;
    if(!name) copystring(tname, t->name);
    else
    {
        copystring(tname, name);
        t = textures.access(path(tname));
        if(t)
        {
            if(!transient && t->type&Texture::TRANSIENT) t->type &= ~Texture::TRANSIENT;
            return t;
        }
    }
    char *wildcard = strchr(tname, '*');
    ImageData surface[6];
    string sname;
    if(!wildcard) copystring(sname, tname);
    int tsize = 0, compress = 0;
    loopi(6)
    {
        if(wildcard)
        {
            copystring(sname, stringslice(tname, wildcard));
            concatstring(sname, cubemapsides[i].name);
            concatstring(sname, wildcard+1);
        }
        ImageData &s = surface[i];
        texturedata(s, sname, msg, &compress);
        if(!s.data) return NULL;
        if(s.w != s.h)
        {
            if(msg) conoutf(CON_ERROR, "cubemap texture %s does not have square size", sname);
            return NULL;
        }
        if(s.compressed ? s.compressed!=surface[0].compressed || s.w!=surface[0].w || s.h!=surface[0].h || s.levels!=surface[0].levels : surface[0].compressed || s.bpp!=surface[0].bpp)
        {
            if(msg) conoutf(CON_ERROR, "cubemap texture %s doesn't match other sides' format", sname);
            return NULL;
        }
        tsize = max(tsize, max(s.w, s.h));
    }
    if(name)
    {
        char *key = newstring(tname);
        t = &textures[key];
        t->name = key;
    }
    t->type = Texture::CUBEMAP;
    if(transient) t->type |= Texture::TRANSIENT;
    GLenum format;
    if(surface[0].compressed)
    {
        format = uncompressedformat(surface[0].compressed);
        t->bpp = formatsize(format);
        t->type |= Texture::COMPRESSED;
    }
    else
    {
        format = texformat(surface[0].bpp, true);
        t->bpp = surface[0].bpp;
        if(hasTRG && !hasTSW && swizzlemask(format))
        {
            loopi(6) swizzleimage(surface[i]);
            format = texformat(surface[0].bpp, true);
            t->bpp = surface[0].bpp;
        }
    }
    if(alphaformat(format)) t->type |= Texture::ALPHA;
    t->mipmap = mipit;
    t->clamp = 3;
    t->xs = t->ys = tsize;
    t->w = t->h = min(1<<envmapsize, tsize);
    resizetexture(t->w, t->h, mipit, false, GL_TEXTURE_CUBE_MAP, compress, t->w, t->h);
    GLenum component = format;
    if(!surface[0].compressed)
    {
        component = compressedformat(format, t->w, t->h, compress);
        switch(component)
        {
            case GL_RGB: component = hasES2 ? GL_RGB565 : GL_RGB5; break;
        }
    }
    glGenTextures(1, &t->id);
    loopi(6)
    {
        ImageData &s = surface[i];
        const cubemapside &side = cubemapsides[i];
        texreorient(s, side.flipx, side.flipy, side.swapxy);
        if(s.compressed)
        {
            int w = s.w, h = s.h, levels = s.levels, level = 0;
            uchar *data = s.data;
            while(levels > 1 && (w > t->w || h > t->h))
            {
                data += s.calclevelsize(level++);
                levels--;
                if(w > 1) w /= 2;
                if(h > 1) h /= 2;
            }
            createcompressedtexture(t->id, w, h, data, s.align, s.bpp, levels, i ? -1 : 3, mipit ? 2 : 1, s.compressed, side.target, true);
        }
        else
        {
            createtexture(t->id, t->w, t->h, s.data, i ? -1 : 3, mipit ? 2 : 1, component, side.target, s.w, s.h, s.pitch, false, format, true);
        }
    }
    return t;
}

Texture *cubemapload(const char *name, bool mipit, bool msg, bool transient)
{
    string pname;
    copystring(pname, makerelpath("media/sky", name));
    path(pname);
    Texture *t = NULL;
    if(!strchr(pname, '*'))
    {
        defformatstring(jpgname, "%s_*.jpg", pname);
        t = cubemaploadwildcard(NULL, jpgname, mipit, false, transient);
        if(!t)
        {
            defformatstring(pngname, "%s_*.png", pname);
            t = cubemaploadwildcard(NULL, pngname, mipit, false, transient);
            if(!t && msg) conoutf(CON_ERROR, "could not load envmap %s", name);
        }
    }
    else t = cubemaploadwildcard(NULL, pname, mipit, msg, transient);
    return t;
}

VARR(envmapradius, 0, 128, 10000);
VARR(envmapbb, 0, 0, 1);
VARP(aaenvmap, 0, 1, 1);

struct envmap
{
    int radius, size, blur;
    vec o;
    GLuint tex;

    envmap() : radius(-1), size(0), blur(0), o(0, 0, 0), tex(0) {}

    void clear()
    {
        if(tex) { glDeleteTextures(1, &tex); tex = 0; }
    }
};

static vector<envmap> envmaps;

void clearenvmaps()
{
    loopv(envmaps) envmaps[i].clear();
    envmaps.shrink(0);
}

static GLuint emfbo[3] = { 0, 0, 0 }, emtex[2] = { 0, 0 };
static int emtexsize = -1;

GLuint genenvmap(const vec &o, int envmapsize, int blur, bool onlysky)
{
    int rendersize = 1<<(envmapsize+aaenvmap), sizelimit = min(hwcubetexsize, min(gw, gh));
    if(maxtexsize) sizelimit = min(sizelimit, maxtexsize);
    while(rendersize > sizelimit) rendersize /= 2;
    int texsize = min(rendersize, 1<<envmapsize);
    if(!aaenvmap) rendersize = texsize;
    if(!emtex[0]) glGenTextures(2, emtex);
    if(!emfbo[0]) glGenFramebuffers_(3, emfbo);
    if(emtexsize != texsize)
    {
        emtexsize = texsize;
        loopi(2)
        {
            createtexture(emtex[i], emtexsize, emtexsize, NULL, 3, 1, GL_RGB, GL_TEXTURE_RECTANGLE);
            glBindFramebuffer_(GL_FRAMEBUFFER, emfbo[i]);
            glFramebufferTexture2D_(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, emtex[i], 0);
            if(glCheckFramebufferStatus_(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
                fatal("failed allocating envmap buffer!");
        }
    }
    GLuint tex = 0;
    glGenTextures(1, &tex);
    // workaround for Catalyst bug:
    // all texture levels must be specified before glCopyTexSubImage2D is called, otherwise it crashes
    loopi(6) createtexture(tex, texsize, texsize, NULL, i ? -1 : 3, 2, hasES2 ? GL_RGB565 : GL_RGB5, cubemapsides[i].target);
    float yaw = 0, pitch = 0;
    loopi(6)
    {
        const cubemapside &side = cubemapsides[i];
        switch(side.target)
        {
            case GL_TEXTURE_CUBE_MAP_NEGATIVE_X: // lf
                yaw = 90; pitch = 0; break;
            case GL_TEXTURE_CUBE_MAP_POSITIVE_X: // rt
                yaw = 270; pitch = 0; break;
            case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y: // bk
                yaw = 180; pitch = 0; break;
            case GL_TEXTURE_CUBE_MAP_POSITIVE_Y: // ft
                yaw = 0; pitch = 0; break;
            case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z: // dn
                yaw = 270; pitch = -90; break;
            case GL_TEXTURE_CUBE_MAP_POSITIVE_Z: // up
                yaw = 270; pitch = 90; break;
        }
        drawcubemap(rendersize, o, yaw, pitch, side, onlysky);
        copyhdr(rendersize, rendersize, emfbo[0], texsize, texsize, side.flipx, !side.flipy, side.swapxy);
        if(blur > 0)
        {
            float blurweights[MAXBLURRADIUS+1], bluroffsets[MAXBLURRADIUS+1];
            setupblurkernel(blur, blurweights, bluroffsets);
            loopj(2)
            {
                glBindFramebuffer_(GL_FRAMEBUFFER, emfbo[1]);
                glViewport(0, 0, texsize, texsize);
                setblurshader(j, 1, blur, blurweights, bluroffsets, GL_TEXTURE_RECTANGLE);
                glBindTexture(GL_TEXTURE_RECTANGLE, emtex[0]);
                screenquad(texsize, texsize);
                swap(emfbo[0], emfbo[1]);
                swap(emtex[0], emtex[1]);
            }
        }
        for(int level = 0, lsize = texsize;; level++)
        {
            if(hasFBB)
            {
                glBindFramebuffer_(GL_READ_FRAMEBUFFER, emfbo[0]);
                glBindFramebuffer_(GL_DRAW_FRAMEBUFFER, emfbo[2]);
                glFramebufferTexture2D_(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, side.target, tex, level);
                glBlitFramebuffer_(0, 0, lsize, lsize, 0, 0, lsize, lsize, GL_COLOR_BUFFER_BIT, GL_NEAREST);
            }
            else
            {
                glBindFramebuffer_(GL_FRAMEBUFFER, emfbo[0]);
                glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
                glCopyTexSubImage2D(side.target, level, 0, 0, 0, 0, lsize, lsize);
            }
            if(lsize <= 1) break;
            int dsize = lsize/2;
            if(hasFBB)
            {
                glBindFramebuffer_(GL_READ_FRAMEBUFFER, emfbo[0]);
                glBindFramebuffer_(GL_DRAW_FRAMEBUFFER, emfbo[1]);
                glBlitFramebuffer_(0, 0, lsize, lsize, 0, 0, dsize, dsize, GL_COLOR_BUFFER_BIT, GL_LINEAR);
            }
            else
            {
                glBindFramebuffer_(GL_FRAMEBUFFER, emfbo[1]);
                glBindTexture(GL_TEXTURE_RECTANGLE, emtex[0]);
                glViewport(0, 0, dsize, dsize);
                SETSHADER(scalelinear);
                screenquad(lsize, lsize);
            }
            lsize = dsize;
            swap(emfbo[0], emfbo[1]);
            swap(emtex[0], emtex[1]);
        }
    }
    glBindFramebuffer_(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, hudw, hudh);
    clientkeepalive();
    return tex;
}

void initenvmaps()
{
    clearenvmaps();
    envmaps.add().size = hasskybox() ? 0 : 1;
    const vector<extentity *> &ents = entities::getents();
    loopv(ents)
    {
        const extentity &ent = *ents[i];
        if(ent.type != ET_ENVMAP) continue;
        envmap &em = envmaps.add();
        em.radius = ent.attr1 ? clamp(int(ent.attr1), 0, 10000) : envmapradius;
        em.size = ent.attr2 ? clamp(int(ent.attr2), 4, 9) : 0;
        em.blur = ent.attr3 ? clamp(int(ent.attr3), 1, 2) : 0;
        em.o = ent.o;
    }
}

void genenvmaps()
{
    if(envmaps.empty()) return;
    renderprogress(0, "generating environment maps...");
    int lastprogress = SDL_GetTicks();
    gl_setupframe(true);
    loopv(envmaps)
    {
        envmap &em = envmaps[i];
        em.tex = genenvmap(em.o, em.size ? min(em.size, envmapsize) : envmapsize, em.blur, em.radius < 0);
        if(renderedframe) continue;
        int millis = SDL_GetTicks();
        if(millis - lastprogress >= 250)
        {
            renderprogress(float(i+1)/envmaps.length(), "generating environment maps...", true);
            lastprogress = millis;
        }
    }
    if(emfbo[0]) { glDeleteFramebuffers_(3, emfbo); memset(emfbo, 0, sizeof(emfbo)); }
    if(emtex[0]) { glDeleteTextures(2, emtex); memset(emtex, 0, sizeof(emtex)); }
    emtexsize = -1;
}

ushort closestenvmap(const vec &o)
{
    ushort minemid = EMID_SKY;
    float mindist = 1e16f;
    loopv(envmaps)
    {
        envmap &em = envmaps[i];
        float dist;
        if(envmapbb)
        {
            if(!o.insidebb(vec(em.o).sub(em.radius), vec(em.o).add(em.radius))) continue;
            dist = em.o.dist(o);
        }
        else
        {
            dist = em.o.dist(o);
            if(dist > em.radius) continue;
        }
        if(dist < mindist)
        {
            minemid = EMID_RESERVED + i;
            mindist = dist;
        }
    }
    return minemid;
}

ushort closestenvmap(int orient, const ivec &co, int size)
{
    vec loc(co);
    int dim = dimension(orient);
    if(dimcoord(orient)) loc[dim] += size;
    loc[R[dim]] += size/2;
    loc[C[dim]] += size/2;
    return closestenvmap(loc);
}

static inline GLuint lookupskyenvmap()
{
    return envmaps.length() && envmaps[0].radius < 0 ? envmaps[0].tex : 0;
}

GLuint lookupenvmap(Slot &slot)
{
    loopv(slot.sts) if(slot.sts[i].type==TEX_ENVMAP && slot.sts[i].t) return slot.sts[i].t->id;
    return lookupskyenvmap();
}

GLuint lookupenvmap(ushort emid)
{
    if(emid==EMID_SKY || emid==EMID_CUSTOM || drawtex) return lookupskyenvmap();
    if(emid==EMID_NONE || !envmaps.inrange(emid-EMID_RESERVED)) return 0;
    GLuint tex = envmaps[emid-EMID_RESERVED].tex;
    return tex ? tex : lookupskyenvmap();
}

void cleanuptexture(Texture *t)
{
    DELETEA(t->alphamask);
    if(t->id) { glDeleteTextures(1, &t->id); t->id = 0; }
    if(t->type&Texture::TRANSIENT) textures.remove(t->name);
}

void cleanuptextures()
{
    cleanupmipmaps();
    clearenvmaps();
    loopv(slots) slots[i]->cleanup();
    loopv(vslots) vslots[i]->cleanup();
    loopi((MATF_VOLUME|MATF_INDEX)+1) materialslots[i].cleanup();
    loopv(decalslots) decalslots[i]->cleanup();
    enumerate(textures, Texture, tex, cleanuptexture(&tex));
}

bool reloadtexture(const char *name)
{
    Texture *t = textures.access(path(name, true));
    if(t) return reloadtexture(*t);
    return true;
}

bool reloadtexture(Texture &tex)
{
    if(tex.id) return true;
    switch(tex.type&Texture::TYPE)
    {
        case Texture::IMAGE:
        {
            int compress = 0;
            ImageData s;
            if(!texturedata(s, tex.name, true, &compress) || !newtexture(&tex, NULL, s, tex.clamp, tex.mipmap, false, false, compress)) return false;
            break;
        }

        case Texture::CUBEMAP:
            if(!cubemaploadwildcard(&tex, NULL, tex.mipmap, true)) return false;
            break;
    }
    return true;
}

void reloadtex(char *name)
{
    Texture *t = textures.access(path(name, true));
    if(!t) { conoutf(CON_ERROR, "texture %s is not loaded", name); return; }
    if(t->type&Texture::TRANSIENT) { conoutf(CON_ERROR, "can't reload transient texture %s", name); return; }
    DELETEA(t->alphamask);
    Texture oldtex = *t;
    t->id = 0;
    if(!reloadtexture(*t))
    {
        if(t->id) glDeleteTextures(1, &t->id);
        *t = oldtex;
        conoutf(CON_ERROR, "failed to reload texture %s", name);
    }
}

COMMAND(reloadtex, "s");

void reloadtextures()
{
    int reloaded = 0;
    enumerate(textures, Texture, tex,
    {
        loadprogress = float(++reloaded)/textures.numelems;
        reloadtexture(tex);
    });
    loadprogress = 0;
}
#endif

enum
{
    DDSD_CAPS                  = 0x00000001,
    DDSD_HEIGHT                = 0x00000002,
    DDSD_WIDTH                 = 0x00000004,
    DDSD_PITCH                 = 0x00000008,
    DDSD_PIXELFORMAT           = 0x00001000,
    DDSD_MIPMAPCOUNT           = 0x00020000,
    DDSD_LINEARSIZE            = 0x00080000,
    DDSD_BACKBUFFERCOUNT       = 0x00800000,
    DDPF_ALPHAPIXELS           = 0x00000001,
    DDPF_FOURCC                = 0x00000004,
    DDPF_INDEXED               = 0x00000020,
    DDPF_ALPHA                 = 0x00000002,
    DDPF_RGB                   = 0x00000040,
    DDPF_COMPRESSED            = 0x00000080,
    DDPF_LUMINANCE             = 0x00020000,
    DDSCAPS_COMPLEX            = 0x00000008,
    DDSCAPS_TEXTURE            = 0x00001000,
    DDSCAPS_MIPMAP             = 0x00400000,
    DDSCAPS2_CUBEMAP           = 0x00000200,
    DDSCAPS2_CUBEMAP_POSITIVEX = 0x00000400,
    DDSCAPS2_CUBEMAP_NEGATIVEX = 0x00000800,
    DDSCAPS2_CUBEMAP_POSITIVEY = 0x00001000,
    DDSCAPS2_CUBEMAP_NEGATIVEY = 0x00002000,
    DDSCAPS2_CUBEMAP_POSITIVEZ = 0x00004000,
    DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x00008000,
    DDSCAPS2_VOLUME            = 0x00200000,
    FOURCC_DXT1                = 0x31545844,
    FOURCC_DXT2                = 0x32545844,
    FOURCC_DXT3                = 0x33545844,
    FOURCC_DXT4                = 0x34545844,
    FOURCC_DXT5                = 0x35545844,
    FOURCC_ATI1                = 0x31495441,
    FOURCC_ATI2                = 0x32495441
};

struct DDCOLORKEY { uint dwColorSpaceLowValue, dwColorSpaceHighValue; };
struct DDPIXELFORMAT
{
    uint dwSize, dwFlags, dwFourCC;
    union { uint dwRGBBitCount, dwYUVBitCount, dwZBufferBitDepth, dwAlphaBitDepth, dwLuminanceBitCount, dwBumpBitCount, dwPrivateFormatBitCount; };
    union { uint dwRBitMask, dwYBitMask, dwStencilBitDepth, dwLuminanceBitMask, dwBumpDuBitMask, dwOperations; };
    union { uint dwGBitMask, dwUBitMask, dwZBitMask, dwBumpDvBitMask; struct { ushort wFlipMSTypes, wBltMSTypes; } MultiSampleCaps; };
    union { uint dwBBitMask, dwVBitMask, dwStencilBitMask, dwBumpLuminanceBitMask; };
    union { uint dwRGBAlphaBitMask, dwYUVAlphaBitMask, dwLuminanceAlphaBitMask, dwRGBZBitMask, dwYUVZBitMask; };

};
struct DDSCAPS2 { uint dwCaps, dwCaps2, dwCaps3, dwCaps4; };
struct DDSURFACEDESC2
{
    uint dwSize, dwFlags, dwHeight, dwWidth;
    union { int lPitch; uint dwLinearSize; };
    uint dwBackBufferCount;
    union { uint dwMipMapCount, dwRefreshRate, dwSrcVBHandle; };
    uint dwAlphaBitDepth, dwReserved, lpSurface;
    union { DDCOLORKEY ddckCKDestOverlay; uint dwEmptyFaceColor; };
    DDCOLORKEY ddckCKDestBlt, ddckCKSrcOverlay, ddckCKSrcBlt;
    union { DDPIXELFORMAT ddpfPixelFormat; uint dwFVF; };
    DDSCAPS2 ddsCaps;
    uint dwTextureStage;
};

#define DECODEDDS(name, dbpp, initblock, writeval, nextval) \
static void name(ImageData &s) \
{ \
    ImageData d(s.w, s.h, dbpp); \
    uchar *dst = d.data; \
    const uchar *src = s.data; \
    for(int by = 0; by < s.h; by += s.align) \
    { \
        for(int bx = 0; bx < s.w; bx += s.align, src += s.bpp) \
        { \
            int maxy = min(d.h - by, s.align), maxx = min(d.w - bx, s.align); \
            initblock; \
            loop(y, maxy) \
            { \
                int x; \
                for(x = 0; x < maxx; ++x) \
                { \
                    writeval; \
                    nextval; \
                    dst += d.bpp; \
                }  \
                for(; x < s.align; ++x) { nextval; } \
                dst += d.pitch - maxx*d.bpp; \
            } \
            dst += maxx*d.bpp - maxy*d.pitch; \
        } \
        dst += (s.align-1)*d.pitch; \
    } \
    s.replace(d); \
}

DECODEDDS(decodedxt1, s.compressed == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT ? 4 : 3,
    ushort color0 = lilswap(*(const ushort *)src);
    ushort color1 = lilswap(*(const ushort *)&src[2]);
    uint bits = lilswap(*(const uint *)&src[4]);
    bvec4 rgba[4];
    rgba[0] = bvec4(bvec::from565(color0), 0xFF);
    rgba[1] = bvec4(bvec::from565(color1), 0xFF);
    if(color0 > color1)
    {
        rgba[2].lerp(rgba[0], rgba[1], 2, 1, 3);
        rgba[3].lerp(rgba[0], rgba[1], 1, 2, 3);
    }
    else
    {
        rgba[2].lerp(rgba[0], rgba[1], 1, 1, 2);
        rgba[3] = bvec4(0, 0, 0, 0);
    }
,
    memcpy(dst, rgba[bits&3].v, d.bpp);
,
    bits >>= 2;
);

DECODEDDS(decodedxt3, 4,
    ullong alpha = lilswap(*(const ullong *)src);
    ushort color0 = lilswap(*(const ushort *)&src[8]);
    ushort color1 = lilswap(*(const ushort *)&src[10]);
    uint bits = lilswap(*(const uint *)&src[12]);
    bvec rgb[4];
    rgb[0] = bvec::from565(color0);
    rgb[1] = bvec::from565(color1);
    rgb[2].lerp(rgb[0], rgb[1], 2, 1, 3);
    rgb[3].lerp(rgb[0], rgb[1], 1, 2, 3);
,
    memcpy(dst, rgb[bits&3].v, 3);
    dst[3] = ((alpha&0xF)*1088 + 32) >> 6;
,
    bits >>= 2;
    alpha >>= 4;
);

static inline void decodealpha(uchar alpha0, uchar alpha1, uchar alpha[8])
{
    alpha[0] = alpha0;
    alpha[1] = alpha1;
    if(alpha0 > alpha1)
    {
        alpha[2] = (6*alpha0 + alpha1)/7;
        alpha[3] = (5*alpha0 + 2*alpha1)/7;
        alpha[4] = (4*alpha0 + 3*alpha1)/7;
        alpha[5] = (3*alpha0 + 4*alpha1)/7;
        alpha[6] = (2*alpha0 + 5*alpha1)/7;
        alpha[7] = (alpha0 + 6*alpha1)/7;
    }
    else
    {
        alpha[2] = (4*alpha0 + alpha1)/5;
        alpha[3] = (3*alpha0 + 2*alpha1)/5;
        alpha[4] = (2*alpha0 + 3*alpha1)/5;
        alpha[5] = (alpha0 + 4*alpha1)/5;
        alpha[6] = 0;
        alpha[7] = 0xFF;
    }
}

DECODEDDS(decodedxt5, 4,
    uchar alpha[8];
    decodealpha(src[0], src[1], alpha);
    ullong alphabits = lilswap(*(const ushort *)&src[2]) + ((ullong)lilswap(*(const uint *)&src[4]) << 16);
    ushort color0 = lilswap(*(const ushort *)&src[8]);
    ushort color1 = lilswap(*(const ushort *)&src[10]);
    uint bits = lilswap(*(const uint *)&src[12]);
    bvec rgb[4];
    rgb[0] = bvec::from565(color0);
    rgb[1] = bvec::from565(color1);
    rgb[2].lerp(rgb[0], rgb[1], 2, 1, 3);
    rgb[3].lerp(rgb[0], rgb[1], 1, 2, 3);
,
    memcpy(dst, rgb[bits&3].v, 3);
    dst[3] = alpha[alphabits&7];
,
    bits >>= 2;
    alphabits >>= 3;
);

DECODEDDS(decodergtc1, 1,
    uchar red[8];
    decodealpha(src[0], src[1], red);
    ullong redbits = lilswap(*(const ushort *)&src[2]) + ((ullong)lilswap(*(const uint *)&src[4]) << 16);
,
    dst[0] = red[redbits&7];
,
    redbits >>= 3;
);

DECODEDDS(decodergtc2, 2,
    uchar red[8];
    decodealpha(src[0], src[1], red);
    ullong redbits = lilswap(*(const ushort *)&src[2]) + ((ullong)lilswap(*(const uint *)&src[4]) << 16);
    uchar green[8];
    decodealpha(src[8], src[9], green);
    ullong greenbits = lilswap(*(const ushort *)&src[10]) + ((ullong)lilswap(*(const uint *)&src[12]) << 16);
,
    dst[0] = red[redbits&7];
    dst[1] = green[greenbits&7];
,
    redbits >>= 3;
    greenbits >>= 3;
);

// determines format to use and allocates memory
bool ddsformat(DDSURFACEDESC2 &d, ImageData &image, GLenum &format, bool &supported, int force)
{
    if(d.ddpfPixelFormat.dwFlags & DDPF_FOURCC)
    {
        switch(d.ddpfPixelFormat.dwFourCC)
        {
            case FOURCC_DXT1:
                if((supported = hasS3TC) || force) format = d.ddpfPixelFormat.dwFlags & DDPF_ALPHAPIXELS ? GL_COMPRESSED_RGBA_S3TC_DXT1_EXT : GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
                break;
            case FOURCC_DXT2:
            case FOURCC_DXT3:
                if((supported = hasS3TC) || force) format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
                break;
            case FOURCC_DXT4:
            case FOURCC_DXT5:
                if((supported = hasS3TC) || force) format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
                break;
            case FOURCC_ATI1:
                if((supported = hasRGTC) || force) format = GL_COMPRESSED_RED_RGTC1;
                else if((supported = hasLATC)) format = GL_COMPRESSED_LUMINANCE_LATC1_EXT;
                break;
            case FOURCC_ATI2:
                if((supported = hasRGTC) || force) format = GL_COMPRESSED_RG_RGTC2;
                else if((supported = hasLATC)) format = GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT;
                break;
        }
    }
    if(!format || (!supported && !force)) { return false; }
    int blocksize = 0;
    switch(format)
    {
        case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT: blocksize = 8; break;
        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT: blocksize = 16; break;
        case GL_COMPRESSED_LUMINANCE_LATC1_EXT:
        case GL_COMPRESSED_RED_RGTC1: blocksize = 8; break;
        case GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT:
        case GL_COMPRESSED_RG_RGTC2: blocksize = 16; break;
    }
    // for compressed textures, ImageData::bpp means blocksize in bytes
    image.setdata(NULL, d.dwWidth, d.dwHeight, blocksize, !supported || force > 0 ? 1 : d.dwMipMapCount+1, 4, format);
    return true;
}

// decode dds to rgb
void dds_to_rgb(ImageData &image, GLenum format) {
    switch(format)
    {
        case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
            decodedxt1(image);
            break;
        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
            decodedxt3(image);
            break;
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
            decodedxt5(image);
            break;
        case GL_COMPRESSED_LUMINANCE_LATC1_EXT:
        case GL_COMPRESSED_RED_RGTC1:
            decodergtc1(image);
            break;
        case GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT:
        case GL_COMPRESSED_RG_RGTC2:
            decodergtc2(image);
            break;
    }
}

bool loaddds(const char *filename, ImageData &image, int force)
{
    stream *f = openfile(filename, "rb");
    if(!f) return false;
    uchar magic[4];
    if(f->read(magic, 4) != 4 || memcmp(magic, "DDS ", 4)) { delete f; return false; }
    DDSURFACEDESC2 d;
    if(f->read(&d, sizeof(d)) != sizeof(d)) { delete f; return false; }
    lilswap((uint *)&d, sizeof(d)/sizeof(uint));
    if(d.dwSize != sizeof(DDSURFACEDESC2) || d.ddpfPixelFormat.dwSize != sizeof(DDPIXELFORMAT)) { delete f; return false; }
    GLenum format = GL_FALSE;
    bool supported = false;
    if(!ddsformat(d, image, format, supported, force)) { delete f; return false; }
    if(dbgdds) conoutf(CON_DEBUG, "%s: format 0x%X, %d x %d, %d mipmaps", filename, format, d.dwWidth, d.dwHeight, d.dwMipMapCount);
    size_t size = image.calcsize();
    if(f->read(image.data, size) != size) { delete f; image.cleanup(); return false; }
    if(dbgdds) conoutf(CON_DEBUG, "read %d bytes of image data", size);
    delete f;
    if(!supported || force > 0) dds_to_rgb(image, format);
    return true;
}

// setup image from in-memory ddsheader and ddsdata via ddsformat
bool setupddsimage(uchar *ddsheader, uchar *ddsdata, ImageData &image, int force)
{
    DDSURFACEDESC2 d;
    memcpy((uchar *)&d, ddsheader+4, sizeof(d));
    GLenum format = GL_FALSE;
    bool supported = false;
    if(!ddsformat(d, image, format, supported, force)) return false;

    // clean up the memory allocated by ddsformat
    image.cleanup();
    // set data to ddsdata, but don't take ownership of ddsdata
    image.data = ddsdata;

    if(!supported || force > 0) dds_to_rgb(image, format);
    return true;
}

#ifdef USE_TEXTURE_MAN
void gendds(char *infile, char *outfile)
{
    if(!hasS3TC || usetexcompress <= 1) { conoutf(CON_ERROR, "OpenGL driver does not support S3TC texture compression"); return; }

    glHint(GL_TEXTURE_COMPRESSION_HINT, GL_NICEST);

    defformatstring(cfile, "<compress>%s", infile);
    extern void reloadtex(char *name);
    Texture *t = textures.access(path(cfile));
    if(t) reloadtex(cfile);
    t = textureload(cfile);
    if(t==notexture) { conoutf(CON_ERROR, "failed loading %s", infile); return; }

    glBindTexture(GL_TEXTURE_2D, t->id);
    GLint compressed = 0, format = 0, width = 0, height = 0;
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED, &compressed);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &format);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);

    if(!compressed) { conoutf(CON_ERROR, "failed compressing %s", infile); return; }
    int fourcc = 0;
    switch(format)
    {
        case GL_COMPRESSED_RGB_S3TC_DXT1_EXT: fourcc = FOURCC_DXT1; conoutf("compressed as DXT1"); break;
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT: fourcc = FOURCC_DXT1; conoutf("compressed as DXT1a"); break;
        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT: fourcc = FOURCC_DXT3; conoutf("compressed as DXT3"); break;
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT: fourcc = FOURCC_DXT5; conoutf("compressed as DXT5"); break;
        case GL_COMPRESSED_LUMINANCE_LATC1_EXT:
        case GL_COMPRESSED_RED_RGTC1: fourcc = FOURCC_ATI1; conoutf("compressed as ATI1"); break;
        case GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT:
        case GL_COMPRESSED_RG_RGTC2: fourcc = FOURCC_ATI2; conoutf("compressed as ATI2"); break;
        default:
            conoutf(CON_ERROR, "failed compressing %s: unknown format: 0x%X", infile, format); break;
            return;
    }

    if(!outfile[0])
    {
        static string buf;
        copystring(buf, infile);
        int len = strlen(buf);
        if(len > 4 && buf[len-4]=='.') memcpy(&buf[len-4], ".dds", 4);
        else concatstring(buf, ".dds");
        outfile = buf;
    }

    stream *f = openfile(path(outfile, true), "wb");
    if(!f) { conoutf(CON_ERROR, "failed writing to %s", outfile); return; }

    int csize = 0;
    for(int lw = width, lh = height, level = 0;;)
    {
        GLint size = 0;
        glGetTexLevelParameteriv(GL_TEXTURE_2D, level++, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &size);
        csize += size;
        if(max(lw, lh) <= 1) break;
        if(lw > 1) lw /= 2;
        if(lh > 1) lh /= 2;
    }

    DDSURFACEDESC2 d;
    memset(&d, 0, sizeof(d));
    d.dwSize = sizeof(DDSURFACEDESC2);
    d.dwWidth = width;
    d.dwHeight = height;
    d.dwLinearSize = csize;
    d.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT | DDSD_LINEARSIZE | DDSD_MIPMAPCOUNT;
    d.ddsCaps.dwCaps = DDSCAPS_TEXTURE | DDSCAPS_COMPLEX | DDSCAPS_MIPMAP;
    d.ddpfPixelFormat.dwSize = sizeof(DDPIXELFORMAT);
    d.ddpfPixelFormat.dwFlags = DDPF_FOURCC | (alphaformat(uncompressedformat(format)) ? DDPF_ALPHAPIXELS : 0);
    d.ddpfPixelFormat.dwFourCC = fourcc;

    uchar *data = new uchar[csize], *dst = data;
    for(int lw = width, lh = height;;)
    {
        GLint size;
        glGetTexLevelParameteriv(GL_TEXTURE_2D, d.dwMipMapCount, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &size);
        glGetCompressedTexImage_(GL_TEXTURE_2D, d.dwMipMapCount++, dst);
        dst += size;
        if(max(lw, lh) <= 1) break;
        if(lw > 1) lw /= 2;
        if(lh > 1) lh /= 2;
    }

    lilswap((uint *)&d, sizeof(d)/sizeof(uint));

    f->write("DDS ", 4);
    f->write(&d, sizeof(d));
    f->write(data, csize);
    delete f;

    delete[] data;

    conoutf("wrote DDS file %s", outfile);

    setuptexcompress();
}
COMMAND(gendds, "ss");

bool loadimage(const char *filename, ImageData &image)
{
    SDL_Surface *s = loadsurface(path(filename, true));
    if(!s) return false;
    image.wrap(s);
    return true;
}
#endif
