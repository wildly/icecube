// Copyright (c) 2001-2011 Cube authors, Sauerbraten authors

// generic useful stuff for any C++ program

#ifndef _TOOLS_H
#define _TOOLS_H

#ifdef __GNUC__
#define gamma __gamma
#endif

// Updated with "_USE_MATH_DEFINES" (from Sauerbraten 2020 release's tools.h)
#ifdef WIN32
#define _USE_MATH_DEFINES
#endif
#include <math.h>

// VC++ versions beginning with VS 2013 have exp2, log2 (C99 library support)
// The functions below are for earlier versions.
#if _MSC_VER >= 1800
#elif _MSC_VER
static float  exp2(float x)  { return pow(2, x); }
static double exp2(double x) { return pow(2, x); }
static float  log2(float x)  { return log(x)/(float)M_LN2; }
static double log2(double x) { return log(x)/M_LN2; }
#endif

#ifdef __GNUC__
#undef gamma
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#if _MSC_VER >= 1600    // VC++ versions beginning with VS 2010 have stdint.h
#include <stdint.h>
#elif _MSC_VER >= 1310  // VC++ 2003-2008 have stddef.h
#include <stddef.h>
#endif

#if _MSC_VER >= 1400    // VC++ versions beginning with VS 2005 have intrin.h
#include <intrin.h>
#endif

#include <limits.h>
#include <assert.h>
#ifdef __GNUC__
#include <new>
#else
#include <new.h>
#endif

#ifdef NULL
#undef NULL
#endif
#define NULL 0

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;

#define max(a,b) (((a) > (b)) ? (a) : (b))
#define min(a,b) (((a) < (b)) ? (a) : (b))
#define rnd(max) (rand()%(max))
#define rndreset() (srand(1))
#define rndtime(offset) { srand(SDL_GetTicks()+offset); }
#define loop(v,m) for(int v = 0; v<(m); v++)
#define loopi(m) loop(i,m)
#define loopj(m) loop(j,m)
#define loopk(m) loop(k,m)
#define loopl(m) loop(l,m)

// Loop through each element in a vector in normal or reverse order.
//
#define loopv(v)    if(false) {} else for(int i = 0; i<(v).length(); i++)
#define loopvrev(v) if(false) {} else for(int i = (v).length()-1; i>=0; i--)

#define ARRLEN(a) (sizeof(a)/sizeof(a[0]))

// Find the first matching element in an array indexed by integers.
//
// iterator - Something that can store the index of the matching element.
//            It should be initialized to the starting index of the search before using ARRFIND().
// test     - An expression that is true when applied to the matching element.
// bounds   - An expression that is false until reaching the bounds of the array.
// next     - The operation on the iterator such that it produces the next element of the array.
//
// If no matching element is found, iterator will be equal to ARRLEN(the_array).
//
// For example:
//
// float constants[] = { 0.0f, 1.0f, MATH_E, 2*MATH_PI };
// /* Declare iterator "i" outside the loop; it will be used later. */
// int i = 0;
// ARRFIND(constants[i] == MATH_E, i < ARRLEN(constants), ++i);
// if(i == ARRLEN(constants)) { /* statements for if not found */ }
// else { /* statements for if found */ };
#define ARRFIND(test, bounds, next) if(false) {} else { for(; bounds; next) if(test) break; };

#ifdef WIN32
#pragma warning( 3 : 4189 )       // local variable is initialized but not referenced
//#pragma comment(linker,"/OPT:NOWIN98")
#define strcasecmp _stricmp
#define strncasecmp _strnicmp
#define PATHDIV '\\'
#else
#define __cdecl
#define _vsnprintf vsnprintf
#define PATHDIV '/'
#endif


// easy safe strings

#define MAXSTRLEN 260
typedef char string[MAXSTRLEN];

inline void vformatstring(char *d, const char *fmt, va_list v, int len = MAXSTRLEN) { _vsnprintf(d, len, fmt, v); d[len-1] = 0; }
inline char *copystring(char *d, const char *s, size_t len = MAXSTRLEN)
{
    size_t slen = min(strlen(s)+1, len);
    memcpy(d, s, slen);
    d[slen-1] = 0;
    return d;
}
inline char *concatstring(char *d, const char *s, size_t len = MAXSTRLEN) { size_t used = strlen(d); return used < len ? copystring(d+used, s, len-used) : d; }

struct stringformatter
{
    char *buf;
    stringformatter(char *d): buf((char *)d) {}
    void operator()(const char *fmt, ...)
    {
        va_list v;
        va_start(v, fmt);
        vformatstring(buf, fmt, v);
        va_end(v);
    }
};

#define formatstring(d) stringformatter((char *)d)
#define defformatstring(d) string d; formatstring(d)
#define defvformatstring(d,last,fmt) string d; { va_list ap; va_start(ap, last); vformatstring(d, fmt, ap); va_end(ap); }

inline void zerostringmemory(char *d, size_t len = MAXSTRLEN)
{
    memset(d, 0, len);
}

// fast pentium f2i

// optional SSE2 and SSE3 paths
// if both are defined, SSE3 is used
//#define USE_SSE2
//#define USE_SSE3

// only use asm with MSVC and x86 (ia32)
#if defined(_MSC_VER) && defined(_M_IX86)
#if !defined(USE_SSE2) && !defined(USE_SSE3)
inline int fast_f2nat(float a) {        // only for positive floats
    static const float fhalf = 0.5f;
    int retval;
    
    __asm fld a
    __asm fsub fhalf
    __asm fistp retval      // perf regalloc?

    return retval;
};
#elif defined(USE_SSE3)     // SSE3 "Prescott New Instructions"
inline int fast_f2nat(float a) {
    int retval;
    __asm fld a
    __asm fisttp retval
    return retval;
};
#else                       // TODO: SSE2, or have the compiler figure it out with /arch:SSE2
#define fast_f2nat(val) ((int)(val))
#endif // endif USE_SSE2 or USE_SSE3
#else
#define fast_f2nat(val) ((int)(val))
#endif



extern char *path(char *s);
extern char *loadfile(const char *fn, int *size);
extern void endianswap(void *, int, int);

template <class T> struct vector
{
    static const int MINSIZE = 8;

    T *buf;
    int alen;   // allocated length (capacity)
    int ulen;   // elements in use (count)

    vector()
    {
        alen = 0;
        buf = NULL;
        ulen = 0;
    };

    vector(const vector &v)
    {
        alen = 0;
        buf = NULL;
        ulen = 0;
        *this = v;
    };

    ~vector() { shrink(0); if(buf) delete[] (uchar *)buf; };

    vector &operator=(const vector &v)
    {
        if(v.buf == buf) { alen = v.alen; ulen = v.ulen; return *this; };
        shrink(0);
        if(v.length() > alen) growbuf(v.length());
        loopv(v) add(v[i]);
        return *this;
    };

    // for fixing an unconstructed vector
    void recover() { alen = ulen = 0; buf = NULL; };

    // Grow the buffer to at least the size specified.
    void growbuf(int sz)
    {
        int newalen;
        // If alen is 0 or 1, set vector to a sensible size
        // Otherwise, grow vector at 1.5x rate
        if(alen == 0 || alen == 1)
        {
            newalen = max(MINSIZE, sz);
        }
        else
        {
            newalen = alen;
            while(newalen < sz) newalen += newalen/2;
        }
        if(newalen < alen) return;
        uchar *newbuf = new uchar[newalen*sizeof(T)];
        // Copy data to new buffer
        // alen is 0 for new vectors
        if(alen > 0)
        {
            memcpy(newbuf, buf, alen*sizeof(T));
            delete[] (uchar *)buf;
        }
        buf = (T *)newbuf;
        alen = newalen;
    };

    void reserve(int sz) { if(sz > capacity()) growbuf(sz); };

    T &add(const T &x)
    {
        if(ulen == alen) growbuf(ulen+1);
        new (&buf[ulen]) T(x);
        return buf[ulen++];
    };

    T &add()
    {
        if(ulen == alen) growbuf(ulen+1);
        new (&buf[ulen]) T;
        return buf[ulen++];
    };

    // A prior version of pop returned a reference to the original element and did not call the original element's destructor.
    // This version of pop returns a copy of the element and calls the original element's destructor, similar to the remove function (see below).
    T    pop()  { T e = buf[--ulen]; buf[ulen].~T(); return e; };
    void drop() { buf[--ulen].~T(); };

    void shrink(int i)  { assert(i<=ulen); while(ulen>i) drop(); };

    // Remove elements from the vector without calling each element's destructor
    void setsize(int i) { assert(i<=ulen); ulen = i; };

    T       &operator[](int i)       { assert(i>=0 && i<ulen); return buf[i]; };
    const T &operator[](int i) const { assert(i>=0 && i<ulen); return buf[i]; };
    T       &last()                  { return buf[ulen-1]; };
    const T &last() const            { return buf[ulen-1]; };

    bool empty() const    { return ulen == 0; };
    int  length() const   { return ulen; };
    int  capacity() const { return alen; };

    T       *getbuf()       { return buf; };
    const T *getbuf() const { return buf; };

    void push_back(const T &x) { add(x); };
    void pop_back()            { drop(); };

    void sort(void *cf) { qsort(buf, ulen, sizeof(T), (int (__cdecl *)(const void *,const void *))cf); };

    // Remove the element at the specified index
    // and the count-1 elements following it.
    void remove(int i, int count)
    {
        for(int p = i; p<i+count; p++)
            buf[p].~T();
        for(int p = i+count; p<ulen; p++)
            buf[p-count] = buf[count];
        ulen -= count;
    };

    T remove(int i)
    {
        T e = buf[i];
        buf[i].~T();
        for(int p = i+1; p<ulen; p++) buf[p-1] = buf[p];
        ulen--;
        return e;
    };

    // Remove the element at the specified index
    // and efficiently fill the vacated space
    // by moving the last element in the vector.
    // This could break the ordering of a sorted vector.
    T removeunordered(int i)
    {
        T e = buf[i];
        buf[i].~T();
        ulen--;
        if(ulen>0) buf[i] = buf[ulen];
        return e;
    };

    T &insert(int i, const T &e)
    {
        add(T());
        for(int p = ulen-1; p>i; p--) buf[p] = buf[p-1];
        buf[i] = e;
        return buf[i];
    };
};

template <class T> struct hashtable
{
    struct chain { chain *next; const char *key; T data; };

    int size;
    int numelems;
    chain **table;
    chain *enumc;

    hashtable()
    {
        this->size = 1<<10;
        numelems = 0;
        table = new chain*[size];
        for(int i = 0; i<size; i++) table[i] = NULL;
    };

    hashtable(hashtable<T> &v);
    void operator=(hashtable<T> &v);

    // Retrieve a pointer to the hashtable's copy of data under the given arg 'key'.
    // Optionally, this access can also update hashtable's copy of data for this key.
    // If updating, new information is copied from location pointed to by arg 'data'.
    // The returned pointer is to hashtable's copy of the data, so de-ref will produce the updated data not the prior data.
    // If 'key' didn't match any hashtable entry, then store 'key' and store a copy of 'data' in hashtable, and return NULL.
    // Note: hashtable's key is the same object as the key used when creating the hashtable entry.
    // The key object used for creating the entry must have a lifetime as long as the hashtable containing the entry.
    T *access(const char *key, T *data = NULL)
    {
        unsigned int h = 5381;
        for(int i = 0, k; k = key[i]; i++) h = ((h<<5)+h)^k;    // bernstein k=33 xor
        h = h&(size-1);                                         // primes not much of an advantage
        for(chain *c = table[h]; c; c = c->next)
        {
            char ch;
            for(const char *p1 = key, *p2 = c->key; (ch = *p1++)==*p2++; ) if(!ch)    //if(strcmp(key,c->key)==0)
            {
                T *d = &c->data;
                if(data) c->data = *data;
                return d;
            };
        };
        if(data)
        {
            chain *c = new chain;
            c->data = *data;
            c->key = key;
            c->next = table[h];
            table[h] = c;
            numelems++;
        };
        return NULL;
    };
};


#define enumerate(ht,t,e,b) loopi(ht->size) for(ht->enumc = ht->table[i]; ht->enumc; ht->enumc = ht->enumc->next) { t e = &ht->enumc->data; b; }

inline char *newstring(size_t l)                 { return new char[l+1];                    };
inline char *newstring(const char *s, size_t l)  { return copystring(newstring(l), s, l+1); };
inline char *newstring(const char *s)            { return newstring(s, strlen(s));          };
inline char *newstringbuf(const char *s)         { return newstring(s, MAXSTRLEN-1);        };

#if defined(_WINDOWS) && !defined(__GNUC__)
#define MEMORY_ALIGNED(type, bytes) __declspec(align(bytes)) type
#else
#ifdef __GNUC__
#define MEMORY_ALIGNED(type, bytes) type __attribute__((aligned(bytes)))
#else
#define MEMORY_ALIGNED(type, bytes) type
#endif //ifdef __GNUC__
#endif //if defined(_WINDOWS) && !defined(__GNUC__)

#include "tools_new.h"

#endif

