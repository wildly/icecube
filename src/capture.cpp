// Screenshot and video capture
// Screenshot functions from Sauerbraten Collect Edition's texture.cpp
// Movie recorder design/functions is/are a barebones version modified from Tesseract's movie.cpp

// Portions from Sauerbraten are: Copyright (C) 2001-2012 Wouter van Oortmerssen, Lee Salzman, Mike Dysart, Robert Pointon, and Quinton Reeves
// See LICENSE-Sauerbraten.txt for the Sauerbraten source code license

// Portions from Tesseract are: Copyright (C) 2001-2020 Wouter van Oortmerssen, Lee Salzman, Mike Dysart, Robert Pointon, Quinton Reeves, and Benjamin Segovia
// See LICENSE-Tesseract.txt for the Tesseract source code license

#include "cube.h"
#include "texture.h"
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <time.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../3rdparty/stb/stb_image_write.h"

#include "../3rdparty/jo/jo_mpeg.cpp"

// Screenshot functions, from Sauerbraten, modified for IceCube

enum
{
    IMG_BMP = 0,
    IMG_TGA = 1,
    IMG_PNG = 2,
    IMG_JPG = 3,
    IMG_WEBP = 4,
    IMG_HEIC = 5,
    IMG_AVIF = 6,
    IMG_JPEG_XL = 7,
    NUMIMG
};

VARP(screenshotformat, 0, IMG_JPG, NUMIMG-1);

const char *imageexts[NUMIMG] = { ".bmp", ".tga", ".png", ".jpg", ".webp", ".heic", ".avif", ".jxl" };

const int JPG_QUALITY = 95;
const int WEBP_QUALITY = 95;
const int HEIC_QUALITY = 95;
const int AVIF_QUALITY = 95;

// Choose whether to use SDL2 Image for writing JPG
#define USE_SDL2_IMG_JPG

// saveimage has been modified to use SDL functions instead of Sauerbraten's custom stream functions,
// and uses a different implementation of appending imageexts to the filename.
// Writing TGA, JPG, PNG uses stb_image_write.
// It's also possible to write JPG using SDL Image 2.0, which is how Tesseract does it.
// TODO: Writing WEBP isn't implemented yet.
// TODO: Writing HEIC isn't implemented yet.
// TODO: Writing AVIF isn't implemented yet.
// TODO: Writing JPEG XL isn't implemented yet.
void screenshot();
void saveimage_with_sdl(const char *filename, int format, ImageData &image, bool flip = false)
{
    ImageData flipped(image.w, image.h, image.bpp, image.data);
    if(flip) texflip(flipped);
    SDL_Surface *s = wrapsurface(flipped.data, flipped.w, flipped.h, flipped.bpp);
    if(!s) return;
    SDL_RWops *f = SDL_RWFromFile(filename, "wb");
    if(f)
    {
        if(format == IMG_JPG)
            IMG_SaveJPG_RW(s, f, 1, JPG_QUALITY);
        else
            SDL_SaveBMP_RW(s, f, 1);
    }
    SDL_FreeSurface(s);
}
void saveimage(const char *filename, int format, ImageData &image, bool flip = false)
{
    // Assume we're writing using a stb image write function and configure its flip flag
    if(flip) stbi_flip_vertically_on_write(1);
    else stbi_flip_vertically_on_write(0);

    switch(format)
    {
        case IMG_PNG:
            stbi_write_png(filename, image.w, image.h, 3, image.data, image.w*image.bpp);
            break;
        case IMG_TGA:
            stbi_write_tga(filename, image.w, image.h, 3, image.data);
            break;
        case IMG_JPG:
#if defined(USE_SDL2_IMG_JPG)
            saveimage_with_sdl(filename, IMG_JPG, image, flip);
#else
            stbi_write_jpg(filename, image.w, image.h, 3, image.data, JPG_QUALITY);
#endif
            break;
        case IMG_WEBP:
        case IMG_HEIC:
        case IMG_AVIF:
        case IMG_JPEG_XL:
            conoutf("saveimage: %s not supported, switching to .jpg", imageexts[format]);
            screenshotformat = IMG_JPG;
            screenshot(); // be careful not to infinite loop; we have to call screenshot again to regenerate the filename with the correct file extension
            break;
        default: // IMG_BMP
        {
            saveimage_with_sdl(filename, IMG_BMP, image, flip);
            break;
        }
    }
}

// screen is defined in main.cpp
extern SDL_Window *screen;

// Notes for taking a screenshot:
// Reads pixels from the back buffer (GL_BACK).
// Depends on swap buffers being a copy and not a true swap?
// Can only be used before gl_drawframe()?

// This is a less configuable version of the screenshot function from Sauerbraten.
// Description of changes: The directory and file name is hardcoded, other misc changes.
void screenshot()
{
    int screenW, screenH;
    SDL_GL_GetDrawableSize(screen, &screenW, &screenH);

    defformatstring(filename)("screenshots/screenshot_%d-%d", (int)time(NULL), lastmillis);
    defformatstring(filename2)("screenshots/screenshot_%d-%d-flip", (int)time(NULL), lastmillis);

    // append file extension
    int format = screenshotformat;
    concatstring(filename, imageexts[format]);
    concatstring(filename2, imageexts[format]);

    ImageData image(screenW, screenH, 3);
    glPixelStorei(GL_PACK_ALIGNMENT, texalign(image.data, screenW, 3));
    glReadPixels(0, 0, screenW, screenH, GL_RGB, GL_UNSIGNED_BYTE, image.data);

    // We need to flip the image (can comment/uncomment the lines below to check)
    // We don't want to leave "flip" in the filename, so filename/filename2 are used swapped here
    //saveimage(filename2, format, image, false);
    saveimage(filename, format, image, true);
}

COMMAND(screenshot, ARG_NONE);

int video_length_msec;
int VideoSegmentMsecLimit = 1000*60*10; // 10 minutes

// When using jo_mpeg:
// only support 24, 25, 30, 50, or 60 fps
// 24 fps = 41.67 msec
// 25 fps = 40 msec
// 30 fps = 33.33 msec
// 50 fps = 20 msec
// 60 fps = 16.67 msec
// (unsupported fps are 29.97, 59.94)
int nearest_fps(int millis)
{
    if(millis <= 18) return 60;
    else if(millis <= 26) return 50;
    else if(millis <= 36) return 30;
    else if(millis <= 40) return 25;
    else return 24;
}

// Scale width and height by moviescale% before encoding the frame.
// If the full resolution divides evenly by 50%, scaleimage will use the halvetexture function.
VARP(moviescale, 10, 100, 100);

// Encoder threads (TODO)
// "0" means encoder is on main thread.
// Will try to use up to that many threads, when supported.
// This setting is checked when the recorder starts.
VARP(encoderthreads, 0, 1, 10);

// Audio capturing pre-declarations
void startifnotaudiocapturing();
void checkaudiocapture();
void stopifaudiocapturing();

// Movie recorder design and certain selected functions based on src/engine/movie.cpp from Tesseract
// Modifications include:
// - writes MPEG-1/2 via jo_mpeg (instead of writing AVI),
// - uses one encoding thread and mutex, but no semaphores,
// - uses a single ImageData (allocated with new) instead of a buffer/queue, and
// - "other changes" that make this a barebones version compared to the one in Tesseract.
// TODO: Movie file seems to have no timecode or wrong duration in certain players?
// TODO: Record sound in a separate file? (MP3?)
//
// When writing MPEG-1/2:
// The max picture width and height is 4095x4095.
namespace recorder
{
    FILE *file = NULL;
    int moview, movieh;

    ImageData *image = NULL;
    int fps = 0;

    SDL_Thread *thread = NULL;
    SDL_mutex *videolock = NULL;
    const char *encoderthreadname = "Video Encoder";
    int recorderthreads = 0;

    bool isrecording() { return file != NULL; }
    int videoencoder(void *data)
    {
        SDL_LockMutex(videolock);
        jo_write_mpeg(file, (unsigned char *)data, moview, movieh, fps);
        delete image;
        SDL_UnlockMutex(videolock);
        return 0;
    }
    void readbuffer()
    {
        // if there is an off-main-thread encoder, wait for it to finish
        if(thread)
        {
            SDL_WaitThread(thread, NULL); // block until thread is finished
            thread = NULL;
        }
        // acquire mutex
        SDL_LockMutex(videolock);
        // allocate buffer
        int screenW, screenH;
        SDL_GL_GetDrawableSize(screen, &screenW, &screenH);
        image = new ImageData(screenW, screenH, 4);
        // read screen into buffer
        glPixelStorei(GL_PACK_ALIGNMENT, texalign(image->data, screenW, 4));
        glReadPixels(0, 0, screenW, screenH, GL_RGBA, GL_UNSIGNED_BYTE, image->data);
        // set fps
        fps = nearest_fps(lastmillis);
        // set output resolution
        moview = screenW*moviescale/100;
        movieh = screenH*moviescale/100;
        // scale and flip
        scaleimage(*image, moview, movieh);
        texflip(*image);
        // release mutex
        SDL_UnlockMutex(videolock);
        // call encoder function if encoder is on main thread
        if(recorderthreads == 0) videoencoder(image->data);
        // otherwise, create off-main-thread encoder to encode this frame
        else
            thread = SDL_CreateThread(videoencoder, encoderthreadname, image->data);
    }
    void start(const char *name, bool withsound)
    {
        // Save to "movie" directory
        defformatstring(filenameandpath)("movie/%s-%d.mpg", name, (int)time(NULL));
        file = fopen(filenameandpath, "wb");

        // Set up mutex
        videolock = SDL_CreateMutex();

        // Configure the number of encoding threads
        recorderthreads = encoderthreads;

        startifnotaudiocapturing();
    }
    void stop()
    {
        // do this first to give movie encoding thread more time to finish
        stopifaudiocapturing();

        if(thread)
        {
            SDL_WaitThread(thread, NULL); // block until thread is finished
            thread = NULL;
        }

        // clean up mutex
        SDL_DestroyMutex(videolock);

        fclose(file);
        file = NULL;
    }
    void check()
    {
        if(recorder::isrecording()) readbuffer();
        checkaudiocapture();
    }
}

void movie(char *name)
{
    if(name[0] == '\0')
    {
        if(recorder::isrecording()) recorder::stop();
        else recorder::start("movie", false);
    }
    else if(!recorder::isrecording()) recorder::start(name, false);
}
COMMAND(movie, ARG_1STR);

void checkmovie()
{
    recorder::check();
}

void stopmovie()
{
    if(recorder::isrecording()) recorder::stop();
    stopifaudiocapturing();
}

// At this time, we generate separate movie and audio output files.
// It is left as an exercise for the user to properly mux them together and place into an appropriate container.

// SDL 2.0.7: SDL_AudioStream
// We will copy the SDL_mixer output to (SDL_AudioStream) mixercopy, then send one "unit" of audio data from mixercopy to the selected encoder.
// Note: SDL_AudioStream is opaque.
SDL_AudioStream *mixercopy = NULL;

void audiostream_init()
{
    int mixfreq;
    Uint16 mixfmt;
    int mixchannels; // assumed to be stereo
    Mix_QuerySpec(&mixfreq, &mixfmt, &mixchannels);

    if(!mixercopy)
        mixercopy = SDL_NewAudioStream(mixfmt, mixchannels, mixfreq, AUDIO_F32, 2, 48000);
}

void audiostream_destroy()
{
    if(mixercopy)
    {
        SDL_FreeAudioStream(mixercopy);
        mixercopy = NULL;
    }
}

const int debugaudiocapture = 0;
void copysound(void *udata, Uint8 *stream, int len)
{
    if(debugaudiocapture) conoutf("copysound,etime:%d,len:%d)", lastmillis, len);
    SDL_AudioStreamPut(mixercopy, stream, len);
}

// Audio capture / encoders

//#define USE_OPUS_ENC
#if defined(USE_OPUS_ENC)
void opus_enc_setup();
void opus_enc_update(bool flush);
void opus_enc_teardown();
#else
void opus_enc_setup() {};
void opus_enc_update(bool flush) {};
void opus_enc_teardown() {};
#endif

void startaudiocapture()
{
    audiostream_init();
    Mix_SetPostMix(copysound, NULL);
    opus_enc_setup();
}

void stopaudiocapture()
{
    Mix_SetPostMix(NULL, NULL);
    opus_enc_update(true); // get last bytes from audiostream
    opus_enc_teardown();
    audiostream_destroy();
}

int audiocapturing = 0;
void audiocapture()
{
    if(!audiocapturing)
    {
        startaudiocapture();
        audiocapturing = 1;
    }
    else
    {
        stopaudiocapture();
        audiocapturing = 0;
    }
}
COMMAND(audiocapture, ARG_NONE);

// gracefully finish capturing on quit
void stopifaudiocapturing()
{
    if(audiocapturing) audiocapture();
}

// start audio capturing when movie starts if not already audio capturing
void startifnotaudiocapturing()
{
    if(!audiocapturing) audiocapture();
}

// periodically service opus encoder
void checkaudiocapture()
{
    if(audiocapturing) opus_enc_update(false);
}

#if defined(USE_OPUS_ENC)
// Opus encoder
// Note: Opus decoding is available through SDL_mixer, so we don't need to handle decoding here.

// Some of the source code is adapted from 'examples/opusenc_example.c' at https://gitlab.xiph.org/xiph/libopusenc
// Some of the source code is adapted from example code at https://wiki.libsdl.org/Tutorials/AudioStream

#include <opusenc.h>

OggOpusEnc *opusenc = NULL;
OggOpusComments *comments = NULL;

void opus_enc_setup()
{
        // Save to "movie" directory
        defformatstring(filenameandpath)("movie/audio-%d.opus", (int)time(NULL));

        // Create Opus Encoder
        comments = ope_comments_create();
        opusenc = ope_encoder_create_file(filenameandpath, comments, 48000, 2, 0, NULL);
}

void opus_enc_update(bool flush)
{
        if(!opusenc) { conoutf("opus_enc_update: no opusenc!"); return; }

        for(;;)
        {
            const int READ_SIZE = 256;
            const int neededBytesLen =  2*READ_SIZE*sizeof(float);
            float converted[2*READ_SIZE];
            int availableLen = SDL_AudioStreamAvailable(mixercopy);
            if(availableLen < neededBytesLen)
            {
                if(flush == false) return; // wait until we have READ_SIZE samples times the number of channels before sending it to the Opus encoder
                SDL_AudioStreamFlush(mixercopy);
                int gotten = SDL_AudioStreamGet(mixercopy, converted, sizeof(converted));
                ope_encoder_write_float(opusenc, converted, gotten/sizeof(float)/2); // 'number of bytes' / sizeof(float) / '2 channels'
                return;
            }

            int gotten = SDL_AudioStreamGet(mixercopy, converted, sizeof(converted));
            ope_encoder_write_float(opusenc, converted, READ_SIZE);
        }
}

void opus_enc_teardown()
{
        ope_encoder_drain(opusenc);
        ope_comments_destroy(comments);
        ope_encoder_destroy(opusenc);
        opusenc = NULL;
        comments = NULL;
}
#endif
