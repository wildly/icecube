// Copyright (c) 2001-2012 Cube authors, Sauerbraten authors, IceCube authors

// main.cpp: initialisation & main loop

#include "cube.h"
#include "benchmark.h"
#include "gamestate.h"
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_filesystem.h>

#if _MSC_VER >= 1900
#ifdef main
#undef main
#endif
#endif

#ifdef WIN32
const bool swap_buffers_copy_preserve = true;
#else
const bool swap_buffers_copy_preserve = false;
#endif

void cleanup(char *msg)         // single program exit point;
{
	stop();
    stopmovie();
    disconnect(true);
    cleangl();
    cleansound();
    cleanupserver();
    SDL_ShowCursor(1);
    if(msg)
    {
        #ifdef WIN32
        MessageBox(NULL, msg, "cube fatal error", MB_OK|MB_SYSTEMMODAL);
        #else
        printf("%s", msg);
        #endif
    };
    SDL_Quit();
    exit(1);
};

void quit()                     // normal exit
{
    writecfg();
    writeservercfg();
    cleanup(NULL);
};

void fatal(const char *s, const char *o)    // failure exit
{
    defformatstring(msg)("%s%s (%s)\n", s, o, SDL_GetError());
    cleanup(msg);
};

// for some big chunks... most other allocs use the memory pool
// update: memory pool has been removed; those allocs now use the default memory allocator
void *alloc(int s)
{
    void *b = calloc(1,s);
    if(!b) fatal("out of memory!");
    return b;
};

SDL_Window *screen = NULL;
SDL_GLContext context = NULL;

#define SCR_MINW 320
#define SCR_MINH 200
#define SCR_MAXW 10000
#define SCR_MAXH 10000
#define SCR_DEFAULTW 640
#define SCR_DEFAULTH 480

int scr_w = 640;
int scr_h = 480;
VAR(vsync, 1, 0, 0); // TODO: Change to to (vsync, -1, -1, 1) when vsync is configurable within engine after startup

COMMAND(quit, ARG_NONE);

// Does nothing in SDL 2.0. (Check the repeat flag instead.)
void keyrepeat(bool on)
{
#if SDL_VERSION_ATLEAST(2, 0, 0)
#else
    SDL_EnableKeyRepeat(on ? SDL_DEFAULT_REPEAT_DELAY : 0,
                             SDL_DEFAULT_REPEAT_INTERVAL);
#endif
};

// Note: minillis and deferupdatemillis must not be less than 0
VARP(minmillis, 0, 5, 2000);
VARP(deferupdatemillis, 0, 0, 100);

static bool minimized = false;

// checkgrab has two members "mousefocus" and "inputfocus" which track whether
// the application should have mouse and keyboard focus.
//
// checkgrab() returns whether or not input should be grabbed.
//
// checkgrab.set(bool setmousefocus, bool setinputfocus) sets mousefocus and
// inputfocus to the respective values.
//
// If only one of mousefocus or keyboardfocus needs to be set, their values
// can be directly changed using conventional structure member access.
struct
{
    bool mousefocus, inputfocus, acceptsfocus;
    bool operator()() { return mousefocus && inputfocus && acceptsfocus; };
    void set(bool setmousefocus, bool setinputfocus, bool setacceptsfocus)
    {
        mousefocus = setmousefocus;
        inputfocus = setinputfocus;
        acceptsfocus = setacceptsfocus;
    };
} checkgrab = { false, false, true };

void inputgrab(bool on)
{
#if SDL_VERSION_ATLEAST(2, 0, 0)
    SDL_SetRelativeMouseMode(on ? SDL_TRUE : SDL_FALSE);
#else
    SDL_WM_GrabInput(on ? SDL_GRAB_ON : SDL_GRAB_OFF);
#endif
    SDL_ShowCursor(on ? SDL_DISABLE : SDL_ENABLE);
}

// Release input focus (mouse & keyboard) when windowed.
//
// Primarily intended for Linux, on which users cannot take
// focus away from the game while it is running.
// This command does nothing in fullscreen because fullscreen
// without input focus makes no sense.
void releasefocus()
{
#if SDL_VERSION_ATLEAST(2, 0, 0)
    uint windowFlags = SDL_GetWindowFlags(screen);
    if(windowFlags&(SDL_WINDOW_FULLSCREEN|SDL_WINDOW_FULLSCREEN_DESKTOP))
        return;
#else
    if(screen->flags&SDL_FULLSCREEN)
        return;
#endif

    checkgrab.set(false, false, false);
    inputgrab(checkgrab());
};

COMMAND(releasefocus, ARG_NONE);

bool fullscreen = true;
int depthbits = 0;
int msaa = 0;

void setmsaa(int samples)
{
    if(samples == 2 || samples == 4 || samples == 8 || samples == 16)
        msaa = samples;
    else if(samples < 2) msaa = 0;
    else if(samples < 4) msaa = 2;
    else if(samples < 8) msaa = 4;
    else if(samples < 16) msaa = 8;
    else msaa = 16;
};

void resizescreen(int w, int h)
{
    SDL_GL_GetDrawableSize(screen, &scr_w, &scr_h);
    glViewport(0, 0, scr_w, scr_h);
}

void screeninfo()
{
    SDL_GL_GetDrawableSize(screen, &scr_w, &scr_h);
    SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &depthbits);
    SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, &msaa);
#if SDL_VERSION_ATLEAST(2, 0, 0)
    vsync = SDL_GL_GetSwapInterval();
#elif SDL_VERSION_ATLEAST(1, 2, 11)
    SDL_GL_GetAttribute(SDL_GL_SWAP_CONTROL, &vsync);
#endif
};

void setupscreen(const char *windowTitle)
{
    uint wflags = fullscreen ? SDL_WINDOW_FULLSCREEN : SDL_WINDOW_RESIZABLE;
    //uint rflags = 0;

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    if(depthbits>0) SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, depthbits);
#if SDL_VERSION_ATLEAST(2, 0, 0)
    //if(vsync>=0) rflags |= SDL_RENDERER_PRESENTVSYNC;
#elif SDL_VERSION_ATLEAST(1, 2, 11)
    if(vsync>=0) SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, vsync);
#endif

    // Multisample AA (a form of full screen anti-aliasing)
    if(msaa)
    {
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, msaa);
    };

    // OpenGL context profile
    // Can be: SDL_GL_CONTEXT_PROFILE_CORE, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY, or SDL_GL_CONTEXT_PROFILE_ES
    //SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    // Also need to specify a version
    //SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    //SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

    screen = SDL_CreateWindow(windowTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, scr_w, scr_h, SDL_WINDOW_OPENGL|wflags);
    context = SDL_GL_CreateContext(screen);
    if(!screen || !context) fatal("Unable to create OpenGL screen");

#if SDL_VERSION_ATLEAST(2, 0, 0)
    if(vsync>=0) SDL_GL_SetSwapInterval(vsync);
#endif
}

void checkinput()
{
    SDL_Event event;
    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_QUIT:
                quit();
                break;

            case SDL_WINDOWEVENT:
                switch(event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    quit();
                    break;

                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    resizescreen(event.window.data1, event.window.data2);
                    break;

                case SDL_WINDOWEVENT_HIDDEN:
                case SDL_WINDOWEVENT_MINIMIZED:
                    minimized = true;
                    inputgrab(checkgrab());
                    break;

                case SDL_WINDOWEVENT_SHOWN:
                case SDL_WINDOWEVENT_EXPOSED:
                case SDL_WINDOWEVENT_RESTORED:
                case SDL_WINDOWEVENT_MAXIMIZED:
                case SDL_WINDOWEVENT_TAKE_FOCUS:
                    minimized = false;
                    inputgrab(checkgrab());
                    break;
                };
                break;

            case SDL_KEYDOWN:
            case SDL_KEYUP:
                keypress(event.key.keysym.sym, event.key.state==SDL_PRESSED, event.key.repeat, event.key.keysym.sym);
                latency_set_reaction_state(event.key.state==SDL_PRESSED); // latency test
                if(!(event.key.repeat) && event.key.state==SDL_PRESSED) latency_play_reaction_sound();
                break;

            case SDL_TEXTINPUT:
                textinput(event.text.text);
                break;

            case SDL_MOUSEMOTION:
                if(!checkgrab()) break;
                mousemove(event.motion.xrel, event.motion.yrel);
                break;

            // SDL2: Mouse wheel - see data/keymap.cfg
            case SDL_MOUSEWHEEL:
                if(event.wheel.y > 0)
                    keypress(-50, true, false, 0);
                else if(event.wheel.y < 0)
                    keypress(-51, true, false, 0);
                if(event.wheel.x > 0)
                    keypress(-52, true, false, 0);
                else if(event.wheel.x < 0)
                    keypress(-53, true, false, 0);
                break;

            // Mouse button presses are sent for processing as though keyboard symbols,
            // substituting a negative MouseButtonEvent id for keyboard symbol id.
            case SDL_MOUSEBUTTONDOWN:
                checkgrab.set(true, true, true); // to regain focus after releasefocus command
                inputgrab(checkgrab());
            case SDL_MOUSEBUTTONUP:
                keypress(-event.button.button, event.button.state!=SDL_RELEASED, 0);
                latency_set_reaction_state(event.button.state!=SDL_RELEASED); // latency test
                if(event.button.state!=SDL_RELEASED) latency_play_reaction_sound();
                break;

            // Joystick buttons are sent for processing as though keyboard symbols,
            // substituting a negative JoyButtonEvent id offset by -100 for keyboard symbol id.
            case SDL_JOYBUTTONDOWN:
            case SDL_JOYBUTTONUP:
                extern int debugjoystick;
                if(debugjoystick)
                    conoutf("jbutton %d: %d", -100-event.jbutton.button, event.jbutton.state==SDL_RELEASED? 0 : 1);
                keypress(-100-event.jbutton.button, event.jbutton.state!=SDL_RELEASED, 0);
                break;

            case SDL_JOYAXISMOTION:
                joyaxismotion(event.jaxis.which, event.jaxis.axis, event.jaxis.value);
                break;

            case SDL_JOYHATMOTION:
                joyhatmotion(event.jhat.which, event.jhat.hat, event.jhat.value);
                break;
        };
    };
};

#define log(s, ...) conoutf("init: " s, ##__VA_ARGS__)

static bool findarg(int argc, char **argv, const char *str)
{
    for(int i = 1; i<argc; i++) if(strstr(argv[i], str)==argv[i]) return true;
    return false;
}

struct MainLoopController
{
    //Uint32 lastupdate; // wall clock timestamp of most recent world simulation update
    Uint32 lastrender; // Wall clock timestamp of most recent render frame
    bool noDelay;      // e.g. game start or map start
    void init() { noDelay = true; lastrender = SDL_GetTicks(); };
    Uint32 calcDelay();
    bool minimizedRenderNeeded(const Uint32 period)
    {
        Uint32 renderUpdateDiff = SDL_GetTicks() - lastrender;
        if(renderUpdateDiff >= period)
            return true;
        else
            return false;
    };
} mainLoopController;

void mainloop_mapchanged_nodelay()
{
    mainLoopController.noDelay = true;
};

extern float totalmillis;
uint lastrealtime;
int getmillis(uint realtime)
{
    totalmillis += (realtime-lastrealtime) * (float)getgamespeed()/100.0f;
    lastrealtime = realtime;

    return (int)totalmillis;
};

Uint32 MainLoopController::calcDelay()
{
    if(mainLoopController.noDelay)
    {
        mainLoopController.noDelay = false;
        return 0;
    };

    Uint32 deferupdateworld = 0;
    Uint32 worldUpdateDiff = SDL_GetTicks() - (Uint32)lastrealtime;

    // The difference between two getmillis() timestamps should be at least minmillis
    // This is calculated as SDL_GetTicks() - [previous getmillis()].
    if(worldUpdateDiff < (Uint32)minmillis)
        deferupdateworld = minmillis-worldUpdateDiff;

    // In case there was a frame that took abnormally long to generate, there is a configurable floor for the delay.
    // This should prevent the frame (with typical characteristics) after that outlier from generating too quickly.
    deferupdateworld = max(deferupdateworld, (Uint32)deferupdatemillis);
    deferupdateworld = clamp(deferupdateworld, 0, 2000);

    return deferupdateworld;
};

void quickStartup()
{
    int millis = getmillis(SDL_GetTicks());

    // client -> server (client sends toservermap)
    updateworld(millis);

    // server -> client (receive mapchanged information from server)
    serverslice(millis, 0);

    // 1/50 second delay before the first real mainloop update
    //SDL_Delay(1000/50);
};

int main(int argc, char **argv)
{
    bool dedicated = false;
    int uprate = 0, maxcl = 4;
    char *sdesc = "", *ip = "", *master = NULL, *passwd = "";
    int enablejoystick = true;

    log("args");

    for(int i = 1; i<argc; i++)
    {
        char *a = &argv[i][2];
        if(argv[i][0]=='-') switch(argv[i][1])
        {
            case 'g': if(a[0] == 'c') classicRuleset = true; else if(a[0] == 'i') classicRuleset = false; break;
            case 'd': dedicated = true; break;
            case 't': fullscreen = (a[0] == '\0') ? false : (atoi(a) == 0); break;
            case 'w': if(findarg(argc, argv, "-h")) scr_w = clamp(atoi(a), SCR_MINW, SCR_MAXW); break;
            case 'h': if(findarg(argc, argv, "-w")) scr_h = clamp(atoi(a), SCR_MINH, SCR_MAXH); break;
            case 'v': vsync  = (a[0] == '\0') ? 1 : atoi(a); break;
            case 'z': depthbits = atoi(a); break;
            case 'a': setmsaa(atoi(a)); break;
            case 'j': enablejoystick = (a[0] == '\0') ? 1 : (atoi(a)); break;
            case 'u': uprate = atoi(a); break;
            case 'n': sdesc  = a; break;
            case 'i': ip     = a; break;
            case 'm': master = a; break;
            case 'p': passwd = a; break;
            case 'c': maxcl  = atoi(a); break;
            default:  conoutf("unknown commandline option");
        }
        else conoutf("unknown commandline argument");
    };

    log("sdl");
    if(SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO|SDL_INIT_NOPARACHUTE)<0) fatal("Unable to initialize SDL");

    SDL_version v;
    SDL_GetVersion(&v);
    log("Library: SDL %u.%u.%u", v.major, v.minor, v.patch);
    const SDL_version *vi = IMG_Linked_Version();
    log("Library: SDL Image %u.%u.%u", vi->major, vi->minor, vi->patch);
    const SDL_version *vm = Mix_Linked_Version();
    log("Library: SDL Mixer %u.%u.%u", vm->major, vm->minor, vm->patch);

    log("clockcore");
    constructClocks();
    lastrealtime = SDL_GetTicks();

    log("net");
    if(enet_initialize()<0) fatal("Unable to initialise network module");
    log("Library: ENet %010X", (unsigned int)enet_linked_version());

    initclient();
    initserver(dedicated, uprate, sdesc, ip, master, passwd, maxcl);  // never returns if dedicated

    log("gameclient");
    // Dedicated server uses priority increase specified in initserver() and never reaches here
    init_speedy_client();
    mainLoopController.init();
    perfcounters_init();
    director.log_event(FRAMESTART_EV, SDL_GetTicks());
    director.log_event(NONE_ST, SDL_GetTicks());

    log("input");
    keyrepeat(false);
    checkgrab.set(true, true, true);
    inputgrab(checkgrab());
    initconsole();

    if(enablejoystick == 0)
    {
        log("Skipping joystick initialization");
    }
    else if(SDL_InitSubSystem(SDL_INIT_JOYSTICK) < 0)
    {
        log("Unable to initialize joystick subsystem");
    }
    else
    {
        log("input: Checking for joysticks and game controllers...");
        if(SDL_NumJoysticks() > 0)
        {
            log("Detected %d device(s):", SDL_NumJoysticks());
            loopi(SDL_NumJoysticks())
            {
                SDL_Joystick *joy = SDL_JoystickOpen(i);
                if(!joy)
                {
                    log(" -- Couldn't open device at index %d!", i);
                    continue;
                };
                controllers.add(joy);
                log(" -- Opened device at index %d: %s", i, SDL_JoystickName(joy));
                log(" -- %d Buttons, %d Axes, %d Balls, %d Hats", SDL_JoystickNumButtons(joy), SDL_JoystickNumAxes(joy), SDL_JoystickNumBalls(joy), SDL_JoystickNumHats(joy));
            };
        };
        log("input: Joystick event polling is currently %s", SDL_JoystickEventState(SDL_QUERY) == SDL_ENABLE ? "enabled" : "ignored");
    };

    log("world");
    director.log_event(UPDATE_ST, SDL_GetTicks());
    empty_world(7, true);

    log("video");
    director.log_event(NONE_ST, SDL_GetTicks());
    if(SDL_InitSubSystem(SDL_INIT_VIDEO)<0) fatal("Unable to initialize SDL video subsystem");
    setupscreen("cube engine");
    log(" -- resolution requested: %dx%d", scr_w, scr_h);
    int requestedVsync = vsync;
    screeninfo();
    log(" -- resolution received: %dx%d", scr_w, scr_h);
    defformatstring(vsync_info)("vsync is %s", vsync == 0 ? "off" : "on");
    if(requestedVsync != -1)
    {
        defformatstring(vsync_req)(" (vsync mode requested: %d)", requestedVsync);
        concatstring(vsync_info, vsync_req);
    };
    log(" -- %s", vsync_info);
    log(" -- depth buffer is %d bits", depthbits);
    if(msaa)
    {
        log(" -- multisample AA is on (%d samples)", msaa);
    };

/*
    log("filesystem");
    char *base_path;
    base_path = SDL_GetBasePath();
    log("base directory is: %s", base_path);
    SDL_free(base_path);
    base_path = NULL;
*/

    log("cfg");
    initmenus();
    exec("data/keymap.cfg");
    exec("data/menus.cfg");
    exec("data/prefabs.cfg");
    exec("data/sounds.cfg");
    exec("servers.cfg");
    if(!execfile("config.cfg")) execfile("data/defaults.cfg");
    exec("autoexec.cfg");

    log("gl");
    gl_init(scr_w, scr_h);

    log("basetex");
    int xs, ys;
    if(!installtex(2,  path(newstring("data/newchars.png")), xs, ys) ||
       !installtex(3,  path(newstring("data/martin/base.png")), xs, ys) ||
       !installtex(6,  path(newstring("data/martin/ball1.png")), xs, ys) ||
       !installtex(7,  path(newstring("data/martin/smoke.png")), xs, ys) ||
       !installtex(8,  path(newstring("data/martin/ball2.png")), xs, ys) ||
       !installtex(9,  path(newstring("data/martin/ball3.png")), xs, ys) ||
       !installtex(4,  path(newstring("data/explosion.jpg")), xs, ys) ||
       !installtex(5,  path(newstring("data/items.png")), xs, ys) ||
       !installtex(1,  path(newstring("data/crosshair.png")), xs, ys)) fatal("could not find core textures (hint: run cube from the parent of the bin directory)");

    log("sound");
    initsound();

    // Both of the following settings only take effect on next startup when changed using the in-game console; they are checked in initsound()
    // Since they are saved/read in autoexec.cfg, init cfg is a prerequisite
    extern int soundfreq, soundbufferlen;
    log(" -- sampling frequency (soundfreq): %d", soundfreq);
    log(" -- chunksize (soundbufferlen): %d", soundbufferlen);

    // Apply (update) sound volume settings (if any) that were present in config files
    updatevol();

    // There could be a startmap command in autoexec.cfg (e.g. /sp mapname, /dmsp mapname, /ffa mapname, etc)
    // If not, toservermap[0] == 0
    // Save the mapname to startupmap because this is overwritten when we receive the server's send_welcome in localconnect()
    extern string toservermap;
    string startupmap;
    if(toservermap[0])
    {
        log("startup map: %s", toservermap);
        copystring(startupmap, toservermap);
    }
    else
        startupmap[0] = 0;

    log("localconnect");
    director.log_event(UPDATE_ST, SDL_GetTicks());
    localconnect();
    if(startupmap[0])
        changemap(startupmap);
    else
        changemap("metl3");

    log("mainloop");
    lastmillis = getmillis(SDL_GetTicks());
    quickStartup();

    InternalClockPtr->update();
    GameClockPtr->update();
    PhysicsClockPtr->update();

    // Modified/partial section of main loop for initial iteration of the loop
    reset_orient_and_worldpos(); // need orientation for rendering particles, worldpos for shooting/editing; normally set by readdepth()

    // Stats for 1st frame(s). (CPU only, no rendering.)
    // Record the data so far as a startup frame, and start tracking a new frame from here.
    director.log_event(FRAMEDONE_EV, SDL_GetTicks());
    director.log_event(FRAMESTART_EV, SDL_GetTicks());
    director.log_event(TABULATE_ST, SDL_GetTicks());
    director.commit();

    for(;;)
    {
        director.log_event(SLEEP_ST, SDL_GetTicks());
        Uint32 deferUpdate = mainLoopController.calcDelay();
        if(deferUpdate > 0)
        {
            SDL_Delay(deferUpdate);
        };

        director.log_event(UPDATE_ST, SDL_GetTicks());
        int millis = getmillis(SDL_GetTicks());
        int last_realtime = InternalClockPtr->getMillis();
        InternalClockPtr->update();
        GameClockPtr->update();
        int last_physicstime = PhysicsClockPtr->getMillis();
        PhysicsClockPtr->setScaleRate(getgamespeed());
        PhysicsClockPtr->update();

        // Input and commands
        checkinput();
        freelook_periodic_update(InternalClockPtr->getMillis() - last_realtime);
        checksleepcmd();
        updatevol();

        // Video texture update
        updatevideotexture(PhysicsClockPtr->getMillis() - last_physicstime);

        // World update
        updateworld(millis);
        if(!demoplayback) serverslice(millis, 0);

        // Render
        const Uint32 MinimizedRenderInterval = 1000 / 10; // 10 updates for 1000 milliseconds
        if(!minimized || mainLoopController.minimizedRenderNeeded(MinimizedRenderInterval))
        {

        mainLoopController.lastrender = SDL_GetTicks();

        director.log_event(OCULL_ST, SDL_GetTicks());
        computeraytable(player1->o.x, player1->o.y);

        // Do dynlights after map triggers (world update).
        // CALCLIGHT_ST includes time inside of calclight, dodynlights, and cleardynlights.
        director.log_event(CALCLIGHT_ST, SDL_GetTicks());
        dodynlights();

        director.log_event(RENDER_ST, SDL_GetTicks());
        gl_drawframe(scr_w, scr_h);

        // It is necessary to call readdepth() after a new frame is completed.
        // readdepth() uses glReadPixels(), therefore it should be called after
        // the new frame is complete and before the SwapBuffers function.
        // On some implementations SwapBuffers() performs a copy instead of an exchange.
        // In that case, readdepth() produces the same result when called after
        // SwapBuffers() and before any new drawing.
        if(swap_buffers_copy_preserve == false)
        {
            director.log_event(UPDATE_ST, SDL_GetTicks());
            readdepth(scr_w, scr_h);
            director.log_event(SWAPBUFFERS_ST, SDL_GetTicks());
            SDL_GL_SwapWindow(screen);
        }
        else
        {
            // We choose to put readdepth() after SwapBuffers() because we
            // want SwapBuffers() to run as early as possible.
            director.log_event(SWAPBUFFERS_ST, SDL_GetTicks());
            SDL_GL_SwapWindow(screen);
            director.log_event(UPDATE_ST, SDL_GetTicks());
            readdepth(scr_w, scr_h);
        };

        // Check if recording movie
        director.log_event(UPDATE_ST, SDL_GetTicks());
        checkmovie();

        }; // !minimized

        // Clear dynlights every frame after rendering, and restores backed up blocks to reach the previous state of no dynlights.
        // Removed dynlights will be added back in the next world update.
        // If we did not call dodynlights this frame because we were minimized, there are no backed up blocks to restore,
        // but it's still necessary to clear the dynlights since they are added back every world update.
        director.log_event(CALCLIGHT_ST, SDL_GetTicks());
        cleardynlights();

        director.log_event(FRAMEDONE_EV, SDL_GetTicks());

        // New frame
        director.log_event(FRAMESTART_EV, SDL_GetTicks());

        // Tabulate metrics data collected from previous frame
        director.log_event(TABULATE_ST, SDL_GetTicks());
        director.commit();
    };
    quit();
    return 1;
};


