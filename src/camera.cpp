#include "cube.h"

VARP(zoompower, 1, 2, 10);  // maximum magnification power. e.g. 2x zoom.
VARP(zoomsteps, 1, 1, 5);   // e.g. a setting of "1" means there is 1 zoomed in state at 100% "zoompower" (zoomstate 1), and of course, the unzoomed state (zoomstate 0).

int zoomstate = 0;

int zoomobs_state = 0; // zoom out to monitor environment, or as an option spectators/observers for a QOL improvement
VARP(zoomobsfov, 45, 120, 160); // TODO: want this to be an equirect camera

void zoomnext()
{
    if(zoomobs_state) zoomobs_state = 0;

    ++zoomstate;
    if(zoomstate > zoomsteps) zoomstate = 0;
}

void zoomprev()
{
    if(zoomobs_state) zoomobs_state = 0;

    --zoomstate;
    if(zoomstate < 0) zoomstate = zoomsteps;
}

void zoomquit()
{
    zoomstate = 0;
    zoomobs_state = 0;
}

void zoomobs()
{
    if(zoomobs_state) zoomobs_state = 0;
    else { zoomobs_state = 1; zoomstate = 0; }
}

COMMAND(zoomnext, ARG_NONE);
COMMAND(zoomprev, ARG_NONE);
COMMAND(zoomquit, ARG_NONE);
COMMAND(zoomobs, ARG_NONE);

int zoomspectator_state = 0;
VARP(spectatorfov, 45, 120, 160); // perspective camera, can have wider FOV than active player

void zoomspectator()
{
    if(zoomspectator_state) zoomspectator_state = 0;
    else zoomspectator_state = 1;
}

COMMAND(zoomspectator, ARG_NONE);

// Get zoom power
float getzoompower()
{
    if(zoomobs_state)
        return 1;

    if(zoomstate)
    {
        //float zoompowerdiff = (float)(zoompower - 1);
        //float effectivezoompower = 1 + zoomstate/((float)zoomsteps)*zoompowerdiff;
        float effectivezoompower = pow((float)zoompower, zoomstate/((float)zoomsteps));
        return effectivezoompower;
    }
    else return 1;
}

// Get FOV for rendering
float getzoomfov(float nominalFOV)
{
    if(zoomobs_state)
        return (float)zoomobsfov;

    if(zoomstate)
    {
        float effectivezoompower = getzoompower();
        return nominalFOV/effectivezoompower;
    }
    else
        return nominalFOV;
}

// Echo current FOV and zoom power to console
void showfov()
{
    extern int fov;
    conoutf("%.5f FOV (%.5fx zoom)", getzoomfov((float)fov), getzoompower());
}

COMMAND(showfov, ARG_NONE);
