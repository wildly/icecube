// renderparticles.cpp

#include "cube.h"
#include "benchmark.h"

extern uint haspointsprite;

const int MAXPARTICLES = 10000;
struct particle { vec o, d; int fade, type; particle *next; };
particle particles[MAXPARTICLES];

// Particles that have collision may need to be unstuck
extern bool hascollision(particle *p);

// Un-stick certain particles from geometry
// The list will be processed as one of the first steps when updating/rendering
// particles. We can't run the unstick function in the newparticle function because
// there is a modify group velocity function that runs after creating particles.
vector<particle *> particles_needing_unstick;

// parslist: sprites
// pardlist: decoration, i.e. smoke, sparks, edit sparklies, blood, demo tracks, which can be removed when close to max particle limit
// pardriplist: stuck blood particles (until they drip off the nearby surface)
// parempty: empty particle slots
particle *parslist = NULL, *pardlist = NULL, *pardriplist = NULL, *parempty = NULL;
bool parinit = false;
int nsprites = 0;
int ndecoration = 0;
int nculled = 0;    // this is reset after print out in gl_drawhud()
int nrendered = 0;  // this is reset before use in render_particles()

// immediately after a function creates particles, this can be used to apply additional effects to those particles
// only applies to decoration particles
particle *newparlist = NULL;

// these two lists are needed for storing updated particles that are moving between lists
// the lists will be merged with the corresponding list when the update loop finishes
particle *new_pardlist = NULL;
particle *new_pardriplist = NULL;

void newparlist_merge(particle* &dest_list, particle* &src_list)
{
    for(particle *p, **pp = &src_list; p = *pp;)
    {
        if(p->next == NULL)
        {
            p->next = dest_list;
            dest_list = src_list;
            src_list = NULL;
            return;
        }
        pp = &p->next;
    };
};

void on_created_particles_add_velocity(vec &v)
{
    for(particle *p, **pp = &newparlist; p = *pp;)
    {
        vadd(p->d, v);
        pp = &p->next;
    };
    newparlist_merge(pardlist, newparlist);
};

VARP(maxparticles, 100, 2000, MAXPARTICLES);
VARP(maxparticlesreservedpercent, 20, 20, 100);

void reset_particles()
{
    parempty = NULL;
    parslist = NULL;
    pardlist = NULL;
    pardriplist = NULL;
    nsprites = 0;
    ndecoration = 0;
    nculled = 0;

    loopi(MAXPARTICLES)
    {
        particles[i].next = parempty;
        parempty = &particles[i];
    };

    particles_needing_unstick.setsize(0);
};

// Determine whether to create the particle when total particles reaches the upper reserved slots
bool newparticle_can_create_in_upper(int fade, int type)
{
    // Always create particle sprites
    if(fade < 0)
        return true;

    // Reduce creation of decoration particles
    // The probability to create the particle is number_of_free_slots/maxparticles
    if(rand()%maxparticles < nsprites + ndecoration)
        return false;
    else
        return true;
};

void newparticle(vec &o, vec &d, int fade, int type, bool grouped = false)
{
    // Skip creating particles when they have no lifetime
    if(fade == 0)
        return;
    // Reduce creation of particles when total particles reaches the upper reserved slots
    if(nsprites + ndecoration >= maxparticles*(100-maxparticlesreservedpercent)/100)
    {
        bool createable = newparticle_can_create_in_upper(fade, type);
        if(!createable) { nculled++; return; };
    };

    // Initialize empty particle list if not initialized
    if(!parinit)
    {
        reset_particles();
        parinit = true;
    };
    if(parempty)
    {
        particle *p = parempty;
        parempty = p->next;
        p->o = o;
        p->d = d;
        p->fade = fade;
        p->type = type;
        if(fade < 0 || p->type == PART_SPARKTRAIL)
        {
            p->next = parslist;
            parslist = p;
            nsprites++;
        }
        else
        {
            p->next = newparlist;
            newparlist = p;
            if(!grouped) newparlist_merge(pardlist, newparlist);
            ndecoration++;
        };
        if(hascollision(p)) particles_needing_unstick.add(p);
    }
    else
    {
        // out of space to create the particle!
        nculled++;
    };
};

VAR(demotracking, 0, 0, 1);
VARP(particlesize, 20, 100, 500);

vec right, up;

void setorient(vec &r, vec &u) { right = r; up = u; };

VARP(particlescanbounce, 0, 1, 1);

enum worldcollision
{
    COLLIDE_NONE   = 0,
    COLLIDE_BORDER = 1<<0,
    COLLIDE_SOLID  = 1<<1,
    COLLIDE_FLOOR  = 1<<2,
    COLLIDE_CEIL   = 1<<3,
    COLLIDE_FHF    = 1<<4,
    COLLIDE_CHF    = 1<<5
};

uint collideworld(vec &o)
{
    if(OUTBORD(o.x, o.y)) return COLLIDE_BORDER;

    sqr *s = S(fast_f2nat(o.x), fast_f2nat(o.y));
    if(SOLID(s)) return COLLIDE_SOLID;

    float floor = s->floor;
    if(s->type==FHF) floor -= s->vdelta/4.0f;
    if(o.z <= floor) return (s->type == FHF) ? COLLIDE_FHF : COLLIDE_FLOOR;

    float ceil = s->ceil;
    if(s->type==CHF) ceil += s->vdelta/4.0f;
    if(o.z >= ceil) return (s->type == CHF) ? COLLIDE_CHF : COLLIDE_CEIL;

    return COLLIDE_NONE;
};

const float StepSize = 0.25f;
void raycheck(const vec &o, vec &to)
{
    vdist(dist, stepv, o, to);
    if(dist == 0) return;
    vmul(stepv, StepSize/dist);

    float progress = 0;
    vec test = o;
    while(progress < dist)
    {
        if(collideworld(test) != COLLIDE_NONE) { to = test; break; };
        vec unused;
        if(mmcollidephysent(test, unused)) { to = test; break; };
        vadd(test, stepv);
        progress += StepSize;
    };
};

void displace_into_world(vec &o)
{
    const float ExtraSeparation = 0.001f; // 1e-3f

    uint ctype = collideworld(o);

    if(ctype == COLLIDE_BORDER)
    {
        // teleport in? what if it's air?
        if(o.x < MINBORD) o.x = MINBORD + ExtraSeparation;
        if(o.x > ssize-MINBORD) o.x = ssize - MINBORD - ExtraSeparation;
        if(o.y < MINBORD) o.y = MINBORD + ExtraSeparation;
        if(o.y > ssize-MINBORD) o.y = ssize - MINBORD - ExtraSeparation;
    }
    else if(ctype == COLLIDE_SOLID)
    {
        // todo: least penetration into solid region
        return;
    };
};

bool iscollided(vec &o)
{
    if(OUTBORD(o.x, o.y)) return true;

    sqr *s = S(fast_f2nat(o.x), fast_f2nat(o.y));
    if(SOLID(s)) return true;

    float floor = s->floor;
    if(s->type==FHF) floor -= s->vdelta/4.0f;
    if(o.z <= floor) return true;

    float ceil = s->ceil;
    if(s->type==CHF) ceil += s->vdelta/4.0f;
    if(o.z >= ceil) return true;

    return false;
};

int leastpenetration(float (&penetration)[6])
{
    int leastIndex = 0;
    loopi(6)
    {
        if(penetration[i] < penetration[leastIndex])
            leastIndex = i;
    };
    return leastIndex;
};

// collided with the world... try to bounce off world
// also needs to change physent's velocity (bounce)
#define MaxPenetration 1000;
const float ExtraSeparation = 0.001f; // 1e-3f
// o: location
// d: direction (velocity)
int trybounce(vec &o, vec &d, sqr *s, bool solid)
{
    float penetration[6];
    loopi(6) penetration[i] = MaxPenetration;
    int checkdirections = 2;

    if(!SOLID(s))
    {
        checkdirections = 3;
        // 0: floor, 1: ceiling
        float floor = s->floor;
        if(s->type==FHF) floor -= s->vdelta/4.0f;
        float ceil = s->ceil;
        if(s->type==CHF) ceil += s->vdelta/4.0f;
        penetration[0] = fabs(o.z - floor);
        penetration[1] = fabs(o.z - ceil);
    };

    // 1: low x, 2: high x
    int x = fast_f2nat(o.x);
    penetration[2] = fabs(o.x - x);
    penetration[3] = fabs(o.x - (x+1));

    // 1: low y, 2: high y
    int y = fast_f2nat(o.y);
    penetration[4] = fabs(o.y - y);
    penetration[5] = fabs(o.y - (y+1));

    loopi(checkdirections)
    {
        int side = leastpenetration(penetration);
        if(side == 0 || side == 1)
        {
            o.z = (side == 0) ? o.z + 2/2*penetration[0] + ExtraSeparation : o.z - 2/2*penetration[1] - ExtraSeparation;
            if((side == 0 && d.z < 0) || (side == 1 && d.z > 0)) d.z *= -1;
            penetration[0] = penetration[1] = MaxPenetration;
        };
        if(side == 2 || side == 3)
        {
            o.x = (side == 2) ? o.x - 2/2*penetration[2] - ExtraSeparation : o.x + 2/2*penetration[3] + ExtraSeparation;
            if((side == 2 && d.x > 0) || (side == 3 && d.x < 0)) d.x *= -1;
            penetration[2] = penetration[3] = MaxPenetration;
        };
        if(side == 4 || side == 5)
        {
            o.y = (side == 4) ? o.y - 2/2*penetration[4] - ExtraSeparation : o.y + 2/2*penetration[5] + ExtraSeparation;
            if((side == 4 && d.y > 0) || (side == 5 && d.y < 0)) d.y *= -1;
            penetration[4] = penetration[5] = MaxPenetration;
        };
        if(iscollided(o) == false) break;
        else if(i == checkdirections) return COL_RESULT_ERROR;
    };

    return COL_RESULT_BOUNCE;
};

// also needs to change physent's velocity (bounce)
int trybouncemm(vec &o, vec &v, vec &out)
{
    vec absdiff = o;
    vsub(absdiff, out);
    absdiff.x = fabs(absdiff.x);
    absdiff.y = fabs(absdiff.y);
    absdiff.z = fabs(absdiff.z);

    if(absdiff.x < absdiff.y)
    {
        if(absdiff.x < absdiff.z)
        {
            o.x = out.x;
            v.x *= -1;
        }
        else
        {
            o.z = out.z;
            v.z *= -1;
        };
    }
    else // y is smaller, buy z may be smallest
    {
        if(absdiff.y < absdiff.z)
        {
            o.y = out.y;
            v.y *= -1;
        }
        else
        {
            o.z = out.z;
            v.z *= -1;
        }
    };

    return COL_RESULT_BOUNCE;
};

bool outbordbounce(particle *p)
{
    if((p->o.x)<MINBORD) { p->o.x += 2*(MINBORD-p->o.x);            if(p->d.x < 0) p->d.x *= -1; }
    else if(p->o.x > ssize-MINBORD) { p->o.x -= 2*(p->o.x-MINBORD); if(p->d.x > 0) p->d.x *= -1; };

    if((p->o.y)<MINBORD) { p->o.y += 2*(MINBORD-p->o.y);            if(p->d.y < 0) p->d.y *= -1; }
    else if(p->o.y > ssize-MINBORD) { p->o.y -= 2*(p->o.y-MINBORD); if(p->d.y > 0) p->d.y *= -1; };

    return OUTBORD(p->o.x, p->o.y);
};

bool isunderwater(particle *p)
{
    return (p->o.z <= hdr.waterlevel - 0.5f ? true : false);
};

// Does the particle have the "collision" attribute?
bool hascollision(particle *p)
{
    switch(p->type)
    {
    case PART_BLOOD:
    case PART_SPARKS:
    case PART_SMALLSMOKE:
    case PART_BIGSMOKE:
    case PART_SEAFOAM:
    case PART_BIGFOG:
        return true;
    default:
        return false;
    }
};

// This check is used to determine whether to "glue" a blood particle to a surface until it "drips" off
bool isnearwall(particle *p, float radius = 0.01f)
{
    vec x1 = vec(1, 0, 0).mul(radius);
    vec x2 = vec(1, 0, 0).mul(-radius);
    vec y1 = vec(0, 1, 0).mul(radius);
    vec y2 = vec(0, 1, 0).mul(-radius);
    vec z1 = vec(0, 0, 1).mul(radius);
    vec z2 = vec(0, 0, 1).mul(-radius);

    if(iscollided(x1.add(p->o))) return true;
    if(iscollided(x2.add(p->o))) return true;
    if(iscollided(y1.add(p->o))) return true;
    if(iscollided(y2.add(p->o))) return true;
    if(iscollided(z1.add(p->o))) return true;
    if(iscollided(z2.add(p->o))) return true;

    return false;
}

// Returns false if no collision
// Return true if some collision
// "initial" is a flag specifying whether this check is un-sticking particles that were just created
bool check_particle_collision(particle *p, bool initial = false)
{
    if(!hascollision(p))
        return false;

    int collide = trybounce_physent(p->o, p->d);

    if(collide == COL_RESULT_FREESPACE)
        return false;

    // Stop blood particles when they collide with the world
    if(p->type == PART_BLOOD)
    {
        p->d = vec(0);
        return true;
    }

    // Slow down other particles when they collide with the world (unless we are un-sticking them)
    if(!initial)
    {
        if(p->type == PART_SPARKS) p->d.mul(1/sqrt(2.0f));
        else p->d.mul(0.5f);
    }

    return true;
};

// Returns a value in { COL_RESULT_FREESPACE, COL_RESULT_BOUNCE, COL_RESULT_ERROR }
int trybounce_physent(vec &o, vec &d)
{
    if(OUTBORD(o.x, o.y)) { displace_into_world(o); };
    sqr *s = S(fast_f2nat(o.x), fast_f2nat(o.y));

    if(SOLID(s)) return trybounce(o, d, s, true);

    float floor = s->floor;
    if(s->type==FHF) floor -= s->vdelta/4.0f;
    if(o.z <= floor)
    {
        return trybounce(o, d, s, false);
    };

    float ceil = s->ceil;
    if(s->type==CHF) ceil += s->vdelta/4.0f;
    if(o.z >= ceil)
    {
        return trybounce(o, d, s, false);
    };

    vec out = o;
    if(mmcollidephysent(o, out)) return trybouncemm(o, d, out);

    return COL_RESULT_FREESPACE;
};

void transform_particle(particle *p, float &friction)
{
    // Blood particles aren't affected by gravity when underwater and their speed should also decay rapidly
    if(p->type == PART_BLOOD && isunderwater(p))
    {
        friction = exp(-1.3863f*curtime/1000.0f);
    }
    // Transform sparks into seafoam when underwater
    else if(p->type == PART_SPARKS && isunderwater(p))
    {
        p->type = PART_SEAFOAM;
        friction = exp(-1.3863f*curtime/1000.0f);
    }
    // Transform smoke into foam or fog when underwater
    else if((p->type == PART_SMALLSMOKE || p->type == PART_BIGSMOKE) && isunderwater(p))
    {
        if(p->type == PART_SMALLSMOKE) p->type = PART_SEAFOAM;
        else p->type = PART_BIGFOG;
    }

    // Fog and foam quickly stop in air and when underwater
    if((p->type == PART_SEAFOAM || p->type == PART_BIGFOG))
    {
        friction = exp(-1.3863f*curtime/1000.0f);
    };
}

void update_particle_motion(particle *p, int time, int gr, float friction, bool droplet_drip)
{
    const float BloodMaxSpeed = 40.0f;
    const float BloodMaxSpeedInWater = 5.0f;
    const float SparksMaxSpeed = 80.0f;
    vec deltapos = vec( 0, 0, 0 );

    // determine particle's gravity-based movement and velocity
    if(gr)
    {
        extern int gravitypercent;
        float localgravityratio = gravitypercent/100.0f/gr;

        if(droplet_drip) localgravityratio /= 10;

        // update particle's velocity
        p->d.z -= time/30.0f*localgravityratio;

        // limit particle's velocity
        // (opposes particle's net motion)
        float speed = sqrt(dotprod(p->d, p->d));
        if(p->type == PART_BLOOD && speed > BloodMaxSpeed) { vmul(p->d, BloodMaxSpeed/speed); };
        if(p->type == PART_SPARKS && speed > SparksMaxSpeed) { vmul(p->d, SparksMaxSpeed/speed); };

        // PART_BLOOD and PART_SPARKS optionally are affected by tractor component of dynent gravity (applied to deltapos)
        // (not affected by speed limit)
        //if(p->type == PART_BLOOD || p->type == PART_SPARKS) deltapos.z -= time*9.5f/1000*localgravityratio;
    };

    // limit particle's velocity
    if(friction != 1.0f) vmul(p->d, friction);

    // limit particle's velocity (terminal velocity of blood in water)
    // gr == 0 is a hack to detect if the particle is underwater
    if(p->type == PART_BLOOD && gr == 0)
    {
        float sqrlen = dotprod(p->d, p->d);
        if(sqrlen > BloodMaxSpeedInWater*BloodMaxSpeedInWater) vmul(p->d, BloodMaxSpeedInWater/sqrt(sqrlen));
    };

    // determine particle's velocity-based movement
    vec a = p->d;
    vmul(a,time/1000.0f);
    vadd(deltapos, a);

    // update particle's position
    vec newpos = p->o;
    vadd(newpos, deltapos);
    if(particlescanbounce && hascollision(p)) raycheck(p->o, newpos);
    p->o = newpos;
};

int render_particles_in_list(int time, particle* &parlist)
{
    int nexpired = 0;

    // canspin: 0 = doesn't spin, 1 = CW, 2 = CCW, 3 = random
    struct parttype { float r, g, b, a; int gr, grw, tex; float sz; uint canfadein:1, canfadeout:1, canshrink:1, cangrow:1, canproxfade:1, canspin:2, cantumble:1; } parttypes[] =
    {
        { 0.7f, 0.6f, 0.3f, 1.0f,   2,   4, 3, 0.06f, 0, 1, 0, 0, 1, 0, 0 }, // yellow: sparks
        { 0.5f, 0.5f, 0.5f, 0.8f,  20,  -1, 7, 0.15f, 1, 1, 0, 0, 1, 0, 0 }, // grey:   small smoke
        { 0.2f, 0.2f, 1.0f, 1.0f,   0,   0, 3, 0.08f, 0, 0, 0, 0, 0, 0, 0 }, // blue:   edit mode entities
        { 1.0f, 0.1f, 0.1f, 1.0f,   1,   0, 7, 0.06f, 0, 1, 0, 0, 1, 0, 0 }, // red:    blood spats
        { 1.0f, 0.8f, 0.8f, 1.0f,   0,   0, 6, 1.2f,  0, 0, 0, 0, 0, 0, 0 }, // yellow: fireball1
        { 0.5f, 0.5f, 0.5f, 0.8f,  20,  -1, 7, 0.6f,  1, 1, 0, 0, 1, 0, 0 }, // grey:   big smoke
        { 1.0f, 1.0f, 1.0f, 1.0f,   0,   0, 8, 1.2f,  0, 0, 0, 0, 0, 0, 0 }, // blue:   fireball2
        { 1.0f, 1.0f, 1.0f, 1.0f,   0,   0, 9, 1.2f,  0, 0, 0, 0, 0, 0, 0 }, // green:  fireball3
        { 1.0f, 0.1f, 0.1f, 1.0f,   0,   0, 7, 0.2f,  0, 0, 0, 0, 0, 0, 0 }, // red:    demotrack
        { 0.2f, 0.2f, 1.0f, 1.0f, -40, -40, 3, 0.08f, 0, 1, 0, 0, 1, 0, 0 }, // blue:   teleport sparks
        { 0.7f, 0.6f, 0.3f, 0.2f,   0,   0, 3, 0.03f, 0, 1, 0, 0, 0, 0, 0 }, // yellow: spark trail (tracer bullets)
        { 0.5f, 0.5f, 0.5f, 0.8f,   1,  -2, 7, 0.15f, 1, 1, 0, 0, 1, 0, 0 }, // grey:   sea foam (small smoke)
        { 0.5f, 0.5f, 0.5f, 0.8f,   2,  -2, 7, 0.6f,  1, 1, 0, 0, 1, 0, 0 }, // grey:   big fog (big smoke)
        { 1.0f, 1.0f, 1.0f, 1.0f,  10,  40, 3, 0.6f,  0, 0, 0, 0, 1, 0, 1 }, // no mod: confetti (tex = sparks)
        { 1.0f, 1.0f, 1.0f, 1.0f,  10, -40, 3, 0.6f,  0, 0, 0, 0, 1, 3, 1 }, // no mod: snow (tex = sparks)
        { 1.0f, 1.0f, 1.0f, 1.0f,  10, -40, 3, 0.6f,  0, 0, 0, 0, 1, 0, 1 }, // no mod: feathers (tex = sparks)
        { 1.0f, 1.0f, 1.0f, 1.0f,   1,   0, 3, 0.6f,  0, 0, 0, 0, 1, 0, 0 }  // no mod: rain trail billboard (this isn't rendered like spark trail) (tex = sparks)
    };

    const float FadeInMsec = 50.0f;
    const float FadeOutMsec = 100.0f;
    const float ShrinkMsec = 200.0f;

    for(particle *p, **pp = &parlist; p = *pp;)
    {
        parttype *pt = &parttypes[p->type];

        // Intialize scale for particles to 1.0f
        // It is currently used for scaling the particle's size, but later this is used for scaling the particle's alpha
        float scale = 1.0f;
        if(pt->canshrink) scale = (p->fade>=0 && p->fade<ShrinkMsec) ? p->fade/ShrinkMsec : 1.0f; // scale size for particles that can shrink
        float sz = pt->sz*particlesize/100.0f*scale;

        scale = 1.0f; // Reinitialize scale for scaling alpha for particles that can fade
        if(p->fade>=0 && (pt->canfadeout || pt->canfadein || pt->canproxfade)) // fade must be at least 0 in order for particle to fade
        {
            // Render close proximity smoke as semi-transparent
            float proxScale = 1.0f;
            if(pt->canproxfade)
            {
                const float ProxFadeDist = 3.0f;
                const float ProxFadeMinAlpha = 0.25f;
                vdist(dist, dvec, player1->o, p->o);
                if(dist < ProxFadeDist)
                    proxScale *= max(dist/ProxFadeDist, ProxFadeMinAlpha);
            };
            // Smoke also has a 50 msec fade in
            float fadeInScale = 1.0f;
            if(pt->canfadein)
            {
                const float SmokeFadeInMinScale = 0.1f;
                if(p->fade < FadeInMsec)
                    fadeInScale *= max(p->fade/FadeInMsec, SmokeFadeInMinScale);
            };
            // Calculate the fade out effect
            float fadeOutScale = 1.0f;
            if(pt->canfadeout)
            {
                if(p->fade < FadeOutMsec)
                    fadeOutScale *= p->fade/FadeOutMsec;
            };
            // Apply the greater of the fadein/fadeout effect (greater effect = smaller alpha)
            scale *= min(fadeInScale, fadeOutScale);
            // Apply the proximity fade effect
            scale *= proxScale;
        };

        glBindTexture(GL_TEXTURE_2D, pt->tex);

        if(haspointsprite && p->type == PART_DEMOTRACK) // only render these particles as point sprites
        {
            glEnable(GL_POINT_SPRITE);
            glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
            glPointSize(getGUIPointScaling()*10.0f);
            glBegin(GL_POINTS);
            glColor4f(pt->r, pt->g, pt->b, scale*pt->a);
            glVertex3f(p->o.x, p->o.z, p->o.y);
            glEnd();
            xtraverts += 1;
        }
        else if(p->type == PART_SPARKTRAIL)
        {
            #define gl_vertex_from_vec(v) glVertex3d(v.x, v.y, v.z);
            const vec origin[4] =       // { (0,1), (1,1), (1,0), (1,1) }
            {
                vec( p->o.x+(-right.x+up.x)*sz, p->o.z+(-right.y+up.y)*sz, p->o.y+(-right.z+up.z)*sz ),
                vec( p->o.x+( right.x+up.x)*sz, p->o.z+( right.y+up.y)*sz, p->o.y+( right.z+up.z)*sz ),
                vec( p->o.x+( right.x-up.x)*sz, p->o.z+( right.y-up.y)*sz, p->o.y+( right.z-up.z)*sz ),
                vec( p->o.x+(-right.x-up.x)*sz, p->o.z+(-right.y-up.y)*sz, p->o.y+(-right.z-up.z)*sz )
            };
            const vec dest[4] =         // { (0,1), (1,1), (1,0), (1,1) }
            {
                vec( p->d.x+(-right.x+up.x)*sz, p->d.z+(-right.y+up.y)*sz, p->d.y+(-right.z+up.z)*sz ),
                vec( p->d.x+( right.x+up.x)*sz, p->d.z+( right.y+up.y)*sz, p->d.y+( right.z+up.z)*sz ),
                vec( p->d.x+( right.x-up.x)*sz, p->d.z+( right.y-up.y)*sz, p->d.y+( right.z-up.z)*sz ),
                vec( p->d.x+(-right.x-up.x)*sz, p->d.z+(-right.y-up.y)*sz, p->d.y+(-right.z-up.z)*sz )
            };
            // Since sparktrails are beams, a pixel covered by a sparktrail overlaps two quads
            // We will multiply alpha by 0.5 to give sparktrails enough transparency
            glBegin(GL_QUAD_STRIP);
            glColor4f(pt->r, pt->g, pt->b, 0.5f*scale*pt->a);
            const float tex_slice_lower = (float)(0.5f-1.0f/32);
            const float tex_slice_upper = (float)(0.5f+1.0f/32);
            glTexCoord2f(tex_slice_lower, tex_slice_upper); gl_vertex_from_vec(origin[0]);
            glTexCoord2f(tex_slice_upper, tex_slice_upper); gl_vertex_from_vec(  dest[0]);
            glTexCoord2f(tex_slice_upper, tex_slice_lower); gl_vertex_from_vec(origin[1]);
            glTexCoord2f(tex_slice_lower, tex_slice_lower); gl_vertex_from_vec(  dest[1]);
            glTexCoord2f(tex_slice_lower, tex_slice_upper); gl_vertex_from_vec(origin[2]);
            glTexCoord2f(tex_slice_upper, tex_slice_upper); gl_vertex_from_vec(  dest[2]);
            glTexCoord2f(tex_slice_upper, tex_slice_lower); gl_vertex_from_vec(origin[3]);
            glTexCoord2f(tex_slice_lower, tex_slice_lower); gl_vertex_from_vec(  dest[3]);
            glTexCoord2f(tex_slice_lower, tex_slice_upper); gl_vertex_from_vec(origin[0]);
            glTexCoord2f(tex_slice_upper, tex_slice_upper); gl_vertex_from_vec(  dest[0]);
            glEnd();
            xtraverts += 10;
            glBegin(GL_QUADS);
            glTexCoord2f(tex_slice_lower, tex_slice_upper); gl_vertex_from_vec(origin[0]);
            glTexCoord2f(tex_slice_upper, tex_slice_upper); gl_vertex_from_vec(origin[1]);
            glTexCoord2f(tex_slice_upper, tex_slice_lower); gl_vertex_from_vec(origin[2]);
            glTexCoord2f(tex_slice_lower, tex_slice_lower); gl_vertex_from_vec(origin[3]);
            glTexCoord2f(tex_slice_lower, tex_slice_upper); gl_vertex_from_vec(  dest[0]);
            glTexCoord2f(tex_slice_upper, tex_slice_upper); gl_vertex_from_vec(  dest[1]);
            glTexCoord2f(tex_slice_upper, tex_slice_lower); gl_vertex_from_vec(  dest[2]);
            glTexCoord2f(tex_slice_lower, tex_slice_lower); gl_vertex_from_vec(  dest[3]);
            glEnd();
            xtraverts += 8;
        }
        else
        {
            glBegin(GL_QUADS);
            glColor4f(pt->r, pt->g, pt->b, scale*pt->a);
            // perf varray?
            glTexCoord2f(0.0, 1.0); glVertex3d(p->o.x+(-right.x+up.x)*sz, p->o.z+(-right.y+up.y)*sz, p->o.y+(-right.z+up.z)*sz);
            glTexCoord2f(1.0, 1.0); glVertex3d(p->o.x+( right.x+up.x)*sz, p->o.z+( right.y+up.y)*sz, p->o.y+( right.z+up.z)*sz);
            glTexCoord2f(1.0, 0.0); glVertex3d(p->o.x+( right.x-up.x)*sz, p->o.z+( right.y-up.y)*sz, p->o.y+( right.z-up.z)*sz);
            glTexCoord2f(0.0, 0.0); glVertex3d(p->o.x+(-right.x-up.x)*sz, p->o.z+(-right.y-up.y)*sz, p->o.y+(-right.z-up.z)*sz);
            glEnd();
            xtraverts += 4;
        };
        nrendered++;

        director.log_event(UPDATE_PARTICLES_ST, SDL_GetTicks());
        bool is_driplist = (parlist == pardriplist);
        float friction = 1.0f;
        transform_particle(p, friction);        // check for particle transformations even if the particle was just created. (otherwise, the untransformed particle would exist for 1 frame, which is an arbitrary amount of time.)
        if(p->fade<0) p->fade++;                // if p->fade is less than 0, increment p->fade towards 0; the particle expires upon reaching 0 and is removed via the msec elapsed check below.
        if(p->fade>=0 && (p->fade -= time)<=0)  // if p->fade is at least 0, it expires upon or after msec elapsed.
        {
            *pp = p->next;
            p->next = parempty;
            parempty = p;
            nexpired++;
        }
        else
        {
            // only update particle's position if time has passed since the last update
            if(time > 0)
            {
                if(p->type != PART_SPARKTRAIL) // PART_SPARKTRAIL is a particle_trace particle; it doesn't travel in a direction depending on time
                {
                    update_particle_motion(p, time, !(isunderwater(p)) ? pt->gr : pt->grw, friction, is_driplist);
                };
            };

            // check for collision if enabled
            bool collided = false;
            if(particlescanbounce) collided = check_particle_collision(p);

            // check for dripping droplets
            // 1. dripping droplets can drip free of all nearby surfaces
            // 2. free droplets can collide with a surface
            bool dripping = false;
            if(particlescanbounce && p->type==PART_BLOOD) dripping = isnearwall(p);

            // traverse the list to the next particle, possibly moving a particle between the lists of dripping and freely moving particles
            if(!dripping && is_driplist)
            {
                // move this particle to a new list of droplets in newly in free space and traverse the list to the next particle
                *pp = p->next;
                p->next = new_pardlist;
                new_pardlist = p;
            }
            else if(p->type==PART_BLOOD && collided && !is_driplist)
            {
                // move this particle to a new list of newly stuck droplets and traverse the list to the next particle
                *pp = p->next;
                p->next = new_pardriplist;
                new_pardriplist = p;
            }
            else
                // just traverse the list to the next particle
                pp = &p->next;
        };
        director.log_event(RENDER_ST, SDL_GetTicks());
    };
    return nexpired;
};

void render_particles(int time)
{
    director.log_event(UPDATE_PARTICLES_ST, SDL_GetTicks());

    if(demoplayback && demotracking)
    {
        vec motion = vec( 0, 0, 0 );
        newparticle(player1->o, motion, 100000000, PART_DEMOTRACK);
    };

    // Unstick particles
    loopv(particles_needing_unstick) check_particle_collision(particles_needing_unstick[i], true);
    particles_needing_unstick.setsize(0);

    director.log_event(RENDER_ST, SDL_GetTicks());

    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glDisable(GL_FOG);

    nrendered = 0;
    nsprites -= render_particles_in_list(time, parslist);
    ndecoration -= render_particles_in_list(time, pardlist);
    ndecoration -= render_particles_in_list(time, pardriplist);

    newparlist_merge(pardlist, new_pardlist);
    newparlist_merge(pardriplist, new_pardriplist);

    glEnable(GL_FOG);
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
};

// particle_splash creates particles with a lifetime of [0, 3*fade] msec
// If fade is negative, particles have a lifetime of abs(fade) frames
void particle_splash(int type, int num, int fade, vec &p)
{
    loopi(num)
    {
        const bool slower = (type==PART_BIGSMOKE || type==PART_EDIT);
        const float radius = slower ? 2.5f : 7.5f;
        const uint MaxTries = 10;
        vec vel = rndVecInSpace(UNIFORM_SPHERE, MaxTries, radius);
        newparticle(p, vel, (fade<=0)?fade:rnd(1+fade*3), type);
    };
};

void particle_jet_velocity(int type, vec &vel, const vec &groupv)
{
    const float MaxSpeed = type==PART_BLOOD ? 40.0f : 80.0f;

    // reduce base speed
    float speed = sqrt(dotprod(groupv, groupv));
    float scale = speed > MaxSpeed ? MaxSpeed/speed : 1;
    vec vbase = groupv;
    vmul(vbase, scale);

    vadd(vel, vbase);

    // reduce speed again
    speed = sqrt(dotprod(vel, vel));
    scale = speed > MaxSpeed ? MaxSpeed/speed : 1;
    vmul(vel, scale);
};

// particle_jet is nearly the same as particle_splash
void particle_jet(int type, int num, int fade, vec &p, const vec &dv)
{
    vec groupv = dv;
    loopi(num)
    {
        const float radius = 7.5f;
        const uint MaxTries = 10;
        vec vel = rndVecInSpace(UNIFORM_SPHERE, MaxTries, radius);
        particle_jet_velocity(type, vel, groupv);
        newparticle(p, vel, (fade<=0)?fade:rnd(1+fade*3), type);
    };
};

// this version of particle_jet is used when there is no applicable group velocity
void particle_jet(int type, int num, int fade, vec &p)
{
    vec gv = vec( 0, 0, 0 );
    particle_jet(type, num, fade, p, gv);
};

// particle_trail creates particles with a lifetime of [fade, 2*fade] msec
void particle_trail(int type, int fade, vec &s, vec &e)
{
    vdist(d, v, s, e);
    vdiv(v, d*2+0.1f);
    vec p = s;
    loopi(int(d*2))
    {
        vadd(p, v);
        const uint MaxTries = 10;
        vec vel = rndVecInSpace(UNIFORM_SPHERE, MaxTries, 0.25f);
        newparticle(p, vel, rnd(1+fade)+fade, type);
    };
};

// particle_trace creates particles with a lifetime of fade msec
// If fade is negative, particles have a lifetime of abs(fade) frames
// For this type of particle, particle::o is the start point and particle::d is end/destination point (not direction!)
void particle_trace(int type, int fade, vec &s, vec &e)
{
    newparticle(s, e, fade, type);
};

// particle_cloud creates a number of particles within a spherical volume
// particles travel in a random direction at speed less-equal to speed
// particle lifetime is [fade, fade+fadeRnd]
void particle_cloud(const int type, const int num, const uint fade, const uint fadeRnd, vec &source, const float radius, const float speed, bool grouped)
{
    loopi(num)
    {
        const uint MaxTriesPos = 10;
        const uint MaxTriesVel = 10;
        // random start position
        vec offset = rndVecInSpace(UNIFORM_SPHERE, MaxTriesPos, radius);
        vec p = source;
        vadd(p, offset);
        // random velocity
        vec vel = vec( 0, 0, 0 );
        if(speed)
            vel = rndVecInSpace(UNIFORM_SPHERE, MaxTriesVel, speed);
        newparticle(p, vel, fade+rnd(1+fadeRnd), type, grouped);
    };
};

// particle_bubble creates a number of particles on a spherical surface
// particles travel in a random direction at speed less-equal to speed
// particle lifetime is [fade, fade+fadeRnd]
void particle_bubble(const int type, const int num, const uint fade, const uint fadeRnd, vec &source, const float radius, const float speed, bool grouped)
{
    loopi(num)
    {
        const uint MaxTriesPos = 100;
        const uint MaxTriesVel = 10;
        int numFailure = 0;
        // random start position
        vec offset = rndVecOnSurface(UNIFORM_SPHERICAL_SURFACE, MaxTriesPos, radius);
        if(offset.iszero())
        {
            ++numFailure;
            continue;
        };
        vec p = source;
        vadd(p, offset);
        // random velocity
        vec vel = vec( 0, 0, 0 );
        if(speed)
            vel = rndVecInSpace(UNIFORM_SPHERE, MaxTriesVel, speed);
        newparticle(p, vel, fade+rnd(1+fadeRnd), type, grouped);
        extern int debugparticles;
        if(debugparticles && numFailure > 0)
            conoutf("particle_bubble: %d/%d failures", numFailure, num);
    };
};
