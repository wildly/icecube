// rendergl.cpp: core opengl rendering stuff

#include "cube.h"
#include <SDL_image.h>
#include "mpimesh.h"
#include "texture.h"

// Replaces GL_EXT_texture_env_combine with GL_ARB_texture_env_combine
#ifdef __APPLE__
#define GL_COMBINE_EXT GL_COMBINE_ARB
#define GL_COMBINE_RGB_EXT GL_COMBINE_RGB_ARB
#define GL_SOURCE0_RGB_EXT GL_SOURCE0_RGB_ARB
#define GL_SOURCE1_RGB_EXT GL_SOURCE1_RGB_ARB
#define GL_RGB_SCALE_EXT GL_RGB_SCALE_ARB
#define GL_PRIMARY_COLOR_EXT GL_PRIMARY_COLOR_ARB
#endif

VAR(logopengl,0,0,1);

extern int curvert;

bool hasoverbright = false;
uint haspointsprite = 0;    // 1 for ARB_point_sprite, 2 for NV_point_sprite
uint hasfbo = 0;            // 1 for GL_EXT_framebuffer_object, 2 for GL_ARB_framebuffer_object
uint hasaf = 0;             // 1 for GL_EXT_texture_filter_anisotropic, 2 for GL_ARB_texture_filter_anisotropic

void purgetextures();

GLUquadricObj *qsphere = NULL;
int glmaxtexsize = 256;

extern void gl_print_version();
extern void gl_detect_features();
extern void gl_load_extensions();
void gl_init(int w, int h)
{
    //#define fogvalues 0.5f, 0.6f, 0.7f, 1.0f

    glViewport(0, 0, w, h);
    glClearDepth(1.0);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    
    
    glEnable(GL_FOG);
    glFogi(GL_FOG_MODE, GL_LINEAR);
    glFogf(GL_FOG_DENSITY, 0.25);
    glHint(GL_FOG_HINT, GL_NICEST);
    

    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_POLYGON_OFFSET_LINE);
    glPolygonOffset(-3.0, -3.0);

    glCullFace(GL_FRONT);
    glEnable(GL_CULL_FACE);

    gl_print_version();

    const char *exts = (const char *)glGetString(GL_EXTENSIONS);

    if(strstr(exts, "GL_EXT_texture_env_combine")) hasoverbright = true;
    else if(strstr(exts, "GL_ARB_texture_env_combine")) hasoverbright = true;
    else conoutf("WARNING: cannot use overbright lighting, using old lighting model!");

    if(strstr(exts, "GL_ARB_point_sprite")) haspointsprite = 1;
    else if(strstr(exts, "NV_point_sprite")) haspointsprite = 2;
    else conoutf("WARNING: point sprites not available, using quads instead!");
    if(haspointsprite)
    {
        GLfloat fSizes[2];
        glGetFloatv(GL_ALIASED_POINT_SIZE_RANGE, fSizes);
        conoutf("gl_init: %s point sprite sizes: min=%f, max=%f", haspointsprite == 2 ? "NV" : "ARB", fSizes[0], fSizes[1]);
    };

    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &glmaxtexsize);
    conoutf("gl_init: GL_MAX_TEXTURE_SIZE: %d", glmaxtexsize);

    // check for GL_EXT_framebuffer_object and GL_ARB_framebuffer_object
    // prefer GL_ARB_framebuffer_object
    if(strstr(exts, "GL_EXT_framebuffer_object")) { hasfbo = 1; conoutf("gl_init: GL_EXT_framebuffer_object"); };
    if(strstr(exts, "GL_ARB_framebuffer_object")) { hasfbo = 2; conoutf("gl_init: GL_ARB_framebuffer_object"); };
    if(!hasfbo) conoutf("WARNING: framebuffer objects not available");

    // check for GL_EXT_texture_filter_anisotropic and GL_ARB_texture_filter_anisotropic
    // prefer GL_EXT_texture_filter_anisotropic
    if(strstr(exts, "GL_ARB_texture_filter_anisotropic")) { hasaf = 2; conoutf("gl_init: GL_ARB_texture_filter_anisotropic"); };
    if(strstr(exts, "GL_EXT_texture_filter_anisotropic")) { hasaf = 1; conoutf("gl_init: GL_EXT_texture_filter_anisotropic"); };
    if(!hasaf) conoutf("WARNING: anisotropic filtering not available");

    gl_detect_features();

#ifdef LOAD_OPENGL_EXTENSIONS
    gl_load_extensions();
#endif

    purgetextures();

    if(!(qsphere = gluNewQuadric())) fatal("glu sphere");
    gluQuadricDrawStyle(qsphere, GLU_FILL);
    gluQuadricOrientation(qsphere, GLU_INSIDE);
    gluQuadricTexture(qsphere, GL_TRUE);
    glNewList(1, GL_COMPILE);
    gluSphere(qsphere, 1, 12, 6);
    glEndList();
};

void cleangl()
{
    if(qsphere) gluDeleteQuadric(qsphere);
};

// If installtex is called with mipmap = false, then createtexture is used.
// However, since filter = 2, mipmaps will still be created in createtexture.
bool installtex(int tnum, char *texname, int &xs, int &ys, bool clamp, bool mipmap)
{
    int flen = strlen(texname);
    if(flen >= 4 && (!strcasecmp(texname + flen - 4, ".dds")))
        return installtex_dds(tnum, texname, xs, ys, clamp, mipmap);
    if(flen >= 4 && (!strcasecmp(texname + flen - 4, ".crn")))
        return installtex_crn(tnum, texname, xs, ys, clamp, mipmap);

    SDL_Surface *s = IMG_Load(texname);
    if(!s) { conoutf("couldn't load texture %s", texname); return false; };
    // SDL 2.0 on macOS uses CoreImage to load images. This can result in a 32bpp RGBA SDL_Surface.
    // IMG_Load with grayscale images returns an 8bpp grayscale surface.
    if(s->format->BitsPerPixel!=8 && s->format->BitsPerPixel!=24 && s->format->BitsPerPixel!=32) { conoutf("texture must be 24bpp or grayscale: %s", texname); return false; };
    setuptexparameters(tnum, NULL, clamp ? 3 : 0, 2, GL_RGB, GL_TEXTURE_2D, false);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); 
    xs = s->w;
    ys = s->h;
    if(s->format->BitsPerPixel!=24)
        s = creatergbsurface(s); // replace s with a new RGB surface via blitting from source; creatergbsurface frees the old surface.
    ImageData scaledimg(s);
    while(xs>glmaxtexsize || ys>glmaxtexsize) { xs /= 2; ys /= 2; };
    if(xs!=scaledimg.w)
    {
        conoutf("warning: quality loss: scaling %s", texname);     // for voodoo cards under linux
        scaleimage(scaledimg, xs, ys);
    };
    if(mipmap)
    {
        if(gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, xs, ys, GL_RGB, GL_UNSIGNED_BYTE, scaledimg.data)) fatal("could not build mipmaps");
    }
    else
    {
        createtexture(tnum, xs, ys, scaledimg.data, clamp, 2, GL_RGB, GL_TEXTURE_2D, xs, ys, scaledimg.pitch, false, GL_RGB, false);
    };
    return true;
};

// no mipmap, so set filter = 1
// videowidth and videoheight should be a multiple of 16
void uploadvideotex(int tnum, uchar *videoTexture)
{
    extern int videoWidth;
    extern int videoHeight;
    setuptexparameters(tnum, NULL, 3, 1, GL_RGB, GL_TEXTURE_2D, false);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    createtexture(tnum, videoWidth, videoHeight, videoTexture, 3, 1, GL_RGB, GL_TEXTURE_2D, videoWidth, videoHeight, videoWidth*3, false, GL_RGB, false);
};

// Load a dds file, then install it to a texture slot.
// If the compressed format is not supported on the GPU, returns false.
// If calling this function with the force flag set, loaddds will decode the compressed format,
// then this function will assume the decoded format to be RGB and install it to a texture slot using createtexture.
bool installtex_dds(int tnum, char *texname, int &xs, int &ys, bool clamp, bool mipmap)
{
    ImageData idds;
    int force = 0;
    if(!loaddds(texname, idds, force)) { conoutf("couldn't load dds: %s", texname); return false; };
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    if(force == 0)
        createcompressedtexture(tnum, idds.w, idds.h, idds.data, idds.align, idds.bpp, idds.levels, clamp ? 3 : 0, 2, idds.compressed, GL_TEXTURE_2D, true);
    else
        createtexture(tnum, idds.w, idds.h, idds.data, clamp, 2, GL_RGB, GL_TEXTURE_2D, idds.w, idds.h, idds.pitch, false, GL_RGB, false);
    return true;
};

bool installtex_crn(int tnum, char *texname, int &xs, int &ys, bool clamp, bool mipmap)
{
    extern int transcode_crn(const char* pSrc_filename, void*& ddsheader, void*& ddsdata, int *width, int *height, int *levels, bool inspectCRN, bool inspectDDS);
    extern void discard_dds(void *ddsheader, void *ddsdata);

    void *ddsheader = NULL;
    void *ddsdata = NULL;
    int width, height, levels;

    // transcode_crn returns non-zero if there's an error
    if(transcode_crn(texname, ddsheader, ddsdata, &width, &height, &levels, false, false)) { conoutf("couldn't transcode crn: %s", texname); return false; };

    // Setup ImageData with information from ddsheader.
    // ImageData doesn't become the owner of ddsdata.
    // Note: ImageData and ddsdata lifetime ends when this function returns.
    ImageData idds;
    int force = 0;
    if(!setupddsimage((uchar *)ddsheader, (uchar *)ddsdata, idds, force)) { conoutf("unsupported format: %s", texname); return false; };

    // upload the texture
    if(force == 0)
        createcompressedtexture(tnum, width, height, idds.data, idds.align, idds.bpp, levels, clamp ? 3 : 0, 2, idds.compressed, GL_TEXTURE_2D, true);
    else
        createtexture(tnum, idds.w, idds.h, idds.data, clamp, 2, GL_RGB, GL_TEXTURE_2D, idds.w, idds.h, idds.pitch, false, GL_RGB, false);

    // discard dds from memory after uploading
    discard_dds(ddsheader, ddsdata);

    return true;
};

// management of texture slots
// each texture slot can have multople texture frames, of which currently only the first is used
// additional frames can be used for various shaders

VAR(dbgtex, 0, 0, 1);

const int MAXTEX = 1000;
int texx[MAXTEX];                           // ( loaded texture ) -> ( name, size )
int texy[MAXTEX];                           
string texname[MAXTEX];
int curtex = 0;
const int FIRSTTEX = 1000;                  // opengl id = loaded id + FIRSTTEX
// std 1+, sky 14+, mdls 20+

const int MAXFRAMES = 2;                    // increase to allow more complex shader defs
int mapping[256][MAXFRAMES];                // ( cube texture, frame ) -> ( opengl id, name )
string mapname[256][MAXFRAMES];

void purgetextures()
{
    loopi(256) loop(j,MAXFRAMES) mapping[i][j] = 0;
};

int curtexnum = 0;

void texturereset() { curtexnum = 0; };

void texture(char *aframe, char *name)
{
    int num = curtexnum++, frame = atoi(aframe);
    if(num<0 || num>=256 || frame<0 || frame>=MAXFRAMES) return;
    mapping[num][frame] = 1;
    char *n = mapname[num][frame];
    copystring(n, name);
    path(n);
};

COMMAND(texturereset, ARG_NONE);
COMMAND(texture, ARG_2STR);

int lookuptexture(int tex, int &xs, int &ys)
{
    int frame = 0;                      // other frames?
    int tid = mapping[tex][frame];

    if(tid>=FIRSTTEX)
    {
        xs = texx[tid-FIRSTTEX];
        ys = texy[tid-FIRSTTEX];
        return tid;
    };

    xs = ys = 16;
    if(!tid) return 1;                  // crosshair :)

    loopi(curtex)       // lazily happens once per "texture" command, basically
    {
        if(strcmp(mapname[tex][frame], texname[i])==0)
        {
            mapping[tex][frame] = tid = i+FIRSTTEX;
            xs = texx[i];
            ys = texy[i];
            return tid;
        };
    };

    if(curtex==MAXTEX) fatal("loaded too many textures");

    int tnum = curtex+FIRSTTEX;
    copystring(texname[curtex], mapname[tex][frame]);

    defformatstring(name)("packages%c%s", PATHDIV, texname[curtex]);

    if(installtex(tnum, name, xs, ys))
    {
        mapping[tex][frame] = tnum;
        texx[curtex] = xs;
        texy[curtex] = ys;
        curtex++;
        if(dbgtex) conoutf(CON_DEBUG, "lookuptexture: installtex: %s", name);
        return tnum;
    }
    else
    {
        if(dbgtex) conoutf(CON_DEBUG, "lookuptexture: installtex failed! (will use temp fix)");
        return mapping[tex][frame] = FIRSTTEX;  // temp fix
    };
};

void setupworld()
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY); 
    setarraypointers();

    if(hasoverbright)
    {
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT); 
        glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_MODULATE);
        glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_TEXTURE);
        glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_PRIMARY_COLOR_EXT);
    };
};

int skyoglid;

struct strip { int tex, start, num; };
vector<strip> strips;

void renderstripssky()
{
    glBindTexture(GL_TEXTURE_2D, skyoglid);
    loopv(strips) if(strips[i].tex==skyoglid) glDrawArrays(GL_TRIANGLE_STRIP, strips[i].start, strips[i].num);
};

void renderstrips()
{
    int lasttex = -1;
    loopv(strips) if(strips[i].tex!=skyoglid)
    {
        if(strips[i].tex!=lasttex)
        {
            glBindTexture(GL_TEXTURE_2D, strips[i].tex); 
            lasttex = strips[i].tex;
        };
        glDrawArrays(GL_TRIANGLE_STRIP, strips[i].start, strips[i].num);  
    };   
};

void overbright(float amount) { if(hasoverbright) glTexEnvf(GL_TEXTURE_ENV, GL_RGB_SCALE_EXT, amount ); };

void addstrip(int tex, int start, int n)
{
    strip &s = strips.add();
    s.tex = tex;
    s.start = start;
    s.num = n;
};

#if SDL_VERSION_ATLEAST(2, 0, 0)
#else
VARFP(gamma, 30, 100, 300,
{
    float f = gamma/100.0f;
    if(SDL_SetGamma(f,f,f)==-1)
    {
        conoutf("Could not set gamma (card/driver doesn't support it?)");
        conoutf("sdl: %s", SDL_GetError());
    };
});
#endif

void transplayer()
{
    const float camera_position_delta = getCameraOffsetZ(player1);
    extern float autofloat_camera_offset;
    glLoadIdentity();
    
    glRotated(player1->roll,0.0,0.0,1.0);
    glRotated(player1->pitch,-1.0,0.0,0.0);
    glRotated(player1->yaw,0.0,1.0,0.0);

    glTranslated(-player1->o.x, (player1->state==CS_DEAD ? -(-player1->eyeheight+0.2f+autofloat_camera_offset) : 0)-(player1->o.z+camera_position_delta), -player1->o.y);
};

VARP(fov, 10, 90, 160);

int xtraverts;

VAR(fog, 64, 180, 1024);
VAR(fogcolour, 0, 0x8099B3, 0xFFFFFF);

VARP(hudgun,0,1,1);

// Returns equivalent hud "gun" for new weapons.
// Used by the hud gunmodel and gunicon.
int gethudgunalias(int gun)
{
    // Standard guns
    if(gun <= GUN_RIFLE)
        return gun;

#ifdef NEWGUNS
    // New guns which use Fist placeholder
    if(gun == GUN_THRUSTERS || gun == GUN_STOMP || gun == GUN_GRAPPLE)
        return GUN_FIST;

    // New guns which use Rocket Launcher placeholder
    if(gun == GUN_GRENADE || gun == GUN_LASER)
        return GUN_RL;

    // New guns which use Chaingun placeholder
    if(gun == GUN_PISTOL)
        return GUN_CG;

    // New guns which use Rifle placeholder
    if(gun == GUN_VORPAL || gun == GUN_PORTAL)
        return GUN_RIFLE;
#endif

    // All other new guns use Shotgun placeholder
    return GUN_SG;
};

const char *hudgunnames[] = { "hudguns/fist", "hudguns/shotg", "hudguns/chaing", "hudguns/rocket", "hudguns/rifle" };

const char *gethudgunmodel(int gun)
{
    int hudgun = gethudgunalias(gun);
    return hudgunnames[hudgun];
};

void drawhudmodel(int start, int end, float speed, int base)
{
    const char *hudgunModel = gethudgunmodel(player1->gunselect);
    const float cameraOffsetZ = getCameraOffsetZ(player1);
    rendermodel(hudgunModel, start, end, 0, 1.0f, player1->o.x, player1->o.z+cameraOffsetZ, player1->o.y, player1->yaw+90, player1->pitch, false, 1.0f, speed, 0, base);
};

void drawhudgun(float fovy, float aspect, int farplane)
{
    if(!hudgun /*|| !player1->gunselect*/) return;
    if(player1->state == CS_DEAD) return;

    glEnable(GL_CULL_FACE);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fovy, aspect, 0.3f, farplane);
    glMatrixMode(GL_MODELVIEW);
    
    //glClear(GL_DEPTH_BUFFER_BIT);
    int rtime = reloadtime(player1->gunselect);
    if(player1->lastaction && player1->lastattackgun==player1->gunselect && lastmillis-player1->lastaction<rtime)
    {
        drawhudmodel(7, 18, rtime/18.0f, player1->lastaction);
    }
    else
    {
        drawhudmodel(6, 1, 100, 0);
    };

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fovy, aspect, 0.15f, farplane);
    glMatrixMode(GL_MODELVIEW);

    glDisable(GL_CULL_FACE);
};

void gl_drawframe(int w, int h)
{
    float hf = hdr.waterlevel-0.3f;
    float renderfov = getzoomfov((float)fov);
    float halffov = renderfov/2.0f/360.0f*TAU;
    float aspect = w/(float)h;
    float fovy = fabs(atanf(1.0f/(aspect/tanf(halffov))))*2.0f/TAU*360.0f;
    bool underwater = player1->o.z<hf;
    
    glFogi(GL_FOG_START, (fog+64)/8);
    glFogi(GL_FOG_END, fog);
    float fogc[4] = { (fogcolour>>16)/256.0f, ((fogcolour>>8)&255)/256.0f, (fogcolour&255)/256.0f, 1.0f };
    glFogfv(GL_FOG_COLOR, fogc);
    glClearColor(fogc[0], fogc[1], fogc[2], 1.0f);

    extern int reducemotion; // reducemotion is in physics.cpp
    if(underwater)
    {
        if(!reducemotion)
        {
            fovy += (float)sin(lastmillis/1000.0)*2.0f;
            aspect += (float)sin(lastmillis/1000.0+PI)*0.1f;
        }
        glFogi(GL_FOG_START, 0);
        glFogi(GL_FOG_END, (fog+96)/8);
    };
    
    glClear((player1->outsidemap ? GL_COLOR_BUFFER_BIT : 0) | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    int farplane = fog*5/2;
    gluPerspective(fovy, aspect, 0.15f, farplane);
    glMatrixMode(GL_MODELVIEW);

    transplayer();

    glEnable(GL_TEXTURE_2D);
    
    int xs, ys;
    skyoglid = lookuptexture(DEFAULT_SKY, xs, ys);
   
    resetcubes();
            
    curvert = 0;
    strips.setsize(0);
  
    render_world(player1->o.x, player1->o.y, player1->o.z, 
            (int)player1->yaw, (int)player1->pitch, renderfov, w, h);
    finishstrips();

    setupworld();

    renderstripssky();

    glLoadIdentity();
    glRotated(player1->roll, 0.0, 0.0, 1.0);
    glRotated(player1->pitch, -1.0, 0.0, 0.0);
    glRotated(player1->yaw,   0.0, 1.0, 0.0);
    glRotated(90.0, 1.0, 0.0, 0.0);
    glColor3f(1.0f, 1.0f, 1.0f);
    glDisable(GL_FOG);
    glDepthFunc(GL_GREATER);
    draw_envbox(14, fog*4/3);
    glDepthFunc(GL_LESS);
    glEnable(GL_FOG);

//#define ENABLE_MPIMESH
#ifdef ENABLE_MPIMESH
    transplayer();

    glDisable(GL_FOG);
    rendermpimesh();
    glEnable(GL_FOG);
#endif

    transplayer();

    overbright(2);
    
    renderstrips();

    xtraverts = 0;

    renderclients();
    monsterrender();

    renderentities();

    renderspheres(curtime);
    renderents();

    glDisable(GL_CULL_FACE);

    drawhudgun(fovy, aspect, farplane);

    overbright(1);
    extern int nquads;
    nquads += renderwater(hf);

    overbright(2);
    render_particles(curtime);
    overbright(1);

    glDisable(GL_FOG);

    glDisable(GL_TEXTURE_2D);

    gl_drawhud(w, h, underwater);

    glEnable(GL_CULL_FACE);
    glEnable(GL_FOG);
};

