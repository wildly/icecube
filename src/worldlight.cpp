// worldlight.cpp

#include "cube.h"
#include "benchmark.h"

extern bool hasoverbright;

VAR(lightscale,1,4,100);

void lightray(float bx, float by, const persistent_entity &light)     // done in realtime, needs to be fast
{
    float lx = light.x+(rnd(21)-10)*0.1f;
    float ly = light.y+(rnd(21)-10)*0.1f;
    float dx = bx-lx;
    float dy = by-ly; 
    float dist = (float)sqrt(dx*dx+dy*dy);
    if(dist<1.0f) return;
    int reach = light.attr1;
    int steps = fast_f2nat(reach*reach*1.6f/dist); // can change this for speedup/quality?
    if(steps<=0) return;
    const int PRECBITS = 12;
    const float PRECF = 4096.0f;
    int x = (int)(lx*PRECF); 
    int y = (int)(ly*PRECF); 
    int l = light.attr2<<PRECBITS;
    int stepx = (int)(dx/(float)steps*PRECF);
    int stepy = (int)(dy/(float)steps*PRECF);
    int stepl = fast_f2nat(l/(float)steps); // incorrect: light will fade quicker if near edge of the world

    if(hasoverbright)
    {
        l /= lightscale;
        stepl /= lightscale;
        const int lrecover = 25; // (+25/4096) per step, makes larger area lights fade less toward the edges
        
        if(light.attr3 || light.attr4)      // coloured light version, special case because most lights are white
        {
            // push lightray start point and end point "dimness" steps away along the original direction.
            int dimness = rnd((255-(light.attr2+light.attr3+light.attr4)/3)/16+1);
            x += stepx*dimness;
            y += stepy*dimness;

            if(OUTBORD(x>>PRECBITS, y>>PRECBITS)) return;

            int g = light.attr3<<PRECBITS;
            int stepg = fast_f2nat(g/(float)steps);
            int b = light.attr4<<PRECBITS;
            int stepb = fast_f2nat(b/(float)steps);
            g /= lightscale;
            stepg /= lightscale;
            b /= lightscale;
            stepb /= lightscale;
            loopi(steps)
            {
                sqr *s = S(x>>PRECBITS, y>>PRECBITS); 
                int tl = (l>>PRECBITS)+s->r;
                s->r = tl>255 ? 255 : tl;
                tl = (g>>PRECBITS)+s->g;
                s->g = tl>255 ? 255 : tl;
                tl = (b>>PRECBITS)+s->b;
                s->b = tl>255 ? 255 : tl;
                if(SOLID(s)) return;
                x += stepx;
                y += stepy;
                l -= stepl;
                g -= stepg;
                b -= stepb;
                stepl -= lrecover;
                stepg -= lrecover;
                stepb -= lrecover;
            };
        }
        else        // white light, special optimized version
        {
            int dimness = rnd((255-light.attr2)/16+1);
            x += stepx*dimness;
            y += stepy*dimness;

            if(OUTBORD(x>>PRECBITS, y>>PRECBITS)) return;

            loopi(steps)
            {
                sqr *s = S(x>>PRECBITS, y>>PRECBITS);
                int tl = (l>>PRECBITS)+s->r;
                // There is a special case where "s" has equal rgb light values, but we will leave
                // this branch commented out because I don't know whether it is an optimization.
                // It may be faster to process this case as though "s" was already lit by rgb light.
                //if(s->r == s->g && s->r == s->b) { s->r = s->g = s->b = tl>255 ? 255 : tl; }
                //else
                {
                    s->r = tl>255 ? 255 : tl;
                    tl = (l>>PRECBITS)+s->g;
                    s->g = tl>255 ? 255 : tl;
                    tl = (l>>PRECBITS)+s->b;
                    s->b = tl>255 ? 255 : tl;
                }
                if(SOLID(s)) return;
                x += stepx;
                y += stepy;
                l -= stepl;
                stepl -= lrecover;
            };
        };
    }
    else        // the old (white) light code, here for the few people with old video cards that don't support overbright
    {
        loopi(steps)
        {
            sqr *s = S(x>>PRECBITS, y>>PRECBITS); 
            int light = l>>PRECBITS;
            if(light>s->r) s->r = s->g = s->b = (uchar)light;
            if(SOLID(s)) return;
            x += stepx;
            y += stepy;
            l -= stepl;
        };
    };
    
};

// squarelights configures the coverage of light sources (lit area)
VAR(squarelights, 0, 1, 1);
void calclightsource(const persistent_entity &l)
{
    // rndtimeoffset is needed so that rnd() can produce different results when SDL_GetTicks() is the same (i.e. more than 1000 fps)
    static Uint32 rndtimeoffset = 0;

    // don't need this assert because we know this must be positive, or else the square lights algorithm doesn't work
    //assert(l.attr1 >= 0);
    //int areach = abs(l.attr1);
    int reach = l.attr1;

    // needed for stable lighting due to random lightray offsets
    rndreset();

    // spacing between lightray endpoints
    const float s = 2*0.8f;

    if(squarelights)
    {
        int sx = l.x-reach;
        int ex = l.x+reach;
        int sy = l.y-reach;
        int ey = l.y+reach;
        int lrays = fast_f2nat(2*reach/s);     // lrays per "side" of the square
        float soffset = (2*reach-lrays*s)/2;   // offset to align square to center of light
        if(lrays) lrays = (4*lrays)-2;         // total number of lrays; subtract 2 to avoid double counting the corners
        for(float sx2 = sx+soffset;   sx2<=ex;   sx2+=s) { lightray(sx2, sy+soffset, l); lightray(sx2, ey-soffset, l); };
        for(float sy2 = sy+soffset+s; sy2<=ey-s; sy2+=s) { lightray(sx+soffset, sy2, l); lightray(ex-soffset, sy2, l); }; // start at "sy+s" and end at "ey-s" to avoid double counting the corners
    }
    else // circular lights
    {
        // determine # of lightrays
        const float c = TAU*reach;
        const float scale = 4.0f/PI;        // ratio of "perimeter of square" (4*reach*2) to "circumference of circle" (TAU*reach)
        int lrays = fast_f2nat(scale*c/s);  // preserve brightness by generating same # of rays
        //if(lrays < 4) lrays = 4;          // minimum 4 lightrays

        // random offset to starting radian
        const float theta0 = TAU*rnd(128)/128;
        loopi(lrays)
        {
            // preserve lit area by scaling dx and dy to make the circle of light rays bigger
            // the scale factor is the square root of the ratio of "area of square" (2*reach*2*reach) to "area of circle" (1/2*TAU*reach*reach) and is equal to sqrt(4/PI)
            float dx = sqrt(scale)*reach*sin(PI2*i/lrays+theta0);
            float dy = sqrt(scale)*reach*cos(PI2*i/lrays+theta0);
            lightray(l.x+dx, l.y+dy, l);
        };
    };

    rndtimeoffset++;
    rndtime(rndtimeoffset);
};

struct lightsample_s
{
    uchar r, g, b;
};

lightsample_s *fetchsamples(block &a)
{
    lightsample_s *samples = (lightsample_s *)malloc((a.xs+2)*(a.ys+2)*sizeof(lightsample_s));
    loopi(a.xs+2) loopj(a.ys+2)
    {
        samples[i*(a.ys+2)+j].r = S(a.x+i-1,a.y+j-1)->r;
        samples[i*(a.ys+2)+j].g = S(a.x+i-1,a.y+j-1)->g;
        samples[i*(a.ys+2)+j].b = S(a.x+i-1,a.y+j-1)->b;
    };
    return samples;
};

void freesamples(lightsample_s *s)
{
    free(s);
};

void postlightarea_lightfilter(block &a)
{
#define LIGHT_SMOOTHING
#ifdef LIGHT_SMOOTHING
    lightsample_s *samples = fetchsamples(a);
    loop(x,a.xs) loop(y,a.ys)   // assumes area not on edge of world
    {
        sqr *s = S(x+a.x,y+a.y);
//        #define median(m) s->m = (s->m*2 + SW(s,1,0)->m*2  + SW(s,0,1)->m*2 \
//                                         + SW(s,-1,0)->m*2 + SW(s,0,-1)->m*2 \
//                                         + SW(s,1,1)->m    + SW(s,1,-1)->m \
//                                         + SW(s,-1,1)->m   + SW(s,-1,-1)->m)/14;  // median is 4/2/1 instead
        #define weightedmean(m) s->m = (samples[(x+1)*(a.ys+2)+(y+1)].m*2 + samples[(x+1)*(a.ys+2)+y].m*2 + samples[(x+1)*(a.ys+2)+y+2].m*2 \
                                                                          + samples[(x)*(a.ys+2)+y+1].m*2 + samples[(x+2)*(a.ys+2)+y+1].m*2 \
                                                                          + samples[(x)*(a.ys+2)+y].m + samples[(x)*(a.ys+2)+y+2].m \
                                                                          + samples[(x+2)*(a.ys+2)+y].m + samples[(x+2)*(a.ys+2)+y+2].m)/14;
        weightedmean(r);
        weightedmean(g);
        weightedmean(b);
    };
    freesamples(samples);
#endif
}

void postlightarea(block &a)    // median filter, smooths out random noise in light and makes it more mipable
{
    postlightarea_lightfilter(a);
    remip(a);
};

void setlightarea(block &a, int light)
{
    loop(x,a.xs) loop(y,a.ys)   // assumes area not on edge of world
    {
        sqr *s = S(x+a.x,y+a.y);
        s->r = s->g = s->b = light;
    };
};

void mergelightarea(block &a, lightsample_s *ls)
{
    loop(x,a.xs) loop(y,a.ys)   // assumes area not on edge of world
    {
        sqr *s = S(x+a.x,y+a.y);
        int tl;
        #define addsaturate(m) tl = s->m + ls[(x+1)*(a.ys+2)+(y+1)].m; if(tl>255) tl = 255; s->m = tl;
        addsaturate(r);
        addsaturate(g);
        addsaturate(b);
    };
};

void calclight()
{
    director.log_event(CALCLIGHT_ST, SDL_GetTicks());
    loop(x,ssize) loop(y,ssize)
    {
        sqr *s = S(x,y);
        s->r = s->g = s->b = 10;
    };

    loopv(ents)
    {
        persistent_entity &e = ents[i];
        if(e.type==LIGHT) calclightsource(e);
    };

    block entiremap = { 1, 1, ssize-2, ssize-2 };
    postlightarea(entiremap);
    setvar("fullbright", 0);
    director.log_event(UPDATE_ST, SDL_GetTicks()); // assumes we were in UPDATE_ST before calling calclight!
};

VARP(dynlight, 0, 16, 32);

typedef struct dynlight_s { vec v; int reach, strength, colorindex; } DynLight;
vector<DynLight> dynlights;

// A DynLightSource represents a dynlight that is not attached to another object, i.e. they only have a location and duration.
// It is currently used to create the blue glow from teleport sparks.
// A DynLightSource's lifetime can span multiple frames.
// A DynLightSource starts fading out when duration is less than fadeout.
typedef struct dynlight_source_s { DynLight d; int duration, fadeout; bool expired; } DynLightSource;
vector<DynLightSource> sources;

vector<block *> dynlightbackups;

void cleardynlights()
{
    while(!dynlightbackups.empty())
    {
        block *backup = dynlightbackups.pop();
        blockpaste(*backup);
        free(backup);
    };

    dynlights.setsize(0);
};

enum { DYNLIGHT_ROCKET = 0, DYNLIGHT_QUAD, DYNLIGHT_GRENADE };

int dynlightcolors[][3] =
{
    { 232, 184, 104 }, // rocket
    { 255,   0,   0 }, // quad damage
    { 104, 184, 232 }  // grenade
};

void adddynlight(vec &v, int reach, int strength, dynent *owner, int colorindex)
{
    if(!owner) return;
    if(!reach) reach = dynlight;
    if(owner->monsterstate && colorindex != DYNLIGHT_QUAD) reach = reach/2;
    if(!reach) return;
    if(v.x<0 || v.y<0 || v.x>ssize || v.y>ssize) return;

    DynLight d = { v, reach, strength, colorindex };

    dynlights.add(d);
}

void add_dynlightsource(vec &v, int reach, int strength, int colorindex, int duration, int fadeout)
{
    if(!reach) reach = dynlight;
    if(!reach) return;
    if(v.x<0 || v.y<0 || v.x>ssize || v.y>ssize) return;
    DynLight d = { v, reach, strength, colorindex };

    DynLightSource s = { d, duration, fadeout, false };

    sources.add(s);
};

// Look for and remove expired dynlightsources.
// This is the same algorithm to remove expired dynlightsources that is used in cleanupprojectiles.
void cleanup_dynlightsources()
{
    if(sources.length() < 1) return;

    // initialize and find the location of backmost unexpired dynlightsource
    int numExpired = 0;
    int backIndex = sources.length() - 1;
    while(sources[backIndex].expired) { --backIndex; ++numExpired; if(backIndex == -1) break; };

    // Case "backIndex == -1: all dynlightsource are expired
    if(!(backIndex == -1))
    {
        loopv(sources)
        {
            // Case "i == backIndex": we've reached the backmost unexpired dynlightsource
            if(i == backIndex) break;

            // sources[i] is not expired. Continue.
            if(!sources[i].expired) continue;

            // sources[i] is expired. Swap it with sources[backIndex].
            DynLightSource tmp = sources[i];
            sources[i] = sources[backIndex];
            sources[backIndex] = tmp;

            // find the next backmost unexpired dynlightsource
            while(sources[backIndex].expired) { --backIndex; ++numExpired; if(backIndex == i) break; };

            // Case "i == backIndex": we've reached the backmost unexpired dynlightsource
            if(i == backIndex) break;
        };
    };

    sources.setsize(sources.length() - numExpired);
};

void clear_dynlightsources()
{
    sources.setsize(0);
};

void check_dynlightsources(int time)
{
    loopv(sources)
    {
        if((sources[i].duration -= time)<=0)
        {
            sources[i].expired = true;
            continue;
        }

        DynLight d = sources[i].d;
        if(sources[i].fadeout && sources[i].duration < sources[i].fadeout)
        {
            d.strength = (int)(d.strength*((float)sources[i].duration/(float)sources[i].fadeout));
        }
        dynlights.add(d);
    }

    // this is the same algorithm to remove expired dynlightsources that is used in cleanupprojectiles
    cleanup_dynlightsources();
}

void dodynlights_subroutine(DynLight &d)
{
    int backupradius = 50; // dependent on lightray random offsets!
    block backup = { (int)d.v.x-backupradius, (int)d.v.y-backupradius, 2*backupradius+1, 2*backupradius+1 };

    // ensure backup dimensions are valid
    if(backup.x<1) backup.x = 1;
    if(backup.y<1) backup.y = 1;
    if(backup.xs+backup.x>ssize-2) backup.xs = ssize-2-backup.x;
    if(backup.ys+backup.y>ssize-2) backup.ys = ssize-2-backup.y;

    dynlightbackups.add(blockcopy(backup));      // backup area before rendering in dynlight
    // also backup light values before rendering in this dynlight
    // note: fetchsamples has additional border of 1 on each side compared to the dimensions specified in backup for the light filter
    lightsample_s *ls = fetchsamples(backup);

    // set up the dynlight's light
    int reach = d.reach;
    int strength = d.strength;
    int colorindex = d.colorindex;

    int color[3] = { dynlightcolors[colorindex][0], dynlightcolors[colorindex][1], dynlightcolors[colorindex][2] };
    loopi(3) color[i] = strength*color[i]/255;
    if(!color[1] && !color[2])          // ensure light is processed as a colored light
    {
        color[0] = max(color[0], 1);    // prefer dimmest yellow instead of dimmest green
        color[1] = 1;
    };
    persistent_entity l = { (int)d.v.x, (int)d.v.y, (int)d.v.z, reach, LIGHT, color[0], color[1], color[2] };

    // process the dynlight's light
    setlightarea(backup, 0); // reset each square's light in the area to 0
    calclightsource(l);
    // hack: run the light filter in this smaller area (shrink by 1 on each border), then merge in the original light values in the FULL backup, then remip
    block smallerbackup = { backup.x+1, backup.y+1, backup.xs-2, backup.ys-2 };
    postlightarea_lightfilter(smallerbackup);
    mergelightarea(backup, ls);
    freesamples(ls);
    remip(backup);
};

void dodynlights()
{
    // update dynlightsources (they will add a dynlight to the loop below or become expired)
    check_dynlightsources(curtime);

    // loop over all dynlights
    const int count = dynlights.length();
    loopi(count)
    {
        dodynlights_subroutine(dynlights[i]);
    }; // loop over all dynlights
};

void dynlightstats(int &numlights, int &numsources)
{
    numlights = dynlights.length();
    numsources = sources.length();
}

// utility functions also used by editing code

block *blockcopy(block &s)
{
    block *b = (block *)alloc(sizeof(block)+s.xs*s.ys*sizeof(sqr));
    *b = s;
    sqr *q = (sqr *)(b+1);
    for(int x = s.x; x<s.xs+s.x; x++) for(int y = s.y; y<s.ys+s.y; y++) *q++ = *S(x,y);
    return b;
};

void blockpaste(block &b)
{
    sqr *q = (sqr *)((&b)+1);
    for(int x = b.x; x<b.xs+b.x; x++) for(int y = b.y; y<b.ys+b.y; y++) *S(x,y) = *q++;
    remipmore(b);
};


