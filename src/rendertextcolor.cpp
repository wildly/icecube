// rendertextcolor.cpp for IceCube
// Copyright (C) 2015-2016 Willy Deng
// See COPYING-IceCube.txt for the license text.

// This file contains a modified version of text_color() and related code from
// the Sauerbraten Collect Edition release. See COPYING-Sauerbraten.txt for the
// license text.
// Sauerbraten game engine source code, any release.
// Copyright (C) 2001-2012 Wouter van Oortmerssen, Lee Salzman, Mike Dysart, Robert Pointon, and Quinton Reeves

#include "cube.h"

#ifdef LOAD_OPENGL_EXTENSIONS
extern PFNGLBLENDEQUATIONSEPARATEPROC glBlendEquationSeparate;
#endif

vector<char> colorStack;
const char defaultColor = '7'; // default text color is white
char curColor = defaultColor;

void text_color(char c)
{
    if(c=='s') // save color
    {
        if(colorStack.length() < 100)
            colorStack.push_back(curColor);
        return;
    }
    if(c=='r') // restore color
    {
        if(!colorStack.empty())
        {
            c = colorStack.last();
            colorStack.pop_back();
            if(c != 8) if(glBlendEquationSeparate) glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
        }
        else
        {
            c = defaultColor;
            // don't return here; continue onward to update curColor using retrieved 'c' and also set GL color state.
        }
    }
    switch(c) // set curColor
    {
        case '0': curColor = c; glColor3ub( 64, 255, 128); break;   // green: player talk
        case '1': curColor = c; glColor3ub( 96, 160, 255); break;   // blue: "echo" command
        case '2': curColor = c; glColor3ub(255, 192,  64); break;   // yellow: gameplay messages
        case '3': curColor = c; glColor3ub(255,  64,  64); break;   // red: important errors
        case '4': curColor = c; glColor3ub(128, 128, 128); break;   // gray
        case '5': curColor = c; glColor3ub(192,  64, 192); break;   // magenta
        case '6': curColor = c; glColor3ub(255, 128,   0); break;   // orange
        case '7': curColor = c; glColor3ub(255, 255, 255); break;   // white
        case '8':
            // black text color via subtraction.
            // use only as "/fs/f8[text]/fr" so that the blend function can be restored to GL_FUNC_ADD upon restoring the previous color.
            curColor = c;
            glColor3ub(255, 255, 255);
            if(glBlendEquationSeparate) glBlendEquationSeparate(GL_FUNC_REVERSE_SUBTRACT, GL_FUNC_ADD);
            break;
    }
}

void clearstack()
{
    colorStack.setsize(0);
    text_color(defaultColor);
}
