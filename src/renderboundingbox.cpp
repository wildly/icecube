// Selected functions from rendermodel.cpp in Sauerbraten Collect Edition
// Modified to be compatible with Cube

// Portions from Sauerbraten are: Copyright (C) 2001-2012 Wouter van Oortmerssen, Lee Salzman, Mike Dysart, Robert Pointon, and Quinton Reeves
// See LICENSE-Sauerbraten.txt for the Sauerbraten source code license

#include "cube.h"

// showboundingbox is a bitfield
// 0x01: Show bounding boxes for dynents
// 0x02: Show bounding boxes for map models
VAR(showboundingbox, 0, 0, 3);

// Cube uses XZY coordinates, while Sauerbraten and newer use XYZ coordinates?
// 90 degree rotation along x axis, then using inverted z values seems to be one way to get the needed transform.
void render2dbox(vec &o, float x, float y, float z)
{
    float invert = -1.0f;
    glRotatef(90, 1, 0, 0);
    glBegin(GL_LINE_LOOP);
    glVertex3f(o.x, o.y, invert*o.z);
    glVertex3f(o.x, o.y, invert*(o.z+z));
    glVertex3f(o.x+x, o.y+y, invert*(o.z+z));
    glVertex3f(o.x+x, o.y+y, invert*o.z);
    glEnd();
    glRotatef(-90, 1, 0, 0);
}

void render3dbox(vec &o, float tofloor, float toceil, float xradius, float yradius = 0)
{
    if(yradius<=0) yradius = xradius;
    vec c = o;
    vec cdelta = vec( xradius, yradius, tofloor );
    vsub(c, cdelta);
    float xsz = xradius*2, ysz = yradius*2;
    float h = tofloor+toceil;
    glDisable(GL_TEXTURE_2D);
    glDepthMask(GL_FALSE);
    glColor3f(1, 1, 1);
    render2dbox(c, xsz, 0, h);
    render2dbox(c, 0, ysz, h);
    cdelta.x = xsz; cdelta.y = ysz; cdelta.z = 0;
    vadd(c, cdelta);
    render2dbox(c, -xsz, 0, h);
    render2dbox(c, 0, -ysz, h);
    xtraverts += 16;
    glEnable(GL_TEXTURE_2D);
    glDepthMask(GL_TRUE);
}

// d or e must be valid, and valid e must have valid mapmodelinfo
void drawboundingbox(dynent *d, extentity *e, bool editmode)
{
    if(showboundingbox && editmode)
    {
        if(d && showboundingbox & 1)
        {
            render3dbox(d->o, d->eyeheight, d->aboveeye, d->radius);
        }
        else if(e && showboundingbox & 2)
        {
            vec center, radius;
            mapmodelinfo &mmi = getmminfo(e->attr2);
            radius.x = radius.y = mmi.rad;
            radius.z = mmi.h/2.0f; // "z radius" is 1/2 height
            center.x = e->x;
            center.y = e->y;
            center.z = S(e->x, e->y)->floor+e->attr3+mmi.zoff+mmi.h/2.0f; // "center" is bottom of the mapmodel in the map + 1/2 height
            render3dbox(center, radius.z, radius.z, radius.x, radius.y);
        }
    }
}
