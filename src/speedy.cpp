// speedy.cpp: Talk with the operating system to ensure everything runs smoothly

#include "cube.h"

#if defined(_WINDOWS)
/**
 *  Register the game with Windows Multimedia Class Scheduler Service (MMCSS)
 *  MMCSS is only present on Windows Vista, Server 2008, or later.
 *
 *  The performance of the game and our other multimedia should improve when
 *  our tasks/threads are registered with the Multimedia Class Scheduler Service.
 *  We can set any of our tasks/threads as a kind of multimedia class.
 *  The presence of improved performance depends on the user's MMCSS settings
 *  in their Windows Registry for the multimedia classes which we use.
 *  This implementation interfaces with MMCSS through Avrt.dll for backward
 *  compatibility with Windows XP.
 */

// When two or more threads cooperate to perform a task, they can be scheduled
// together if those threads register with MMCSS using the same TaskIndex.
static DWORD taskIndex_Game = 0;
//static DWORD taskIndex_Music = 0;     // not used
//static DWORD taskIndex_Mumble = 0;    // not used

void registerGameWithMMCSS()
{
    typedef HANDLE (CALLBACK* FP_AvSetMmThreadCharacteristicsA)(LPCTSTR TaskName, LPDWORD TaskIndex);

    FP_AvSetMmThreadCharacteristicsA AvSetMmThreadCharacteristicsA;
    HINSTANCE hDLL;
    HANDLE hTask;
    //HRESULT hr; // todo: Use HRESULT error messages instead of or complementing mine.
    OSVERSIONINFOEX versioninfo;

    ZeroMemory(&versioninfo, sizeof(OSVERSIONINFOEX));
    versioninfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
    GetVersionEx((OSVERSIONINFO*)&versioninfo);

    if (versioninfo.dwMajorVersion < 6) // Not available on Windows XP (5.1), Windows XP 64-bit / Server 2003 (5.2) or earlier
    {
        conoutf("Unable to register task with Multimedia Class Scheduler Service:");
        conoutf("Multimedia Class Scheduler Service is not available on Windows XP or earlier.");
        return;
    }

    hDLL = LoadLibrary("Avrt.dll");
    if (hDLL == NULL)
    {
        conoutf("Unable to register task with Multimedia Class Scheduler Service:");
        conoutf("Unable to load Avrt.dll");
    }
    else
    {
        AvSetMmThreadCharacteristicsA = (FP_AvSetMmThreadCharacteristicsA)GetProcAddress(hDLL, "AvSetMmThreadCharacteristicsA");
        if (AvSetMmThreadCharacteristicsA == NULL)
        {
            conoutf("Unable to register task with Multimedia Class Scheduler Service:");
            conoutf("Unable to find proc AvSetMmThreadCharacteristicsA");
        }
        else
        {
            hTask = AvSetMmThreadCharacteristicsA("Games", &taskIndex_Game);
            if (hTask == NULL)
            {
                conoutf("Unable to register task with Multimedia Class Scheduler Service:");
                conoutf("AvSetMmThreadCharacteristicsA returned NULL");
            }
            else
            {
                conoutf("Registering task with MMCSS using MM thread characteristic 'Games'");
                // Close handle when valid handle returned.
                // We are not using it for anything at this time.
                CloseHandle(hTask);
            }
        }
    }
    FreeLibrary(hDLL);
}
#endif

// If Windows client, try to register with MMCSS.
// On other platforms, do nothing.
void init_speedy_client()
{
#if defined(_WINDOWS)
    registerGameWithMMCSS();
#endif
}

// If win32 dedicated server, set to HIGH_PRIORITY_CLASS.
// On other platforms, do nothing.
void init_speedy_dedicated_server()
{
#ifdef WIN32
    SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
#endif
}
