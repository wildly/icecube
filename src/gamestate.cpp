#include "cube.h"
#include "gamestate.h"

BaseClock *InternalClockPtr;
WarpableClock *GameClockPtr;
WarpableClock *PhysicsClockPtr;

void constructClocks()
{
    static BaseClock internalClock;
    static WarpableClock gameClock(internalClock);  // can be paused
    static WarpableClock physicsClock(gameClock);   // can be warped
    InternalClockPtr = &internalClock;
    GameClockPtr = &gameClock;
    PhysicsClockPtr = &physicsClock;
};
