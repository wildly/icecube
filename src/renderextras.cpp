// renderextras.cpp: misc gl render code and the HUD

#include "cube.h"
#include "benchmark.h"
#include "gamestate.h"

void line(int x1, int y1, float z1, int x2, int y2, float z2)
{
    glBegin(GL_POLYGON);
    glVertex3f((float)x1, z1, (float)y1);
    glVertex3f((float)x1, z1, y1+0.01f);
    glVertex3f((float)x2, z2, y2+0.01f);
    glVertex3f((float)x2, z2, (float)y2);
    glEnd();
    xtraverts += 4;
};

void linestyle(float width, int r, int g, int b)
{
    glLineWidth(width);
    glColor3ub(r,g,b);
};

void box(block &b, float z1, float z2, float z3, float z4)
{
    glBegin(GL_POLYGON);
    glVertex3f((float)b.x,      z1, (float)b.y);
    glVertex3f((float)b.x+b.xs, z2, (float)b.y);
    glVertex3f((float)b.x+b.xs, z3, (float)b.y+b.ys);
    glVertex3f((float)b.x,      z4, (float)b.y+b.ys);
    glEnd();
    xtraverts += 4;
};

void dot(int x, int y, float z)
{
    const float DOF = 0.1f;
    glBegin(GL_POLYGON);
    glVertex3f(x-DOF, (float)z, y-DOF);
    glVertex3f(x+DOF, (float)z, y-DOF);
    glVertex3f(x+DOF, (float)z, y+DOF);
    glVertex3f(x-DOF, (float)z, y+DOF);
    glEnd();
    xtraverts += 4;
};

void blendbox(int x1, int y1, int x2, int y2, bool border)
{
    glDepthMask(GL_FALSE);
    glDisable(GL_TEXTURE_2D);
    glBlendFunc(GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
    glBegin(GL_QUADS);
    if(border) glColor3d(0.5, 0.3, 0.4); 
    else glColor3d(1.0, 1.0, 1.0);
    glVertex2i(x1, y1);
    glVertex2i(x2, y1);
    glVertex2i(x2, y2);
    glVertex2i(x1, y2);
    glEnd();
    glDisable(GL_BLEND);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_POLYGON);
    glColor3d(0.2, 0.7, 0.4); 
    glVertex2i(x1, y1);
    glVertex2i(x2, y1);
    glVertex2i(x2, y2);
    glVertex2i(x1, y2);
    glEnd();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    xtraverts += 8;
    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glDepthMask(GL_TRUE);
};

const int MAXSPHERES = 50;
struct sphere { vec o; float size, max; int type; sphere *next; dynent *owner; };
sphere spheres[MAXSPHERES], *slist = NULL, *sempty = NULL;
bool sinit = false;

void newsphere(vec &o, float max, int type, dynent *owner)
{
    if(!sinit)
    {
        loopi(MAXSPHERES)
        {
            spheres[i].next = sempty;
            sempty = &spheres[i];
        };
        sinit = true;
    };
    if(sempty)
    {
        sphere *p = sempty;
        sempty = p->next;
        p->o = o;
        p->max = max;
        p->size = 1;
        p->type = type;
        p->next = slist;
        slist = p;
        p->owner = owner;
    };
};

void dodynlightforspheres()
{
    for(sphere *p, **pp = &slist; p = *pp;)
    {
        // Explosions have fading dynlight
        if(p->type == 0 || p->type == 2)
        {
            float fade = clamp(1.0f-p->size/p->max, 0.0f, 1.0f);
            adddynlight(p->o, 0, (int)(255*fade), p->owner, p->type <= 1 ? 0 : 2);
        }
        else // Glowing spherical projectiles have constant dynlight
        {
            adddynlight(p->o, 0, 128, p->owner, p->type <= 1 ? 0 : 2);
        }
        pp = &p->next;
    };
};

void clearspheres()
{
    for(sphere *p, **pp = &slist; p = *pp;)
    {
        *pp = p->next;
        p->next = sempty;
        sempty = p;
    };
};

int renderrocketexplosion(sphere *p, float size, int color)
{
    float colorscale[] = { 1, 1, 1 };
    if(color == 2) { colorscale[0] = 104/255.0f; colorscale[1] = 184/255.0f; colorscale[2] = 232/255.0f; }; // hack to turn smoke texture aqua
    glPushMatrix();
    glColor4f(colorscale[0]*1.0f, colorscale[1]*1.0f, colorscale[2]*1.0f, 1.0f-size);
    glTranslatef(p->o.x, p->o.z, p->o.y);
    glRotatef(lastmillis/5.0f, 1, 1, 1);
    glScalef(p->size, p->size, p->size);
    glCallList(1);
    glScalef(0.8f, 0.8f, 0.8f);
    glCallList(1);
    glPopMatrix();
    return 12*6*2;
};

int renderrocketmissile(sphere *p, float alphaScale, int color)
{
    float colorscale[] = { 1, 1, 1 };
    if(color == 2) { colorscale[0] = 104/255.0f; colorscale[1] = 184/255.0f; colorscale[2] = 232/255.0f; }; // hack to turn smoke texture aqua
    glPushMatrix();
    glColor4f(colorscale[0]*1.0f, colorscale[1]*1.0f, colorscale[2]*1.0f, alphaScale*0.5f);
    glTranslatef(p->o.x, p->o.z, p->o.y);
    glRotatef(lastmillis/5.0f, 1, 1, 1);
    glScalef(p->size, p->size, p->size);
    glCallList(1);
    glPopMatrix();
    return 12*6*2;
};

void renderspheres(int time)
{
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    for(sphere *p, **pp = &slist; p = *pp;)
    {
        if(p->type <= 1) glBindTexture(GL_TEXTURE_2D, 4);
        else glBindTexture(GL_TEXTURE_2D, 7);

        if(p->type == 0 || p->type == 2) // RL_EXPLOSION / Grenade Explosion
            xtraverts += renderrocketexplosion(p, p->size/p->max, p->type <= 1 ? 0 : 2);
        else // RL_MISSILE / Grenade
        {
            p->size = p->max;
            // Fade in player1's rocket missile to avoid its sudden appearance in the player's screen upon firing
            float alpha = 1.0f;
            if(p->owner == player1)
            {
                vdist(dist, v, p->o, p->owner->o);
                alpha = dist/3.0f;
            };
            xtraverts += renderrocketmissile(p, alpha, p->type <= 1 ? 0 : 2);
        };

        if(p->size>=p->max || p->type == 1 || p->type == 3)
        {
            *pp = p->next;
            p->next = sempty;
            sempty = p;
        }
        else
        {
            p->size += time/100.0f;   
            pp = &p->next;
        };
    };

    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
};

// Show selected info about the dynent in crosshair
enum
{
    IN_NOTHING = 0,
    IN_HEALTH,
    IN_ARMOR,
    IN_AMMO,
    IN_MOVEMENT,
    IN_RANGE,
    IN_INTENT,
    IN_WORLDPOS,
    IN_MAX = IN_WORLDPOS
};
string inspectstring;
VAR(inspect, 0, 0, IN_MAX);

string closeent;
const char *entnames[] =
{
    "none?", "light", "playerstart",
    "shells", "bullets", "rockets", "riflerounds",
    "health", "healthboost", "greenarmour", "yellowarmour", "quaddamage", 
    "teleport", "teledest", 
    "mapmodel", "monster", "trigger", "jumppad",
    "?", "?", "?", "?", "?", 
};

extern float totalmillis;
int nextEntitySparkle = 0;
const int entitySparkleInterval = 10;
bool doEntitySparkle = true;
extern uint haspointsprite;
void renderents()       // show sparkly thingies for map entities in edit mode
{
    closeent[0] = 0;
    if(!editmode) return;
    if(doEntitySparkle || (int)totalmillis >= nextEntitySparkle)
    {
        nextEntitySparkle = (int)totalmillis + entitySparkleInterval;
        doEntitySparkle = true;
    };
    loopv(ents)
    {
        extentity &e = ents[i];
        if(e.type==NOTUSED) continue;
        vec v = vec(e.x, e.y, e.z);
        if(doEntitySparkle) particle_splash(PART_EDIT, 1, 120, v);
    };
    doEntitySparkle = false;
    int e = closestent();
    if(e>=0)
    {
        extentity &c = ents[e];
        formatstring(closeent)("closest entity = %s (%d, %d, %d, %d), selection = (%d, %d)", entnames[c.type], c.attr1, c.attr2, c.attr3, c.attr4, getvar("selxs"), getvar("selys"));
    };
};

void loadsky(char *basename)
{
    static string lastsky = "";
    if(strcmp(lastsky, basename)==0) return;
    const char *side[] = { "ft", "bk", "lf", "rt", "dn", "up" };
    int texnum = 14;
    loopi(6)
    {
        defformatstring(name)("packages/%s_%s.jpg", basename, side[i]);
        int xs, ys;
        if(!installtex(texnum+i, path(name), xs, ys, true)) conoutf("could not load sky textures");
    };
    copystring(lastsky, basename);
};

COMMAND(loadsky, ARG_1STR);

float cursordepth;
GLint viewport[4];
GLdouble mm[16], pm[16];
vec worldpos;

void readmatrices()
{
    glGetIntegerv(GL_VIEWPORT, viewport);
    glGetDoublev(GL_MODELVIEW_MATRIX, mm);
    glGetDoublev(GL_PROJECTION_MATRIX, pm);
};

void reset_orient_and_worldpos()
{
    cursordepth = 0.9f;
    worldpos.x = 0.0f;
    worldpos.y = 0.0f;
    worldpos.z = 0.0f;
    glGetDoublev(GL_MODELVIEW_MATRIX, mm);
    vec r = vec( (float)mm[0], (float)mm[4], (float)mm[8] );
    vec u = vec( (float)mm[1], (float)mm[5], (float)mm[9] );
    setorient(r, u);
};

// stupid function to cater for stupid ATI linux drivers that return incorrect depth values

float depthcorrect(float d)
{
	return (d<=1/256.0f) ? d*256 : d;
};

// find out the 3d target of the crosshair in the world easily and very acurately.
// sadly many very old cards and drivers appear to fuck up on glReadPixels() and give false
// coordinates, making shooting and such impossible.
// also hits map entities which is unwanted.
// could be replaced by a more acurate version of monster.cpp los() if needed

void readdepth(int w, int h)
{
    glReadPixels(w/2, h/2, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &cursordepth);
    double worldx = 0, worldy = 0, worldz = 0;
    gluUnProject(w/2, h/2, depthcorrect(cursordepth), mm, pm, viewport, &worldx, &worldz, &worldy);
    worldpos.x = (float)worldx;
    worldpos.y = (float)worldy;
    worldpos.z = (float)worldz;
    vec r = vec( (float)mm[0], (float)mm[4], (float)mm[8] );
    vec u = vec( (float)mm[1], (float)mm[5], (float)mm[9] );
    setorient(r, u);
};

void drawicon(float tx, float ty, int x, int y)
{
    glBindTexture(GL_TEXTURE_2D, 5);
    glBegin(GL_QUADS);
    tx /= 192;
    ty /= 192;
    float o = 1/3.0f;
    int s = 120;
    glTexCoord2f(tx,   ty);   glVertex2i(x,   y);
    glTexCoord2f(tx+o, ty);   glVertex2i(x+s, y);
    glTexCoord2f(tx+o, ty+o); glVertex2i(x+s, y+s);
    glTexCoord2f(tx,   ty+o); glVertex2i(x,   y+s);
    glEnd();
    xtraverts += 4;
};

void invertperspective()
{
    // This only generates a valid inverse matrix for matrices generated by gluPerspective()
    GLdouble inv[16];
    memset(inv, 0, sizeof(inv));

    inv[0*4+0] = 1.0/pm[0*4+0];
    inv[1*4+1] = 1.0/pm[1*4+1];
    inv[2*4+3] = 1.0/pm[3*4+2];
    inv[3*4+2] = -1.0;
    inv[3*4+3] = pm[2*4+2]/pm[3*4+2];

    glLoadMatrixd(inv);
};

VARP(crosshairsize, 0, 15, 50);

int dblend = 0;
void damageblend(int n) { dblend += 3*n; };

VAR(hidehud, 0, 0, 1);
VAR(hidestats, 0, 0, 1);
VARP(showfps,0,1,1);
VARP(fpsminmax,0,1,1);
VARP(showgamespeed,0,0,1);
VARP(particleinfo,0,0,1);
VARP(lightinfo,0,0,1);
VARP(healthinfo,0,0,1);
VARP(armourinfo,0,0,1);
VARP(movementinfo,0,0,1);
VARP(clockinfo,0,0,1);
VAR(debugsound,0,0,1);
VARP(crosshairfx, 0, 1, 1);

extern int scr_w, scr_h;
float getGUIAspectRatio()
{
    //return aspectRatio*w/h;
    return (float)scr_w/(float)scr_h;
};

// Point scaling for point sprites, etc
// 0.5 = 800x600 4:3 aspect ratio (1.333), intended for ~60DPI
// 0.9 = 1920x1080 wide screen (1.778), intended for ~96DPI
// 1.3 = 2560x1600 16:10 aspect ratio (1.600)
// 1.8 = 3840x2160 wide screen (1.778)
float getGUIPointScaling()
{
    return scr_w/1200.0f;
};

int relativehudpos(float pos)
{
    return (int)(pos/VIRTW_4_BY_3*VIRTW);
};

void do_stats();
void do_stats_required();
extern int nquads, curvert;
extern int nrendered, nculled;
void gl_drawhud(int w, int h, bool underwater)
{
    readmatrices();
    if(editmode)
    {
        if(cursordepth==1.0f) worldpos = player1->o;
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        cursorupdate();
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    };

    glDisable(GL_DEPTH_TEST);
    invertperspective();
    glPushMatrix();  
    glOrtho(0, VIRTW, VIRTH, 0, -1, 1);
    glEnable(GL_BLEND);

    glDepthMask(GL_FALSE);

    if(dblend || underwater)
    {
        glBlendFunc(GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
        glBegin(GL_QUADS);
        if(dblend) glColor3d(0.0f, 0.9f, 0.9f);
        else glColor3d(0.9f, 0.5f, 0.0f);
        glVertex2i(0, 0);
        glVertex2i(VIRTW, 0);
        glVertex2i(VIRTW, VIRTH);
        glVertex2i(0, VIRTH);
        glEnd();
        dblend -= curtime;
        if(dblend<0) dblend = 0;
    };

    glEnable(GL_TEXTURE_2D);

    char *command = getcurcommand();
    char *player = playerincrosshair();
    dynent *dinfo = dynentincrosshair();
    if(command) draw_textf("> %s_", relativehudpos(20), 1570, 2, command);
    else if(closeent[0]) draw_text(closeent, relativehudpos(20), 1570, 2);
    else if(player) draw_text(player, relativehudpos(20), 1570, 2);
    else if((dinfo && inspect > 0) || inspect==IN_WORLDPOS)
    {
        if(inspect == IN_HEALTH)
            formatstring(inspectstring)("health: %d/%d (%.1f%%)", dinfo->health, dinfo->maxhealth(), (float)dinfo->health/(float)dinfo->maxhealth()*100.0f);
        else if(inspect == IN_MOVEMENT)
        {
            extern const char *mhintnames[2][2];
            vec v = estimatevelocity(dinfo);
            string vecStr; vecStr[0] = 0; prettyprintvec(vecStr, v, ",", 0.1f);

            // prepare movement type string
            defformatstring(mtStr)("%s", dinfo->onfloor ? "floor" : dinfo->timeinair ? "air" : "float");
            if(dinfo->inwater) concatstring(mtStr, "|water");

            formatstring(inspectstring)("movement: %s state: %s timeinair: %d ", vecStr, mtStr, dinfo->timeinair);
            concatstring(inspectstring, mhintnames[dinfo->moving][dinfo->blocked]);
        }
        else if(inspect == IN_RANGE)
        {
            vdist(dist, positionDiff, player1->o, dinfo->o);
            string vecStr; vecStr[0] = 0; prettyprintvec(vecStr, dinfo->o, ",", 0.1f);
            formatstring(inspectstring)("distance: %.1f position: %s", dist, vecStr);
        }
        else if(inspect == IN_INTENT && dinfo->monsterstate)
        {
            extern const char *monsterstatenames[];
            extern const char *mhintnames[2][2];
            formatstring(inspectstring)("intent: %s nextaction: %d anger: %d ", monsterstatenames[dinfo->monsterstate], dinfo->trigger-lastmillis, dinfo->anger);
            concatstring(inspectstring, mhintnames[dinfo->moving][dinfo->blocked]);
        }
        else if(inspect == IN_WORLDPOS)
        {
            vdist(dist, positionDiff, player1->o, worldpos);
            string vecStr; vecStr[0] = 0; prettyprintvec(vecStr, worldpos, ",", 0.1f);
            formatstring(inspectstring)("distance: %.1f, worldpos: %s", dist, vecStr);
        }
        else
            inspectstring[0] = 0;
        if(inspectstring[0]) draw_text(inspectstring, relativehudpos(20), 1570, 2);
    };

    extern int arenaRespawnMessageType;
    if(player1->state!=CS_ALIVE || arenaRespawnMessageType)
    {
        extern Uint32 nextRespawn;
        extern int timeDiff(Uint32, Uint32);
        int timeleft = -1*timeDiff(nextRespawn,SDL_GetTicks());
        if(m_sp)
        {
            if(timeleft > 0)
                draw_textf("waiting to respawn %.1f", relativehudpos(20), 1500, 2, timeleft/1000.0f);
            else
                draw_textf("jump or fire to respawn", relativehudpos(20), 1500, 2);
        }
        else if(m_arena)
        {
            // Note: Arena respawn is controlled by the game client
            // If changed to be controlled by server, timeleft could be negative for a tiny duration
            if(arenaRespawnMessageType == 0)    // round in progress
                draw_textf("wait for the round to finish", relativehudpos(20), 1500, 2);
            else if(arenaRespawnMessageType == 1)   // new round is starting, shown to players dead or alive
                draw_textf("new round will start in %.1f", relativehudpos(20), 1500, 2, timeleft/1000.0f);
            else
                draw_textf("wait for new round to start... %.1f", relativehudpos(20), 1500, 2, timeleft/1000.0f);   // shown if player tries to respawn during countdown
        }
        else
        {
            if(timeleft > 0)
                draw_textf("jump or fire to respawn or wait %.1f", relativehudpos(20), 1500, 2, timeleft/1000.0f);
        };
    };

    renderscores();
    renderaccuracy();
    if(!rendermenu())
    {
        glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
        glBindTexture(GL_TEXTURE_2D, 1);
        glBegin(GL_QUADS);
        glColor3ub(255,255,255);
        if(crosshairfx)
        {
            if(player1->gunwait) glColor3ub(128,128,128);
            else if(player1->health<=25) glColor3ub(255,0,0);
            else if(player1->health<=50) glColor3ub(255,128,0);
        };
        float chsize = (float)crosshairsize;
        glTexCoord2d(0.0, 0.0); glVertex2f(VIRTW/2 - chsize, VIRTH/2 - chsize);
        glTexCoord2d(1.0, 0.0); glVertex2f(VIRTW/2 + chsize, VIRTH/2 - chsize);
        glTexCoord2d(1.0, 1.0); glVertex2f(VIRTW/2 + chsize, VIRTH/2 + chsize);
        glTexCoord2d(0.0, 1.0); glVertex2f(VIRTW/2 - chsize, VIRTH/2 + chsize);
        glEnd();
    };

    glPopMatrix();

    glPushMatrix();
    glOrtho(0, VIRTW*4/3, VIRTH*4/3, 0, -1, 1);
    renderconsole();

    glPopMatrix();

    if(!hidehud)
    {
        #define relativehudoffset(anchor, pos) (relativehudpos(anchor)+(pos-anchor))
        glPushMatrix();
        glOrtho(0, VIRTW/2, VIRTH/2, 0, -1, 1);
        draw_textf("%d",  relativehudoffset(20/2, 90), 827, 2, player1->health);
        if(player1->armourtotal() > 0) draw_textf("%d", relativehudoffset(620/2, 390), 827, 2, player1->armourtotal());
        draw_textf("%d", relativehudoffset(1220/2, 690), 827, 2, player1->ammo[player1->gunselect]);
        glPopMatrix();

        glPushMatrix();
        glOrtho(0, VIRTW, VIRTH, 0, -1, 1);
        glDisable(GL_BLEND);
        drawicon(128, 128, relativehudpos(20), 1650);
        if(player1->armourtotal() > 0) drawicon((float)(player1->armourtype*64), 0, relativehudpos(620), 1650);
        int g = gethudgunalias(player1->gunselect);
        int r = 64;
        if(g>2) { g -= 3; r = 128; };
        drawicon((float)(g*64), (float)r, relativehudpos(1220), 1650);
        glEnable(GL_BLEND);
        glPopMatrix();

        glPushMatrix();
        glOrtho(0, VIRTW*3/2, VIRTH*3/2, 0, -1, 1);
        // Health Buff Maximum Health Indicator
        if(player1->buffs[BUFF_HEALTH])
        {
            draw_textf("%d", relativehudpos(30), 2600, 2, player1->maxhealth());
        };
        // Best Armor Amount Indicator
        if(player1->armourtotal() > player1->armour[player1->armourtype])
        {
            draw_textf("%d", relativehudpos(930), 2600, 2, player1->armour[player1->armourtype]);
        };
#ifdef NEWGUNS
        // Draw (G)renade (L)auncher, (PI)stol, or (LA)ser on the hudgun icon to indicate the selected new weapon
        if(player1->gunselect == GUN_GRENADE)        draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_GRENADE));
        else if(player1->gunselect == GUN_PISTOL)    draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_PISTOL));
        else if(player1->gunselect == GUN_LASER)     draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_LASER));
        else if(player1->gunselect == GUN_THRUSTERS) draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_THRUSTERS));
        else if(player1->gunselect == GUN_HOOK)      draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_HOOK));
        else if(player1->gunselect == GUN_STOMP)     draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_STOMP));
        else if(player1->gunselect == GUN_GRAPPLE)   draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_GRAPPLE));
        else if(player1->gunselect == GUN_VORPAL)    draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_VORPAL));
        else if(player1->gunselect == GUN_PORTAL)    draw_textf("%s", relativehudpos(1830), 2600, 2, getWeaponNameAbbrev(GUN_PORTAL));
#endif
        glPopMatrix();
    };

    if(!hidestats || true)
    {
        glPushMatrix();
        glOrtho(0, VIRTW*3/2, VIRTH*3/2, 0, -1, 1);
        if(hidestats) do_stats_required();
        else do_stats();
        nculled = 0; // reset nculled counter (see renderparticles.cpp)
        glPopMatrix();
    };

    extern int showlatency;
    if(showlatency)
    {
        glPushMatrix();
        glOrtho(0, VIRTW*3/2, VIRTH*3/2, 0, -1, 1);
        do_latencystrip();
        glPopMatrix();
    };

    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
};

// In the first pass, process all the stats lines without drawing them to get their total height.
// This is needed because the stats are aligned to bottom, but they are drawn from top to bottom.
// In the second pass, actually draw the stats lines.
bool layoutFirstPass = true;
int layoutHorizontalCursor = 0;
const int layoutLineSize = 70;
const int layoutSpacerSize = 40;
// layoutBottomAlignment is off-by-one line when defined as 2600; need to add one layoutLineSize to fix it.
const int layoutBottomAlignment = 2600 + layoutLineSize;
int layoutLeftAlignment = 3200; // correct this later depending on screen aspect ratio
bool stats_line_long = false; // 2x long stats line
int layoutLeftAlignmentLong = 2800; // correct this later depending on screen aspect ratio
bool stats_line_extra_long = false; // 3x long stats line
int layoutLeftAlignmentExtraLong = 2400; // correct this later depending on screen aspect ratio
bool stats_line_75_percent = false; // starting at 3/4 of the screen
int layoutLeftAlignment75Percent = 2700; // correct this later depending on screen aspect ratio

void do_stats_lines(bool is_first_pass)
{
    void stats_spacer();
    void stats_line(const char *fstr, ...);
    layoutFirstPass = is_first_pass;

    // Reset the cursor if this is the first pass.
    if(layoutFirstPass) layoutHorizontalCursor = 0;

    // Determine layout left alignment
    // Can't be done at global init time; we can only do this once the screen size is known.
    layoutLeftAlignment = relativehudpos(3200);
    layoutLeftAlignmentLong = relativehudpos(2800);
    layoutLeftAlignmentExtraLong = relativehudpos(2400);
    layoutLeftAlignment75Percent = relativehudpos(2700);

    if(player1->buffs[BUFF_QUAD])
    {
        stats_line("quad %.1f", player1->buffs[BUFF_QUAD]/1000.0f);
        stats_spacer();
    };
    if(healthinfo)
    {
        extern int damagereceived, damagedealt;
        stats_line("dmgr %d", damagereceived);
        stats_line("dmgd %d", damagedealt);
        stats_spacer();
        stats_line("hreg %.2f", player1->infoHPRegenRate());
        if(player1->buffs[BUFF_HEALTH])
            stats_line("hbt %.1f", player1->buffs[BUFF_HEALTH_TIMER]/1000.0f);
        stats_spacer();
    };
    if(armourinfo)
    {
        stats_line("ap %d|%d|%d", player1->armour[A_YELLOW], player1->armour[A_GREEN], player1->armour[A_BLUE]);
        stats_line("areg %.2f", player1->infoArmourRegenRate());
        stats_spacer();
    };
    if(movementinfo)
    {
        const float DisplayZeroThreshold = 0.01f;
        vec v = estimatevelocity(player1);
        string indicator;
        indicator[0] = 0;
        prettyprintvec(indicator, v, "|", DisplayZeroThreshold);
        stats_line("mv %s", indicator);
        defformatstring(msg)("ms %.3f", sqrt(dotprod(v, v)));
        // if player should be moving but isn't, then append "*" or "**" depending on the state
        if(!(player1->vel.x > -0.01f && player1->vel.x < 0.01f && player1->vel.y > -0.01f && player1->vel.y < 0.01f))
        {
            extern int getmhintnameabbrev(bool moving, bool blocked);
            int mhint = getmhintnameabbrev(player1->moving, player1->blocked);
            loopi(mhint)
                concatstring(msg, "*");
        };
        stats_line(msg);
        defformatstring(mtype)("mt %s", player1->onfloor ? "floor" : player1->timeinair ? "air" : "float");
        if(player1->inwater) concatstring(mtype, "|water");
        stats_line(mtype);
        stats_spacer();
    };
    if(showfps)
    {
        stats_line("fps %4.1f%s", fps_average.result(), fps_average.data_ns?"*":"");
        if(fpsminmax)
        {
            extern int framedatawindowlength, framedatawindowismillis, framedatawindowpartitionpercent;
            stats_line("wlen %d %s", framedatawindowlength, framedatawindowismillis?"ms":"frames");
            stats_line("top%d %4.1f%s fps", framedatawindowpartitionpercent, minpart_fpsmax(), fps_max.data_ns?"*":"");
            stats_line("bot%d %4.1f%s fps", framedatawindowpartitionpercent, maxpart_fpsmin(), fps_min.data_ns?"*":"");
            if(fps_min.data->count > 0 && fps_max.data->count > 0)
                stats_line("xft %d%s|%d%s ms", fps_min.data->history(fps_min.minmaxd.age), fps_min.data_ns?"*":"", fps_max.data->history(fps_max.minmaxd.age), fps_max.data_ns?"*":"");
            else
                stats_line("xft n/a");
        };
        stats_spacer();
    };
    if(debugsound)
    {
        extern int lastsoundr, lastsoundl, lastsoundv;
        stats_line("sloc L%d|R%d", lastsoundl, lastsoundr);
        stats_line("svol %d", lastsoundv);
        stats_spacer();
    };
    if(showgamespeed)
    {
        stats_line("gspd %.2fx", (float)getgamespeed()/100.0f);
        stats_spacer();
    };
    if(clockinfo)
    {
        stats_line("iclk %.3f", InternalClockPtr->getMillis()/1000.0f);
        stats_line("gclk %.3f", GameClockPtr->getMillis()/1000.0f);
        stats_line("pclk %.3f", PhysicsClockPtr->getMillis()/1000.0f);
        extern float vtime;
        stats_line("vclk %.3f", vtime);
        stats_line("schk %d", soundstream_get_chunkcount());
        stats_spacer();
    };
    if(lightinfo)
    {
        int dlt, dltsrc;
        dynlightstats(dlt, dltsrc);
        stats_line("dylt %d", dlt);
    };
    if(particleinfo)
    {
        if(nculled) stats_line("part %d|-%d", nrendered, nculled);
        else stats_line("part %d", nrendered);
    };
    stats_line("wqd %d", nquads);
    stats_line("wvt %d", curvert);
    stats_line("evt %d", xtraverts);
    extern bool showm; // showmip
    extern string showmStr;
    if(showm)
    {
        stats_spacer();
        stats_line_75_percent = true;
        stats_line(showmStr);
        stats_line_75_percent = false;
    };
};

void do_stats()
{
    do_stats_lines(true);
    do_stats_lines(false);
};

// only render mipstats
// used when showmip is on but we can't render the usual stats because hidestats is on
void do_stats_required()
{
    void stats_spacer();
    void stats_line(const char *fstr, ...);

    // Reset the cursor if this is the first pass.
    layoutHorizontalCursor = 0;

    // Determine layout left alignment
    // Can't be done at global init time; we can only do this once the screen size is known.
    layoutLeftAlignment = relativehudpos(3200);
    layoutLeftAlignmentLong = relativehudpos(2800);
    layoutLeftAlignmentExtraLong = relativehudpos(2400);
    layoutLeftAlignment75Percent = relativehudpos(2700);

    extern bool showm; // showmip
    extern string showmStr;

    layoutFirstPass = true;
    if(showm)
    {
        stats_spacer();
        stats_line(showmStr);
    };

    layoutFirstPass = false;
    if(showm)
    {
        stats_spacer();
        stats_line_75_percent = true;
        stats_line(showmStr);
        stats_line_75_percent = false;
    };
};

void stats_line(const char *fstr, ...)
{
    va_list args;
    va_start(args, fstr);
    if(layoutFirstPass)
    {
        layoutHorizontalCursor += layoutLineSize;   // Move the cursor by one layoutLinesize to reserve space
    }
    else
    {
        defvformatstring(str, fstr, fstr);
        if(stats_line_75_percent)      draw_text(str, layoutLeftAlignment75Percent, layoutBottomAlignment-layoutHorizontalCursor, 2);
        else if(stats_line_extra_long) draw_text(str, layoutLeftAlignmentExtraLong, layoutBottomAlignment-layoutHorizontalCursor, 2);
        else if(stats_line_long)       draw_text(str, layoutLeftAlignmentLong, layoutBottomAlignment-layoutHorizontalCursor, 2);
        else                           draw_text(str, layoutLeftAlignment, layoutBottomAlignment-layoutHorizontalCursor, 2);
        layoutHorizontalCursor -= layoutLineSize;   // Advance the cursor by one layoutLinesize after drawing
    };
    va_end(args);
};

void stats_spacer()
{
    if(layoutFirstPass)
        layoutHorizontalCursor += layoutSpacerSize; // Move the cursor by one layoutSpacerSize to reserve space
    else
        layoutHorizontalCursor -= layoutSpacerSize; // Advance the cursor by one layoutSpacerSize
};
