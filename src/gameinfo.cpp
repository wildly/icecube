// Data and access methods only, required for client and shared with server.

// Most of the gamemode descriptions are from Sauerbraten's game.h.
// Portions from Sauerbraten are: Copyright (C) 2001-2012 Wouter van Oortmerssen, Lee Salzman, Mike Dysart, Robert Pointon, and Quinton Reeves
// See LICENSE-Sauerbraten.txt for the Sauerbraten source code license

#include "cube.h"

// M_DEMO and M_LOBBY aren't used in the code; they are from Sauerbraten (aka "the future").
// Gamemodes 13-26 aren't implemented yet; they are also from Sauerbraten.
// You can type "/help" to get the gamemode info in game.
gamemodeinfo gamemodes[] =
{
    { "demo playback", INT_MIN, M_DEMO | M_LOCAL,                                       "Demo Playback: Load and watch demos and replays by yourself or with friends." },
    { "SP",                 -2, M_LOCAL | M_CLASSICSP,                                  "Classic Single Player: Play on specially designed progress-based maps." },
    { "DMSP",               -1, M_LOCAL | M_DMSP,                                       "DMSP: An arena style SP mode played on DM maps where you kill monsters." },
    { "ffa/default",         0, M_LOBBY,                                                "Free For All: Collect items for ammo. Frag everyone to score points." },
    { "coop edit",           1, M_EDIT,                                                 "Cooperative Editing: Edit maps with multiple players simultaneously." },
    { "one-on-one duel",     2, M_LOBBY,                                                "One-on-One Duel: Teamplay is not in the rules. This mode can be used for one-on-one duel or ffa." },
    { "teamplay",            3, M_TEAM | M_OVERTIME,                                    "Teamplay: Collect items for ammo. Frag \fs\f3the enemy team\fr to score points for \fs\f1your team\fr." },
    { "instagib",            4, M_INSTAGIB,                                             "Instagib: You spawn with full rifle ammo and die instantly from one shot. There are no items. Frag everyone to score points." },
    { "instagib team",       5, M_INSTAGIB | M_TEAM | M_OVERTIME,                       "Instagib Team: You spawn with full rifle ammo and die instantly from one shot. There are no items. Frag \fs\f3the enemy team\fr to score points for \fs\f1your team\fr." },
    { "efficiency",          6, M_NOITEMS | M_EFFICIENCY,                               "Efficiency: You spawn with all weapons and armour. There are no items. Frag everyone to score points." },
    { "efficiency team",     7, M_NOITEMS | M_EFFICIENCY | M_TEAM | M_OVERTIME,         "Efficiency Team: You spawn with all weapons and armour. There are no items. Frag \fs\f3the enemy team\fr to score points for \fs\f1your team\fr." },
    { "insta arena",         8, M_NOITEMS | M_RIFLEONLY | M_ARENA,                      "Insta Arena: You spawn with full rifle ammo, no armour and 256 health. There are no items. Frag everyone to score points. Gameplay proceeds in rounds." },
    { "insta team arena",    9, M_NOITEMS | M_RIFLEONLY | M_ARENA | M_TEAM | M_OVERTIME,"Insta Team Arena: You spawn with full rifle ammo, no armour and 256 health. There are no items. Frag \fs\f3the enemy team\fr to score points for \fs\f1your team\fr. Gameplay proceeds in rounds." },
    { "tactics arena",      10, M_NOITEMS | M_TACTICS | M_ARENA,                        "Tactics Arena: You spawn with two random weapons, no armour and 256 health. There are no items. Frag everyone to score points. Gameplay proceeds in rounds." },
    { "tactics team arena", 11, M_NOITEMS | M_TACTICS | M_ARENA | M_TEAM | M_OVERTIME,  "Tactics Team Arena: You spawn with two random weapons, no armour and 256 health. There are no items. Frag \fs\f3the enemy team\fr to score points for \fs\f1your team\fr. Gameplay proceeds in rounds." },
    { "instafist",          12, M_NOITEMS | M_ARENA,                                    "Instafist: eihrul's secret instafist mode" },
    { "capture",            13, M_NOAMMO | M_TACTICS | M_CAPTURE | M_TEAM,              "Capture: Capture neutral bases or steal \fs\f3enemy bases\fr by standing next to them. \fs\f1Your team\fr scores points for every 10 seconds it holds a base. You spawn with two random weapons and armour. Collect extra ammo that spawns at \fs\f1your bases\fr. There are no ammo items." },
    { "regen capture",      14, M_NOITEMS | M_REGEN | M_CAPTURE | M_TEAM,               "Regen Capture: Capture neutral bases or steal \fs\f3enemy bases\fr by standing next to them. \fs\f1Your team\fr scores points for every 10 seconds it holds a base. Regenerate health and ammo by standing next to \fs\f1your bases\fr. There are no items." },
    { "ctf",                15,              M_CTF | M_TEAM,                            "Capture The Flag: Capture \fs\f3the enemy flag\fr and bring it back to \fs\f1your flag\fr to score points for \fs\f1your team\fr. Collect items for ammo." },
    { "instagib ctf",       16, M_INSTAGIB | M_CTF | M_TEAM,                            "Instagib Capture The Flag: Capture \fs\f3the enemy flag\fr and bring it back to \fs\f1your flag\fr to score points for \fs\f1your team\fr. You spawn with full rifle ammo and die instantly from one shot. There are no items." },
    { "protect",            17,              M_CTF | M_PROTECT | M_TEAM,                "Protect The Flag: Touch \fs\f3the enemy flag\fr to score points for \fs\f1your team\fr. Pick up \fs\f1your flag\fr to protect it. \fs\f1Your team\fr loses points if a dropped flag resets. Collect items for ammo." },
    { "instagib protect",   18, M_INSTAGIB | M_CTF | M_PROTECT | M_TEAM,                "Instagib Protect The Flag: Touch \fs\f3the enemy flag\fr to score points for \fs\f1your team\fr. Pick up \fs\f1your flag\fr to protect it. \fs\f1Your team\fr loses points if a dropped flag resets. You spawn with full rifle ammo and die instantly from one shot. There are no items." },
    { "hold",               19,              M_CTF | M_HOLD | M_TEAM,                   "Hold The Flag: Hold \fs\f7the flag\fr for 20 seconds to score points for \fs\f1your team\fr. Collect items for ammo." },
    { "instagib hold",      20, M_INSTAGIB | M_CTF | M_HOLD | M_TEAM,                   "Instagib Hold The Flag: Hold \fs\f7the flag\fr for 20 seconds to score points for \fs\f1your team\fr. You spawn with full rifle ammo and die instantly from one shot. There are no items." },
    { "efficiency ctf",     21, M_NOITEMS | M_EFFICIENCY | M_CTF | M_TEAM,              "Efficiency Capture The Flag: Capture \fs\f3the enemy flag\fr and bring it back to \fs\f1your flag\fr to score points for \fs\f1your team\fr. You spawn with all weapons and armour. There are no items." },
    { "efficiency protect", 22, M_NOITEMS | M_EFFICIENCY | M_CTF | M_PROTECT | M_TEAM,  "Efficiency Protect The Flag: Touch \fs\f3the enemy flag\fr to score points for \fs\f1your team\fr. Pick up \fs\f1your flag\fr to protect it. \fs\f1Your team\fr loses points if a dropped flag resets. You spawn with all weapons and armour. There are no items." },
    { "efficiency hold",    23, M_NOITEMS | M_EFFICIENCY | M_CTF | M_HOLD | M_TEAM,     "Efficiency Hold The Flag: Hold \fs\f7the flag\fr for 20 seconds to score points for \fs\f1your team\fr. You spawn with all weapons and armour. There are no items." },
    { "collect",            24,                            M_COLLECT | M_TEAM,          "Skull Collector: Frag \fs\f3the enemy team\fr to drop \fs\f3skulls\fr. Collect them and bring them to \fs\f3the enemy base\fr to score points for \fs\f1your team\fr or steal back \fs\f1your skulls\fr. Collect items for ammo." },
    { "insta collect",      25, M_INSTAGIB |               M_COLLECT | M_TEAM,          "Instagib Skull Collector: Frag \fs\f3the enemy team\fr to drop \fs\f3skulls\fr. Collect them and bring them to \fs\f3the enemy base\fr to score points for \fs\f1your team\fr or steal back \fs\f1your skulls\fr. You spawn with full rifle ammo and die instantly from one shot. There are no items." },
    { "efficiency collect", 26, M_NOITEMS | M_EFFICIENCY | M_COLLECT | M_TEAM,          "Efficiency Skull Collector: Frag \fs\f3the enemy team\fr to drop \fs\f3skulls\fr. Collect them and bring them to \fs\f3the enemy base\fr to score points for \fs\f1your team\fr or steal back \fs\f1your skulls\fr. You spawn with all weapons and armour. There are no items." }
};

int gm2index(int gm)
{
    int i = 0;
    ARRFIND(gamemodes[i].id == gm, i < ARRLEN(gamemodes), i++);
    return i;
};

bool m_valid(int gm)
{
    return (gm2index(gm) != ARRLEN(gamemodes));
};

const char *modename(int gm) { return m_valid(gm) ? gamemodes[gm2index(gm)].name : "unknown"; };
const char *modeinfo(int gm) { return m_valid(gm) ? gamemodes[gm2index(gm)].info : NULL; };
