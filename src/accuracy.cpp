// Accuracy tracking and score bonus
// Copyright (C) 2020 Willy Deng
// zlib License

#include "cube.h"

struct AccuracyStat
{
    int shots, hits, notcounted; // hits/(shots minus notcounted)
    int glancing;                // Shotgun, Rocket, Grenade: < 20% nominal damage (< 40, 24, 12 (total) damage)
    int damage;                  // total damage for this gun (before quad)
} accuracy[NUMGUNS];

// Shots that hit a dead monster should count as hit or miss or overkill?
// Currently, it is easiest to count as a miss.

// Rocket or Grenade that only damages yourself is assumed to be a rocket jump, and it is treated as 'notcounted' instead of a miss or hit.

// TODO: Future/Someday: Near hits?
// TODO: Overkill damage/hits as not counted?

void initAccuracy()
{
    loopi(NUMGUNS)
    {
        accuracy[i].shots = accuracy[i].hits = accuracy[i].notcounted = accuracy[i].glancing = 0;
        accuracy[i].damage = 0;
    };
};

void accuracyAddShot(int gun)
{
    // increment shots and notcounted
    ++accuracy[gun].shots;
    ++accuracy[gun].notcounted;
};

enum AccuracyResult
{
    ACCURACY_MISS = 0,
    ACCURACY_HIT = 1<<0,
    ACCURACY_GLANCING = 1<<1,
    ACCURACY_NOTCOUNTED = 1<<2
};

unsigned int accuracyCreateResultFlags(int gun, int damage, int selfdamage)
{
    unsigned int result = ACCURACY_MISS;
    int weaponBaseDamage = getWeaponBaseDamage(gun);

    if(damage > 0) result |= ACCURACY_HIT;
    else if(selfdamage || isMelee(gun))
    {
        // don't count misses for rocket jump or grenade jump that only damages yourself
        // don't count misses for melee attacks
        result |= ACCURACY_NOTCOUNTED;
    };

    if(damage < weaponBaseDamage/5) result |= ACCURACY_GLANCING;

    return result;
};

void accuracyAddResult(int gun, int damage, int selfdamage, unsigned int result)
{
    accuracy[gun].damage += (damage + selfdamage);

    if(result&ACCURACY_HIT)
    {
        ++accuracy[gun].hits;
        --accuracy[gun].notcounted;
        playhitsound();
    }
    else if(!(result&ACCURACY_NOTCOUNTED)) // if the attack counts and it didn't hit
    {
        // decrementing notcounted without incrementing hits implies miss
        --accuracy[gun].notcounted;
    };

    if(result&ACCURACY_GLANCING)
    {
        ++accuracy[gun].glancing;
    };
};

// This is the number that shows up on the accuracy scoreboard
int getWeaponAccuracyHitsNumerator(const int gun)
{
    return accuracy[gun].hits;
};

// This is the number that shows up on the accuracy scoreboard
int getWeaponAccuracyHitsDenominator(const int gun)
{
    if(isMelee(gun)) return accuracy[gun].shots; // show all attempts in the accuracy scoreboard
    return (accuracy[gun].shots - accuracy[gun].notcounted);
};

float getWeaponAccuracy(int gun)
{
    if(accuracy[gun].shots - accuracy[gun].notcounted <= 0) return 1;

    return accuracy[gun].hits / (float)(accuracy[gun].shots - accuracy[gun].notcounted);
};

// Weighted average for accuracy across all weapons
// missWeight is calculated as follows for these weapons: 50% of base damage for Shotgun, Rockets, Grenades
// Melee "misses" are tracked as "notcounted" so there is no need to modify their missWeight.
// Note: It is possible to spam only melee attacks and retain 100% accuracy for the purposes of scoring. This is intended behavior for FPS game modes.
int getWeaponAccuracyWeight(int gun)
{
    if(accuracy[gun].shots - accuracy[gun].notcounted <= 0) return 0;
/*
    // This scoring method weights each weapon according to (hits+misses)*weaponBaseDamage.
    // Shotgun hits and misses both have 50% weight.
    float hitModifier = 1.0f;
    float missModifier = 1.0f;
    if(gun == GUN_SG)
        hitModifier = missModifier = 0.5f;
    else if(isSplashDamage(gun))
        missModifier = 0.5f;
    int hitWeight = accuracy[gun].hits*(int)(getWeaponBaseDamage(gun)*hitModifier);
    int missWeight = (accuracy[gun].shots - accuracy[gun].hits - accuracy[gun].notcounted)*(int)(getWeaponBaseDamage(gun)*missModifier);
    return hitWeight + missWeight;
*/
    // This scoring method weights each weapon according to damage dealt before quad damage ("nominal damage") + misses*weaponBaseDamage. ("hits damage" + misses*weaponBaseDamage)
    float missModifier = 1.0f;
    if(gun == GUN_SG || isSplashDamage(gun))
        missModifier = 0.5f;
    int missWeight = (accuracy[gun].shots - accuracy[gun].hits - accuracy[gun].notcounted)*(int)(getWeaponBaseDamage(gun)*missModifier);
    return accuracy[gun].damage + missWeight;
};

float getTotalAccuracy()
{
    float totalWeight = 0;
    float totalAccuracy = 0;

    // This loop calculates two quantities.
    // This calculates totalWeight, which is valid once the loop exits.
    // On each loop iteration, this calculates and accumulates a partial accuracy term (except not divided by totalWeight); the sum is finally divided by totalWeight after the loop exits.
    loopi(NUMGUNS)
    {
        float w = getWeaponAccuracyWeight(i);
        totalWeight += w;
        totalAccuracy += w*getWeaponAccuracy(i);
    };
    if(totalWeight <= 0) return 1;
    totalAccuracy /= totalWeight;

    return totalAccuracy;
};

// Base Score DMSP
// Base Score is skill * 100

// Classic SP - Bonus Points
// Classic SP doesn't have a base score.
// Instead, scoring is based on time and/or frags.
// We can award Bonus Points depending on applicable Score Bonus items and applicable Score Penalty items.

// Accuracy Bonus
// 1 point for each percent accuracy.

// Score Penalties
// Titan, Mutant, Specter, Scientist: -100 points, but not more than -50% base score, for playing a special class.
// Basic Regeneration: -25 points. Basic regen in SP.

// Score Bonus
// Healthy: +10 points per active Health Boost.
// Safety First: +25 points. Received less than 100 damage. Only given if you didn't get the 'Perfect' bonus.
// Bounty Hunter: +100 points. Cleared all monsters in Classic SP.
// Perfect: +100 points. Received 0 damage.
// Killing Machine: +50 points. Cleared a DMSP map in skill * 30 seconds or less.
// Deadly: +25 points. Cleared a DMSP map in skill * 30 seconds + 30 seconds or less. Only given if you didn't get the 'Killing Machine' bonus.

struct FinalScoreRubric
{
   unsigned int useAccuracy:1, useTime:1, useSlowmoTime:1, awardMapClearedBonus:1;
   int baseScore;
   int time;
   int slowmoTime;
   int skill;
} fsr;

int calculateFinalScore()
{
    int bonus = 0;
    bonus += player1->buffs[BUFF_HEALTH];

    extern int damagereceived;
    if(damagereceived == 0)
        bonus += 100;
    else if(damagereceived < 100)
        bonus += 25;

    int time = 0;
    if(fsr.useTime) time = fsr.time;
    if(fsr.useSlowmoTime)
    {
        time = min(fsr.time, fsr.slowmoTime);
    };

    if(m_dmsp)
    {
        if(time <= 30*fsr.skill)
            bonus += 50;
        else if (time <= 30*(fsr.skill+1))
            bonus += 25;
    }

    if(m_classicsp && fsr.awardMapClearedBonus)
        bonus += 100;

    const int SpecialClassPenaltyPoints = (m_dmsp && fsr.skill == 1) ? 50 : 100;
    int penalty = 0;
    if(player1->special.basicregen) penalty += 25;
    if(player1->special.big) penalty += SpecialClassPenaltyPoints;
    if(player1->special.regenhealth) penalty += SpecialClassPenaltyPoints;
    if(player1->special.regenarmour) penalty += SpecialClassPenaltyPoints;
    if(player1->special.scientist) penalty += SpecialClassPenaltyPoints;

    // accuracy score
    float accuracyScore = fsr.useAccuracy ? 100*getTotalAccuracy() : 0;

    // score should not be below 0
    int finalScore = fsr.baseScore + bonus - time - penalty + (int)accuracyScore;
    int adjustedAccuracyScore = (int)(accuracyScore*(1-SpecialClassPenaltyPoints/1000.0f));

    return max(finalScore, adjustedAccuracyScore);
};

void initFinalScore(int skill)
{
    fsr.skill = skill;
    fsr.useAccuracy = 1;
    fsr.useTime = 0;
    fsr.useSlowmoTime = 0;
    fsr.time = 0;
    fsr.slowmoTime = 0;
    fsr.awardMapClearedBonus = 0;
    if(m_dmsp) fsr.baseScore = fsr.skill * 100;
    else fsr.baseScore = 0;
};

void updateFinalScoreTime(int seconds)
{
    if(m_dmsp)
    {
        fsr.useTime = 1;
        fsr.time = seconds;
    };
};

void updateFinalScoreSlowmo(int seconds)
{
    if(m_dmsp)
    {
        fsr.useSlowmoTime = 1;
        fsr.slowmoTime = seconds;
    };
};

void updateFinalScoreMapCleared(bool allkilled)
{
    if(m_classicsp && allkilled) fsr.awardMapClearedBonus = 1;
};

int getFinalScore()
{
    return calculateFinalScore();
};
