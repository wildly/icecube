#define MAX_RINGBUFFER_LENGTH 500

struct ringbuffer;

struct perfcounter;             // clients; performs calculations on data to provide metrics/analytics
struct perfcounter_rawdata;     // backend; buffers historical performance data
struct perfcounter_director;    // frontend; accepts events with timestamps

// Writes are done at the tail.
struct ringbuffer
{
    ushort buf[MAX_RINGBUFFER_LENGTH];
    ushort head, tail, count;

    bool enqueue(const ushort e, ushort *evicted);
    ushort dequeue();

    void init() { head = tail = 0; count = 0; };
};

enum perfcounter_event
{
    SLEEP_ST = 0,
    UPDATE_ST,
    RENDER_ST,
    SWAPBUFFERS_ST,
    TABULATE_ST,
    OCULL_ST,
    CALCLIGHT_ST,
    WORLD_OP_ST,
    UPDATE_PARTICLES_ST,
    MAX_METRICS,
    METRICS_STGRP = 100,
    NONE_ST,
    FRAMEZERO_EV,
    FRAMESTART_EV,
    FRAMEDONE_EV,
};

struct perfcounter_director
{
    struct perfcounter_metrics
    {
        ushort metrics[MAX_METRICS];
        uint laststarted, lasttouched;
        uint ready : 1;
    } framedata[2];

    struct
    {
        uint active : 1;
        perfcounter_event state;
    } state;

    void init();
    void log_event(perfcounter_event ev, uint millis);

    void commit();
    void bookkeep(perfcounter_event ev, uint millis);
    void open(perfcounter_event ev, uint millis);
    void seal(perfcounter_event ev, uint millis);
};

MEMORY_ALIGNED(struct, 1024)
perfcounter_rawdata : ringbuffer
{
    vector<perfcounter *> listeners;

    void init() { ringbuffer::init(); };

    ushort find_maxima(const ushort length, ushort *index);
    ushort find_minima(const ushort length, ushort *index);

    int subscribe(perfcounter *listener);
    bool unsubscribe(int sub_id);

    void log_val(const ushort v);

    ushort rawindex(const ushort age);
    ushort history(const ushort age);
    ushort age(const ushort rawindex);
};

namespace perfcounter_windowtype
{
    enum windowtype { milliseconds, frames };
};

struct generic_perfc_data
{
    ushort val1;    // totaltime; or age
    ushort val2;    // framecount; or totaltime
    ushort val3;    // rawindex; or unused
};

struct avg_perfc_data
{
    ushort totaltime;  // the amount of history taken into account
    ushort framecount; // the amount of frames taken into account
    ushort rawindex;   // the oldest frame taken into account
};

struct minmax_perfc_data
{
    ushort age;       // the age of the min/max frame
    ushort totaltime; // the amount of history taken into account
    ushort val3;      // unused
};


// Abstract perfcounter
struct perfcounter
{
    perfcounter_rawdata *data;
    int subscription_id;

    perfcounter *limitdata;

    // Window period. Can be either:
    // A. milliseconds (for constant time); OR
    // B. number of buffer entries (for constant work).
    ushort window;

    // Specifies whether the window is in milliseconds or frames.
    bool iswindowmillis;

    // Special values are stored here to allow efficient recalculation.
    union
    {
        struct generic_perfc_data d;
        struct minmax_perfc_data minmaxd;
        struct avg_perfc_data avgd;
    };

    // Determines whether there is enough data to have fully populated the counter.
    // Possible causes of insufficient data:
    // 1. Not enough frames have been logged since game init;
    // 2. Log backing storage exhausted.
    bool data_ns;

    // While inactive, this counter should not update and calculations
    // required by this perfcounter shouldn't be performed.
    bool active;

    perfcounter(ushort setwindow, perfcounter_windowtype::windowtype setwindowtype, bool makeactive, perfcounter_rawdata * data, perfcounter * parent);

    virtual void update(ushort value, ushort rawindex) = 0;
    virtual float result() = 0;

    void zero();
};

struct perfcounter_avg : perfcounter
{
    perfcounter_avg(ushort setwindow, perfcounter_windowtype::windowtype setwindowtype, bool makeactive, perfcounter_rawdata * data, perfcounter * parent = NULL)
        : perfcounter(setwindow, setwindowtype, makeactive, data, parent) {};
    void update(ushort value, ushort rawindex);
    float result();
};

struct perfcounter_min : perfcounter
{
    perfcounter_min(ushort setwindow, perfcounter_windowtype::windowtype setwindowtype, bool makeactive, perfcounter_rawdata * data, perfcounter * parent = NULL)
        : perfcounter(setwindow, setwindowtype, makeactive, data, parent) {};
    void update(ushort value, ushort rawindex);
    float result();
};

struct perfcounter_max : perfcounter
{
    perfcounter_max(ushort setwindow, perfcounter_windowtype::windowtype setwindowtype, bool makeactive, perfcounter_rawdata * data, perfcounter * parent = NULL)
        : perfcounter(setwindow, setwindowtype, makeactive, data, parent) {};
    void update(ushort value, ushort rawindex);
    float result();
};

struct perfcounter_util : perfcounter
{
    perfcounter_util(ushort setwindow, perfcounter_windowtype::windowtype setwindowtype, bool makeactive, perfcounter_rawdata * data, perfcounter * parent = NULL)
        : perfcounter(setwindow, setwindowtype, makeactive, data, parent) {};
    void update(ushort value, ushort rawindex);
    float result();
};

void perfcounters_init();
void perfcounters_setparams();
void cleanbenchmark();

extern perfcounter_director director;
extern perfcounter_rawdata cpu_active_frametimes, total_frametimes, cpu_compute_frametimes;
extern perfcounter_avg fps_average;
extern perfcounter_min fps_min;
extern perfcounter_max fps_max;
extern perfcounter_util cpu_active_average;
extern perfcounter_util cpu_compute_average;
extern float maxpart_fpsmin();
extern float minpart_fpsmax();
extern void push_minmax_frametime(int ft);
extern void reset_minmax_frametime_tracker();
extern void construct_minmax_frametime_heap();
extern void set_minmax_totalframes_totaltime(float tframes, float ttime);
