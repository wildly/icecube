// entities.cpp: map entity related functions (pickup etc.)

#include "cube.h"

vector<extentity> ents;

const char *entmdlnames[] = 
{
	"shells", "bullets", "rockets", "rrounds", "health", "boost",
	"g_armour", "y_armour", "quad",	"teleporter",     
};

int triggertime = 0;

void renderent(extentity &e, const char *mdlname, float z, float yaw, int frame = 0, int numf = 1, int basetime = 0, float speed = 10.0f)
{
	rendermodel(mdlname, frame, numf, 0, 1.1f, e.x, z+S(e.x, e.y)->floor, e.y, yaw, 0, false, 1.0f, speed, 0, basetime);
};

void renderentities()
{
	if(lastmillis>triggertime+1000) triggertime = 0;
    loopv(ents)
    {
        extentity &e = ents[i];
        if(e.type==MAPMODEL)
        {
            mapmodelinfo &mmi = getmminfo(e.attr2);
            if(!mmi_valid(mmi)) continue;
            rendermodel(mmi.name, 0, 1, e.attr4, (float)mmi.rad, e.x, (float)S(e.x, e.y)->floor+mmi.zoff+e.attr3, e.y, (float)((e.attr1+7)-(e.attr1+7)%15), 0, false, 1.0f, 10.0f, mmi.snap);
            drawboundingbox(NULL, &e, editmode);
        }
        else
        {
            if(OUTBORD(e.x, e.y)) continue;
            if(e.type!=CARROT)
            {
				if(!e.spawned && e.type!=TELEPORT) continue;
				if(e.type<I_SHELLS || e.type>TELEPORT) continue;
				renderent(e, entmdlnames[e.type-I_SHELLS], (float)(1+sin(lastmillis/100.0+e.x+e.y)/20), lastmillis/10.0f);
            }
			else switch(e.attr2)
            {			
				case 1:
				case 3:
					continue;
					
                case 2: 
                case 0:
					if(!e.spawned) continue;
					renderent(e, "carrot", (float)(1+sin(lastmillis/100.0+e.x+e.y)/20), lastmillis/(e.attr2 ? 1.0f : 10.0f));
					break;
					
                case 4: renderent(e, "switch2", 3,      (float)e.attr3*90, (!e.spawned && !triggertime) ? 1  : 0, (e.spawned || !triggertime) ? 1 : 2,  triggertime, 1050.0f);  break;
                case 5: renderent(e, "switch1", -0.15f, (float)e.attr3*90, (!e.spawned && !triggertime) ? 30 : 0, (e.spawned || !triggertime) ? 1 : 30, triggertime, 35.0f); break;
            }; 
        };
    };
};

itemstat itemstats[] =
{
    {     10,    50, S_ITEMAMMO         },
    {     20,   100, S_ITEMAMMO         },
    {      5,    25, S_ITEMAMMO         },
    {      5,    25, S_ITEMAMMO         },
    {     25,   100, S_ITEMHEALTH       }, // If DynentBaseHealth is changed, need to update max.
    {     50,   200, S_ITEMHEALTH       },
    {    100,   100, S_ITEMARMOUR       },
    {    100,   200, S_ITEMARMOUR       },
    {      0, 10000, S_ITEMPUP          }  // Disallow Quad pickup until less than 10 seconds of Quad Damage remaining.
};

struct buffstat { int add, max; } buffstats[] =
{
    {  20000, 30000 }, // BUFF_QUAD (millis)
    {     10,   100 }, // BUFF_HEALTH (health)
    {  10000, 20000 }  // BUFF_HEALTH_TIMER (millis)
};

// Used to swap rules from Cube to IceCube
void classicItems(bool enable)
{
    static int origHealthBuffMax = buffstats[BUFF_HEALTH].max;
    static int origYellowArmourAdd = itemstats[I_YELLOWARMOUR-I_SHELLS].add;
    static int origYellowArmourMax = itemstats[I_YELLOWARMOUR-I_SHELLS].max;
    if(enable)
    {
        buffstats[BUFF_HEALTH].max = 0;
        itemstats[I_YELLOWARMOUR-I_SHELLS].add = 150;
        itemstats[I_YELLOWARMOUR-I_SHELLS].max = 150;
    }
    else
    {
        buffstats[BUFF_HEALTH].max = origHealthBuffMax;
        itemstats[I_YELLOWARMOUR-I_SHELLS].add = origYellowArmourAdd;
        itemstats[I_YELLOWARMOUR-I_SHELLS].max = origYellowArmourMax;
    };
};

void baseammo(int gun) { player1->ammo[gun] = itemstats[gun-1].add*2; };

const int HEALTH_BUFF_TIMER_TO_HEALTH_RATIO = 1000;

void givebuff(dynent *d, int buff, int aBaseVal, bool applyBonus);
void givebuff(dynent *d, int buff)
{
    givebuff(d, buff, 0, true);
};

void givebuff(dynent *d, int buff, int aBaseVal, bool applyBonus = true)
{
    buffstat &bs = buffstats[buff];
    const int baseVal = aBaseVal ? aBaseVal : bs.add;
    const int bonusVal = d->special.scientist ? 20*baseVal/100 : 0;
    switch(buff)
    {
    // Health buff
    // Add 10 points (1 instance) of the health buff.
    // Restore grace timer to full duration since this instance is new.
    case BUFF_HEALTH:
        d->buffs[buff] = clamp(d->buffs[buff] + baseVal, 0, bs.max);
        givebuff(d, BUFF_HEALTH_TIMER);
        break;
    // Health buff drains away depending on the current health.
    // A new instance of the health buff gives 10 seconds grace duration before the buff dispells.
    case BUFF_HEALTH_TIMER:
    {
        // Health buff grace timer can be "refreshed" to a certain duration depending on player's current health
        // If the new duration is less than current duration, keep the current duration.
        const int newDuration = clamp(baseVal + bonusVal, 0, bs.max);
        if(newDuration > d->buffs[buff])
            d->buffs[buff] = newDuration;
        break;
    };
    // Quad Damage
    case BUFF_QUAD:
        d->buffs[buff] = clamp(d->buffs[buff] + baseVal + bonusVal, 0, bs.max);
        break;
    };
};

// Note for realpickup and radditem:
// these two functions are called when the server acknowledges that you really
// picked up the item (in multiplayer someone may grab it before you).

// int  i: item id
// int &v: value of dynent's amount of item
void radditem(int i, int &v)
{
    itemstat &is = itemstats[ents[i].type-I_SHELLS];
    v = min(v + is.add, is.max);
    //givearmour(player1, A_BLUE, 5);
};

void addhealth(dynent *d, int addVal, int maxVal)
{
    d->health = min(d->health + addVal, maxVal);
    extern int logbattledata;
    if(d == player1 && logbattledata)
    {
        loghealth();
    };
};

void realpickup(int n, dynent *d)
{
    if(n<0 || n>=ents.length()) { conoutf("invalid item: %d", n); return; };
    itemstat &is = itemstats[ents[n].type-I_SHELLS];
    playsoundc(is.sound);
    ents[n].spawned = false;

    switch(ents[n].type)
    {
        case I_SHELLS:  radditem(n, d->ammo[1]); break;
        case I_BULLETS: radditem(n, d->ammo[2]); break;
        case I_ROCKETS: radditem(n, d->ammo[3]); break;
        case I_ROUNDS:  radditem(n, d->ammo[4]); break;
        case I_HEALTH:  addhealth(d, is.add, d->maxhealth()); break;

        case I_BOOST:
        {
            const int oldmaxhealth = d->maxhealth();
            if(!d->special.big)
                givebuff(d, BUFF_HEALTH);
            if(d->maxhealth() > oldmaxhealth)
                conoutf("you got the health boost! it increased your max health!");
            else
                conoutf("you got the health boost!");
            addhealth(d, is.add, is.max);
            break;
        };

        case I_GREENARMOUR:
            givearmour(d, A_GREEN, is.add);
            break;

        case I_YELLOWARMOUR:
            givearmour(d, A_YELLOW, is.add);
            break;

        case I_QUAD:
            givebuff(d, BUFF_QUAD);
            conoutf("you got the quad!");
            break;
    };
};

// these functions are called when the client touches the item

VARP(greenarmourpickuplt,0,67,200);

// { int i, int spawnsec } used to complete an ItemPickupMsg for SV_ITEMPICKUP
// { int v } used to determine whether this item is necessary to pickup
// { dynent *d } used to modify the item pickup criteria
void trypickup(int i, int v, int spawnsec, dynent *d = NULL)
{
    bool allowPickup = false;

    int testVal = itemstats[ents[i].type-I_SHELLS].max;
    if(d && (ents[i].type == I_HEALTH || ents[i].type == I_BOOST))          // Try to pickup health items according to dynent's max health
        testVal = d->maxhealth();

    if(v < testVal)
        allowPickup = true;

    // Try using alternate criteria to pickup item
    if(d && (ents[i].type == I_BOOST && !d->special.big))                   // try to pickup I_BOOST for the health buff (note: players with special.big cannot receive health buff)
        if(d->buffs[BUFF_HEALTH] < buffstats[BUFF_HEALTH].max)
            allowPickup = true;

    if(allowPickup)                                                         // don't pick up if not needed
    {
        addmsg(1, 3, SV_ITEMPICKUP, i, m_classicsp ? 100000 : spawnsec);    // first ask the server for an ack
        ents[i].spawned = false;                                            // even if someone else gets it first
    };
};

// player1's LockoutTimers
struct LockoutTimers myLockouts = { 0, 0 };

int entityEventID()
{
    static int entityEventID = 0;
    return ++entityEventID;
};

// teleportdynlightoff - too many dynlights can be bad for performance due to block copy/restore operations
// 0 = both dynlights enabled
// 1 = no departure dynlight
// 2 = no arrival dynlight
// 3 = both dynlights off
VARP(teleportdynlightoff, 0, 3, 3);
void teleportEffect(dynent *d, vec &effectLoc, bool arriving)
{
    extern int debugparticles;
    const int ExtraParticles = debugparticles ? 200 : 0;
    const int ExtraLifetime = debugparticles ? 2000 : 0;

    const float effectRadius = max(1.0f, d->radius);
    const int particleCount = int(10*max(1.0f, pow(4*d->radius*d->radius*(d->eyeheight+d->aboveeye), 1/3.0f)/2)) + ExtraParticles;
    const int lightRadius = 0;

    if(!arriving)
    {
        particle_cloud(PART_TELEPORT, particleCount, 400+ExtraLifetime, 600, effectLoc, effectRadius, 0);
        if(!(teleportdynlightoff&1)) add_dynlightsource(effectLoc, lightRadius, min(255, particleCount), 2, 400+ExtraLifetime+600, 600);
    }
    else
    {
        particle_bubble(PART_TELEPORT, 4*particleCount, 400+ExtraLifetime, 600, effectLoc, 2*effectRadius, 0);
        if(!(teleportdynlightoff&2)) add_dynlightsource(effectLoc, lightRadius, min(255, 4*particleCount), 2, 400+ExtraLifetime+600, 600);
        if(d==player1) playsoundc(S_TELEPORT);
        else playsound(S_TELEPORT, &d->o);
    };
};

void teleport(int n, dynent *d)     // also used by monsters
{
    int e = -1, tag = ents[n].attr1, beenhere = -1;
    for(;;)
    {
        e = findentity(TELEDEST, e+1);
        if(e==beenhere || e<0) { conoutf("no teleport destination for tag %d", tag); return; };
        if(beenhere<0) beenhere = e;
        if(ents[e].attr2==tag)
        {
            teleportEffect(d, d->o);
            d->o.x = ents[e].x;
            d->o.y = ents[e].y;
            d->o.z = ents[e].z;
            d->yaw = ents[e].attr1;
            d->pitch = 0;
            moveplayer_timeinair_end(d);
            d->vel.x = d->vel.y = d->vel.z = 0;
            entinmap(d);
            teleportEffect(d, d->o, true);
            break;
        };
    };
};

void teleport(dynent *s, dynent *dest)
{
    teleportEffect(s, s->o);
    s->o.x = dest->o.x;
    s->o.y = dest->o.y;
    s->o.z = dest->o.z;
    s->pitch = 0;
    moveplayer_timeinair_end(s);
    s->vel.x = s->vel.y = s->vel.z = 0;
    entinmap(s);
    teleportEffect(s, s->o, true);
};

VAR(logentityevents, 0, 0, 1);
void jumppad(int n, dynent *d)      // may be used by monsters if desired
{
    int activationID = entityEventID();
    vec v = vec((int)((signed char)ents[n].attr3)/10.0f, (int)((signed char)ents[n].attr2)/10.0f, ents[n].attr1/10.0f);

    // nominalvadd is the nominal magnitude of the jumppad impulse for player 1
    // rescale is used to rescale the impulse when applying the jumpad impulse to monsters
    float nominalvadd = sqrt(dotprod(v,v))*player1->maxspeed;
    float rescale = player1->maxspeed/d->maxspeed;

    if(logentityevents)
    {
        const char* name = d->monsterstate ? "monster" : d == player1 ? "player1" : "otherplayer";
        defformatstring(msg)("jumppad,activate,actor:%s,entID:%d,actID:%d,nominalvadd:%.1f,rawvadd:%.2f|%.2f|%.2f", name, n, activationID, nominalvadd, v.x, v.y, v.z);
        if(d->onfloor) concatstring(msg, ",touch:onfloor");
        else if(d->timeinair) concatstring(msg,",touch:inair");
        else concatstring(msg,",touch:other");
        printf("%s\n", msg);
    };

    moveplayer_timeinair_end(d);
    d->onfloor = false;             // "jump"-pad :)
    if(d->monsterstate) vmul(v, rescale);
    energypush(d, v); // instead of vadd
    if(d==player1) playsoundc(S_JUMPPAD);
    else playsound(S_JUMPPAD, &d->o);
};

// Is the dynent trying to use a jumppad during the lockout time?
// true: nothing happens
// false: jumppad activates
bool jumppadlockout(int n, dynent *d, int &lastjumppad)
{
    if(lastmillis-lastjumppad<300)
    {
        int activationID = entityEventID();
        if(logentityevents)
        {
            const char* name = d->monsterstate ? "monster" : d == player1 ? "player1" : "otherplayer";
            printf("jumppad,lockout,actor:%s,entID:%d,actID:%d,timeleft:%d\n", name, n, activationID, 300-(lastmillis-lastjumppad));
        };
        return true;
    };
    lastjumppad = lastmillis;
    return false;
};

void pickup(int n, dynent *d)
{
    int np = 1;
    loopv(players) if(players[i]) np++;
    // spawn times are dependent on number of players
    const int ammoTime = np<=2 ? 8 : (np<=4 ? 6 : 4);
    const int healthTime = np<=2 ? 20 : (np<=4 ? 15 : 10);
    switch(ents[n].type)
    {
        case I_SHELLS:  trypickup(n, d->ammo[1], ammoTime); break;
        case I_BULLETS: trypickup(n, d->ammo[2], ammoTime); break;
        case I_ROCKETS: trypickup(n, d->ammo[3], ammoTime); break;
        case I_ROUNDS:  trypickup(n, d->ammo[4], ammoTime); break;
        case I_HEALTH:
            trypickup(n, d->health, healthTime, d);
            break;
        case I_BOOST:
            trypickup(n, d->health, 60, d);
            break;

        case I_GREENARMOUR:
        {
            if(d->special.big) break;
            // Check whether to risk discarding yellow armor in order to pick up green armor
            // Pickup is allowed when green+yellow armor is less than itemstats(greenarmor).max
            // If greenarmourpickuplt is configured, pick up green armor only
            // when yellow armor is less than the configured threshold.
            // If not configured, the default threshold is the armor value of
            // the green armour to be picked up.
            // e.g. 100 GA > 99 YA, so try to pickup the Green Armor.
            const int threshold = greenarmourpickuplt ? greenarmourpickuplt :
                itemstats[I_GREENARMOUR-I_SHELLS].add;
            if(classicRuleset && d->armourtype == A_YELLOW && !(d->armour[A_YELLOW] < threshold)) break; // greenarmourpickuplt edge cases: 0 = not configured, 1 = only when 0 yellow armor
            int greenPlusYellowArmourAmt = d->armour[A_GREEN] + d->armour[A_YELLOW];
            trypickup(n, greenPlusYellowArmourAmt, 20);
            break;
        };

        case I_YELLOWARMOUR:
            if(d->special.big) break;
            trypickup(n, d->armour[A_YELLOW], 20);
            break;

        case I_QUAD:
            trypickup(n, d->buffs[BUFF_QUAD], 60);
            break;
            
        case CARROT:
            ents[n].spawned = false;
            triggertime = lastmillis;
            trigger(ents[n].attr1, ents[n].attr2, false);  // needs to go over server for multiplayer
            break;

        case TELEPORT:
            if(lastmillis-myLockouts.lastteleport<500) break;
            myLockouts.lastteleport = lastmillis;
            teleport(n, d);
            break;

        case JUMPPAD:
            if(jumppadlockout(n, d, myLockouts.lastjumppad)) break;
            jumppad(n, d);
            break;
    };
};

void checkitems()
{
    if(editmode) return;
    loopv(ents)
    {
        extentity &e = ents[i];
        if(e.type==NOTUSED) continue;
        if(!ents[i].spawned && e.type!=TELEPORT && e.type!=JUMPPAD) continue;
        if(OUTBORD(e.x, e.y)) continue;
        vec v = vec(e.x, e.y, S(e.x, e.y)->floor+player1->eyeheight);
        vdist(dist, t, player1->o, v);
        if(dist<(e.type==TELEPORT ? 4 : 2.5)) pickup(i, player1);
    };
};

// Only for applying quad glow for other players
void checkbuffs_otherplayers(dynent *d, int time)
{
    // None of the current buffs need to be checked while player is dead.
    if(!d || d->state == CS_DEAD)
        return;

    if(d->buffs[BUFF_QUAD] && (d->buffs[BUFF_QUAD] -= time) <= 0)
    {
        d->buffs[BUFF_QUAD] = 0;
    };

    if(d->buffs[BUFF_QUAD])
    {
        adddynlight(d->o, 0, 255, d, 1);
    };
}

void checkbuffs(int time)
{
    loopv(players) checkbuffs_otherplayers(players[i], time);

    // None of the current buffs need to be checked while player is dead.
    if(player1->state == CS_DEAD)
        return;

    if(player1->buffs[BUFF_QUAD] && (player1->buffs[BUFF_QUAD] -= time) <= 0)
    {
        player1->buffs[BUFF_QUAD] = 0;
        playsoundc(S_PUPOUT);
        conoutf("quad damage is over");
    };

    if(player1->buffs[BUFF_QUAD])
    {
        adddynlight(player1->o, 0, 255, player1, 1);
    };

    // Note:
    // If we were to decrement buffs[BUFF_HEALTH_TIMER] directly, the result may equal 0, but buffs[BUFF_HEALTH] is non-zero.
    // Therefore, to reliably detect an active Health Buff timer, we must check buffs[BUFF_HEALTH].
    if(player1->buffs[BUFF_HEALTH])
    {
        int &duration = player1->buffs[BUFF_HEALTH_TIMER];

        // Update the Health Buff grace timer duration immediately.
        duration -= time;

        // Calculate the "refresh" for timer duration.
        // Potential refreshed duration will be [conversion ratio] * "10 (health)" minus "player's deficit from full health."
        const int restoredDuration = HEALTH_BUFF_TIMER_TO_HEALTH_RATIO * (10 - (player1->maxhealth() - player1->health));

        // The fully refreshed timer duration for an instance is 10000.
        // It can be raised higher via bonuses.
        // Bonuses, if any, will be calculated within givebuff().
        const int maxRestoredDuration = buffstats[BUFF_HEALTH_TIMER].add;

        // Apply the restored duration.
        // For BUFF_HEALTH_TIMER, givebuff() will apply the new timer duration only if it is better than the current duration.
        if(restoredDuration > 0)
        {
            givebuff(player1, BUFF_HEALTH_TIMER, min(restoredDuration, maxRestoredDuration));
        };

        // Grace timer duration expired.
        if(duration <= 0)
        {
            // Remove 1 instance of the health buff.
            int &value = player1->buffs[BUFF_HEALTH];
            value -= buffstats[BUFF_HEALTH].add;
            if(value <= 0)
            {
                conoutf("your health boost has ended");
                duration = 0;
            }
            else
            {
                conoutf("some of your health boost faded away");
                // Activate the new instance's grace timer.
                // The grace timer duration may have been reduced below 0.
                // Track the "deficit" time so we can correctly adjust the new instance's grace timer.
                const int deficit = duration;
                givebuff(player1, BUFF_HEALTH_TIMER);
                duration += deficit; // Correction to the new instance's timer.

                // Since we modified the timer, we will need to repeat the health buff timer checks (refresh duration, etc).
                // A different implementation of Health buff timer's "instance" design may be able to avoid this.
                checkbuffs(0);
            };
        };
    };
};

void putitems(uchar *&p)            // puts items in network stream and also spawns them locally
{
    loopv(ents) if((ents[i].type>=I_SHELLS && ents[i].type<=I_QUAD) || ents[i].type==CARROT)
    {
        putint(p, i);
        ents[i].spawned = true;
    };
};

void resetspawns() { loopv(ents) ents[i].spawned = false; };
void setspawn(uint i, bool on) { if(i<(uint)ents.length()) ents[i].spawned = on; };
