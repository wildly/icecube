// monster.cpp: implements AI for single player monsters, currently client only

#include "cube.h"

dvector monsters;
vector<LockoutTimers> monLockouts;

VARF(skill, 1, 3, 10, conoutf("skill is now %d", skill));

const char *monsterstatenames[] = { "none", "searching", "thinking", "attacking", "pain", "sleep", "aiming" };  // monster states

// movement hint: left digit is "moving" hint, right digit is "blocked" hint
// note: currently, moving is always set to true in moveplayer function regardless of movement input, and is only set to false if doubly blocked
// 00: not moving - idle
// 01: not moving - blocked
// 10: moving freely
// 11: blocked in one of x or y dir
const char *mhintnames[2][2] = { { "idle", "doubly blocked" }, { "moving", "partly blocked" } }; // 00, 01, 10, 11

const char *getmhintname(bool moving, bool blocked)
{
    int m = moving ? 1 : 0;
    int b = blocked ? 1 : 0;
    return mhintnames[m][b];
}

// for renderextras stats - determines number of asterisks next to movement info
// * = partly blocked
// ** = doubly blocked
// [no asterisks] = otherwise
int getmhintnameabbrev(bool moving, bool blocked)
{
    if(!moving && blocked) return 2;
    else if(blocked) return 1;
    else return 0;
}

dvector &getmonsters() { return monsters; };

#define TOTMFREQ 13
#define NUMMONSTERTYPES 8

struct monstertype      // see docs for how these values modify behaviour
{
    short gun, speed, health, freq, lag, rate, pain, loyalty, mscale, bscale;
    short painsound, diesound;
    const char *name, *mdlname;
}

monstertypes[NUMMONSTERTYPES] =
{
    { GUN_FIREBALL,  15, 100, 3, 0,   100, 800, 1, 10, 10, S_PAINO, S_DIE1,   "an ogre",     "monster/ogro"    },
    { GUN_CG,        18,  70, 2, 70,   10, 400, 2,  8,  9, S_PAINR, S_DEATHR, "a rhino",     "monster/rhino"   },
    { GUN_SG,        14, 120, 1, 100, 300, 400, 4, 14, 14, S_PAINE, S_DEATHE, "ratamahatta", "monster/rat"     },
    { GUN_RIFLE,     15, 200, 1, 80,  300, 300, 4, 18, 18, S_PAINS, S_DEATHS, "a slith",     "monster/slith"   },
    { GUN_RL,        13, 500, 1, 0,   100, 200, 6, 24, 24, S_PAINB, S_DEATHB, "bauul",       "monster/bauul"   },
    { GUN_BITE,      22,  50, 3, 0,   100, 400, 1, 12, 15, S_PAINP, S_PIGGR2, "a hellpig",   "monster/hellpig" }, // as a special case, bscale for hellpig will only scale radius and not height
    { GUN_ICEBALL,   12, 250, 1, 0,    10, 400, 6, 18, 18, S_PAINH, S_DEATHH, "a knight",    "monster/knight"  },
    { GUN_SLIMEBALL, 15, 100, 1, 0,   200, 400, 2, 13, 10, S_PAIND, S_DEATHD, "a goblin",    "monster/goblin"  },
};

int monsterbasehealth(int mtype)
{
    return monstertypes[mtype].health;
};

dynent *basicmonster(int type, int yaw, int state, int trigger, int move)
{
    if(type>=NUMMONSTERTYPES)
    {
        conoutf("warning: unknown monster in spawn: %d", type);
        type = 0;
    };
    monstertype *t = &monstertypes[type];

    dynent *m = new dynent;
    m->mtype = type;
    m->eyeheight = 2.0f;
    m->aboveeye = 1.9f;
    m->radius *= t->bscale/10.0f;
    if(type != M_HELLPIG) // to better match the in-game model, don't apply bscale to the height of hellpig's bounding box
    {
        m->eyeheight *= t->bscale/10.0f;
        m->aboveeye *= t->bscale/10.0f;
    };
    m->monsterstate = state;
    if(state!=M_SLEEP) spawnplayer(m);
    m->trigger = lastmillis+trigger;
    m->targetyaw = m->yaw = (float)yaw;
    m->move = move;
    m->enemy = player1;
    m->gunselect = t->gun;
    m->maxspeed = (float)t->speed;
    m->health = t->health;
    loopi(NUMARMOUR) m->armour[i] = 0;
    m->armourtype = A_BLUE;
    loopi(NUMGUNS) m->ammo[i] = 10000;
    loopi(NUMBUFFS) m->buffs[i] = 0;
    m->regen.setsize(0);
    m->armourRegen.setsize(0);
    m->pitch = 0;
    m->roll = 0;
    m->state = CS_ALIVE;
    m->anger = 0;
    copystring(m->name, t->name);
    monsters.add(m);
    LockoutTimers ml = { 0, 0 };
    monLockouts.add(ml);
    return m;
};

dynent *spawnmonster()  // spawn a random monster according to freq distribution in DMSP
{
    int n = rnd(TOTMFREQ), type;
    for(int i = 0; ; i++) if((n -= monstertypes[i].freq)<0) { type = i; break; };
    return basicmonster(type, rnd(360), M_SEARCH, 1000, 1);
};

// Single player, DMSP, and scripted levels

// Monster count
struct sp_monstercount
{
    int total, killed;
} mcount;

// Level run-through time
const int setuptime = 10000;
int timestart;
Uint32 realtimestart;
Uint32 realsetuptimeend;
extern int timeDiff(Uint32, Uint32);
extern uint lastrealtime;

// Enables a teleport arrival effect when a monster spawns in DMSP
VARP(monsterspawnereffect, 0, 1, 1);

// DMSP monster spawner
struct dmsp_monsterspawner
{
    int remain;
    int trigger;
} mspawner;

// called after map start or when toggling edit mode to reset/spawn all monsters to initial state
void monsterclear()
{
    loopv(monsters) delete monsters[i];
    monsters.setsize(0);
    monLockouts.setsize(0);

    // reset SP monster counters
    mcount.killed = 0;
    mcount.total = 0;
    mspawner.remain = 0;

    // reset player's frags to 0, assuming monsters are the only source of frags in SP
    player1->frags = 0;

    if(m_dmsp)
    // setup timed monster spawns in DMSP
    {
        mspawner.trigger = timestart + setuptime;
        mspawner.remain = (!m_mp(gamemode)) ? skill*10 : 0; // in case of multiplayer dmsp?
        mcount.total = mspawner.remain;
    }
    else if(m_classicsp)
    // spawn the placed monsters in classic sp
    {
        loopv(ents) if(ents[i].type==MONSTER)
        {
            dynent *m = basicmonster(ents[i].attr2, ents[i].attr1, M_SLEEP, 100, 0);  
            m->o.x = ents[i].x;
            m->o.y = ents[i].y;
            m->o.z = ents[i].z;
            entinmap(m);
            mcount.total++;
        };
    };
};

// called after map start and is NOT reset when toggling edit mode
void monsterstartmap()
{
    // init SP/DMSP score
    initFinalScore(skill);

    // reset level run-through time
    timestart = lastmillis + curtime;
    realtimestart = realsetuptimeend = lastrealtime;
};

// for savegames -- restore count of killed monsters
void restoremonsterstate() { loopv(monsters) if(monsters[i]->state==CS_DEAD) mcount.killed++; };

void monsterkilled()
{
    mcount.killed++;
    int remain = mcount.total - mcount.killed;
    if(remain>0 && remain<=5) conoutf("only %d monster(s) remaining", remain);
};

void checksp()
{
    if(m_dmsp && mspawner.remain && lastmillis > mspawner.trigger)
    {
        if(mspawner.remain-- == mcount.total) conoutf("The invasion has begun!");
        if(realsetuptimeend == realtimestart) realsetuptimeend = lastrealtime;
        mspawner.trigger += 1000;
        dynent *m = spawnmonster();
        if(monsterspawnereffect) teleportEffect(m, m->o, true);
    };

    // if map has monsters AND none remain AND all monsters are dead
    if(mcount.total > 0 && mspawner.remain == 0 && mcount.killed == mcount.total)
    {
        endsp(true);
    };
};

void endsp(bool allkilled)
{
    int runthroughtime = lastmillis + curtime - timestart - (m_dmsp ? setuptime : 0);
    int runthroughrealtime = timeDiff(realtimestart, lastrealtime);
    conoutf(allkilled ? "you have cleared the map!" : "you reached the exit!");
    updateFinalScoreMapCleared(allkilled);

    conoutf("score: %d kills in %d seconds", mcount.killed, runthroughtime/1000);
    updateFinalScoreTime(runthroughtime/1000);

    if((runthroughrealtime - (m_dmsp ? setuptime : 0)) != runthroughtime)
    {
        defformatstring(realtimescoremsg)("score: %d kills in %d real seconds", mcount.killed, runthroughrealtime/1000);
        if(realtimestart != realsetuptimeend)
            conoutf("%s of which %d were setup time", realtimescoremsg, timeDiff(realtimestart, realsetuptimeend)/1000);
        else
            conoutf(realtimescoremsg);
        updateFinalScoreSlowmo(runthroughrealtime/1000);
    };

    defformatstring(accuracymsg)("accuracy: %.1f%%", 100*getTotalAccuracy());
    if(m_dmsp) conoutf("total score: %d (%s)", getFinalScore(), accuracymsg);
    else if(m_classicsp) conoutf("bonus points: %d (%s)", getFinalScore(), accuracymsg);
    printf("game,endsp,start:%d,end:%d,rStart:%d,rEnd:%d,rSetupEnd:%d\n", timestart, lastmillis + curtime, realtimestart, lastrealtime, realsetuptimeend);

    mcount.total = 0; // needed so endsp() is one-shot
    startintermission();
};

bool los(float lx, float ly, float lz, float bx, float by, float bz, vec &v) // height-correct line of sight for monster shooting/seeing
{
    if(OUTBORD((int)lx, (int)ly) || OUTBORD((int)bx, (int)by)) return false;
    float dx = bx-lx;
    float dy = by-ly; 
    int steps = (int)(sqrt(dx*dx+dy*dy)/0.9);
    if(!steps) return false;
    float x = lx;
    float y = ly;
    int i = 0;
    for(;;)
    {
        sqr *s = S(fast_f2nat(x), fast_f2nat(y));
        if(SOLID(s)) break;
        float floor = s->floor;
        if(s->type==FHF) floor -= s->vdelta/4.0f;
        float ceil = s->ceil;
        if(s->type==CHF) ceil += s->vdelta/4.0f;
        float rz = lz-((lz-bz)*(i/(float)steps));
        if(rz<floor || rz>ceil) break;
        v.x = x;
        v.y = y;
        v.z = rz;
        x += dx/(float)steps;
        y += dy/(float)steps;
        i++;
    };
    return i>=steps;
};

bool enemylos(dynent *m, vec &v)
{
    v = m->o;
    return los(m->o.x, m->o.y, m->o.z, m->enemy->o.x, m->enemy->o.y, m->enemy->o.z, v);
};

// monster AI is sequenced using transitions: they are in a particular state where
// they execute a particular behaviour until the trigger time is hit, and then they
// reevaluate their situation based on the current state, the environment etc., and
// transition to the next state. Transition timeframes are parametrized by difficulty
// level (skill), faster transitions means quicker decision making means tougher AI.

void transition(dynent *m, int state, int moving, int n, int r) // n = at skill 0, n/2 = at skill 10, r = added random factor
{
    m->monsterstate = state;
    m->move = moving;
    n = n*130/100;
    m->trigger = lastmillis+n-skill*(n/16)+rnd(r+1);
};

void normalise(dynent *m, float angle)
{
    while(m->yaw<angle-180.0f) m->yaw += 360.0f;
    while(m->yaw>angle+180.0f) m->yaw -= 360.0f;
};

void monsteraction(dynent *m)           // main AI thinking routine, called every frame for every monster
{
    if(m->enemy->state==CS_DEAD) { m->enemy = player1; m->anger = 0; };
    normalise(m, m->targetyaw);
    if(m->targetyaw>m->yaw)             // slowly turn monster towards his target
    {
        m->yaw += curtime*0.5f;
        if(m->targetyaw<m->yaw) m->yaw = m->targetyaw;
    }
    else
    {
        m->yaw -= curtime*0.5f;
        if(m->targetyaw>m->yaw) m->yaw = m->targetyaw;
    };

    vdist(disttoenemy, vectoenemy, m->o, m->enemy->o);                         
    m->pitch = atan2(m->enemy->o.z-m->o.z, disttoenemy)*180/PI;         

    if(m->blocked)                                                                  // special case: if we run into scenery
    {
        m->blocked = false;
        if(!rnd(20000/monstertypes[m->mtype].speed) &&                              // try to jump over obstacle (rare)
           (curtime>=20 || (curtime>0 && rnd(2000/curtime)<100)))                   // throttle attempts when fps is high
        {
                m->jumpnext = true;
        }
        else if(m->trigger<lastmillis && (m->monsterstate!=M_HOME || !rnd(5)))      // search for a way around (common)
        {
            m->targetyaw += (rnd(3)+1)*90+(rnd(1+180)-90);                          // patented "random walk" AI pathfinding (tm) ;)
            transition(m, M_SEARCH, 1, 400, 1000);
        }
    }

    float enemyyaw = -(float)atan2(m->enemy->o.x - m->o.x, m->enemy->o.y - m->o.y)/PI*180+180;
    
    switch(m->monsterstate)
    {
        case M_PAIN:
        case M_ATTACKING:
        case M_SEARCH:
            if(m->trigger<lastmillis) transition(m, M_HOME, 1, 100, 200);
            break;
            
        case M_SLEEP:                       // state classic sp monster start in, wait for visual contact
        {
            vec target;
            if(editmode || !enemylos(m, target)) return;   // skip running physics
            normalise(m, enemyyaw);
            float angle = (float)fabs(enemyyaw-m->yaw);
            if(disttoenemy<8                   // the better the angle to the player, the further the monster can see/hear
            ||(disttoenemy<16 && angle<135)
            ||(disttoenemy<32 && angle<90)
            ||(disttoenemy<64 && angle<45)
            || angle<10)
            {
                transition(m, M_HOME, 1, 500, 200);
                playsound(S_GRUNT1+rnd(2), &m->o);
            };
            break;
        };
        
        case M_AIMING:                      // this state is the delay between wanting to shoot and actually firing
            if(m->trigger<lastmillis)
            {
                m->lastaction = 0;
                m->attacking = true;
                shoot(m, m->attacktarget);
                transition(m, M_ATTACKING, 0, 600, 0);
            };
            break;

        case M_HOME:                        // monster has visual contact, heads straight for player and may want to shoot at any time
            m->targetyaw = enemyyaw;
            if(m->trigger<lastmillis)
            {
                vec target;
                if(!enemylos(m, target))    // no visual contact anymore, let monster get as close as possible then search for player
                {
                    transition(m, M_HOME, 1, 800, 500);
                }
                else  // the closer the monster is the more likely he wants to shoot
                {
                    if(!rnd((int)disttoenemy/3+1) && m->enemy->state==CS_ALIVE)         // get ready to fire
                    { 
                        m->attacktarget = target;
                        transition(m, M_AIMING, 0, monstertypes[m->mtype].lag, 10);
                    }
                    else                                                                // track player some more
                    {
                        transition(m, M_HOME, 1, monstertypes[m->mtype].rate, 0);
                    };
                };
            };
            break;
    };

    moveplayer(m, 2, false);        // use physics to move monster
};

// returns int ignoredamage
// ignoredamage = 0: regular damage
// ignoredamage = -1: damage ignored
int monsterpain(dynent *m, int damage, dynent *d)
{
    if(editmode && m_classicsp) { playsound(monstertypes[m->mtype].painsound, &m->o); return -1; };
    if(d->monsterstate)     // a monster hit us
    {
        if(m!=d)            // guard for RL guys shooting themselves :)
        {
            m->anger++;     // don't attack straight away, first get angry
            int anger = m->mtype==d->mtype ? m->anger/2 : m->anger;
            if(anger>=monstertypes[m->mtype].loyalty) m->enemy = d;     // monster infight if very angry
        };
    }
    else                    // player hit us
    {
        m->anger = 0;
        m->enemy = d;
    };
    transition(m, M_PAIN, 0, monstertypes[m->mtype].pain,200);      // in this state monster won't attack
    if((m->health -= damage)<=0)
    {
        m->state = CS_DEAD;
        m->lastaction = lastmillis;
        player1->frags++;
        playsound(monstertypes[m->mtype].diesound, &m->o);
        monsterkilled();
    }
    else
    {
        playsound(monstertypes[m->mtype].painsound, &m->o);
    };
    return 0;
};

// choose whether or not monsters can use jumppads :)
VARP(monsterjumppad, 0, 1, 1);

// equivalent of player entity touch, but only teleports are used
// monsters can use jumppads if configured to do so
void monsterpickup(dynent *m, int mn)
{
    loopv(ents)
    {
        extentity &e = ents[i];
        if(e.type==TELEPORT || (monsterjumppad && e.type==JUMPPAD))
        {
            if(OUTBORD(e.x, e.y)) continue;
            vec v = vec(e.x, e.y, S(e.x, e.y)->floor+m->eyeheight);
            vdist(dist, t, m->o, v);
            if(e.type==TELEPORT && dist<4) teleport(i, m);
            else if(e.type==JUMPPAD && dist<2.5f)
            {
                if(jumppadlockout(i, m, monLockouts[mn].lastjumppad) == false) jumppad(i, m);
            };
        };
    };
};

void monsterthink()
{
    checksp();

    loopv(monsters)
    if(monsters[i]->state==CS_DEAD)
    {
        if(lastmillis-monsters[i]->lastaction<30000)
        {
            monsters[i]->move = 0;
            monsters[i]->jumpnext = 0;
            moveplayer(monsters[i], 2, false);
        };
    }
    else
    {
        monsterpickup(monsters[i], i);
        monsteraction(monsters[i]);
    };
};

void monsterrender()
{
    loopv(monsters) renderclient(monsters[i], false, monstertypes[monsters[i]->mtype].mdlname, monstertypes[monsters[i]->mtype].mscale/10.0f);
};
