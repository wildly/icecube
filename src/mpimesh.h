// Display mesh (experimental) - header file
// Copyright (C) 2020-2021 Willy Deng
// zlib License

enum AxialCameraID { MONO_CAMERA = 0, LEFT_CAMERA, RIGHT_CAMERA };
enum MpiType { MPI_CUBEMAP = 0, MPI_EAC, MPI_EQUIRECT, MPI_FISHEYE };
enum Views { MONO_VIEW = 0, STEREO_VIEW };
enum Layout { MONO_PICTURE = 0, LEFT_RIGHT, TOP_BOTTOM };

extern void addmpi(int texID, int type, unsigned int views, unsigned int layout, unsigned int texface, float fov1 = 0.0f, float fov2 = 0.0f);
extern void create_imagebox_eac_strips_vertices();
extern void render_imagebox_eac();
extern void create_imagebox_vertices();
extern void render_imagebox();
extern void create_imagecyl_vertices();
extern void render_imagecyl(bool wireframe = false);
extern void render_imagehtm();
extern void create_fisheye_hemisphere_skirt_vertices();
extern void render_fisheye_hemisphere_skirt(bool wireframe = false);
extern void mpicheckview();
extern int mpigettype();
extern float mpigetfov();
extern unsigned int mpigettexface();
extern void rendermpimesh();
