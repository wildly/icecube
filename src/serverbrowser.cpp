// serverbrowser.cpp: eihrul's concurrent resolver, and server browser window management

#include "cube.h"
#include <SDL_thread.h>

struct resolverthread
{
    SDL_Thread *thread;
    char *query;
    int starttime;
    bool discardresult;
};

struct resolverresult
{
    char *query;
    ENetAddress address;
};

vector<resolverthread> resolverthreads;
vector<char *> resolverqueries;
vector<resolverresult> resolverresults;
SDL_mutex *resolvermutex;
SDL_sem *resolversem;
int resolverlimit = 1000;

int resolverloop(void * data)
{
    resolverthread *rt = (resolverthread *)data;
    for(;;)
    {
        SDL_SemWait(resolversem);
        SDL_LockMutex(resolvermutex);
        // check whether work queue is empty
        if(resolverqueries.empty())
        {
            // unlock mutex and wait until semaphore
            SDL_UnlockMutex(resolvermutex);
            continue;
        }
        // get a work unit
        rt->query = resolverqueries.pop();
        rt->starttime = lastmillis;
        SDL_UnlockMutex(resolvermutex);
        ENetAddress address = { ENET_HOST_ANY, CUBE_SERVINFO_PORT };
        enet_address_set_host(&address, rt->query);
        SDL_LockMutex(resolvermutex);
        // Check whether the result is no longer needed.
        // If it is no longer needed, ignore this resolver result.
        // If it is needed, add it to the collection of resolver results.
        if(rt->discardresult == false)
        {
            resolverresult &rr = resolverresults.add();
            rr.query = rt->query;
            rr.address = address;
        };
        // clear our state
        rt->query = NULL;
        rt->starttime = 0;
        rt->discardresult = false;
        SDL_UnlockMutex(resolvermutex);
    };
    return 0;
};

const char *resolverthreadname = "Server Browser Resolver"; // All resolver threads use this name

void resolverinit(int threads, int limit)
{
    resolverlimit = limit;
    resolversem = SDL_CreateSemaphore(0);
    resolvermutex = SDL_CreateMutex();

    resolverthreads.reserve(threads); // vector::reserve() will reserve a minimum of 8.
    while(threads > 0)
    {
        resolverthread &rt = resolverthreads.add();
        rt.query = NULL;
        rt.starttime = 0;
        rt.discardresult = false;
        rt.thread = SDL_CreateThread(resolverloop, resolverthreadname, &rt);
        --threads;
    };
};

// Pre-req: resolvermutex should already be held by resolverclear()
// Instead of stopping the thread and optionally setting a flag to restart it, this function tells the thread to discard its results.
void resolverstop(resolverthread &rt, bool restart)
{
    rt.discardresult = true;
};

// should never need to restart resolver threads since the threads don't quit
void resolverrestart()
{
    SDL_LockMutex(resolvermutex);
    loopv(resolverthreads)
    {
        resolverthread &rt = resolverthreads[i];
        if(rt.thread == NULL) rt.thread = SDL_CreateThread(resolverloop, resolverthreadname, &rt);
    };
    SDL_UnlockMutex(resolvermutex);
};

void resolverclear()
{
    SDL_LockMutex(resolvermutex);
    resolverqueries.setsize(0);
    resolverresults.setsize(0);
    // tell all resolver threads to discard their results
    loopv(resolverthreads)
    {
        resolverthread &rt = resolverthreads[i];
        resolverstop(rt, true);
    };
    SDL_UnlockMutex(resolvermutex);

    // don't need to wait for the threads to finish resolving since they have been told to discard their results
    //while (SDL_SemTryWait(resolversem) == 0);

    // resolver threads never quit, but if they do in the future, clean them up here (or use SDL_DetachThread?)
    loopv(resolverthreads)
    {
        resolverthread &rt = resolverthreads[i];
        bool thisthreadwillexit = false;
        if(rt.thread && thisthreadwillexit)
        {
            SDL_WaitThread(rt.thread, NULL);
            //SDL_DetachThread(rt.thread);
            rt.thread = NULL;
        };
    };

    // if some of our threads did quit, we should restart them now
    resolverrestart();
};

void resolverquery(char *name)
{
    SDL_LockMutex(resolvermutex);
    resolverqueries.add(name);
    SDL_SemPost(resolversem);
    SDL_UnlockMutex(resolvermutex);
};

bool resolvercheck(char **name, ENetAddress *address)
{
    SDL_LockMutex(resolvermutex);
    if(!resolverresults.empty())
    {
        resolverresult &rr = resolverresults.last();
        *name = rr.query;
        *address = rr.address;
        resolverresults.pop_back();
        SDL_UnlockMutex(resolvermutex);
        return true;
    }
    loopv(resolverthreads)
    {
        resolverthread &rt = resolverthreads[i];
        if(rt.query)
        {
            if(lastmillis - rt.starttime > resolverlimit)
            {
                resolverstop(rt, true);
                *name = rt.query;
                SDL_UnlockMutex(resolvermutex);
                return true;
            };
        };
    };
    SDL_UnlockMutex(resolvermutex);
    return false;
};

struct serverinfo
{
    string name;
    string full;
    string map;
    string sdesc;
    int mode, numplayers, ping, protocol, minremain;
    ENetAddress address;
};

vector<serverinfo> servers;
ENetSocket pingsock = ENET_SOCKET_NULL;
int lastinfo = 0;

char *getservername(int n) { return servers[n].name; };

void addserver(char *servername)
{
    servers.reserve(100);
    loopv(servers) if(strcmp(servers[i].name, servername)==0) return;
    serverinfo &si = servers.insert(0, serverinfo());
    copystring(si.name, servername);
    si.full[0] = 0;
    si.mode = 0;
    si.numplayers = 0;
    si.ping = 9999;
    si.protocol = 0;
    si.minremain = 0;
    si.map[0] = 0;
    si.sdesc[0] = 0;
    si.address.host = ENET_HOST_ANY;
    si.address.port = CUBE_SERVINFO_PORT;
};

void pingservers()
{
    ENetBuffer buf;
    uchar ping[MAXTRANS];
    uchar *p;
    loopv(servers)
    {
        serverinfo &si = servers[i];
        if(si.address.host == ENET_HOST_ANY) continue;
        p = ping;
        putint(p, lastmillis);
        buf.data = ping;
        buf.dataLength = p - ping;
        enet_socket_send(pingsock, &si.address, &buf, 1);
    };
    lastinfo = lastmillis;
};
  
void checkresolver()
{
    char *name = NULL;
    ENetAddress addr = { ENET_HOST_ANY, CUBE_SERVINFO_PORT };
    while(resolvercheck(&name, &addr))
    {
        if(addr.host == ENET_HOST_ANY) continue;
        loopv(servers)
        {
            serverinfo &si = servers[i];
            if(name == si.name)
            {
                si.address = addr;
                addr.host = ENET_HOST_ANY;
                break;
            }
        }
    }
}

void checkpings()
{
    enet_uint32 events = ENET_SOCKET_WAIT_RECEIVE;
    ENetBuffer buf;
    ENetAddress addr;
    uchar ping[MAXTRANS], *p;
    char text[MAXTRANS];
    buf.data = ping; 
    buf.dataLength = sizeof(ping);
    while(enet_socket_wait(pingsock, &events, 0) >= 0 && events)
    {
        if(enet_socket_receive(pingsock, &addr, &buf, 1) <= 0) return;  
        loopv(servers)
        {
            serverinfo &si = servers[i];
            if(addr.host == si.address.host)
            {
                p = ping;
                si.ping = lastmillis - getint(p);
                si.protocol = getint(p);
                if(si.protocol!=PROTOCOL_VERSION) si.ping = 9998;
                si.mode = getint(p);
                si.numplayers = getint(p);
                si.minremain = getint(p);
                sgetstr();
                copystring(si.map, text);
                sgetstr();
                copystring(si.sdesc, text);                
                break;
            };
        };
    };
};

int sicompare(const serverinfo *a, const serverinfo *b)
{
    return a->ping>b->ping ? 1 : (a->ping<b->ping ? -1 : strcmp(a->name, b->name));
};

extern int ServerlistMenu;
void refreshservers()
{
    checkresolver();
    checkpings();
    if(lastmillis - lastinfo >= 5000) pingservers();
    servers.sort((void *)sicompare);
    int maxmenu = 16;
    loopv(servers)
    {
        serverinfo &si = servers[i];
        if(si.address.host != ENET_HOST_ANY && si.ping != 9999)
        {
            if(si.protocol!=PROTOCOL_VERSION) formatstring(si.full)("%s [different cube protocol]", si.name);
            else formatstring(si.full)("%d\t%d\t%s, %s: %s %s", si.ping, si.numplayers, si.map[0] ? si.map : "[unknown]", modename(si.mode), si.name, si.sdesc);
        }
        else
        {
            formatstring(si.full)(si.address.host != ENET_HOST_ANY ? "%s [waiting for server response]" : "%s [unknown host]\t", si.name);
        }
        si.full[50] = 0; // cut off too long server descriptions
        menumanual(ServerlistMenu, i, si.full);
        if(!--maxmenu) return;
    };
};

void servermenu()
{
    if(pingsock == ENET_SOCKET_NULL)
    {
        pingsock = enet_socket_create(ENET_SOCKET_TYPE_DATAGRAM);
        resolverinit(4, 1000);
    };
    resolverclear();
    loopv(servers) resolverquery(servers[i].name);
    refreshservers();
    menuset(ServerlistMenu);
};

void updatefrommaster()
{
    const int MAXUPD = 32000;
    uchar buf[MAXUPD];
    uchar *reply = retrieveservers(buf, MAXUPD);
    if(!*reply || strstr((char *)reply, "<html>") || strstr((char *)reply, "<HTML>")) conoutf("master server not replying");
    else { servers.setsize(0); execute((char *)reply); };
    servermenu();
};

COMMAND(addserver, ARG_1STR);
COMMAND(servermenu, ARG_NONE);
COMMAND(updatefrommaster, ARG_NONE);

void writeservercfg()
{
    FILE *f = fopen("servers.cfg", "w");
    if(!f) return;
    fprintf(f, "// servers connected to are added here automatically\n\n");
    loopvrev(servers) fprintf(f, "addserver %s\n", servers[i].name);
    fclose(f);
};


