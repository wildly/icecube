// client.cpp, mostly network related client game code

#include "cube.h"
#include "gamestate.h"

ENetHost *clienthost = NULL;
int connecting = 0;
int connattempts = 0;
int disconnecting = 0;
int clientnum = -1;         // our client id in the game
bool c2sinit = false;       // whether we need to tell the other clients our stats

int getclientnum() { return clientnum; };

// Called when checking whether a command is available in multiplayer
// If the command isn't allowed in multiplayer, notify the player that the operation isn't available
bool multiplayer()
{
    // check not correct on listen server?
    if(clienthost) conoutf("operation not available in multiplayer");
    return clienthost!=NULL;
};

bool allowedittoggle()
{
    bool allow = !clienthost || m_check(gamemode, M_EDIT);
    if(!allow) conoutf("editing in multiplayer requires coopedit mode");
    return allow; 
};

VARF(rate, 0, 0, 25000, if(clienthost && (!rate || rate>1000)) enet_host_bandwidth_limit (clienthost, rate, rate));

void throttle();

VARF(throttle_interval, 0, 5, 30, throttle());
VARF(throttle_accel,    0, 2, 32, throttle());
VARF(throttle_decel,    0, 2, 32, throttle());

void throttle()
{
    if(!clienthost || connecting) return;
    assert(ENET_PEER_PACKET_THROTTLE_SCALE==32);
    enet_peer_throttle_configure(clienthost->peers, throttle_interval*1000, throttle_accel, throttle_decel);
};

void newname(const char *name) { c2sinit = false; copystring(player1->name, name, 16); };
void newteam(const char *name) { c2sinit = false; copystring(player1->team, name, 5); };

COMMANDN(team, newteam, ARG_1STR);
COMMANDN(name, newname, ARG_1STR);

void writeclientinfo(FILE *f)
{
    fprintf(f, "name \"%s\"\nteam \"%s\"\n", player1->name, player1->team);
};

void connects(char *servername)
{   
    disconnect(1);  // reset state
    addserver(servername);

    conoutf("attempting to connect to %s", servername);
    ENetAddress address = { ENET_HOST_ANY, CUBE_SERVER_PORT };
    if(enet_address_set_host(&address, servername) < 0)
    {
        conoutf("could not resolve server %s", servername);
        return;
    };

    clienthost = enet_host_create(NULL, 1, 0, rate, rate);

    if(clienthost)
    {
        enet_host_connect(clienthost, &address, 1, 0);
        enet_host_flush(clienthost);
        connecting = InternalClockPtr->getMillis();
        connattempts = 0;
    }
    else
    {
        conoutf("could not connect to server");
        disconnect();
    };
};

void disconnect(int onlyclean, int async)
{
    if(clienthost)
    {
        if(!connecting && !disconnecting)
        {
            enet_peer_disconnect(clienthost->peers, 0);
            enet_host_flush(clienthost);
            disconnecting = InternalClockPtr->getMillis();
        };
        if(clienthost->peers->state != ENET_PEER_STATE_DISCONNECTED)
        {
            if(async) return;
            enet_peer_reset(clienthost->peers);
        };
        enet_host_destroy(clienthost);
    };

    if(clienthost && !connecting) conoutf("disconnected");
    clienthost = NULL;
    connecting = 0;
    connattempts = 0;
    disconnecting = 0;
    clientnum = -1;
    c2sinit = false;
    player1->lifesequence = 0;
    loopv(players) zapdynent(players[i]);
    
    localdisconnect();

    if(!onlyclean) { stop(); localconnect(); };
};

void trydisconnect()
{
    if(!clienthost)
    {
        conoutf("not connected");
        return;
    };
    if(connecting) 
    {
        conoutf("aborting connection attempt");
        disconnect();
        return;
    };
    conoutf("attempting to disconnect...");
    disconnect(0, !disconnecting);
};

string ctext;
void toserver(char *text) { conoutf("%s:\f %s", player1->name, text); copystring(ctext, text, 80); };
void echo(char *text) { conoutf("%s", text); };

COMMAND(echo, ARG_VARI);
COMMANDN(say, toserver, ARG_VARI);
COMMANDN(connect, connects, ARG_1STR);
COMMANDN(disconnect, trydisconnect, ARG_NONE);

// collect c2s messages conveniently

vector<ivector> messages;

void addmsg(int rel, int num, int type, ...)
{
    if(demoplayback) return;
    if(num!=msgsizelookup(type)) { defformatstring(s)("inconsistent msg size for %d (%d != %d)", type, num, msgsizelookup(type)); fatal(s); };
    if(messages.length()==100) { conoutf("command flood protection (type %d)", type); return; };
    ivector &msg = messages.add();
    msg.add(num);
    msg.add(rel);
    msg.add(type);
    va_list marker;
    va_start(marker, type); 
    loopi(num-1) msg.add(va_arg(marker, int));
    va_end(marker);  
};

void server_err()
{
    conoutf("server network error, disconnecting...");
    disconnect();
};

int lastupdate = 0, lastping = 0;
string toservermap;
bool senditemstoserver = false;     // after a map change, since server doesn't have map data

string clientpassword;
void password(char *p) { copystring(clientpassword, p); };
COMMAND(password, ARG_1STR);

bool netmapstart() { senditemstoserver = true; return clienthost!=NULL; };

void initclientnet()
{
    ctext[0] = 0;
    toservermap[0] = 0;
    clientpassword[0] = 0;
    newname("unnamed");
    newteam("red");
};

void sendpackettoserv(void *packet)
{
    if(clienthost) { enet_host_broadcast(clienthost, 0, (ENetPacket *)packet); enet_host_flush(clienthost); }
    else localclienttoserver((ENetPacket *)packet);
}

const int PING_INTERVAL_MSEC = 250;         // 4 pings per sec
const int C2S_INFO_INTERVAL_MSEC = 40;      // don't update faster than 25fps

// if 10% early, will send c2sinfo and record the time error here
// if late, will accumulate the time error here to send the next frame earlier by up to 1/2 of the interval
int c2sinfo_interval_accumulated_err = 0;
bool should_send_c2sinfo()
{
    int msecDiff = lastmillis-lastupdate;
    if(c2sinfo_interval_accumulated_err + msecDiff < 0.9f*C2S_INFO_INTERVAL_MSEC)
        return false;

    //conoutf("net: sending update: interval is %d+(%d)", msecDiff, c2sinfo_interval_accumulated_err);
    c2sinfo_interval_accumulated_err += (msecDiff - C2S_INFO_INTERVAL_MSEC);

    // reset c2sinfo_time_accumulated_err if it exceeds C2S_INFO_INTERVAL_MSEC/2 to avoid excessive back-to-back updates
    if(c2sinfo_interval_accumulated_err > C2S_INFO_INTERVAL_MSEC/2)
        c2sinfo_interval_accumulated_err = (C2S_INFO_INTERVAL_MSEC/2);

    return true;
};

// SV_POS:
// 1. Velocity with baked-in timeinair:
// SV_POS doesn't send d->timeinair, so we need to estimate it.
//  - TODO: We can reset d->timeinair when d->onfloor is received
//  - Alternately, we can bake d->timeinair into d->vel, but all clients need to use this interpretation
// Due to SV_POS quantizing the position, the model's position for other clients can flicker.
//  - TODO: We may be able to avoid this by snapping the model to the floor if d->onfloor is received
// Setting for SV_POS_BAKED_IN_TIMEINAIR:
// 0: Send d->vel without d->timeinair "baked in"
// 1: Send d->vel with d->timeinair "baked in"
int SV_POS_BAKED_IN_TIMEINAIR = 1;

// SV_POS_EXT:
// 1. Don't need to send timeinair because we have accurate gravity?

void c2sinfo(dynent *d)                     // send update to the server
{
    if(clientnum<0) return;                 // we haven't had a welcome message from the server yet
    if(!should_send_c2sinfo()) return;      // limit c2sinfo update rate
    ENetPacket *packet = enet_packet_create (NULL, MAXTRANS, 0);
    uchar *start = packet->data;
    uchar *p = start+2;
    bool serveriteminitdone = false;
    if(toservermap[0])                      // suggest server to change map
    {                                       // do this exclusively as map change may invalidate rest of update
        packet->flags = ENET_PACKET_FLAG_RELIABLE;
        putint(p, SV_MAPCHANGE);
        sendstring(toservermap, p);
        toservermap[0] = 0;
        putint(p, nextmode);
    }
    else
    {
        putint(p, SV_POS);
        putint(p, clientnum);
        putint(p, (int)(d->o.x*DMF));       // quantize coordinates to 1/16th of a cube, between 1 and 3 bytes
        putint(p, (int)(d->o.y*DMF));
        putint(p, (int)(d->o.z*DMF));
        putint(p, (int)(d->yaw*DAF));
        putint(p, (int)(d->pitch*DAF));
        putint(p, (int)(d->roll*DAF));
        vec netcode_v;
        if(PROTOCOL_VERSION >= (1<<8))      // has SV_POS_EXT support
            netcode_v = d->vel;
        else
        {
            netcode_v = d->vel;
            if(SV_POS_BAKED_IN_TIMEINAIR)
            {
                netcode_v = getPhysVelocity(d);
                vdiv(netcode_v, d->maxspeed);
            }
        }
        putint(p, (int)(netcode_v.x*DVF));  // quantize to 1/100, almost always 1 byte
        putint(p, (int)(netcode_v.y*DVF));
        putint(p, (int)(netcode_v.z*DVF));  // If the remote client falls fast enough to make a thud sound under normal gravity,
                                            // this will need more than 1 byte (it is sent as 3 bytes over the network). This is fine
                                            // because a remote client that is jumping also needs 3 bytes for its vertical speed.
        // pack rest in 1 byte: strafe:2, move:2, onfloor:1, state:3
        int net_move = d->move;
        int net_strafe = d->strafe;
        if(d==player1 && d->move == 0) net_move = getjoymovementmove() >= 0.5f ? 1 : getjoymovementmove() <= -0.5f ? -1 : 0;
        if(d==player1 && d->strafe == 0) net_strafe = getjoymovementstrafe() >= 0.5f ? 1 : getjoymovementstrafe() <= -0.5f ? -1 : 0;
        putint(p, (net_strafe&3) | ((net_move&3)<<2) | (((int)d->onfloor)<<4) | ((editmode ? CS_EDITING : d->state)<<5) );

        // SV_POS_EXT
        if(PROTOCOL_VERSION >= (1<<8))
        {
            putint(p, SV_POS_EXT);
            // quantize to a 1/4th of a unit (this is in cubes/sec; it has been pre-multiplied by maxspeed compared to netcode_v; maxspeed is 22 for players)
            loopi(6) putint(p, 0*DVRF);
            // TBD: timeinair?
            putint(p, d->timeinair);
            // TBD: glide, slide, crouch, prone, ADS, climbing? (these are mutually exclusive, so represent it as an enum)
            putint(p, 0);
        }

        // SV_BUFF: Include an update on the time remaining for your buffs. Rate limit to once per second.
        if(PROTOCOL_VERSION >= (1<<8))
        {
            static Uint32 lastbuffupdate = 0;
            if(SDL_GetTicks()/1000 != lastbuffupdate)
            {
                lastbuffupdate = SDL_GetTicks()/1000;
                if(d->buffs[BUFF_QUAD])
                {
                    putint(p, SV_BUFF);
                    putint(p, BUFF_QUAD);
                    putint(p, d->buffs[BUFF_QUAD]);
                }
            }
        }

        if(senditemstoserver)
        {
            packet->flags = ENET_PACKET_FLAG_RELIABLE;
            putint(p, SV_ITEMLIST);
            if(!m_noitems) putitems(p);
            putint(p, -1);
            senditemstoserver = false;
            serveriteminitdone = true;
        };
        if(ctext[0])    // player chat, not flood protected for now
        {
            packet->flags = ENET_PACKET_FLAG_RELIABLE;
            putint(p, SV_TEXT);
            sendstring(ctext, p);
            ctext[0] = 0;
        };
        if(!c2sinit)    // tell other clients who I am
        {
            packet->flags = ENET_PACKET_FLAG_RELIABLE;
            c2sinit = true;
            putint(p, SV_INITC2S);
            sendstring(player1->name, p);
            sendstring(player1->team, p);
            putint(p, player1->lifesequence);
        };
        loopv(messages)     // send messages collected during the previous frames
        {
            ivector &msg = messages[i];
            if(msg[1]) packet->flags = ENET_PACKET_FLAG_RELIABLE;
            loopi(msg[0]) putint(p, msg[i+2]);
        };
        messages.shrink(0);
        if(lastmillis-lastping>PING_INTERVAL_MSEC)
        {
            putint(p, SV_PING);
            putint(p, lastmillis);
            lastping = lastmillis;
        };
    };
    *(ushort *)start = ENET_HOST_TO_NET_16(p-start);
    enet_packet_resize(packet, p-start);
    incomingdemodata(start, p-start, true);
    if(clienthost) { enet_host_broadcast(clienthost, 0, packet); enet_host_flush(clienthost); }
    else localclienttoserver(packet);
    lastupdate = lastmillis;
    if(serveriteminitdone) loadgamerest();  // hack
};

void gets2c()           // get updates from the server
{
    ENetEvent event;
    if(!clienthost) return;
    if(connecting && InternalClockPtr->getMillis()/3000 > connecting/3000)
    {
        conoutf("attempting to connect...");
        connecting = InternalClockPtr->getMillis();
        ++connattempts; 
        if(connattempts > 3)
        {
            conoutf("could not connect to server");
            disconnect();
            return;
        };
    };
    while(clienthost!=NULL && enet_host_service(clienthost, &event, 0)>0)
    switch(event.type)
    {
        case ENET_EVENT_TYPE_CONNECT:
            conoutf("connected to server");
            connecting = 0;
            throttle();
            break;
         
        case ENET_EVENT_TYPE_RECEIVE:
            if(disconnecting) conoutf("attempting to disconnect...");
            else localservertoclient(event.packet->data, event.packet->dataLength);
            enet_packet_destroy(event.packet);
            break;

        case ENET_EVENT_TYPE_DISCONNECT:
            if(disconnecting) disconnect();
            else server_err();
            return;
    }
};

