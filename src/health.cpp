// health.cpp: dynent health, armor, damage, and regeneration

#include "cube.h"

const int DynentBaseHealth = 100;

// returns a dynent's maximum health without buffs
int dynent::basehealth()
{
    if(monsterstate) return monsterbasehealth(this->mtype);

    int health = DynentBaseHealth;

    // special players
    if(special.big) health = 2*health;

    return health;
};

// returns a dynent's basehealth + any buffs to its maximum health
int dynent::maxhealth()
{
    return basehealth() + buffs[BUFF_HEALTH];
};

// Armor Damage
// complicated armor damage calculation follows...

const int ArmourDamageRatio = 60;
// Columns: Blue, Green, Yellow
// Rows: armourdamagetabletype()
const int ArmourDamageTable[][3] =
{
    {  0,  0,  0 }, // no armour
    { 60,  0,  0 }, // BA
    {  0, 60,  0 }, // GA
    { 20, 40,  0 }, // BA+GA
    {  0,  0, 60 }, // YA
    { 15,  0, 45 }, // BA+YA
    {  0, 24, 36 }, // GA+YA
    { 10, 20, 30 }, // BA+GA+YA
};

uint getarmourdamagetabletype(dynent *d, int *adamage)
{
    bool hasBlueArmour   = (d->armour[A_BLUE]   - adamage[A_BLUE])   != 0;
    bool hasGreenArmour  = (d->armour[A_GREEN]  - adamage[A_GREEN])  != 0;
    bool hasYellowArmour = (d->armour[A_YELLOW] - adamage[A_YELLOW]) != 0;

    unsigned int row = 0;
    if(hasBlueArmour) row |= 1<<0;
    if(hasGreenArmour) row |= 1<<1;
    if(hasYellowArmour) row |= 1<<2;

    return row;
};

// armourdamagecalc
int armourdamagestep(int remainingpoints, int *adamage)
{
    unsigned int armourtype = getarmourdamagetabletype(player1, adamage);
    int absorb = 0;
    if(armourtype&(1<<2)) absorb = 60;
    else if(armourtype&(1<<1)) absorb = 40;
    else if(armourtype&(1<<0)) absorb = 20;

    if(absorb == 0 || rnd(100) >= absorb) return remainingpoints - 1;
    int r = rnd(ArmourDamageRatio);
    if(r < ArmourDamageTable[armourtype][A_BLUE])
    {
        adamage[A_BLUE] += 1;
        return remainingpoints - 1;
    } else if((r-=ArmourDamageTable[armourtype][A_BLUE]) < ArmourDamageTable[armourtype][A_GREEN])
    {
        adamage[A_GREEN] += 1;
        return remainingpoints - 1;
    } else
    {
        adamage[A_YELLOW] += 1;
        return remainingpoints - 1;
    };
};

// classic armour damage calculation
// Note: Only for classic cube gamemode where dynents have at most one kind of armour at any particular time.
void classicarmourdamagestep(int fulldamage, int *adamage)
{
    // player1 has no armor points whatsoever
    if(player1->armour[A_BLUE] == 0 && player1->armour[A_GREEN] == 0 && player1->armour[A_YELLOW] == 0)
        return;

    int armourtype = player1->armourtype;
    const int absorb = armourtype == A_BLUE ? 20 : armourtype == A_GREEN ? 40 : 60;
    int armourdamage = fulldamage*absorb/100;

    // armourdamage can't exceed current armor points
    armourdamage = min(player1->armour[armourtype] - adamage[armourtype], armourdamage);

    // update armourdamage amount in adamage
    adamage[armourtype] += armourdamage;
};

// try to use classic armor damage when player1 has only one type of armor active/remaining
// note: armor detection needs to take into account adamage because armor damage calculation may be in progress
int tryclassicarmourdamagestep(int remainingdamage, int *adamage)
{
    // refer to getarmourdamagetabletype() for the armor detection
    uint row = getarmourdamagetabletype(player1, adamage);
    if((row == 1<<0) ||
       (row == 1<<1) ||
       (row == 1<<2))
    {
        classicarmourdamagestep(remainingdamage, adamage);
        return true;
    } else
        return false;
};

// armourloss is a pointer to int[3] or it is NULL
int armourdamagecalc(int damage, int *armourloss)
{
    const bool saveresults = (armourloss != NULL);

    int adcalc[NUMARMOUR];
    loopi(NUMARMOUR) adcalc[i] = 0;

    if(classicRuleset)
        classicarmourdamagestep(damage, adcalc);
    else
    {
        int remainingdamage = damage;
        for(;;)
        {
            if(remainingdamage == 0) break;
            if(tryclassicarmourdamagestep(remainingdamage, adcalc)) remainingdamage = 0;
            else remainingdamage = armourdamagestep(remainingdamage, adcalc);
        };
    };

    if(player1->armour[A_BLUE] < adcalc[A_BLUE] ||
       player1->armour[A_GREEN] < adcalc[A_GREEN] ||
       player1->armour[A_YELLOW] < adcalc[A_YELLOW])
    {
        conoutf("armour error");
        return 0;
    };

    player1->armour[A_BLUE] -= adcalc[A_BLUE];
    player1->armour[A_GREEN] -= adcalc[A_GREEN];
    player1->armour[A_YELLOW] -= adcalc[A_YELLOW];

    updatearmourtype(player1);

    if(saveresults)
    {
        armourloss[A_BLUE] = adcalc[A_BLUE];
        armourloss[A_GREEN] = adcalc[A_GREEN];
        armourloss[A_YELLOW] = adcalc[A_YELLOW];
    };
    return adcalc[A_BLUE] + adcalc[A_GREEN] + adcalc[A_YELLOW];
};

// Armor addition
extern itemstat itemstats[];

// 0: none, 1: armour pickup only, 2: armour pickup and regeneration
VAR(logarmourdata,0,0,2);

void givearmour(dynent *d, const int type, int addVal, int maxVal) // maxVal: default value (0) set in protos.h
{
    int &ba = d->armour[A_BLUE];
    int &ga = d->armour[A_GREEN];
    int &ya = d->armour[A_YELLOW];

    int unusedArmourAmount[NUMARMOUR];
    loopi(NUMARMOUR) unusedArmourAmount[i] = 0;
    int lostArmourAmount[NUMARMOUR];
    loopi(NUMARMOUR) lostArmourAmount[i] = 0;

    if(classicRuleset)
    {
        if(type == A_GREEN) { lostArmourAmount[A_YELLOW] = ya; ya = 0; };
        if(type == A_YELLOW) { lostArmourAmount[A_GREEN] = ga; ga = 0; };
    };

    switch(type)
    {
    // Blue Armour pickup adds to Blue Armour until total armour reaches maxVal.
    case A_BLUE:
    {
        if(maxVal == 0) maxVal = max(d->armourtotal(), d->maxarmour());
        const int superiorBAVal = ga + ya;
        const int baPlusAddVal = ba + addVal;
        ba = clamp(baPlusAddVal, 0, max(0, maxVal - superiorBAVal));
        unusedArmourAmount[A_BLUE] += baPlusAddVal - ba;
        break;
    };
    // Green Armour pickup adds to Green Armour until total of GA+YA reaches maxVal.
    // It replaces Blue Armour where necessary to prevent total armour from exceeding 200.
    case A_GREEN:
    {
        const int startingArmour = d->armourtotal();
        const int startingGAPlusYA = ga + ya;
        if(maxVal == 0) maxVal = max(startingGAPlusYA, itemstats[I_GREENARMOUR-I_SHELLS].max);
        const int gaPlusAddVal = ga + addVal;
        ga = clamp(gaPlusAddVal, 0, maxVal - ya);
        unusedArmourAmount[A_GREEN] += gaPlusAddVal - ga;
        // Limit Blue Armour
        const int maxBAVal = max(startingArmour, d->maxarmour());
        const int superiorBAVal = ga + ya;
        const int newBAVal = clamp(ba, 0, max(0, maxBAVal - superiorBAVal));
        lostArmourAmount[A_BLUE] = ba - newBAVal;
        ba = newBAVal;
        break;
    };
    // Yellow Armour pickup adds to Yellow Armour until YA reaches maxVal;
    // It replaces Green Armour and Blue Armour where necessary.
    case A_YELLOW:
    {
        if(maxVal == 0) maxVal = itemstats[I_YELLOWARMOUR-I_SHELLS].max;
        const int startingArmour = d->armourtotal();
        const int startingGAPlusYA = ga + ya;
        const int yaPlusAddVal = ya + addVal;
        ya = clamp(yaPlusAddVal, 0, maxVal);
        unusedArmourAmount[A_YELLOW] += yaPlusAddVal - ya;
        // Limit Green Armour
        const int maxGAVal = max(startingGAPlusYA, max(itemstats[I_GREENARMOUR-I_SHELLS].max, maxVal));
        const int newGAVal = clamp(ga, 0, maxGAVal - ya);
        lostArmourAmount[A_GREEN] = ga - newGAVal;
        ga = newGAVal;
        // Limit Blue Armour
        const int maxBAVal = max(startingArmour, max(d->maxarmour(), maxVal));
        const int superiorBAVal = ga + ya;
        const int newBAVal = clamp(ba, 0, max(0, maxBAVal - superiorBAVal));
        lostArmourAmount[A_BLUE] = ba - newBAVal;
        ba = newBAVal;
        break;
    };
    default:
        conoutf("givearmour: bad armour type");
    }; // switch type

    // update armourtype
    updatearmourtype(d);

    if(logarmourdata)
    {
        // Don't log armour regeneration unless log level 2 or at least 5 armour was awarded in this update.
        // Detect this as armour regeneration if type is Blue Armour and addVal is less than 5.
        if(type != A_BLUE || addVal >= 5 || logarmourdata == 2)
        {
            const char *armournames[] = { "blue", "green", "yellow" };
            printf("pickup,givearmour,type:%s,amount:%d,maxVal:%d\n", armournames[type], addVal, maxVal);
        };
        if(unusedArmourAmount[A_BLUE] || unusedArmourAmount[A_GREEN] || unusedArmourAmount[A_YELLOW])
            printf("pickup,givearmour,unusedAmount,blue:%d,green:%d,yellow:%d\n", unusedArmourAmount[A_BLUE], unusedArmourAmount[A_GREEN], unusedArmourAmount[A_YELLOW]);
        if(lostArmourAmount[A_BLUE] || lostArmourAmount[A_GREEN] || lostArmourAmount[A_YELLOW])
            printf("pickup,givearmour,lostAmount,blue:%d,green:%d,yellow:%d\n", lostArmourAmount[A_BLUE], lostArmourAmount[A_GREEN], lostArmourAmount[A_YELLOW]);
        loghealth();
    };
};

void updatearmourtype(dynent *d)
{
    int armourType = A_BLUE;
    if(d->armour[A_GREEN] > 0) armourType = A_GREEN;
    if(d->armour[A_YELLOW] > 0) armourType = A_YELLOW;
    d->armourtype = armourType;
};

// Health and Armor regeneration

ExpRegenData &basicRegen(int time, bool dynentNeedsHealing);
ExpRegenData &expHealthRegen(int time, bool dynentNeedsHealing);
void regendata_reconcile_step(float &award, float &regendata_award);

void checkarmourregen(int time);
void checkregen(int time)
{
    if(player1->state == CS_DEAD)
        return;

    vector<RegenData> &rv = player1->regen;

    if(player1->health < player1->maxhealth())
    {
        // 1: update the regen data structures
        loopv(rv) rv[i].award += rv[i].rate * (float)time/1000.0f;

        // 2: determine the amount of possible hp recovery
        float award = 0.0f;
        loopv(rv) award += rv[i].award;

        // (steps 1 and 2 above, for slowmo clarity regen)
        ExpRegenData &h = basicRegen(time, true);
        award += h.award;
        // (steps 1 and 2 above, for exp health regen)
        ExpRegenData &e = expHealthRegen(time, player1->special.regenhealth ? true : false);
        award += e.award;

        // 3: determine how much hp recovery is consumed
        award = min(award, (float)max(0, player1->maxhealth() - player1->health));
        award = (float)((int)award);

        // 4: apply the hp recovery
        player1->health += (int)award;
        extern int logbattledata;
        if((int)award && logbattledata)
        {
            loghealth();
        };
        loopv(rv)
        {
            regendata_reconcile_step(award, rv[i].award);
            if(award == 0.0f) break;
        };
        if(award)
        {
            regendata_reconcile_step(award, h.award);
        };
        if(award)
        {
            regendata_reconcile_step(award, e.award);
        };
        // award should be 0 at this point...

        // 5: zero out the remaining regendata awards if healed to full hp
        if(!(player1->health < player1->maxhealth()))
        {
            loopv(rv) rv[i].award = 0.0f;
            h.award = 0.0f;
            e.award = 0.0f;
        };
    }
    else
    {
        // (1.b, 5.b): exp regen data structure has to be updated even when fully (or over) healthy
        basicRegen(time, false);
        expHealthRegen(time, false);
        // (5.b): if fully healthy, zero out previously accumulated hp recovery
        loopv(rv) rv[i].award = 0.0f;
    };

    if(player1->special.regenarmour || player1->special.scientist || player1->armourRegen.length() > 0)
        checkarmourregen(time);
};

// this step removes an amount of awarded regeneration from the collection of regendata
// &award: remaining amount to be reconciled
// &regendata_award: individual awards from the collection of regendata
void regendata_reconcile_step(float &award, float &regendata_award)
{
    if(award == 0.0f) return;
    if(award > regendata_award)
    {
        award -= regendata_award;
        regendata_award = 0.0f;
    }
    else
    {
        regendata_award -= award;
        award = 0.0f;
    };
};

const float EXPREGEN_REGEN_CAPACITY = 200.0f;
const float EXPREGEN_REGEN_RATE = 10.0f;
const float CLARITY_CAPACITY = 100.0f;
const float CLARITY_REGEN_RATE = 10.0f;

void resetexpregen(dynent *d)
{
    d->exphealthregen.tank = EXPREGEN_REGEN_CAPACITY;
    d->exphealthregen.award = 0.0f;
    d->exparmourregen.tank = EXPREGEN_REGEN_CAPACITY;
    d->exparmourregen.award = 0.0f;
    d->basicregen.tank = CLARITY_CAPACITY;
    d->basicregen.award = 0.0f;
};

void inhibitexpregen(int incomingdamage, int healthdamage)
{
    // Regen types where only health damage causes loss
    // (Nothing here.)

    // Regen types where health and armor damage causes loss
    player1->exphealthregen.tank = max(0.0f, player1->exphealthregen.tank - (float)incomingdamage);
    player1->exparmourregen.tank = max(0.0f, player1->exparmourregen.tank - (float)incomingdamage);
    player1->basicregen.tank = max(0.0f, player1->basicregen.tank - (float)incomingdamage);
};

ExpRegenData &basicRegen(int time, bool dynentNeedsHealing)
{
    //float &award = player1->basicregen.award; // not used
    float &tank = player1->basicregen.tank;

    float inflow = CLARITY_REGEN_RATE * (float)time/1000.0f;
    float retained = inflow;

    tank = min(tank + retained, CLARITY_CAPACITY);

    return player1->basicregen;
};

ExpRegenData &expHealthRegen(int time, bool dynentNeedsHealing)
{
    float &award = player1->exphealthregen.award;
    float &tank = player1->exphealthregen.tank;

    float inflow = EXPREGEN_REGEN_RATE * (float)time/1000.0f;
    float retained = (EXPREGEN_REGEN_CAPACITY - tank) * (1.0f - exp(-0.05f * (float)time / 1000.0f));

    if(!dynentNeedsHealing)
    {
        retained = inflow;
        award = 0.0f;
    };

    award += (inflow - retained);
    tank = min(tank + retained, EXPREGEN_REGEN_CAPACITY);

    return player1->exphealthregen;
};

ExpRegenData &expArmourRegen(int time, bool dynentNeedsArmour)
{
    float &award = player1->exparmourregen.award;
    float &tank = player1->exparmourregen.tank;

    float inflow = EXPREGEN_REGEN_RATE * (float)time/1000.0f;
    float retained = (EXPREGEN_REGEN_CAPACITY - tank) * (1.0f - exp(-0.05f * (float)time / 1000.0f));

    if(!dynentNeedsArmour)
    {
        retained = inflow;
        award = 0.0f;
    };

    award += (inflow - retained);
    tank = min(tank + retained, EXPREGEN_REGEN_CAPACITY);

    return player1->exparmourregen;
};

void checkarmourregen(int time)
{
    if(player1->state == CS_DEAD)
        return;

    const int maxBlueArmour = player1->maxarmour();
    vector<RegenData> &rv = player1->armourRegen;

    //todo: unduplicate this code within checkregen() and checkarmourregen()
    if(player1->armourtotal() < maxBlueArmour)
    {
        // 1.a: update the regen data structures
        loopv(rv) rv[i].award += rv[i].rate * (float)time/1000.0f;

        // 2: determine the amount of possible armour recovery
        float award = 0.0f;
        loopv(rv) award += rv[i].award;

        // (1, 2): Exp Armour regen
        ExpRegenData &expArmour = expArmourRegen(time, player1->special.regenarmour ? true : false);
        award += expArmour.award;

        // 3: determine how many armour points are consumed
        // Note: Not necessary, givearmour() will do that for us
        // Note: Still need to round to an int
        award = (float)((int)award);

        // 4: apply the armour recovery
        if((int)award) givearmour(player1, A_BLUE, (int)award, maxBlueArmour);
        loopv(rv)
        {
            regendata_reconcile_step(award, rv[i].award);
            if(award == 0.0f) break;
        };
        if(award)
        {
            regendata_reconcile_step(award, expArmour.award);
        };
        // award should be 0 at this point...

        // 5: zero out the remaining award if healed to full armour
        if(!(player1->armourtotal() < maxBlueArmour))
        {
            loopv(rv) rv[i].award = 0.0f;
            expArmour.award = 0.0f;
        };
    }
    else
    {
        // (1.b, 5.b): exp regen data structure has to be updated even when full (or more) armour
        expArmourRegen(time, false);
        // (5.b): if full armour, zero out previously accumulated armour recovery
        loopv(rv) rv[i].award = 0.0f;
    };
};

void loghealth()
{
    extern uint lastrealtime;
    extern float totalmillis;
    printf("health,realtime:%u,phystime:%d,health:%d,bluearmour:%d,greenarmour:%d,yellowarmour:%d\n", lastrealtime, (int)totalmillis, player1->health, player1->armour[A_BLUE], player1->armour[A_GREEN], player1->armour[A_YELLOW]);
};
