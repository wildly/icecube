#include "cube.h"
#include <SDL_mixer.h>

#define PL_MPEG_IMPLEMENTATION
#include "../3rdparty/phoboslab/pl_mpeg.h"

// Pointer to a I420 texture created with SDL
//SDL_Texture *videoTexture = NULL;

// Pointer to a buffer representing a 24-bit RGB texture
uchar *videoTexture = NULL;

// Cube engine texture slot tnum
int videoTextureNum = 0;

// soundstream num
int soundstreamNum = -1;

int videoWidth = 0;
int videoHeight = 0;
int videoBufferWidth = 0;
int videoBufferHeight = 0;
int audioSamplerate = 0;

float vtime = 0.0f;
int frameskip = 0;

// 0 = don't skip video frames, 1 = render every other video frame
VARP(videoframeskip, 0, 0, 1);

// Playback rate in percent
VARP(videoplaybackrate, 0, 100, 200);

plm_t *plm = NULL;

void updatevideotexture(int deltamsec)
{
    if(!plm) return;

    float deltatime = deltamsec/1000.0f;
    deltatime = deltatime*videoplaybackrate/100.0f;

    if(deltatime > 1/25.0f) deltatime = 1/25.0f;
    // When deltatime is a large value, pl_mpeg may have to decode multiple frames. This would cause frametime to go up and the next frame's deltatime to also be a large value.
    // TODO: Maybe instead of limiting deltatime, pl_mpeg can stop decoding once a given timeout is exceeded.

    plm_decode(plm, deltatime);

    vtime += deltatime;

    // Check soundstream sources.
    // This is called on every engine frame update, typically at 60 fps.
    // Maybe plm_decode didn't decode any additional audio data, but an SDL_AudioStream has processed more data?
    soundstream_check_sources();
}

void videocallback(plm_t *plm, plm_frame_t *frame, void *user)
{
    const int YPlaneBytes = videoBufferHeight*videoBufferWidth;
    const int UPlaneBytes = videoBufferHeight*videoBufferWidth/4;
    const int VPlaneBytes = videoBufferHeight*videoBufferWidth/4;

/*
    uchar *i420_buffer = (uchar *)malloc(videoBufferHeight*videoBufferWidth*12/8);
    memcpy(i420_buffer, frame->y.data, YPlaneBytes);
    memcpy(i420_buffer+YPlaneBytes, frame->cr.data, UPlaneBytes);
    memcpy(i420_buffer+UPlaneBytes, frame->cb.data, VPlaneBytes);

    SDL_UpdateTexture(videoTexture, NULL, i420_buffer, videoBufferWidth*12/8);
*/

/*
    memcpy(videoTexture, frame->y.data, YPlaneBytes);
    memcpy(videoTexture+YPlaneBytes, frame->cr.data, UPlaneBytes);
    memcpy(videoTexture+YPlaneBytes+UPlaneBytes, frame->cb.data, VPlaneBytes);

    // TODO: upload I420 texture data to GPU via OpenGL call?
*/

    // plm_frame_to_rgb is expensive when done on the CPU. The preferred method is to render using a GPU shader.
    // We will render frames when the frameskip variable is even, starting at frameskip = 0.
    // TODO: If seeking the video, we should also set frameskip to 0 so the video texture can be updated promptly.
    if(videoframeskip)
    {
        if(frameskip&1) { ++frameskip; return; }
        else ++frameskip;
    }

    plm_frame_to_rgb(frame, videoTexture, videoBufferWidth*3);

    uploadvideotex(videoTextureNum, videoTexture);

    //conoutf("texturevideo: video callback completed");
}

// SDL_AudioStream (SDL 2.0.7)
// Create an SDL_AudioStream to hold the samples and perform samplerate conversion if needed.
// Note: SDL_AudioStream is opaque.
SDL_AudioStream *resampledAudio = NULL;

void audiocallback(plm_t *plm, plm_samples_t *samples, void *user)
{
    SDL_AudioStreamPut(resampledAudio, samples->interleaved, PLM_AUDIO_SAMPLES_PER_FRAME*2*sizeof(float));

    // Try immediately to play audio
    soundstream_check_sources();

    //printf("audiocallback completed\n");
}

void closevideo();
bool loadvideo(int tnum, const char *filename)
{
    if(plm)
    {
        conoutf("loadvideo: Closing previously opened video since only one video can be opened");
        closevideo();
    }

    plm = plm_create_with_filename(filename);
    if(!plm)
        return false;

    // FIXME: Use pointer to texturevideo context as user-supplied data to callback function?
    plm_set_video_decode_callback(plm, videocallback, NULL);
    plm_set_audio_decode_callback(plm, audiocallback, NULL);

    // Set video to loop
    plm_set_loop(plm, true);

    // Get video's width and height
    // Assumes video isn't anamorphic
    videoWidth = plm_get_width(plm);
    videoHeight = plm_get_height(plm);

    // Width is required to be a multiple of 16
    // Height is rounded up to a multiple of 16
    videoBufferWidth = videoWidth;
    videoBufferHeight = ((videoHeight+15)/16)*16;

    // Create I420 texture
    //videoTexture = SDL_CreateTexture(context, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, videoWidth, videoHeight);

    // Create a buffer representing a I420 texture
    //videoTexture = (uchar *)malloc(videoBufferHeight*videoBufferWidth*12/8);

    // Create a buffer representing a RGB (24-bit) texture
    videoTexture = (uchar *)malloc(videoBufferHeight*videoBufferWidth*3);

    // Save the texture slot tnum
    videoTextureNum = tnum;

    // Get audio samplerate
    audioSamplerate = plm_get_samplerate(plm);

    // Create an SDL_AudioStream to hold the samples and perform samplerate conversion if needed
    {
        int mixfreq;
        Uint16 mixfmt;
        int mixchannels;
        Mix_QuerySpec(&mixfreq, &mixfmt, &mixchannels);

        // Audio from pl_mpeg is decoded to interleaved float32 by default
        resampledAudio = SDL_NewAudioStream(AUDIO_F32, 2, audioSamplerate, mixfmt, mixchannels, mixfreq);
    }

    // Get a texture video soundstream
    soundstreamNum = createsoundstream(resampledAudio);
    conoutf("loadvideo: Got soundstream channel %d", soundstreamNum);
    // FIXME: Error or "noaudio" state if can't get soundstream channel

    vtime = 0.0f;
    frameskip = 0;

    return true;
}

void closevideo()
{
    plm_destroy(plm);
    plm = NULL;

    //SDL_DestroyTexture(videoTexture);
    free(videoTexture);
    videoTexture = NULL;
    //videoTextureNum = 0;

    videoWidth = 0;
    videoHeight = 0;
    videoBufferWidth = 0;
    videoBufferHeight = 0;

    audioSamplerate = 0;

    haltsoundstream(soundstreamNum);
    soundstreamNum = -1;

    SDL_FreeAudioStream(resampledAudio);
    resampledAudio = NULL;

    vtime = 0.0f;
    frameskip = 0;
}
