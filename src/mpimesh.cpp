// Display mesh (experimental)
// Copyright (C) 2020-2023 Willy Deng
// zlib License

#include "cube.h"
#include "mpimesh.h"

VAR(mpiwireframe, 0, 0, 1);

// Sets the mpi to be displayed
VAR(mpimesh, 0, 0, 5);

struct mpilist_s
{
    int texID, type;
    unsigned int num_additional_views:1; // 0-1
    unsigned int layout:2; // 0-3
    unsigned int texface:1; // 0 = front; 1 = back
    float fov[2]; // fisheye: forward/rear; equirect: horizontal/vertical
};

vector<mpilist_s> mpilist;

void addmpi(int texID, int type, unsigned int views, unsigned int layout, unsigned int texface, float fov1, float fov2)
{
    struct mpilist_s m = { texID, type, views, layout, texface, fov1, fov2 };
    mpilist.add(m);
}

int mpigettype()
{
    const int len = mpilist.length();
    if(mpimesh >= len) return 0;
    return mpilist[mpimesh].type;
}

float mpigetfov()
{
    const int len = mpilist.length();
    if(mpimesh >= len) return 0;
    float fov = mpilist[mpimesh].fov[0];
    if(fov < 20.0f) fov = 180.0f; // detect the default value for this param (0.0f) and replace it with 180.0f; furthermore, don't support low fov.
    return fov;
}

unsigned int mpigettexface()
{
    const int len = mpilist.length();
    if(mpimesh >= len) return 0;
    return mpilist[mpimesh].texface;
}

unsigned int viewID = 0;
void mpicheckview()
{
    if(viewID > mpilist[mpimesh].num_additional_views) viewID = 0;
}
void mpinextview()
{
    ++viewID;
    mpicheckview();
}
COMMAND(mpinextview, ARG_NONE);


// Sets the scale of the mpi mesh
// Units are cubes.
// Approximately 4 cubes = 1 meter.
VAR(mpiscale, 1, 4, 100);


// texcoord, texface

void debugtexcoord(float s, float t)
{
    printf("s=%.5f, t=%.5f\n", s, t);
}

// Flip mesh vertices' x-coord according to mpi's texface setting.
// Call this function if needed before rendering the quadstrip.
void flip_mesh_geometry(vector<vec> &quadstrip)
{
    loopv(quadstrip) quadstrip[i].x *= -1;
}

// When reading a subpicture from a frame, use the opposite of the value specified by 'leftside' or 'tophalf'
VAR(mpiswapsubpict, 0, 0, 1);

void texcoord_subpicture_in_left_right(vec &q, bool leftside)
{
    if(mpiswapsubpict) leftside = !leftside;
    q.x *= 0.5f;
    if(!leftside) q.x += 0.5f;
}

void texcoord_subpicture_in_top_bottom(vec &q, bool tophalf)
{
    if(mpiswapsubpict) tophalf = !tophalf;
    q.y *= 0.5f;
    if(!tophalf) q.y += 0.5f;
}


// Image box mesh for cubemap
const int imagebox_cube_substrips = 1;
vector<vec> imagebox_cube_strips[2][imagebox_cube_substrips];

// Draw a box with height h = 2*r
// TODO: ... with location centered on camera and with orientation aligned with engine axes
// Note: By way of example, if imagebox_radius is 4, we draw an 8x8x8 box.
#define imagebox_radius ((float)(mpiscale))

// TODO: Change to OpenGL coordinate convention
// Front (+x) / Back (-x)
// Left (-y) / Right (+y)
// Up (+z) / Down (-z)

// Divide the EAC surface into equal squares.
// A square in the EAC surface is transformed to a different size square or rectangle in perspective screen space.
// The transformed edge lengths (in perspective screen space) are saved into this vector.
vector<float> imagebox_eac_edge_lengths;

// We only need to subdivide faces into substrips when displaying EAC.
// Suppose imagebox_face_substrips = 64.
// We then have 6 faces * 64*64 quads/face = 24576 quads,
// which are rendered as 2*64 quad strips of 3*64 quads per strip,
// and the number of vertices per strip is 2*(192+1) = 386.
const int imagebox_face_substrips = 64;

// Calculate transformed edge lengths for EAC display mesh.
// Range should be [-1, 1].
void calc_imagebox_eac_edge_lengths()
{
    imagebox_eac_edge_lengths.setsize(0);
    const float halffov = 45.0f/360.0f*TAU;
    loopi(imagebox_face_substrips/2)
    {
        const int count = imagebox_face_substrips/2;
        float currentangle = (float)(count-i)/count*halffov; // loop backwards; in the final iteration, count-i = 1.
        float edgelen = -tanf(currentangle);
        imagebox_eac_edge_lengths.add(edgelen);
    }
    imagebox_eac_edge_lengths.add(0.0f);
    loopi(imagebox_face_substrips/2)
    {
        const int count = imagebox_face_substrips/2;
        int index = (count-i)-1;  // loop backwards; in the final iteration, count-i = 1, and index = 0.
        float edgelen = -imagebox_eac_edge_lengths[index];
        imagebox_eac_edge_lengths.add(edgelen);
    }
}

vector<vec> imagebox_eac_strips[2][imagebox_face_substrips];
void create_imagebox_eac_strips_vertices()
{
    // reset storage
    loopi(imagebox_face_substrips) imagebox_eac_strips[0][i].setsize(0);
    loopi(imagebox_face_substrips) imagebox_eac_strips[1][i].setsize(0);

    // calculate edge lengths
    calc_imagebox_eac_edge_lengths();

    loopi(imagebox_face_substrips)
    {
        // create strips. from -z to +z
        vec v;
        const float r = imagebox_radius;
        // left face
        v.x = -r; v.y = -r; v.z = r*imagebox_eac_edge_lengths[i];   imagebox_eac_strips[0][i].add(v);
        v.x = -r; v.y = -r; v.z = r*imagebox_eac_edge_lengths[i+1]; imagebox_eac_strips[0][i].add(v);
        loopj(imagebox_face_substrips)
        {
            v.x =  r*(float)imagebox_eac_edge_lengths[j+1]; v.y = -r; v.z = r*imagebox_eac_edge_lengths[i];   imagebox_eac_strips[0][i].add(v);
            v.x =  r*(float)imagebox_eac_edge_lengths[j+1]; v.y = -r; v.z = r*imagebox_eac_edge_lengths[i+1]; imagebox_eac_strips[0][i].add(v);
        }
        // center face
        loopj(imagebox_face_substrips)
        {
            v.x =  r; v.y =  r*(float)imagebox_eac_edge_lengths[j+1]; v.z = r*imagebox_eac_edge_lengths[i];   imagebox_eac_strips[0][i].add(v);
            v.x =  r; v.y =  r*(float)imagebox_eac_edge_lengths[j+1]; v.z = r*imagebox_eac_edge_lengths[i+1]; imagebox_eac_strips[0][i].add(v);
        }
        // right face
        loopj(imagebox_face_substrips)
        {
            v.x = -r*(float)imagebox_eac_edge_lengths[j+1]; v.y =  r; v.z = r*imagebox_eac_edge_lengths[i];   imagebox_eac_strips[0][i].add(v);
            v.x = -r*(float)imagebox_eac_edge_lengths[j+1]; v.y =  r; v.z = r*imagebox_eac_edge_lengths[i+1]; imagebox_eac_strips[0][i].add(v);
        }
    }
    loopi(imagebox_face_substrips)
    {
        // create strips. from -z to +z
        vec v;
        const float r = imagebox_radius;
        // top face, new strip
        v.x =  r; v.y = r*-imagebox_eac_edge_lengths[i];   v.z =  r; imagebox_eac_strips[1][i].add(v);
        v.x =  r; v.y = r*-imagebox_eac_edge_lengths[i+1]; v.z =  r; imagebox_eac_strips[1][i].add(v);
        loopj(imagebox_face_substrips)
        {
            v.x = -r*(float)imagebox_eac_edge_lengths[j+1]; v.y = r*-imagebox_eac_edge_lengths[i];   v.z =  r; imagebox_eac_strips[1][i].add(v);
            v.x = -r*(float)imagebox_eac_edge_lengths[j+1]; v.y = r*-imagebox_eac_edge_lengths[i+1]; v.z =  r; imagebox_eac_strips[1][i].add(v);
        }
        // back face
        loopj(imagebox_face_substrips)
        {
            v.x = -r; v.y = r*-imagebox_eac_edge_lengths[i];   v.z = -r*(float)imagebox_eac_edge_lengths[j+1]; imagebox_eac_strips[1][i].add(v);
            v.x = -r; v.y = r*-imagebox_eac_edge_lengths[i+1]; v.z = -r*(float)imagebox_eac_edge_lengths[j+1]; imagebox_eac_strips[1][i].add(v);
        }
        // bottom face
        loopj(imagebox_face_substrips)
        {
            v.x =  r*(float)imagebox_eac_edge_lengths[j+1]; v.y = r*-imagebox_eac_edge_lengths[i];   v.z = -r; imagebox_eac_strips[1][i].add(v);
            v.x =  r*(float)imagebox_eac_edge_lengths[j+1]; v.y = r*-imagebox_eac_edge_lengths[i+1]; v.z = -r; imagebox_eac_strips[1][i].add(v);
        }
    }
}

void imagebox_texcoord(vec &q, int i, int region, int substrip, int total_substrips)
{
    float texcoordoffset = ((int)(i/2))/(float)total_substrips/3.0f; // Note: (i/2) needs to use integer math so it is rounded down.
    if(region == 0)
    {
        if(i%2 == 0)      { q.x = 0.5f*(total_substrips  -substrip)/(float)total_substrips; q.y=1-texcoordoffset; }
        else if(i%2 == 1) { q.x = 0.5f*(total_substrips-1-substrip)/(float)total_substrips; q.y=1-texcoordoffset; }
    }
    else
    {
        if(i%2 == 0)      { q.x = 0.5f+0.5f*(substrip  )/(float)total_substrips; q.y=texcoordoffset; }
        else if(i%2 == 1) { q.x = 0.5f+0.5f*(substrip+1)/(float)total_substrips; q.y=texcoordoffset; }
    }
}

void mpiwireframemode(bool on)
{
    if(on)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glColor3f(1.0f, 1.0f, 1.0f);
        glLineWidth(2.0f);
        glDisable(GL_TEXTURE_2D);
        glDepthMask(GL_FALSE);
    }
    else // turn off
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_TEXTURE_2D);
        glDepthMask(GL_TRUE);
    }
}

void render_imagebox_strip(vector<vec> &quadstrip, int region, int substrip, int total_substrips, bool wireframe = false)
{
    // only flip once if needed, and only flip if drawing filled quadstrips, which are always drawn before drawing the wireframe
    if(!wireframe)
    {
        if(mpilist[mpimesh].texface) flip_mesh_geometry(quadstrip);
    }

    glBindTexture(GL_TEXTURE_2D, mpilist[mpimesh].texID);
    glDisable(GL_CULL_FACE);
    if(wireframe)
    {
        mpiwireframemode(true);
    }
    glBegin(GL_QUAD_STRIP);
    const int viewoffset = (mpilist[mpimesh].num_additional_views > 0) ? viewID : 0;
    const int len = quadstrip.length();
    if(region == 0) loopi(len)
    {
        vec q(0);
        imagebox_texcoord(q, i, region, substrip, total_substrips);
        texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        glTexCoord2f(q.x, q.y);
        glVertex3f(quadstrip[i].x, quadstrip[i].y, quadstrip[i].z);
    }
    else loopi(len)
    {
        vec q(0);
        imagebox_texcoord(q, i, region, substrip, total_substrips);
        texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        glTexCoord2f(q.x, q.y);
        glVertex3f(quadstrip[i].x, quadstrip[i].y, quadstrip[i].z);
    }
    glEnd();
    glEnable(GL_CULL_FACE);

    if(wireframe) // if we have drawn the wireframe, return here instead of checking whether to draw it again
    {
        mpiwireframemode(false);
        return;
    }
    else // call the function with wireframe set to true
        if(mpiwireframe) render_imagebox_strip(quadstrip, region, substrip, total_substrips, true);
}

void render_imagebox_eac()
{
    loopi(imagebox_face_substrips) render_imagebox_strip(imagebox_eac_strips[0][i], 0, i, imagebox_face_substrips);
    loopi(imagebox_face_substrips) render_imagebox_strip(imagebox_eac_strips[1][i], 1, i, imagebox_face_substrips);
}

VAR(mpieacsinglestrip, 0, 0, 1);

// Create box vertices
// 1st section starts from left edge of left face and runs to right edge of right face.
// 2nd section starts from front edge of top face and runs to front edge of back face.
// Notes:
// Q: Should a quad strip contain parts of multiple cube faces?
// A: It is OK because there is a direct relation to the storage format of the source image.
void create_imagebox_vertices()
{
    vector<vec> verts;
    vec v;
    const float r = imagebox_radius;
    // left face
    v.x = -r; v.y = -r; v.z = -r; verts.add(v);
    v.x = -r; v.y = -r; v.z =  r; verts.add(v);
    v.x =  r; v.y = -r; v.z = -r; verts.add(v);
    v.x =  r; v.y = -r; v.z =  r; verts.add(v);
    // center face
    v.x =  r; v.y =  r; v.z = -r; verts.add(v);
    v.x =  r; v.y =  r; v.z =  r; verts.add(v);
    // right face
    v.x = -r; v.y =  r; v.z = -r; verts.add(v);
    v.x = -r; v.y =  r; v.z =  r; verts.add(v);
    // reset imagebox_cube
    imagebox_cube_strips[0][0].setsize(0);
    imagebox_cube_strips[1][0].setsize(0);
    // copy to imagebox_cube_strips region 0
    int len = verts.length();
    loopi(len) imagebox_cube_strips[0][0].add(verts[i]);
    verts.setsize(0);
    // top face, new strip
    v.x =  r; v.y =  r; v.z =  r; verts.add(v);
    v.x =  r; v.y = -r; v.z =  r; verts.add(v);
    v.x = -r; v.y =  r; v.z =  r; verts.add(v);
    v.x = -r; v.y = -r; v.z =  r; verts.add(v);
    // back face
    v.x = -r; v.y =  r; v.z = -r; verts.add(v);
    v.x = -r; v.y = -r; v.z = -r; verts.add(v);
    // bottom face
    v.x =  r; v.y =  r; v.z = -r; verts.add(v);
    v.x =  r; v.y = -r; v.z = -r; verts.add(v);
    // copy to imagebox_cube_strips region 1
    len = verts.length();
    loopi(len) imagebox_cube_strips[1][0].add(verts[i]);
}

void render_imagebox()
{
    render_imagebox_strip(imagebox_cube_strips[0][0], 0, 0, 1);
    render_imagebox_strip(imagebox_cube_strips[1][0], 1, 0, 1);
}


// Image cylinder mesh for cylindrical and equirectangular
const int imagecyl_columns = 128;
const int imagecyl_column_stacks = imagecyl_columns/4;
const int imagecyl_columncap_stacks = imagecyl_columns/8;

vector<vec> imagecyl_body[imagecyl_column_stacks];
vector<vec> imagecyl_topcap[imagecyl_columncap_stacks];
vector<vec> imagecyl_bottomcap[imagecyl_columncap_stacks];

#define imagecyl_radius ((float)(mpiscale))

// Draw a cylinder with radius r and height 2*r
// Body of cylinder starts from back edge representing left edge of source texture and runs to back edge representing right edge of source texture.
// Note: By way of example, if imagecyl_radius is 4, we draw a cylinder wirh radius 4 and height 8.
void create_imagecyl_vertices_quadstrip(vector<vec> &quadstrip, float bottom, float top)
{
    vector<vec> verts;
    vec v;
    const float r = imagecyl_radius;
    // back left edge
    v.x = -r; v.y =  0; v.z =  r*bottom; verts.add(v);
    v.x = -r; v.y =  0; v.z =  r*top; verts.add(v);
    // loopi(imagecyl_columns)
    // Note: But we actually loop imagecyl_columns-1 times because we unroll the last iteration
    // of the loop since the vertices for the right and left edges are the identical
    loopi(imagecyl_columns-1)
    {
        float x = -cosf((i+1)/(float)imagecyl_columns*TAU);
        float y = -sinf((i+1)/(float)imagecyl_columns*TAU);
        v.x =  r*x; v.y = r*y; v.z =  r*bottom; verts.add(v);
        v.x =  r*x; v.y = r*y; v.z =  r*top; verts.add(v);
    }
    // back right edge === back left edge
    v.x = -r; v.y =  0; v.z =  r*bottom; verts.add(v);
    v.x = -r; v.y =  0; v.z =  r*top; verts.add(v);

    const int len = verts.length();
    quadstrip.setsize(0);
    loopi(len) quadstrip.add(verts[i]);
}

// Instead of a triangle fan, we continue to use quad strips.
void create_imagecyl_cap_vertices_quadstrip(vector<vec> &quadstrip, float bpos, float tpos, bool top = true)
{
    vector<vec> verts;
    vec v;
    const float r = imagecyl_radius;

    // certain repeated points
    //const vec top_center = { 0, 0, r };
    //const vec bottom_center = { 0, 0, -r };

    if(top)
    {
        // back left edge, top cap
        //v.x = -r; v.y =  0; v.z =  r; verts.add(v);
        //verts.add(top_center);
        v.x =  r*bpos; v.y =  0; v.z =  r; verts.add(v);
        v.x =  r*tpos; v.y =  0; v.z =  r; verts.add(v);
    }
    else
    {
        // back left edge, bottom cap
        //verts.add(bottom_center);
        //v.x = -r; v.y =  0; v.z = -r; verts.add(v);
        v.x = -r*bpos; v.y =  0; v.z = -r; verts.add(v);
        v.x = -r*tpos; v.y =  0; v.z = -r; verts.add(v);
    }

    // loopi(imagecyl_columns)
    // Note: But we actually loop imagecyl_columns-1 times because we unroll the last iteration
    // of the loop since the vertices for the right and left edges are the identical
    loopi(imagecyl_columns-1)
    {
        float x = -cosf((i+1)/(float)imagecyl_columns*TAU);
        float y = -sinf((i+1)/(float)imagecyl_columns*TAU);
        if(top)
        {
            v.x =  r*x*-bpos; v.y = r*y*-bpos; v.z = r; verts.add(v);
            v.x =  r*x*-tpos; v.y = r*y*-tpos; v.z = r; verts.add(v);
        }
        else
        {
            v.x =  r*x*bpos; v.y = r*y*bpos; v.z = -r; verts.add(v);
            v.x =  r*x*tpos; v.y = r*y*tpos; v.z = -r; verts.add(v);
        }
    }
    // back right edge === back left edge
    if(top)
    {
        // back left edge, top cap
        //v.x = -r; v.y =  0; v.z =  r; verts.add(v);
        //verts.add(top_center);
        v.x =  r*bpos; v.y =  0; v.z =  r; verts.add(v);
        v.x =  r*tpos; v.y =  0; v.z =  r; verts.add(v);
    }
    else
    {
        // back left edge, bottom cap
        //verts.add(bottom_center);
        //v.x = -r; v.y =  0; v.z = -r; verts.add(v);
        v.x = -r*bpos; v.y =  0; v.z = -r; verts.add(v);
        v.x = -r*tpos; v.y =  0; v.z = -r; verts.add(v);
    }

    const int len = verts.length();
    quadstrip.setsize(0);
    loopi(len) quadstrip.add(verts[i]);
};

// Pre-req: imagecyl_columns should be a multiple of 4
void slice_imagecyl_hemi(vector<vec> &imagecyl_vertices)
{
    const int len = imagecyl_vertices.length();
    const int quarter = imagecyl_columns/4;
    vector<vec> slice;

    loopi(len)
    {
        if(i < quarter*2)
            continue;
        else if(i < 2 + 3*quarter*2)
            slice.add(imagecyl_vertices[i]);
        else
            break;
    }

    const int len2 = slice.length();
    imagecyl_vertices.setsize(0);
    loopi(len2) imagecyl_vertices.add(slice[i]);
}


// Use UV-sphere instead of imagecyl
VARP(mpiuvsphere, 0, 1, 1);

// Convert imagecyl into UV-sphere
void convert_imagecyl_to_uvsphere(vector<vec> &imagecyl_vertices)
{
    const int len = imagecyl_vertices.length();

    loopi(len)
    {
        imagecyl_vertices[i].normalize();
        imagecyl_vertices[i].mul(mpiscale);
    }
}

// Calculate transformed edge lengths for imagecyl sections.
// It's the same calculation as for EAC edge lengths, but with different numbers of divisions.
// Range should be [-1, 1].
// This calculation is for imagecyl_body, but imagecyl_cap can use the 1st or 2nd half of the vertices, so we don't have to recalculate.
vector<float> imagecyl_edge_lengths;
void calc_imagecyl_edge_lengths(float angle, int divisions)
{
    imagecyl_edge_lengths.setsize(0);
    const float halffov = angle/2.0f/360.0f*TAU;
    loopi(divisions/2)
    {
        const int count = divisions/2;
        float currentangle = (float)(count-i)/count*halffov; // loop backwards; in the final iteration, count-i = 1.
        float edgelen = -tanf(currentangle);
        imagecyl_edge_lengths.add(edgelen);
    }
    imagecyl_edge_lengths.add(0.0f);
    loopi(divisions/2)
    {
        const int count = divisions/2;
        int index = (count-i)-1;  // loop backwards; in the final iteration, count-i = 1, and index = 0.
        float edgelen = -imagecyl_edge_lengths[index];
        imagecyl_edge_lengths.add(edgelen);
    }
}

void create_imagecyl_vertices()
{
    calc_imagecyl_edge_lengths(90.0f, imagecyl_column_stacks);
    loopi(imagecyl_column_stacks)
    {
        create_imagecyl_vertices_quadstrip(imagecyl_body[i], imagecyl_edge_lengths[i], imagecyl_edge_lengths[i+1]);
    }
    loopi(imagecyl_columncap_stacks)
    {
        int offset = imagecyl_columncap_stacks;
        create_imagecyl_cap_vertices_quadstrip(imagecyl_topcap[i], imagecyl_edge_lengths[i], imagecyl_edge_lengths[i+1], true);
        create_imagecyl_cap_vertices_quadstrip(imagecyl_bottomcap[i], imagecyl_edge_lengths[i+offset], imagecyl_edge_lengths[i+1+offset], false);
    }
}

void render_imagecyl(bool wireframe)
{
    glBindTexture(GL_TEXTURE_2D, mpilist[mpimesh].texID);

    // 180-LR images: Using the example of 128 columns, columns 0-31 are the back-left
    // semisphere, 32-95 are the front hemisphere, and 96-127 are the back-right semisphere.
    bool hemisphere = mpigetfov() == 180.0f ? true : false;
    if(!wireframe)
    {
        if(hemisphere)
        {
            loopi(imagecyl_column_stacks) slice_imagecyl_hemi(imagecyl_body[i]);
            loopi(imagecyl_columncap_stacks) slice_imagecyl_hemi(imagecyl_topcap[i]);
            loopi(imagecyl_columncap_stacks) slice_imagecyl_hemi(imagecyl_bottomcap[i]);
        }
    }

    if(!wireframe)
    {
        if(mpiuvsphere)
        {
            loopi(imagecyl_column_stacks) convert_imagecyl_to_uvsphere(imagecyl_body[i]);
            loopi(imagecyl_columncap_stacks) convert_imagecyl_to_uvsphere(imagecyl_topcap[i]);
            loopi(imagecyl_columncap_stacks) convert_imagecyl_to_uvsphere(imagecyl_bottomcap[i]);
        }
    }

    // imagecyl body quad strips cover a vertical FOV of -45 degrees to +45 degrees.
    // This corresponds to the interval [0.25f, 0.75f].
    // These are "Cube" format tex coords. (0 = top of texture, 1 = bottom of texture)
    const float imagecyl_body_tex_bottom = 0.75f;
    const float imagecyl_body_tex_top = 0.25f;
    const int viewoffset = (mpilist[mpimesh].num_additional_views > 0) ? viewID : 0;

    loopj(imagecyl_column_stacks)
    {
    if(!wireframe && mpilist[mpimesh].texface) flip_mesh_geometry(imagecyl_body[j]);
    glDisable(GL_CULL_FACE);
    if(wireframe)
    {
        mpiwireframemode(true);
    }
    glBegin(GL_QUAD_STRIP);
    int len = imagecyl_body[j].length();
    loopi(len)
    {
        // "column" is the counter indicating which column's vertices are about to be specified.
        // For glTexCoord2f, note that two of the 4 vertices making up this quad in the quad strip
        // are specified in the previous loop iteration.
        int column = (i < 4) ? 0 : 1 + (i-4)/2;
        if(hemisphere) column += imagecyl_columns/4;
        float texsegmentlength = 0.5f;

        // calc texcoord
        vec q(0);
        if(i%2==0)
        {
            q.x = (i/2)/(float)(hemisphere?imagecyl_columns/2:imagecyl_columns);
            q.y = imagecyl_body_tex_bottom-(float)j/imagecyl_column_stacks*texsegmentlength;
        }
        else
        {
            q.x = (i/2)/(float)(hemisphere?imagecyl_columns/2:imagecyl_columns);
            q.y = imagecyl_body_tex_bottom-(float)(j+1)/imagecyl_column_stacks*texsegmentlength;
        }

        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);

        glTexCoord2f(q.x, q.y);
        glVertex3f(imagecyl_body[j][i].x, imagecyl_body[j][i].y, imagecyl_body[j][i].z);
    }
    glEnd();
    glEnable(GL_CULL_FACE);
    if(wireframe)
        mpiwireframemode(false);
    } // loopj

    // do it again two more times for the top and bottom cyl cap.
    loopj(imagecyl_columncap_stacks)
    {
    if(!wireframe && mpilist[mpimesh].texface) flip_mesh_geometry(imagecyl_topcap[j]);
    glDisable(GL_CULL_FACE);
    if(wireframe)
    {
        mpiwireframemode(true);
    }
    glBegin(GL_QUAD_STRIP);
    int len = imagecyl_topcap[j].length();
    loopi(len)
    {
        // "column" is the counter indicating which column's vertices are about to be specified.
        // For glTexCoord2f, note that two of the 4 vertices making up this quad in the quad strip
        // are specified in the previous loop iteration.
        int column = (i < 4) ? 0 : 1 + (i-4)/2;
        if(hemisphere) column += imagecyl_columns/4;
        float texsegmentlength = 0.25f;

        // calc texcoord
        vec q(0);
        if(i%2==0)
        {
            q.x = (i/2)/(float)(hemisphere?imagecyl_columns/2:imagecyl_columns);
            q.y = imagecyl_body_tex_top-(float)j/imagecyl_columncap_stacks*texsegmentlength;
        }
        else
        {
            q.x = (i/2)/(float)(hemisphere?imagecyl_columns/2:imagecyl_columns);
            q.y = imagecyl_body_tex_top-(float)(j+1)/imagecyl_columncap_stacks*texsegmentlength;
        }
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);

        glTexCoord2f(q.x, q.y);
        glVertex3f(imagecyl_topcap[j][i].x, imagecyl_topcap[j][i].y, imagecyl_topcap[j][i].z);
    }
    glEnd();
    glEnable(GL_CULL_FACE);
    if(wireframe)
        mpiwireframemode(false);
    } // loopj

    // almost done
    loopj(imagecyl_columncap_stacks)
    {
    if(!wireframe && mpilist[mpimesh].texface) flip_mesh_geometry(imagecyl_bottomcap[j]);
    glDisable(GL_CULL_FACE);
    if(wireframe)
    {
        mpiwireframemode(true);
    }
    glBegin(GL_QUAD_STRIP);
    int len = imagecyl_bottomcap[j].length();
    loopi(len)
    {
        // "column" is the counter indicating which column's vertices are about to be specified.
        // For glTexCoord2f, note that two of the 4 vertices making up this quad in the quad strip
        // are specified in the previous loop iteration.
        int column = (i < 4) ? 0 : 1 + (i-4)/2;
        if(hemisphere) column += imagecyl_columns/4;
        float texsegmentlength = 0.25f;

        // calc texcoord
        vec q(0);
        if(i%2==0)
        {
            q.x = (i/2)/(float)(hemisphere?imagecyl_columns/2:imagecyl_columns);
            q.y = 1-(float)j/imagecyl_columncap_stacks*texsegmentlength;
        }
        else
        {
            q.x = (i/2)/(float)(hemisphere?imagecyl_columns/2:imagecyl_columns);
            q.y = 1-(float)(j+1)/imagecyl_columncap_stacks*texsegmentlength;
        }
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);

        glTexCoord2f(q.x, q.y);
        glVertex3f(imagecyl_bottomcap[j][i].x, imagecyl_bottomcap[j][i].y, imagecyl_bottomcap[j][i].z);
    }
    glEnd();
    glEnable(GL_CULL_FACE);
    if(wireframe)
        mpiwireframemode(false);
    } // loopj

    if(wireframe) // if we have drawn the wireframe, return here instead of checking whether to draw it again
    {
        return;
    }
    else // call the function with wireframe set to true
        if(mpiwireframe) render_imagecyl(true);
}

// HEALPix
// https://en.wikipedia.org/wiki/HEALPix

// Quad sphere
// https://en.wikipedia.org/wiki/Quadrilateralized_spherical_cube

// S2
// http://s2geometry.io/
// https://proj.org/operations/projections/s2.html

// Icosphere
// https://space.mit.edu/home/tegmark/icosahedron.html

// Fisheye

VARF(mpifishfov, 90, 180, 270, if(mpilist[mpimesh].type != MPI_FISHEYE) return; mpilist[mpimesh].fov[0] = mpifishfov);

// The strategy to display the fisheye image is to map it onto a "hierarchical triangular mesh."
// HTM is described in "Indexing the Sphere with the Hierarchical Triangular Mesh" by Szalay, Gray, Fekete, Kunszt, Kukol, Thakar (MSR-TR-2005-123).

// Enumerate six vertices of the octahedron
vec htmv0[6] =
{
    vec(  0,  0,  1 ),
    vec(  1,  0,  0 ),
    vec(  0,  1,  0 ),
    vec( -1,  0,  0 ),
    vec(  0, -1,  0 ),
    vec(  0,  0, -1 )
};

// Enumerate eight faces of the octahedron
// Faces are numbered in counter clockwise order when viewed from center of octahedron and looking in the direction of the hemisphere
int htmf0[8][3] =
{
    { 1, 5, 2 }, // S0
    { 1, 0, 4 }, // N0
    { 2, 5, 3 }, // S1
    { 4, 0, 3 }, // N1
    { 3, 5, 4 }, // S2
    { 3, 0, 2 }, // N2
    { 4, 5, 1 }, // S3
    { 2, 0, 1 }  // N3
};

typedef struct htmnode_s HTMNode;
struct htmnode_s
{
    HTMNode *children[4];
    vec verts[3];
};

vector<HTMNode> htmnodes;

HTMNode htmroot[8];

void reset_htmnodes()
{
    loopi(8) loopj(4) htmroot[i].children[j] = NULL;
    htmnodes.setsize(0); // no shrink!! only grow
    htmnodes.reserve(4096+2048); // also, only grow once... don't allow vector to "realloc" its memory: it will break the quadtree!
}

void subdivide_htmface(HTMNode *p)
{
    loopi(4)
    {
        p->children[i] = &htmnodes.add();
        loopj(4) p->children[i]->children[j] = NULL;
    }

    vec w[3];
    { vec temp = p->verts[1]; vadd(temp, p->verts[2]); float mag = sqrt(dotprod(temp, temp)); vdiv(temp, mag); w[0] = temp; }
    { vec temp = p->verts[0]; vadd(temp, p->verts[2]); float mag = sqrt(dotprod(temp, temp)); vdiv(temp, mag); w[1] = temp; }
    { vec temp = p->verts[0]; vadd(temp, p->verts[1]); float mag = sqrt(dotprod(temp, temp)); vdiv(temp, mag); w[2] = temp; }

    p->children[0]->verts[0] = p->verts[0];
    p->children[0]->verts[1] = w[2];
    p->children[0]->verts[2] = w[1];

    p->children[1]->verts[0] = p->verts[1];
    p->children[1]->verts[1] = w[0];
    p->children[1]->verts[2] = w[2];

    p->children[2]->verts[0] = p->verts[2];
    p->children[2]->verts[1] = w[1];
    p->children[2]->verts[2] = w[0];

    p->children[3]->verts[0] = w[0];
    p->children[3]->verts[1] = w[1];
    p->children[3]->verts[2] = w[2];
}

// We can't flip the HTM because we calculate the tex coord based on the vertex's position.
// Therefore, do the flip in the tex coord calculation.
void calc_imagehtm_texcoord(vec &vert, vec &q, const unsigned int texface)
{
    float r = 90.0f;
    if(vert.x*vert.x+vert.y*vert.y > 0) r = fabs(atanf(vert.z/sqrt(vert.x*vert.x+vert.y*vert.y)))/TAU*360.0f;
    r /= 90.0f;
    r = 1-r;
    r *= 180.0f/mpifishfov;

    q = vert;
    q.z = 0;
    float norm = sqrt(dotprod(q, q));
    if(norm > 0) vdiv(q, norm);
    vmul(q, r);

    if(texface) // flip q.x
        q.x *= -1;

    q.x = (q.x*0.5f+0.5f);
    q.y = (-q.y*0.5f+0.5f);
}

void render_imagehtm_face(int face, bool wireframe = false)
{
    glBindTexture(GL_TEXTURE_2D, mpilist[mpimesh].texID);
    glDisable(GL_CULL_FACE);
    if(wireframe)
    {
        mpiwireframemode(true);
    }
    glBegin(GL_TRIANGLES);

    const int viewoffset = (mpilist[mpimesh].num_additional_views > 0) ? viewID : 0;
    vec q(0);

    calc_imagehtm_texcoord(htmv0[htmf0[face][0]], q, mpilist[mpimesh].texface);
    if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
    if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);
    glTexCoord2f(q.x, q.y);
    glVertex3f(htmv0[htmf0[face][0]].x*mpiscale, htmv0[htmf0[face][0]].y*mpiscale, htmv0[htmf0[face][0]].z*mpiscale);

    calc_imagehtm_texcoord(htmv0[htmf0[face][1]], q, mpilist[mpimesh].texface);
    if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
    if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);
    glTexCoord2f(q.x, q.y);
    glVertex3f(htmv0[htmf0[face][1]].x*mpiscale, htmv0[htmf0[face][1]].y*mpiscale, htmv0[htmf0[face][1]].z*mpiscale);

    calc_imagehtm_texcoord(htmv0[htmf0[face][2]], q, mpilist[mpimesh].texface);
    if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
    if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);
    glTexCoord2f(q.x, q.y);
    glVertex3f(htmv0[htmf0[face][2]].x*mpiscale, htmv0[htmf0[face][2]].y*mpiscale, htmv0[htmf0[face][2]].z*mpiscale);

    glEnd();
    glEnable(GL_CULL_FACE);

    if(wireframe) // if we have drawn the wireframe, return here instead of checking whether to draw it again
    {
        mpiwireframemode(false);
        return;
    }
    else // call the function with wireframe set to true
        if(mpiwireframe) render_imagehtm_face(face, true);
}

int htmnodes_slice_first = 0;
//int htm_subdivs_texcoord = 1;
void render_imagehtm_subdivs(bool wireframe = false)
{
    glBindTexture(GL_TEXTURE_2D, mpilist[mpimesh].texID);
    glDisable(GL_CULL_FACE);
    if(wireframe)
    {
        mpiwireframemode(true);
    }
    glBegin(GL_TRIANGLES);

    const int viewoffset = (mpilist[mpimesh].num_additional_views > 0) ? viewID : 0;
    const int len = htmnodes.length();
    loopi(len)
    {
        if(i < htmnodes_slice_first) continue;
        vec q(0);
        calc_imagehtm_texcoord(htmnodes[i].verts[0], q, mpilist[mpimesh].texface);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);
        glTexCoord2f(q.x, q.y);
        //if(htm_subdivs_texcoord) debugtexcoord(q.x, q.y);
        glVertex3f(htmnodes[i].verts[0].x*mpiscale, htmnodes[i].verts[0].y*mpiscale, htmnodes[i].verts[0].z*mpiscale);
        calc_imagehtm_texcoord(htmnodes[i].verts[1], q, mpilist[mpimesh].texface);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);
        glTexCoord2f(q.x, q.y);
        //if(htm_subdivs_texcoord) debugtexcoord(q.x, q.y);
        glVertex3f(htmnodes[i].verts[1].x*mpiscale, htmnodes[i].verts[1].y*mpiscale, htmnodes[i].verts[1].z*mpiscale);
        calc_imagehtm_texcoord(htmnodes[i].verts[2], q, mpilist[mpimesh].texface);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);
        glTexCoord2f(q.x, q.y);
        //if(htm_subdivs_texcoord) debugtexcoord(q.x, q.y);
        glVertex3f(htmnodes[i].verts[2].x*mpiscale, htmnodes[i].verts[2].y*mpiscale, htmnodes[i].verts[2].z*mpiscale);
    }
    //htm_subdivs_texcoord = 0;

    glEnd();
    glEnable(GL_CULL_FACE);

    if(wireframe) // if we have drawn the wireframe, return here instead of checking whether to draw it again
    {
        mpiwireframemode(false);
        return;
    }
    else // call the function with wireframe set to true
        if(mpiwireframe) render_imagehtm_subdivs(true);
}

void render_imagehtm()
{
    extern int htmtri, allhtmtri;
    int face = htmtri;
    if(face == -1 && !allhtmtri) return;
    if(allhtmtri == 1)
    {
        // render using subdivided sphere
        if(true)
        {
            // reset root tris and list of subdivs
            reset_htmnodes();
            // create root tris
            htmroot[1].verts[0] = htmv0[htmf0[1][0]]; htmroot[1].verts[1] = htmv0[htmf0[1][1]]; htmroot[1].verts[2] = htmv0[htmf0[1][2]];
            htmroot[3].verts[0] = htmv0[htmf0[3][0]]; htmroot[3].verts[1] = htmv0[htmf0[3][1]]; htmroot[3].verts[2] = htmv0[htmf0[3][2]];
            htmroot[5].verts[0] = htmv0[htmf0[5][0]]; htmroot[5].verts[1] = htmv0[htmf0[5][1]]; htmroot[5].verts[2] = htmv0[htmf0[5][2]];
            htmroot[7].verts[0] = htmv0[htmf0[7][0]]; htmroot[7].verts[1] = htmv0[htmf0[7][1]]; htmroot[7].verts[2] = htmv0[htmf0[7][2]];
            // create subdivs: 4 -> 16
            subdivide_htmface(&htmroot[1]);
            subdivide_htmface(&htmroot[3]);
            subdivide_htmface(&htmroot[5]);
            subdivide_htmface(&htmroot[7]);
            // create another level of subdivs: 16 -> 64
            loopi(16) subdivide_htmface(&htmnodes[i]);
            // create another level of subdivs: 64 -> 256
            loopi(64) subdivide_htmface(&htmnodes[i+16]);
            // create another level of subdivs: 256 -> 1024
            loopi(256) subdivide_htmface(&htmnodes[i+16+64]);
            // create another level of subdivs: 1024 -> 4096
            loopi(1024) subdivide_htmface(&htmnodes[i+16+64+256]);
            htmnodes_slice_first = 16+64+256+1024;
        }

        const int len = htmnodes.length();
        //conoutf("number of htm subdivs: %d", len);
        if(len > 0) // render subdivs instead of root faces
        {
            render_imagehtm_subdivs();
        }
    }
    else
    {
        if(allhtmtri == 2 || allhtmtri == 4)
        {
            render_imagehtm_face(1);
            render_imagehtm_face(3);
            render_imagehtm_face(5);
            render_imagehtm_face(7);
        }
        if(allhtmtri == 3 || allhtmtri == 4)
        {
            render_imagehtm_face(0);
            render_imagehtm_face(2);
            render_imagehtm_face(4);
            render_imagehtm_face(6);
        }
        if(allhtmtri == 0)
            render_imagehtm_face(face);
    }
}

int htm_id_to_root_tri(const char *s)
{
    int face = -1;
    if(s[0] == 'S') face = 0;
    else if(s[0] == 'N') face = 1;
    else { conoutf("invalid name"); return -1; }

    if(s[1] == '0') face += 0;
    else if(s[1] == '1') face += 2;
    else if(s[1] == '2') face += 4;
    else if(s[1] == '3') face += 6;
    else { conoutf("invalid name"); return -1; }

    return face;
}

int htmtri = -1;
extern int allhtmtri;

void showhtmtri(const char *s)
{
    int face = htm_id_to_root_tri(s);
    htmtri = face;
    allhtmtri = 0;
}

void nexthtmtri()
{
    htmtri += 1;
    if(htmtri >= 8) htmtri -= 8;
    allhtmtri = 0;
}

COMMAND(showhtmtri, ARG_1STR);
COMMAND(nexthtmtri, ARG_NONE);
// allhtmtri:
// 0 = single base triangle
// 1 = all triangles using HTM subdiv mesh
// 2 = hemisphere of 4 base triangles
// 3 = opposite hemisphere of 4 base triangles
// 4 = all base triangles
VAR(allhtmtri, 0, 1, 4);

// Additional skirt for fisheye "hemispheres" with greater than 180 fov.
// The strategy is to create a cylindrical quad strip extending from the hemisphere edge.
// If the number of edges and the vertex positions don't match, this will cause a gap in the display mesh.

vector<vec>fisheye_hemisphere_skirt;

// Draw a cylinder with radius "r=mpiscale" and height "h=r*tan(additionalFOV/2)"
// Body of cylinder starts from below the dome horizon and runs to the dome horizon.
void create_fisheye_hemisphere_skirt_vertices()
{
    float additionalFOV = mpifishfov - 180.0f;
    if(additionalFOV <= 0) return;

    vector<vec> verts;
    vec v;
    const float r = (float)mpiscale;
    const float h = r*tanf(additionalFOV/2.0f/360.0f*TAU);

    // back left edge
    v.x = -r; v.y =  0; v.z = -h; verts.add(v);
    v.x = -r; v.y =  0; v.z =  0; verts.add(v);
    // loopi(imagecyl_columns)
    // Note: But we actually loop imagecyl_columns-1 times because we unroll the last iteration
    // of the loop since the vertices for the right and left edges are the identical
    loopi(imagecyl_columns-1)
    {
        float x = -cosf((i+1)/(float)imagecyl_columns*TAU);
        float y = -sinf((i+1)/(float)imagecyl_columns*TAU);
        v.x =  r*x; v.y = r*y; v.z = -h; verts.add(v);
        v.x =  r*x; v.y = r*y; v.z =  0; verts.add(v);
    }
    // back right edge === back left edge
    v.x = -r; v.y =  0; v.z = -h; verts.add(v);
    v.x = -r; v.y =  0; v.z =  0; verts.add(v);

    const int len = verts.length();
    fisheye_hemisphere_skirt.setsize(0);
    loopi(len) fisheye_hemisphere_skirt.add(verts[i]);
}

//int skirttexcoord = 1;
void render_fisheye_hemisphere_skirt(bool wireframe)
{
    float additionalFOV = mpifishfov - 180.0f;
    if(additionalFOV <= 0) return;

    if(!wireframe)
    {
        if(mpilist[mpimesh].texface) flip_mesh_geometry(fisheye_hemisphere_skirt);
    }

    // modified from render imagecyl...
    glBindTexture(GL_TEXTURE_2D, mpilist[mpimesh].texID);
    glDisable(GL_CULL_FACE);
    if(wireframe)
    {
        mpiwireframemode(true);
    }
    glBegin(GL_QUAD_STRIP);
    float s, t; // center is at s=(0.5f)/2, t=0.5f
    float fraction = 180.0f/mpifishfov; // the "bottom" edge of the skirt is 1.0x; the "top" edge of the skirt that joins to the hemisphere is a "fraction" of 1.0x.
    const int viewoffset = (mpilist[mpimesh].num_additional_views > 0) ? viewID : 0;
    int len = fisheye_hemisphere_skirt.length();
    loopi(len)
    {
        // "column" is the counter indicating which column's vertices are about to be specified.
        // For glTexCoord2f, note that two of the 4 vertices making up this quad in the quad strip
        // are specified in the previous loop iteration.
        int column = (i < 4) ? 0 : 1 + (i-4)/2;
        const float phase = 3*TAU/4.0f;
        vec q(0);
        if(i == 0)      { t = cosf(0+phase); s = sinf(0+phase); }
        else if(i == 1) { t = cosf(0+phase)*fraction; s = sinf(0+phase)*fraction; }
        else if(i%2==0) { t = cosf((column+1)/(float)imagecyl_columns*TAU+phase); s = sinf((column+1)/(float)imagecyl_columns*TAU+phase); }
        else            { t = cosf((column+1)/(float)imagecyl_columns*TAU+phase)*fraction; s = sinf((column+1)/(float)imagecyl_columns*TAU+phase)*fraction; }
        q.x = 0.5f+0.5f*s;
        q.y = 0.5f+0.5f*t;
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == LEFT_RIGHT) texcoord_subpicture_in_left_right(q, viewoffset==0 ? true : false);
        if(mpilist[mpimesh].num_additional_views && mpilist[mpimesh].layout == TOP_BOTTOM) texcoord_subpicture_in_top_bottom(q, viewoffset==0 ? true : false);
        glTexCoord2f(q.x, q.y);
        glVertex3f(fisheye_hemisphere_skirt[i].x, fisheye_hemisphere_skirt[i].y, fisheye_hemisphere_skirt[i].z);
        //if(skirttexcoord) debugtexcoord(s, t);
    }
    //skirttexcoord = 0;
    glEnd();
    glEnable(GL_CULL_FACE);

    if(wireframe) // if we have drawn the wireframe, return here instead of checking whether to draw it again
    {
        mpiwireframemode(false);
        return;
    }
    else // call the function with wireframe set to true
        if(mpiwireframe) render_fisheye_hemisphere_skirt(true);
}

// AFR stereo (experimental)
VAR(mpiafr, 0, 0, 1);

// IPD - see camera.cpp
#define IPD (0.25f) // 0.25 cubes * 0.25 meters/cube = 62.5 mm

void do_mpi_afr()
{
    if(!mpiafr) return;

    // limit afr rate to 100 msec = 10fps
    const int mpiafrdelay = 100;
    static Uint32 mpiafrlastmillis = 0;
    Uint32 currentmillis = SDL_GetTicks();
    if(currentmillis - mpiafrlastmillis >= mpiafrdelay)
    {
        mpiafrlastmillis = currentmillis;
        mpinextview();
    };

    float halfoffsetmag = IPD/2;
    vec mpimesh_offset(0, -1, 0);
    vec htm_offset(1, 0, 0);

    mpimesh_offset.mul(halfoffsetmag);
    htm_offset.mul(halfoffsetmag);

    vec offset(0);
    int type = mpigettype();
    if(type == MPI_FISHEYE)
        offset.add(htm_offset);
    else
        offset.add(mpimesh_offset);

    if(viewID == 0)
    {
        // translate left to render left eye's display mesh
        //offset.mul(0);
        glTranslatef(offset.x, offset.y, offset.z);
    }
    else
    {
        // translate right to render right eye's display mesh
        //offset.mul(2);
        offset.mul(-1);
        glTranslatef(offset.x, offset.y, offset.z);
    }
}

void rendermpimesh()
{
    if(mpilist.length() <= 0) return;

    mpicheckview();
    int mpitype = mpigettype();
    unsigned int mpitexface = mpigettexface();

    glRotated(-90.0, 1.0, 0.0, 0.0);
    if(mpitype == MPI_EAC && !mpieacsinglestrip) // EAC
    {
        do_mpi_afr();
        create_imagebox_eac_strips_vertices();
        render_imagebox_eac();
    }
    else if((mpitype == MPI_EAC && mpieacsinglestrip) || mpitype == MPI_CUBEMAP) // EAC (single-strip approx) or cubemap
    {
        do_mpi_afr();
        create_imagebox_vertices();
        render_imagebox();
    }
    else if(mpitype == MPI_EQUIRECT) // imagecyl/uvsphere equirect 180 or 360
    {
        do_mpi_afr();
        create_imagecyl_vertices();
        render_imagecyl();
    }
    else // fisheye
    {
        mpifishfov = (int)mpigetfov(); // update fisheye fov setting with the value set in mpilist
        glRotated(90.0, 1.0, 0.0, 0.0);
        glRotated(90.0, 0.0, 1.0, 0.0);
        if(mpitexface) glRotated(180.0, 0.0, 1.0, 0.0);
        do_mpi_afr();
        render_imagehtm();
        create_fisheye_hemisphere_skirt_vertices();
        render_fisheye_hemisphere_skirt();
    }
}
