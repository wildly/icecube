// console.cpp: the console buffer, its display, and command line control

#include "cube.h"
#include <ctype.h>

struct cline { char *cref; int outtime; int fmt_ver; };
vector<cline> conlines;

const int ndraw = 5;
const int WORDWRAP = 80;
int conskip = 0;

bool saycommandon = false;
string commandbuf;

void setconskip(int n)
{
    conskip += n;
    if(conskip<0) conskip = 0;
};

COMMANDN(conskip, setconskip, ARG_1INT);

void puts_stripped(cline &cl);
void conline(const char *sf, bool highlight, int fmt_ver = 0)   // add a line to the console buffer
{
    cline cl;
    cl.cref = conlines.length()>100 ? conlines.pop().cref : newstringbuf("");   // constrain the buffer size
    cl.outtime = lastmillis;                        // for how long to keep line on screen
    cl.fmt_ver = fmt_ver;
    conlines.insert(0,cl);
    if(highlight)                                   // show line in a different colour, for chat etc.
    {
        cl.cref[0] = '\f';
        cl.cref[1] = 0;
        concatstring(cl.cref, sf);
    }
    else
    {
        copystring(cl.cref, sf);
    };
    puts_stripped(cl);
};

// prints a copy of the cline that is stripped of '\f' (and subsequent color control code if fmt_ver is 1)
void puts_stripped(cline &cl)
{
    string s;
    s[0] = 0;
    bool parsingcolorcode = false;
    for(int i = 0, used = 0; used < MAXSTRLEN; i++)
    {
        if(cl.cref[i] == 0)    { s[used++] = cl.cref[i];                      break;    };
        if(parsingcolorcode)   { parsingcolorcode = false;                    continue; };
        if(cl.cref[i] == '\f') { if(cl.fmt_ver == 1) parsingcolorcode = true; continue; };
        if(i == (MAXSTRLEN-1)) s[used++] = 0;
        else s[used++] = cl.cref[i];
    };
    puts(s);
    #ifndef WIN32
        fflush(stdout);
    #endif
};

void conoutf(const char *s, ...)
{
    defvformatstring(sf, s, s);
    s = sf;
    int n = 0;
    while(strlen(s)>WORDWRAP)                       // cut strings to fit on screen
    {
        string t;
        copystring(t, s, WORDWRAP+1);
        conline(t, n++!=0);
        s += WORDWRAP;
    };
    conline(s, n!=0);
};

void conoutf(int type, const char *s, ...)
{
    defvformatstring(sf, s, s);
    s = sf;
    int n = 0;
    while(strlen(s)>WORDWRAP)                       // cut strings to fit on screen
    {
        string t;
        copystring(t, s, WORDWRAP+1);
        conline(t, n++!=0);
        s += WORDWRAP;
    };
    conline(s, n!=0);
};

void conouts(const char *s, int fmt_ver)
{
    conline(s, false, fmt_ver);
};

void renderconsole()                                // render buffer taking into account time & scrolling
{
    int nd = 0;
    cline *refs[ndraw];
    loopv(conlines) if(conskip ? i>=conskip-1 || i>=conlines.length()-ndraw : lastmillis-conlines[i].outtime<20000)
    {
        refs[nd++] = &conlines[i];
        if(nd==ndraw) break;
    };
    loopj(nd)
    {
        draw_text(refs[j]->cref, FONTH/3, (FONTH/4*5)*(nd-j-1)+FONTH/3, 2, refs[j]->fmt_ver);
    };
};

// keymap is defined externally in keymap.cfg

struct keym { int code; char *name; char *action; } keyms[256];
int numkm = 0;                                     

void keymap(char *code, char *key, char *action)
{
    keyms[numkm].code = atoi(code);
    keyms[numkm].name = newstring(key);
    keyms[numkm++].action = newstringbuf(action);
};

COMMAND(keymap, ARG_3STR);

void bindkey(char *key, char *action)
{
    for(char *x = key; *x; x++) *x = toupper(*x);
    loopi(numkm) if(strcmp(keyms[i].name, key)==0)
    {
        copystring(keyms[i].action, action);
        return;
    };
    conoutf("unknown key \"%s\"", key);   
};

COMMANDN(bind, bindkey, ARG_2STR);

void initconsole()
{
#if SDL_VERSION_ATLEAST(2, 0, 0)
    SDL_StopTextInput();
#endif
};

void saycommand(char *init)                         // turns input to the command line on or off
{
#if SDL_VERSION_ATLEAST(2, 0, 0)
    saycommandon = (init!=NULL);
#else
    SDL_EnableUNICODE(saycommandon = (init!=NULL));
#endif
    if(!editmode) keyrepeat(saycommandon);
    if(!init) init = "";
    copystring(commandbuf, init);
#if SDL_VERSION_ATLEAST(2, 0, 0)
    // SDL 2.0: text input API
    if(saycommandon)
        SDL_StartTextInput();
    else
        SDL_StopTextInput();
#endif
};

void mapmsg(char *s) { copystring(hdr.maptitle, s, 128); };

COMMAND(saycommand, ARG_VARI);
COMMAND(mapmsg, ARG_1STR);

// TODO: Need decodeutf8 function (in Tesseract's tools.h / tools.cpp)
void pasteconsole()
{
    if(!SDL_HasClipboardText()) return;
    char *cb = SDL_GetClipboardText();
    if(!cb) return;
    size_t cblen = strlen(cb),
           commandlen = strlen(commandbuf);
#if 0
           decoded = decodeutf8((uchar *)&commandbuf[commandlen], sizeof(commandbuf)-1-commandlen, (const uchar *)cb, cblen);
    commandbuf[commandlen + decoded] = '\0';
#else
    concatstring(commandbuf, cb);
#endif
    SDL_free(cb);
}

cvector vhistory;
int histpos = 0;

void history(int n)
{
    static bool rec = false;
    if(!rec && n>=0 && n<vhistory.length())
    {
        rec = true;
        execute(vhistory[vhistory.length()-n-1]);
        rec = false;
    };
};

COMMAND(history, ARG_1INT);

void textinput(const char* composition)
{
    if(saycommandon)
    {
        resetcomplete();
        concatstring(commandbuf, composition);
    };
};

// int  code:   is the id code of an SDLKey (scancode; was Uint8 in SDL 1.2; now Sint32 as of SDL 2.0)
// bool isdown: identifies the state of the key (SDL_PRESSED)
// bool isrepeat: identifies whether the keypress is a keyrepeat
// int  cooked: keycode (now keysym.sym; was keysym.unicode)
void keypress(int code, bool isdown, int cooked)
{
    keypress(code, isdown, false, cooked);
};
void keypress(int code, bool isdown, bool isrepeat, int cooked)
{
    if(saycommandon)                                // keystrokes go to commandline
    {
        if(isdown)
        {
            switch(code)
            {
                case SDLK_RETURN:
                    break;

                case SDLK_BACKSPACE:
                case SDLK_LEFT:
                {
                    for(int i = 0; commandbuf[i]; i++) if(!commandbuf[i+1]) commandbuf[i] = 0;
                    resetcomplete();
                    break;
                };
                    
                case SDLK_UP:
                    if(histpos) copystring(commandbuf, vhistory[--histpos]);
                    break;
                
                case SDLK_DOWN:
                    if(histpos<vhistory.length()) copystring(commandbuf, vhistory[histpos++]);
                    break;
                    
                case SDLK_TAB:
                    complete(commandbuf);
                    break;

                case SDLK_v:
                    if(SDL_GetModState()&(KMOD_LCTRL|KMOD_RCTRL)) { pasteconsole(); return; };
                    // else fall through to default

                default:
#if SDL_VERSION_ATLEAST(2, 0, 0)
                    // SDL 2.0: We use the text input API, so the following is no longer needed. This section is empty.
#else
                    // For SDL 1.x
                    resetcomplete();
                    if(cooked) { char add[] = { (char)cooked, 0 }; concatstring(commandbuf, add); };
#endif
                    break;
            };
        }
        else
        {
            if(code==SDLK_RETURN)
            {
                if(commandbuf[0])
                {
                    if(vhistory.empty() || strcmp(vhistory.last(), commandbuf))
                    {
                        vhistory.add(newstring(commandbuf));  // cap this?
                    };
                    histpos = vhistory.length();
                    if(commandbuf[0]=='/') execute(commandbuf, true);
                    else toserver(commandbuf);
                };
                saycommand(NULL);
            }
            else if(code==SDLK_ESCAPE)
            {
                saycommand(NULL);
            };
        };
    }
    else if(!isrepeat && !menukey(code, isdown))    // keystrokes go to menu
    {
        loopi(numkm) if(keyms[i].code==code)        // keystrokes go to game, lookup in keymap and execute
        {
            string temp;
            copystring(temp, keyms[i].action);
            execute(temp, isdown); 
            return;
        };
    };
};

char *getcurcommand()
{
    return saycommandon ? commandbuf : NULL;
};

void writebinds(FILE *f)
{
    loopi(numkm)
    {
        if(*keyms[i].action) fprintf(f, "bind \"%s\" [%s]\n", keyms[i].name, keyms[i].action);
    };
};