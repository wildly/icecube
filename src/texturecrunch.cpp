// Adapted from crunch/examples/example1.cpp and crunch/examples/example2.cpp
// https://github.com/BinomialLLC/crunch

// Unpacks CRN to DDS
// This combines a portion of the transcode and inspect functions of crunch/example1 with the crn_decomp example from example2.cpp so that this does NOT need to link crnlib.lib.

// Copyright (C) 2021 Willy Deng
// zlib License

// Changes for IceCube:
// I hacked together a buffer using malloc/memcpy so that the transcoded DDS is in memory instead of written out to disk.
// It uses references to pointers for allocating/setting ddsdata and ddsheader.
// TODO: The magic/FourCC "DDS " is not set in ddsheader due to something with endianess.
// Note: Although I tried to create a ddsheader, it's not needed at all. The needed texture info is in width, height, and levels.

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <cstring>

// uintptr_t (see tools.h)
// Needed for crn_decomp.h which is included later.
#if !defined(_MSC_VER) || _MSC_VER >= 1600
#include <stdint.h>
#elif _MSC_VER >= 1310
#include <stddef.h>
#endif

// vsprintf_s and fopen_s
// fopen_s has a different signature from fopen, so it can't be replaced this way.
#ifndef _MSC_VER
#define vsprintf_s vsnprintf
//#define fopen_s fopen
#endif

using namespace std;

// Public crnlib header.
#include "crnlib.h"

// CRN transcoder library.
#include "crn_decomp.h"
// .DDS file format definitions.
#include "dds_defs.h"

using namespace crnlib;

static int error(const char* pMsg, ...)
{
   va_list args;
   va_start(args, pMsg);
   char buf[512];
   vsprintf_s(buf, sizeof(buf), pMsg, args);
   va_end(args);
   printf("%s", buf);
   return EXIT_FAILURE;
}

// Loads an entire file into an allocated memory block.
static crn_uint8 *read_file_into_buffer(const char *pFilename, crn_uint32 &size)
{
   size = 0;

   FILE* pFile = NULL;
   // Replace fopen_s with fopen and a check for "" as the path - see _MSC_VER define at the beginning of the file
   //fopen_s(&pFile, pFilename, "rb");
   if(!pFilename || !pFilename[0]) return NULL;
   pFile = fopen(pFilename, "rb");
   if (!pFile)
      return NULL;

   fseek(pFile, 0, SEEK_END);
   size = ftell(pFile);
   fseek(pFile, 0, SEEK_SET);

   crn_uint8 *pSrc_file_data = static_cast<crn_uint8*>(malloc(std::max(1U, size)));
   if ((!pSrc_file_data) || (fread(pSrc_file_data, size, 1, pFile) != 1))
   {
      fclose(pFile);
      free(pSrc_file_data);
      size = 0;
      return NULL;
   }

   fclose(pFile);
   return pSrc_file_data;
}

// Cracks a CRN's file header using the helper functions in crn_decomp.h.
static bool print_crn_info(const crn_uint8 *pData, crn_uint32 data_size)
{
   crnd::crn_file_info file_info;
   if (!crnd::crnd_validate_file(pData, data_size, &file_info))
      return false;

   printf("crnd_validate_file:\n");
   printf("File size: %u\nActualDataSize: %u\nHeaderSize: %u\nTotalPaletteSize: %u\nTablesSize: %u\nLevels: %u\n", data_size,
      file_info.m_actual_data_size, file_info.m_header_size, file_info.m_total_palette_size, file_info.m_tables_size, file_info.m_levels);

   printf("LevelCompressedSize: ");
   for (crn_uint32 i = 0; i < cCRNMaxLevels; i++)
      printf("%u ", file_info.m_level_compressed_size[i]);
   printf("\n");

   printf("ColorEndpointPaletteSize: %u\n", file_info.m_color_endpoint_palette_entries);
   printf("ColorSelectorPaletteSize: %u\n", file_info.m_color_selector_palette_entries);
   printf("AlphaEndpointPaletteSize: %u\n", file_info.m_alpha_endpoint_palette_entries);
   printf("AlphaSelectorPaletteSize: %u\n", file_info.m_alpha_selector_palette_entries);

   printf("crnd_get_texture_info:\n");
   crnd::crn_texture_info tex_info;
   if (!crnd::crnd_get_texture_info(pData, data_size, &tex_info))
      return false;

   // CrnFormat is an enum. crn_get_format_string isn't in the decomp only header, so let's just print it as an integer.
   printf("Dimensions: %ux%u\nLevels: %u\nFaces: %u\nBytesPerBlock: %u\nUserData0: %u\nUserData1: %u\nCrnFormat: %ld\n",
      tex_info.m_width, tex_info.m_height, tex_info.m_levels, tex_info.m_faces, tex_info.m_bytes_per_block, tex_info.m_userdata0, tex_info.m_userdata1, tex_info.m_format);

   return true;
}

// Cracks the DDS header and dump its contents.
static bool print_dds_info(const void *pData, crn_uint32 data_size)
{
   if ((data_size < 128) || (*reinterpret_cast<const crn_uint32*>(pData) != crnlib::cDDSFileSignature))
      return false;

   const crnlib::DDSURFACEDESC2 &desc = *reinterpret_cast<const crnlib::DDSURFACEDESC2*>((reinterpret_cast<const crn_uint8*>(pData) + sizeof(crn_uint32)));
   if (desc.dwSize != sizeof(crnlib::DDSURFACEDESC2))
      return false;

   printf("DDS file information:\n");
   printf("File size: %u\nDimensions: %ux%u\nPitch/LinearSize: %u\n", data_size, desc.dwWidth, desc.dwHeight, desc.dwLinearSize);
   printf("MipMapCount: %u\nAlphaBitDepth: %u\n", desc.dwMipMapCount, desc.dwAlphaBitDepth);

   const char *pDDSDFlagNames[] = 
   {
      "DDSD_CAPS", "DDSD_HEIGHT", "DDSD_WIDTH", "DDSD_PITCH",
      NULL, "DDSD_BACKBUFFERCOUNT", "DDSD_ZBUFFERBITDEPTH", "DDSD_ALPHABITDEPTH",
      NULL, NULL, NULL, "DDSD_LPSURFACE",
      "DDSD_PIXELFORMAT", "DDSD_CKDESTOVERLAY", "DDSD_CKDESTBLT", "DDSD_CKSRCOVERLAY",
      "DDSD_CKSRCBLT", "DDSD_MIPMAPCOUNT", "DDSD_REFRESHRATE", "DDSD_LINEARSIZE",
      "DDSD_TEXTURESTAGE", "DDSD_FVF", "DDSD_SRCVBHANDLE", "DDSD_DEPTH" 
   };

   printf("DDSD Flags: 0x%08X ", desc.dwFlags);
   for (int i = 0; i < sizeof(pDDSDFlagNames)/sizeof(pDDSDFlagNames[0]); i++)
      if ((pDDSDFlagNames[i]) && (desc.dwFlags & (1 << i)))
         printf("%s ", pDDSDFlagNames[i]);
   printf("\n\n");

   printf("ddpfPixelFormat.dwFlags: 0x%08X ", desc.ddpfPixelFormat.dwFlags);
   if (desc.ddpfPixelFormat.dwFlags & DDPF_ALPHAPIXELS) printf("DDPF_ALPHAPIXELS ");
   if (desc.ddpfPixelFormat.dwFlags & DDPF_ALPHA) printf("DDPF_ALPHA ");
   if (desc.ddpfPixelFormat.dwFlags & DDPF_FOURCC) printf("DDPF_FOURCC ");
   if (desc.ddpfPixelFormat.dwFlags & DDPF_PALETTEINDEXED8) printf("DDPF_PALETTEINDEXED8 ");
   if (desc.ddpfPixelFormat.dwFlags & DDPF_RGB) printf("DDPF_RGB ");
   if (desc.ddpfPixelFormat.dwFlags & DDPF_LUMINANCE) printf("DDPF_LUMINANCE ");
   printf("\n");

   printf("ddpfPixelFormat.dwFourCC: 0x%08X '%c' '%c' '%c' '%c'\n",
      desc.ddpfPixelFormat.dwFourCC, 
      std::max(32U, desc.ddpfPixelFormat.dwFourCC & 0xFF), 
      std::max(32U, (desc.ddpfPixelFormat.dwFourCC >> 8) & 0xFF), 
      std::max(32U, (desc.ddpfPixelFormat.dwFourCC >> 16) & 0xFF), 
      std::max(32U, (desc.ddpfPixelFormat.dwFourCC >> 24) & 0xFF));

   printf("dwRGBBitCount: %u 0x%08X\n",
      desc.ddpfPixelFormat.dwRGBBitCount, desc.ddpfPixelFormat.dwRGBBitCount);

   printf("dwRGBBitCount as FOURCC: '%c' '%c' '%c' '%c'\n", 
      std::max(32U, desc.ddpfPixelFormat.dwRGBBitCount & 0xFF), 
      std::max(32U, (desc.ddpfPixelFormat.dwRGBBitCount >> 8) & 0xFF), 
      std::max(32U, (desc.ddpfPixelFormat.dwRGBBitCount >> 16) & 0xFF), 
      std::max(32U, (desc.ddpfPixelFormat.dwRGBBitCount >> 24) & 0xFF));

   printf("dwRBitMask: 0x%08X\ndwGBitMask: 0x%08X\ndwBBitMask: 0x%08X\ndwRGBAlphaBitMask: 0x%08X\n",
      desc.ddpfPixelFormat.dwRBitMask, desc.ddpfPixelFormat.dwGBitMask, desc.ddpfPixelFormat.dwBBitMask, desc.ddpfPixelFormat.dwRGBAlphaBitMask);

   printf("\n");
   printf("ddsCaps.dwCaps: 0x%08X ", desc.ddsCaps.dwCaps);
   if (desc.ddsCaps.dwCaps & DDSCAPS_COMPLEX) printf("DDSCAPS_COMPLEX ");
   if (desc.ddsCaps.dwCaps & DDSCAPS_TEXTURE) printf("DDSCAPS_TEXTURE ");
   if (desc.ddsCaps.dwCaps & DDSCAPS_MIPMAP) printf("DDSCAPS_MIPMAP");
   printf("\n");

   printf("ddsCaps.dwCaps2: 0x%08X ", desc.ddsCaps.dwCaps2);
   const char *pDDCAPS2FlagNames[] = 
   {
      NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
      NULL, "DDSCAPS2_CUBEMAP", "DDSCAPS2_CUBEMAP_POSITIVEX", "DDSCAPS2_CUBEMAP_NEGATIVEX", 
      "DDSCAPS2_CUBEMAP_POSITIVEY", "DDSCAPS2_CUBEMAP_NEGATIVEY", "DDSCAPS2_CUBEMAP_POSITIVEZ", "DDSCAPS2_CUBEMAP_NEGATIVEZ", 
      NULL, NULL, NULL, NULL, 
      NULL, "DDSCAPS2_VOLUME"
   };
   for (int i = 0; i < sizeof(pDDCAPS2FlagNames)/sizeof(pDDCAPS2FlagNames[0]); i++)
      if ((pDDCAPS2FlagNames[i]) && (desc.ddsCaps.dwCaps2 & (1 << i)))
         printf("%s ", pDDCAPS2FlagNames[i]);
   printf("\n");

   printf("ddsCaps.dwCaps3: 0x%08X\nddsCaps.dwCaps4: 0x%08X\n", 
      desc.ddsCaps.dwCaps3, desc.ddsCaps.dwCaps4);

   return true;
}

void transcode_crn_prepare_dds_header(void*& ddsheader, crnd::crn_texture_info &tex_info)
{
   // Prepare the DDS header.
   crnlib::DDSURFACEDESC2 dds_desc;
   memset(&dds_desc, 0, sizeof(dds_desc));
   dds_desc.dwSize = sizeof(dds_desc);
   dds_desc.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT | ((tex_info.m_levels > 1) ? DDSD_MIPMAPCOUNT : 0);
   dds_desc.dwWidth = tex_info.m_width;
   dds_desc.dwHeight = tex_info.m_height;
   dds_desc.dwMipMapCount = (tex_info.m_levels > 1) ? tex_info.m_levels : 0;

   dds_desc.ddpfPixelFormat.dwSize = sizeof(crnlib::DDPIXELFORMAT);
   dds_desc.ddpfPixelFormat.dwFlags = DDPF_FOURCC;
   crn_format fundamental_fmt = crnd::crnd_get_fundamental_dxt_format(tex_info.m_format);
   dds_desc.ddpfPixelFormat.dwFourCC = crnd::crnd_crn_format_to_fourcc(fundamental_fmt);
   if (fundamental_fmt != tex_info.m_format)
   {
      // It's a funky swizzled DXTn format - write its FOURCC to dwRGBBitCount.
      dds_desc.ddpfPixelFormat.dwRGBBitCount = crnd::crnd_crn_format_to_fourcc(tex_info.m_format);
   }

   dds_desc.ddsCaps.dwCaps = DDSCAPS_TEXTURE;
   if (tex_info.m_levels > 1)
   {
      dds_desc.ddsCaps.dwCaps |= (DDSCAPS_COMPLEX | DDSCAPS_MIPMAP);
   }

   if (tex_info.m_faces == 6)
   {
      dds_desc.ddsCaps.dwCaps2 = DDSCAPS2_CUBEMAP | 
         DDSCAPS2_CUBEMAP_POSITIVEX | DDSCAPS2_CUBEMAP_NEGATIVEX | DDSCAPS2_CUBEMAP_POSITIVEY | 
         DDSCAPS2_CUBEMAP_NEGATIVEY | DDSCAPS2_CUBEMAP_POSITIVEZ | DDSCAPS2_CUBEMAP_NEGATIVEZ;
   }

   // Set pitch/linearsize field (some DDS readers require this field to be non-zero).
   int bits_per_pixel = crnd::crnd_get_crn_format_bits_per_texel(tex_info.m_format);
   dds_desc.lPitch = (((dds_desc.dwWidth + 3) & ~3) * ((dds_desc.dwHeight + 3) & ~3) * bits_per_pixel) >> 3;
   dds_desc.dwFlags |= DDSD_LINEARSIZE;

   // Set ddsheader
   // TODO: Also write "DDS " to the first 4 bytes
   ddsheader = malloc(cDDSSizeofDDSurfaceDesc2+4);
   memcpy((unsigned char *)ddsheader+4, &dds_desc, cDDSSizeofDDSurfaceDesc2);
}

int transcode_crn_face_and_mipmap_levels(void*& ddsdata, crnd::crnd_unpack_context pContext, void *pSrc_file_data, crnd::crn_texture_info &tex_info)
{
   // Now transcode all face and mipmap levels into memory, one mip level at a time.
   void *pImages[cCRNMaxFaces][cCRNMaxLevels];
   crn_uint32 image_size_in_bytes[cCRNMaxLevels];
   memset(pImages, 0, sizeof(pImages));
   memset(image_size_in_bytes, 0, sizeof(image_size_in_bytes));

   crn_uint32 total_unpacked_texels = 0;

   for (crn_uint32 level_index = 0; level_index < tex_info.m_levels; level_index++)
   {
      // Compute the face's width, height, number of DXT blocks per row/col, etc.
      const crn_uint32 width = std::max(1U, tex_info.m_width >> level_index);
      const crn_uint32 height = std::max(1U, tex_info.m_height >> level_index);
      const crn_uint32 blocks_x = std::max(1U, (width + 3) >> 2);
      const crn_uint32 blocks_y = std::max(1U, (height + 3) >> 2);
      const crn_uint32 row_pitch = blocks_x * crnd::crnd_get_bytes_per_dxt_block(tex_info.m_format);
      const crn_uint32 total_face_size = row_pitch * blocks_y;

      image_size_in_bytes[level_index] = total_face_size;

      for (crn_uint32 face_index = 0; face_index < tex_info.m_faces; face_index++)
      {
         void *p = malloc(total_face_size);
         if (!p)
         {
            for (crn_uint32 f = 0; f < cCRNMaxFaces; f++) 
               for (crn_uint32 l = 0; l < cCRNMaxLevels; l++) 
                  free(pImages[f][l]);
            crnd::crnd_unpack_end(pContext);
            free(pSrc_file_data);
            return error("Out of memory!");
         }

         pImages[face_index][level_index] = p;
      }

      // Prepare the face pointer array needed by crnd_unpack_level().
      void *pDecomp_images[cCRNMaxFaces];
      for (crn_uint32 face_index = 0; face_index < tex_info.m_faces; face_index++)
         pDecomp_images[face_index] = pImages[face_index][level_index];

      // Now transcode the level to raw DXTn
      if (!crnd::crnd_unpack_level(pContext, pDecomp_images, total_face_size, row_pitch, level_index))
      {
         for (crn_uint32 f = 0; f < cCRNMaxFaces; f++) 
            for (crn_uint32 l = 0; l < cCRNMaxLevels; l++) 
               free(pImages[f][l]);

         crnd::crnd_unpack_end(pContext);
         free(pSrc_file_data);

         return error("Failed transcoding texture!");
      }
   }

   // Allocate memory for ddsdata
   crn_uint32 totalsize = 0;
   for (crn_uint32 face_index = 0; face_index < tex_info.m_faces; face_index++)
      for (crn_uint32 level_index = 0; level_index < tex_info.m_levels; level_index++)
         totalsize += image_size_in_bytes[level_index];
   ddsdata = malloc(totalsize);


   // Now write the DXTn data to the DDS file in face-major order.
   void *ddsdata_begin = ddsdata;
   for (crn_uint32 face_index = 0; face_index < tex_info.m_faces; face_index++)
      for (crn_uint32 level_index = 0; level_index < tex_info.m_levels; level_index++)
      {
         memcpy(ddsdata, pImages[face_index][level_index], image_size_in_bytes[level_index]);
         ddsdata = (unsigned char *)ddsdata + image_size_in_bytes[level_index];
      }
   ddsdata = ddsdata_begin;

   for (crn_uint32 f = 0; f < cCRNMaxFaces; f++) 
      for (crn_uint32 l = 0; l < cCRNMaxLevels; l++) 
         free(pImages[f][l]);

   crnd::crnd_unpack_end(pContext);

   return 0;
}

int transcode_crn(const char* pSrc_filename, void*& ddsheader, void*& ddsdata, int *width, int *height, int *levels, bool inspectCRN, bool inspectDDS)
{
   // Load the source file into memory.
   printf("Loading source file: %s\n", pSrc_filename);
   crn_uint32 src_file_size;
   crn_uint8 *pSrc_file_data = read_file_into_buffer(pSrc_filename, src_file_size);
   if (!pSrc_file_data)
      return error("Unable to read source file\n");

   // Inspect CRN (and validate if inspected)
   if (inspectCRN) print_crn_info(pSrc_file_data, src_file_size);

   // Decompress/transcode CRN to DDS.
   printf("Decompressing CRN to DDS\n");

   // Transcode the CRN file to a DDS file in memory.
   // DDS header will be in void *ddsheader
   // DDS data will be in void *ddsdata

   // Get tex info
   crnd::crn_texture_info tex_info;
   if (!crnd::crnd_get_texture_info(pSrc_file_data, src_file_size, &tex_info))
   {
      free(pSrc_file_data);
      return error("crnd_get_texture_info() failed!\n");
   }

   // From CRN tex info, set width, height, levels
   *width = tex_info.m_width;
   *height = tex_info.m_height;
   *levels = tex_info.m_levels;

   // Create CRN transcode context
   crnd::crnd_unpack_context pContext = crnd::crnd_unpack_begin(pSrc_file_data, src_file_size);
   if (!pContext)
   {
      free(pSrc_file_data);
      return error("crnd_unpack_begin() failed!\n");
   }

   // Prepare DDS header
   transcode_crn_prepare_dds_header(ddsheader, tex_info);

   // Transcode CRN to memory
   transcode_crn_face_and_mipmap_levels(ddsdata, pContext, pSrc_file_data, tex_info);

   // Inspect DDS (and validate if inspected)
   //if (inspectDDS) print_dds_info(pDDS_file_data, dds_file_size);

   // Set DDS data
   //ddsdata = pDDS_file_data;

   // Discard CRN
   free(pSrc_file_data);

   return 0;
}

void discard_dds(void *ddsheader, void *ddsdata)
{
   // Discard DDS
   free(ddsheader);
   free(ddsdata);
}
