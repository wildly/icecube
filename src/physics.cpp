// physics.cpp: no physics books were hurt nor consulted in the construction of this code.
// All physics computations and constants were invented on the fly and simply tweaked until
// they "felt right", and have no basis in reality. Collision detection is simplistic but
// very robust (uses discrete steps at fixed fps).

#include "cube.h"

VAR(debugmovement, 0, 0, 1);

// Workaround to stop monsters from landing on top of the player, blocking the player's movement.
// If set to true, monsters will avoid moving into the player's radius, regardless of the height difference,
// and if a monster is already above or below the player, the monster will stop moving, even when mid-air.
const bool monster_vertical_keepout_check = false;

// Workaround to allow jumppads and knockback to send monsters into the air.
// If enabled, monsters tend to be "glued" to the floor.
// TODO: minfloor check should check against bottom of monster's bounding box?
const bool monster_minfloor_check = false;

bool plcollide(dynent *d, dynent *o, float &headspace, float &hi, float &lo) // collide with player or monster
{
    if(o->state!=CS_ALIVE) return true;
    const float r = o->radius+d->radius;
    if(fabs(o->o.x-d->o.x)<r && fabs(o->o.y-d->o.y)<r) 
    {
        if(d->o.z-d->eyeheight<o->o.z-o->eyeheight) { if(o->o.z-o->eyeheight<hi) hi = o->o.z-o->eyeheight-1; }
        else if(o->o.z+o->aboveeye>lo) lo = o->o.z+o->aboveeye+1;
    
        if(fabs(o->o.z-d->o.z)<o->aboveeye+d->eyeheight) return false;
        if(monster_vertical_keepout_check && d->monsterstate) return false; // hack
        headspace = d->o.z-o->o.z-o->aboveeye-d->eyeheight;
        if(headspace<0) headspace = 10;        
    };
    return true;
};

bool cornertest(int mip, int x, int y, int dx, int dy, int &bx, int &by, int &bs)    // recursively collide with a mipmapped corner cube
{
    sqr *w = wmip[mip];
    int sz = ssize>>mip;
    bool stest = SOLID(SWS(w, x+dx, y, sz)) && SOLID(SWS(w, x, y+dy, sz));
    mip++;
    x /= 2;
    y /= 2;
    if(SWS(wmip[mip], x, y, ssize>>mip)->type==CORNER)
    {
        bx = x<<mip;
        by = y<<mip;
        bs = 1<<mip;
        return cornertest(mip, x, y, dx, dy, bx, by, bs);
    };
    return stest;
};

void mmcollide(dynent *d, float &hi, float &lo)           // collide with a mapmodel
{
    loopv(ents)
    {
        extentity &e = ents[i];
        if(e.type!=MAPMODEL) continue;
        mapmodelinfo &mmi = getmminfo(e.attr2);
        if(!mmi_valid(mmi) || !mmi.h) continue;
        const float r = mmi.rad+d->radius;
        if(fabs(e.x-d->o.x)<r && fabs(e.y-d->o.y)<r)
        { 
            float mmz = (float)(S(e.x, e.y)->floor+mmi.zoff+e.attr3);
            if(d->o.z-d->eyeheight<mmz) { if(mmz<hi) hi = mmz; }
            else if(mmz+mmi.h>lo) lo = mmz+mmi.h;
        };
    };
};

// did a zero-radius physent located at "o" collide with a mapmodel?
// if true, "out" contains the x or y or z position to push the physent to (pick only one direction)
// Note: Only used for gunrays, projectiles, and particles.
// Note: Certain mapmodels have a much larger bounding box than the model, resulting in unexpected collisions.
// To work around this, mapmodels with radius 2 or less have no collision in this test.
// A radius of 2 means that the mapmodel has a bounding box of 4x4 or less. (The mapmodel can be of any height.)
// To validate this workaround:
// 1. Check that the steps in the final arena in "camera" work
// 2. Check that the light posts in "uf" allow shooting through the gaps
// 3. Check that "bridges" made from mapmodels have collision with gunrays, projectiles, and particles.
bool mmcollidephysent(vec &o, vec &out)
{
    const float ExtraSeparation = 0.001f; // 1e-3f
    loopv(ents)
    {
        extentity &e = ents[i];
        if(e.type!=MAPMODEL) continue;
        mapmodelinfo &mmi = getmminfo(e.attr2);
        if(!mmi_valid(mmi) || !mmi.h) continue;
        const float r = (float)mmi.rad;
        if(mmi.rad <= 2) continue; // workaround map model and bounding box size mismatch
        if(fabs(e.x-o.x)<r && fabs(e.y-o.y)<r)
        {
            out.x = (o.x < e.x) ? e.x - r - ExtraSeparation : e.x + r + ExtraSeparation;
            out.y = (o.y < e.y) ? e.y - r - ExtraSeparation: e.y + r + ExtraSeparation;
            float mmz = (float)(S(e.x, e.y)->floor+mmi.zoff+e.attr3);
            if(o.z<mmz) { continue; } // no collision
            else if(o.z>mmz+mmi.h) { continue; } // no collision
            else
            {
                out.z = (o.z < mmz+mmi.h/2) ? mmz - ExtraSeparation: mmz + mmi.h + ExtraSeparation;
                return true;
            };
        };
    };
    return false;
};

// Build a list of all collidable dynents
//
// Dynents have an implicit id used in the network code:
//   The first "cn" dynent pointers are other clients,
//   numbered starting at 0 until cn, and is NULL for
//   disconnected clients(?).
// The following dynent pointer is player1.
// After player1, the order of dynent pointers doesn't
// matter because coop singleplayer isn't implemented.
dvector collidable;
dvector &getcollidable()
{
    collidable.setsize(0);
    loopv(players)
        collidable.push_back(players[i]);
    collidable.push_back(player1);
    dvector &mv = getmonsters();
    loopv(mv)
        collidable.push_back(mv[i]);
    return collidable;
};

// all collision happens here
// spawn is a dirty side effect used in spawning
// drop & rise are supplied by the physics below to indicate gravity/push for current mini-timestep

int collideceilingcount = 0, thiscollideceilingcount = 0;
float totalrisedist = 0, thisrisedist = 0;
bool collide(dynent *d, bool spawn, float drop, float rise)
{
    const float fx1 = d->o.x-d->radius;     // figure out integer cube rectangle this entity covers in map
    const float fy1 = d->o.y-d->radius;
    const float fx2 = d->o.x+d->radius;
    const float fy2 = d->o.y+d->radius;
    const int x1 = fast_f2nat(fx1);
    const int y1 = fast_f2nat(fy1);
    const int x2 = fast_f2nat(fx2);
    const int y2 = fast_f2nat(fy2);
    float hi = 127, lo = -128;
    float minfloor = (d->monsterstate && !spawn && d->health>100) ? d->o.z-d->eyeheight-4.5f : -1000.0f;  // big monsters are afraid of heights, unless angry :)

    for(int x = x1; x<=x2; x++) for(int y = y1; y<=y2; y++)     // collide with map
    {
        if(OUTBORD(x,y)) return false;
        sqr *s = S(x,y);
        float ceil = s->ceil;
        float floor = s->floor;
        switch(s->type)
        {
            case SOLID:
                return false;

            case CORNER:
            {
                int bx = x, by = y, bs = 1;
                if(x==x1 && y==y1 && cornertest(0, x, y, -1, -1, bx, by, bs) && fx1-bx+fy1-by<=bs
                || x==x2 && y==y1 && cornertest(0, x, y,  1, -1, bx, by, bs) && fx2-bx>=fy1-by
                || x==x1 && y==y2 && cornertest(0, x, y, -1,  1, bx, by, bs) && fx1-bx<=fy2-by
                || x==x2 && y==y2 && cornertest(0, x, y,  1,  1, bx, by, bs) && fx2-bx+fy2-by>=bs)
                   return false;
                break;
            };

            case FHF:       // FIXME: too simplistic collision with slopes, makes it feels like tiny stairs
                floor -= (s->vdelta+S(x+1,y)->vdelta+S(x,y+1)->vdelta+S(x+1,y+1)->vdelta)/16.0f;
                break;

            case CHF:
                ceil += (s->vdelta+S(x+1,y)->vdelta+S(x,y+1)->vdelta+S(x+1,y+1)->vdelta)/16.0f;

        };
        if(ceil<hi) hi = ceil;
        if(floor>lo) lo = floor;
        if(monster_minfloor_check && floor<minfloor) return false;
    };

    if(hi-lo < d->eyeheight+d->aboveeye) return false;

    float headspace = 10;
    dvector v = getcollidable();
    // don't collide with other dynents if dead
    if(d->state == CS_DEAD) v.setsize(0);
    // this loop can be a performance bottleneck with many monster on a slow cpu,
    // should replace with a blockmap but seems mostly fast enough
    loopv(v) if(v[i] && d!=v[i] && !vreject(d->o, v[i]->o, 7.0f) && !plcollide(d, v[i], headspace, hi, lo)) return false;
    headspace -= 0.01f;
    
    mmcollide(d, hi, lo);    // collide with map models

    if(spawn)
    {
        d->o.z = lo+d->eyeheight;       // just drop to floor (sideeffect)
        d->onfloor = true;
    }
    else
    {
        const float space = d->o.z-d->eyeheight-lo;
        if(space<0)
        {
            if(space>-1.26f)                              // rise thru stair
            {
                const float risedist = min(-space, rise); // stick on step (rise by smaller of the two distances)
                d->o.z += risedist;
                thisrisedist += risedist;
            }
            else return false;
        }
        else
        {
            d->o.z -= min(min(drop, space), headspace);       // gravity
        };

        const float space2 = hi-(d->o.z+d->aboveeye);
        if(space2<0)
        {
            if(space2<-0.1) return false;     // hack alert!
            d->o.z = hi-d->aboveeye;          // glue to ceiling
            ++thiscollideceilingcount;        // decelerate due to hitting the ceiling (once per moveres)
        };

        d->onfloor = d->o.z-d->eyeheight-lo<0.001f;
    };
    return true;
}

float rad(float x) { return x*TAU/360; };

VARP(maxroll, 0, 3, 20);
VARP(reducemotion, 0, 0, 1);

const float EnterDeathCamRoll = 60;
const float DesiredDeathCamPitch = 0;
void enterDeathCamera()
{
    if(reducemotion) return;
    addCameraRoll(player1, EnterDeathCamRoll, EnterDeathCamRoll);
};

// Adds an amount of camera roll in the same direction as the current roll, up to limit
void addCameraRoll(dynent *d, float amount, float limit)
{
    if(d != player1 || reducemotion) return;
    if(d->roll >= limit || d->roll <= -limit) return;

    // invert?
    int invert = d->roll > 0 ? 0 : d->roll < 0 ? 1 : rnd(2);

    if(invert)
    {
        amount = -amount;
        limit = -limit;
    };

    float newroll = d->roll + amount;

    if(!invert)
    {
        // limit positive newroll
        if(newroll > limit) newroll = limit;

        // take the greater (in magnitude) of the player's original roll and newroll
        d->roll = max(d->roll, newroll);
    }
    else
    {
        // limit negative newroll
        if(newroll < limit) newroll = limit;

        // take the "more negative" (greater in magnitude) of the player's original roll and newroll
        d->roll = min(d->roll, newroll);
    };
};

void updateCamera(dynent *d, int msec)
{
    if(d != player1 || reducemotion) return;

    // if dead, remove 90 degrees per second from pitch until reaching desired pitch
    if(d->state == CS_DEAD)
    {
        if(d->pitch > DesiredDeathCamPitch) { d->pitch -= msec*90.0f/1000.0f; if(d->pitch < 0) d->pitch = 0; }
        else if(d->pitch < DesiredDeathCamPitch) { d->pitch += msec*90.0f/1000.0f; if(d->pitch > 0) d->pitch = 0; };
    }
};

// Adjustable Gravity
// 100% = Regular gravity
//  17% = "Low" gravity (about 1/4 effect)
//   0% = "Zero" gravity (but you will still stop after about 4 seconds time in air; think air resistance/drag)

// Enable adjustable gravity?
//#define ADJUSTABLE_GRAVITY

#ifdef ADJUSTABLE_GRAVITY
VARP(gravitypercent, 0, 100, 100);
#else
int gravitypercent = 100;
#endif

int lastgravitypercent = 100;

// Everything that experiences gravity needs to have their physics state updated when gravity changes
// Call this function once per frame, before(?) movement - for convenience, we will call this from physicsframe().
void checkgravitychanged()
{
    if(gravitypercent != lastgravitypercent)
    {
        dvector v = getcollidable();
        loopv(v) if(v[i]) timeinair_end(v[i]);
    };
    lastgravitypercent = gravitypercent;
};

// Estimate dynent's downward or upward speed
float estimateupwardspeed(dynent *d)
{
    const float velz = d->vel.z*d->maxspeed;                            // units: cubes/sec
    float gravityvz = 0.0f;
    if(d->inwater) gravityvz = d->onfloor ? 0.0f : 2.5f;                // gravity effect in water: constant 2.5 cubes/sec
    else if(d->timeinair) gravityvz = d->timeinair/30.0f + 9.5f;        // gravity effect in air: (timeinair/1000) * (1000/30) + 9.5 // units: cubes/sec
    gravityvz *= (gravitypercent/100.0f);                               // adjust for configured percent gravity
    return velz - gravityvz;
};

float estimatedownwardspeed(dynent *d)
{
    return -estimateupwardspeed(d);
};

// Estimate dynent's velocity
vec estimatevelocity(dynent *d)
{
    vec v = d->vel;
    v.z = 0;
    vmul(v, d->maxspeed);                                               // units: cubes/sec
    v.z = estimateupwardspeed(d);
    return v;
};

vec getPhysVelocity(dynent *d)
{
    vec v = d->vel;
    vmul(v, d->maxspeed);
    v.z -= d->timeinair/30.0f*gravitypercent/100.0f;
    return v;
};

// Ceiling "Stickiness"
// Colliding with the ceiling causes an additional -5x regular gravity while in contact.
// This amount should be regardless of the current gravity percent or being in water.
// There is no "correct" amount, but if it is too small, the dynent's z-position will be "stuck" at the ceiling height until gravity takes over (it can take a while).
// Another option?: Cancel all upward movement; can be done by setting a much higher multiplier or by directly setting the dynent's physics state.
void decelerate_dynent_ceiling_collision(dynent *d, float collidetime)
{
    const float currentzspeed = estimateupwardspeed(d);
    const float gravity = 1000.0f/30.0f;                                // the acceleration part of regular gravity
    float zspeeddecrease = 5*gravity*collidetime/1000.0f;               // 5x regular gravity

    if(currentzspeed > 0)
    {
        // don't reduce z speed below 0 this way
        if(zspeeddecrease > currentzspeed) zspeeddecrease = currentzspeed;

        // apply the velocity change
        d->vel.z -= zspeeddecrease/d->maxspeed;

        if(debugmovement) if(d == player1) conoutf("collide ceiling: z-speed remaining %.1f (decr:%.3f,msec:%.1f)", estimateupwardspeed(d), zspeeddecrease, collidetime);
    };
};

// Adjust velocity when colliding with geometry and the physics engine allow you to clip through to a slightly higher floor.
// Note: It may not be necessary to adjust velocity for minor changes (the movement would be free); only need to adjust when "vaulting/parkouring" to a much higher floor.
// Note: Multiply input vel by this factor and maxspeed to get distance.
// The equation is "integrate (1*e^(-decay*t)) from t=0 to t=10", or "1/decay", where decay is the decay factor in the moveplayer function.
const float VelToDistance = 1/1.7f;
const float VelToDistanceWater = 1/2.5f;

void velocity_loss_by_clip(dynent *d, float risedist, bool water)
{
    // don't reduce actual upward speed below 0
    if(d->vel.z <= 0)
    {
        // speedrunner boon? free elevation gain.
        if(debugmovement) if(d == player1) conoutf("collide stairs: z-speed %.1f (decr:none,rise dist:%.2f)", estimateupwardspeed(d), risedist);
        return;
    }

    // calculate amount to reduce

    // 1. convert physics vel to distance
    // 2. compare dynent's physics distance and maxreduction, use the smaller one to zap velocity
    const float factor = water ? VelToDistanceWater : VelToDistance;
    const float maxreduction = min(risedist, d->vel.z*factor*d->maxspeed);

    // 3. convert to input vel and use it to zap velocity
    d->vel.z -= maxreduction/factor/d->maxspeed;

    if(debugmovement) if(d == player1) if(risedist >= 0.01f) conoutf("collide stairs: z-speed remaining %.1f (decr:%.3f,rise dist:%.2f)", estimateupwardspeed(d), maxreduction/factor, risedist);
};

// Sets the camera lag in vertical position when clipping through stairs.
// Gives a smooth feeling of climbing up stairs.
// Only affects player1's position when rendering (camera); this doesn't change player1's physics
// position.
// If using readdepth to get the 3D position of the crosshair, the crosshair will be lower than the
// one calculated from physics position.
// This function doesn't apply smoothing for descending down stairs, smoothing would depend on
// gravity and being able to predict that the future physics floor height will be less than the
// current height and adjusting the camera early. (e.g. predict from IK foot placement)

// based on maxspeed * jump impulse - tractor gravity = 22.0f * 1.7f - 9.5 (units: cubes/sec)
//const float camera_delay_linear = 27.9f;
// still too much... alternate calculation: 400 ft in 4 minutes 25 sec = 0.4 m/s, or 1.6 cubes/sec
//const float camera_delay_linear = 1.6f * 5.0f;
const float camera_delay_linear = 20.0f;
const float camera_delay_exponential = 60.0f;

// accumulates risedist, removes a linear amount of risedist in time
float camera_position_delta = 0.0f;

float getCameraOffsetZ(dynent *d)
{
    if(d != player1) return 0.0f;

    // return the negated amount, since offset is opposite of the accumulator for risedist
    return -camera_position_delta;
};

void resetcamera(dynent *d)
{
    if(d != player1) return;
    camera_position_delta = 0.0f;
};

void stair_movement_add(dynent *d, float risedist)
{
    if(d != player1) return;
    camera_position_delta += risedist;
};

void stair_movement_smoothing(dynent *d, int curtime)
{
    if(d != player1) return;
    const float last_camera_position_delta = camera_position_delta;
    const float linearresult = camera_delay_linear - curtime*camera_delay_linear/1000.0f;
    const float exponentialresult = camera_position_delta * exp(-camera_delay_exponential*curtime/1000.0f);
    camera_position_delta = min(linearresult, exponentialresult);
    if(camera_position_delta < 0.0f) camera_position_delta = 0.0f;

    if(debugmovement) if(camera_position_delta >= 0.01f) conoutf("collide stairs: camera z-pos lag remaining %.2f (decr:%.3f,msec:%d)", camera_position_delta, last_camera_position_delta-camera_position_delta, curtime);
};

// Energy Push: Push effect that adds less to dynent's velocity in same
// direction and adds more when dynent's velocity is in the opposite direction.
// When dynent's v and push are opposite, gives up to 100% of additional push
// to cancel dynent's v projected onto push, then pushes.
// When dynent's v and push are in the same direction, pushes an amount such
// that dynent's resulting v projected onto push does not exceed 1.0x times push,
// or produces no push if already exceeded 1.0x times push.
void energypush(dynent *d, vec push, int type)
{
    float push_length = sqrt(dotprod(push, push));
    if(push_length == 0) return; // no push

    // needed for accurate physics state?
    timeinair_end(d);

    // scalar projection of dynent's v onto push
    float scalarProj = dotprod(d->vel, push)/push_length;

    // scale push up or down
    if(scalarProj < 0)
    {
        // scale up push to cancel part of dynent's v
        float scale = 1-scalarProj/push_length;
        if(scale > 2) scale = 2;
        vmul(push, scale);
        vadd(d->vel, push);
        if(debugmovement) conoutf("energypush: scalarProj:%.3f, scale:%.3f", scalarProj, scale);
    } else {
        // scale down push such that dynent's resulting v projected onto push does not exceed 1.0x push
        float scale = 1-scalarProj/push_length;
        scale = clamp(scale, 0, 1);
        vmul(push, scale);
        vadd(d->vel, push);
        if(debugmovement) conoutf("energypush: scalarProj:%.3f, scale:%.3f", scalarProj, scale);
    };
};

void energypush(dynent *d, vec push)
{
    energypush(d, push, 0);
};

void prettyprintvec(char *buf, const vec &v, const char *separatorChars, const float threshold)
{
    string temp;
    buf[0] = 0;
    temp[0] = 0;

    if(v.x < threshold && v.x > -threshold) concatstring(buf, "0");
    else { formatstring(temp)("%.1f", v.x); concatstring(buf, temp); };
    concatstring(buf, separatorChars);
    if(v.y < threshold && v.y > -threshold) concatstring(buf, "0");
    else { formatstring(temp)("%.1f", v.y); concatstring(buf, temp); };
    concatstring(buf, separatorChars);
    if(v.z < threshold && v.z > -threshold) concatstring(buf, "0");
    else { formatstring(temp)("%.1f", v.z); concatstring(buf, temp); };
};

// Float Control (Low/Zero Gravity Movement)
// Blends between regular "inair" movement (forward/backward input can generate
// x and y components, and "floating/inwater"-type movement (forward/backward
// input can generate a z component).
// This is different from "maneuverability" in air vs on floor (i.e. ramp up to
// full speed in less time when standing on a floor).
// And this is different from changing "movespeed" (e.g. 1/2 movement speed in
// water).
// The blending calculation is: "Float Control" (percent) = 100-gravitypercent
// 100% float control = like floating/inwater movement but normal movement speed
//   0% float control = like onfloor/inair movement (and no z movement control)
int floatcontrol_percent(dynent *d)
{
    // Low gravity
    int floatcontrol = d->timeinair ? 100 - gravitypercent : 0; // or try (d->timeinair || d->onfloor) but only when "d->move" points away from the floor
    floatcontrol = clamp(floatcontrol, 0, 100);

    // Strict movement
    if(currentmutator.strictmovement)
        floatcontrol = 0;

    return floatcontrol;
};

// Convert the timeinair physics final speed and merge it with dynent's velocity
// If this function is needed, call this function before any direct adjustments to dynent's velocity.
// One weird side effect is that after being merged into a dynent's velocity, the gravity effect accumulated will decay to zero as though it came from a movement input.
void timeinair_end(dynent *d)
{
    float timeinair_vel = -d->timeinair/30.0f;
    timeinair_vel *= (lastgravitypercent/100.0f); // lastgravitypercent = gravitypercent, except when timeinair_end is called from checkgravitychanged.
    d->vel.z += timeinair_vel/d->maxspeed;
    d->timeinair = 0;
};

void moveplayer_timeinair_end(dynent *d)
{
    if(debugmovement) if(d == player1) if(d->timeinair) conoutf("moveplayer: time in air = %d", d->timeinair);
    timeinair_end(d);
};

// limit timeinair to about 35 seconds to prevent timeinair*timeinair from overflowing 32-bit int
const int TIMEINAIR_LIMIT = (int)sqrt((double)INT32_MAX)-10000;
void timeinair_periodic_limit()
{
    dvector v = getcollidable();
    loopv(v)
        if(v[i] && v[i]->timeinair > TIMEINAIR_LIMIT) v[i]->timeinair = TIMEINAIR_LIMIT;
};

int physicsrepeat = 0;
const int MINFRAMETIME = 20; // physics always simulated at 50fps or better

// optimally schedule physics frames inside the graphics frames
// deltamillis: total time covered by this group of physics frames
// physicsrepeat: a parameter used by the physics routine to split the physics
//                calculation into smaller time deltas
void physicsframe(int deltamillis)
{
    //physicsrepeat = deltamillis/MINFRAMETIME;
    //physicsrepeat += ((deltamillis%MINFRAMETIME) > 0) ? 1 : 0; // + 1 frame if nonzero deltamillis remainder

    // The alternate method below is more optimized than the method above.
    // The compiler has optimized the division into multiplicaton by magic number, so calculating the remainder is expensive!

    // Determine rounded-up result for # of physicsframes by adding (MINFRAMETIME-1) to deltamillis before dividing by MINFRAMETIME.
    // Discrete math proves this produces identical results.
    // Note: "deltamillis + (MINFRAMETIME-1)" could overflow, so don't call this function with deltamillis near INT_MAX.
    physicsrepeat = (deltamillis + (MINFRAMETIME-1))/MINFRAMETIME;
    if(physicsrepeat<1) physicsrepeat = 1;

    checkgravitychanged();
    timeinair_periodic_limit();
};

VARP(autofloat, 0, 0, 1);
float autofloat_camera_offset = 0.0f;
vec calc_autofloat_input(dynent *d, bool water, float submerged, const vec &input)
{
    vec floatinput(0);

    // Keep autofloating even when dead,
    // and offset the camera up by 3.0f
    if(d == player1 && d->state == CS_DEAD && water)
        autofloat_camera_offset = 3.0f;
    else if(d == player1 && d->state != CS_DEAD)
        autofloat_camera_offset = 0.0f;

    // Monsters don't autofloat when dead
    if(d->monsterstate && d->state == CS_DEAD)
        return floatinput;

    // If the player (or monster) wants to move downward, don't give autofloat; we don't want to fight against their input
    if(input.z < 0 && water)
    {
        // We can still give them neutral buoyancy
        floatinput.z = 2.5f*gravitypercent/100.0f;
    }
    else if(d->monsterstate && water)
    {
        // Monsters only need neutral buoyancy
        floatinput.z = 2.5f*gravitypercent/100.0f;
    }
    else if(submerged >= 0.8f)
    {
        // Note: Player could also press "jump" for dashing upward with correct water feel.
        // There's no cooldown for "jump" when in water and it would cancel too much x and y
        // movement and it would immediately overwrite all downward velocity.
        // Autofloat input doesn't adjust x and y movement and it adds to upward/downward
        // velocity.
        // Near the water surface, scale down from maxiumum autofloat input to avoid oscillation
        // due to adding too much energy to the system; do this via scaling by percent submerged.
        floatinput.z = submerged*d->maxspeed/2;
    }
    else if(water)
    {
        // add an input to cancel the current physics downward velocity or, if the player is
        // already swimming up, add the needed input to cancel out in-water gravity.
        floatinput.z = max(-estimateupwardspeed(d), 2.5f*gravitypercent/100.0f);
    };

    // convert to input-type movement vector
    floatinput.div(d->maxspeed);

    // double the needed input due to 50% impulse ratio in water
    floatinput.mul(2.0f);

    // input can't exceed 1.0
    if(floatinput.z > 1.0f) floatinput.z = 1.0f;

    return floatinput;
};

// Add camera roll when strafing
// TODO: Add camera bob when moving? Add camera FOV or position delay when running?
void updateCameraMovement(dynent *pl, float move, float strafe)
{
    // if a player is dead, their move and strafe input shouldn't register
    if(pl->state == CS_DEAD) move = strafe = 0.0f;

    // camera roll decays unless strafing
    if(fabs(strafe) < 0.1f || fabs(pl->roll) > (float)maxroll)
    {
        const float newroll_exp = pl->roll*((float)exp(-curtime*log(1+1/100.0f)));
        const float newroll_linear = pl->roll >= 0 ? max(0.0f, pl->roll-0.003f*curtime) : min(0.0f, pl->roll+0.003f*curtime);
        pl->roll = pl->roll >= 0 ? min(newroll_exp, newroll_linear) : max(newroll_exp, newroll_linear);
    };

    // For now, this only processes the smooth death camera pitch transition
    updateCamera(pl, curtime);

    // add camera roll when strafing
    if(!reducemotion && fabs(strafe) >= 0.1f)
    {
        // incoming damage may have caused roll to exceed maxroll
        // in that situation, extend maxroll limits where necessary to match dynent's current roll
        const float roll_limit_low  = min(pl->roll, (float)-maxroll);
        const float roll_limit_high = max(pl->roll, (float) maxroll);
        pl->roll += strafe*curtime/-30.0f;
        pl->roll = clamp(pl->roll, roll_limit_low, roll_limit_high);
    };
};

// Apply jump impulse with some physics adjustments
// impulse_ratio: usually equal to 1; nominally 0.5 in water (when jumping while submerged in water, 0.5 to 1.0 depending on the extent submerged)
void applyjumpimpulse(dynent *d, float amount, float impulse_ratio, bool water, float submerged)
{
    // When gravitypercent decreases, removes that fraction of jump impulse that normally would counteract the "tractor beam" effect under full gravity.
    if(gravitypercent != 100)
    {
        float velocity_adjustment = (1-(gravitypercent/100.0f)) * (water ? 2.5f : 9.5f);
        amount -= velocity_adjustment/d->maxspeed;

        // TODO: Then split the remaining amount according to gravitypercent
        // and process part of it as realistic physics and the other part as classic physics.
        // If in water, process all of it as classic physics.
    };

    // impulse_ratio is set to 50% when in water
    // restore some jump impulse upwards when not submerged very far in water
    // for compatiblilty with map designs, players must be able to do a full jump when 70% submerged
    if(water)
    {
        if(submerged < 0.8f)
            impulse_ratio = 1;                                      // total jump impulse = full
        else if(submerged < 1)
            impulse_ratio = impulse_ratio + (1-impulse_ratio)/2;    // total jump impulse = 3/4
        // else do nothing: leave at 50%
    };

    d->vel.z = amount*impulse_ratio;                                // physics impulse upwards
};

// get dynent's movement and store it in vec &d
// also get dynent's merged move and strafe
// should not modify dynent
void getmovement(dynent *pl, vec &d, float &mergedMove, float &mergedStrafe, bool water, bool floating)
{
    // get move and strafe (including joystick/gamepad)
    float move = float(pl->move);
    float strafe = float(pl->strafe);

    if(pl==player1)
    {
        if(pl->move == 0) move = getjoymovementmove();
        if(pl->strafe == 0) strafe = getjoymovementstrafe();
    };

    // if a player is dead, their move and strafe input shouldn't register - see clientgame.cpp (moveplayer check, and freelook check)
    if(pl->state==CS_DEAD) { d.x = d.y = d.z = 0.0f; return; };

    mergedMove = move;
    mergedStrafe = strafe;

    d.x = (float)(move*cos(rad(pl->yaw-90)));
    d.y = (float)(move*sin(rad(pl->yaw-90)));
    d.z = 0;

    if(floating || water)
    {
        d.x *= (float)cos(rad(pl->pitch));
        d.y *= (float)cos(rad(pl->pitch));
        d.z = (float)(move*sin(rad(pl->pitch)));
    };

    d.x += (float)(strafe*cos(rad(pl->yaw-180)));
    d.y += (float)(strafe*sin(rad(pl->yaw-180)));

    // If strict movement, no movement impulse while in-air
    // Note: For correct strict movement feel, need to preserve dynent's current x and y vel
    if(currentmutator.strictmovement && !(floating || pl->inwater || pl->onfloor))
    {
        d.x = pl->vel.x;
        d.y = pl->vel.y;
    };

    // Note: If strict movement, floatcontrol_percent() is zero, but currentfloatcontrol can be 50.0f if parkour is enabled and dynent is blocked
    float currentfloatcontrol = (float)floatcontrol_percent(pl);
    if(currentmutator.parkour && pl == player1 && pl->blocked)
        currentfloatcontrol = max(50.0f, currentfloatcontrol);

    if(currentfloatcontrol > 0)
    {
        // calculate floating movement again
        vec fc;
        fc.x = (float)(move*cos(rad(pl->yaw-90)));
        fc.y = (float)(move*sin(rad(pl->yaw-90)));
        fc.z = 0;

        fc.x *= (float)cos(rad(pl->pitch));
        fc.y *= (float)cos(rad(pl->pitch));
        fc.z = (float)(move*sin(rad(pl->pitch)));

        fc.x += (float)(strafe*cos(rad(pl->yaw-180)));
        fc.y += (float)(strafe*sin(rad(pl->yaw-180)));

        // blend d and fc
        float blend = currentfloatcontrol/100.0f;
        d.x = (1-blend)*d.x + blend*fc.x;
        d.y = (1-blend)*d.y + blend*fc.y;
        d.z = (1-blend)*d.z + blend*fc.z;
    };
};

// main physics routine, moves a player/monster for a curtime step
// moveres indicated the physics precision (which is lower for monsters and multiplayer prediction)
// local is false for multiplayer prediction

void moveplayer(dynent *pl, int moveres, bool local, int curtime)
{
    // for players: eyeheight = 3.2, aboveeye = 0.7, water is true when waterlevel is 2.7 above player's feet (3.2-0.5),
    // minimum for submerged is 2.7/3.9 = 69%, underwater graphic shows when waterlevel is 3.5 above player's feet (3.2+0.3)
    const bool water = hdr.waterlevel>pl->o.z-0.5f;
    float submerged = 1-(pl->o.z+pl->aboveeye-hdr.waterlevel)/(pl->eyeheight+pl->aboveeye);
    submerged = clamp(submerged, 0, 1);
    const bool floating = (editmode && local) || pl->state==CS_EDITING;

    vec d(0);   // vector of direction we ideally want to move in
    float mergedMove = 0;
    float mergedStrafe = 0;
    getmovement(pl, d, mergedMove, mergedStrafe, water, floating);

    // We don't know whether a remote player has autofloat enabled and we don't need to simulate it.
    // If they do have autofloat enabled, the effect will be baked into their velocity update.
    // Monsters should have autofloat.
    const bool useautofloat = (pl == player1) ? autofloat : pl->monsterstate ? true : false;
    if(useautofloat && !floating)   // don't autofloat when editing
    {
        vec a = calc_autofloat_input(pl, water, submerged, d);

        // If input in the z direction is more than 1.0f, don't add autofloat (already moving at maxspeed upward).
        // If input in the z direction is less than 1.0f, add autofloat, and limit the result to 1.0f.
        // Then if the resulting input would exceed sqrt(2), limit the result to sqrt(2).
        vec sum = d;
        if(sum.z < 1.0f) { sum.add(a); if(sum.z > 1.0f) sum.z = 1.0f; }
        if(sum.squaredlen()>2.0f) sum.normalize().mul(sqrt(2.0f));
        d = sum;
    };

    const float speed = curtime/1000.0f*pl->maxspeed;
    const float decay = floating ? 8.7f : water ? 2.5f : pl->onfloor ? 8.7f : 1.7f; // floating (editing), water, onfloor, inair
    const float friction = exp(-decay*curtime/1000.0f);
    const float impulse = 1-friction;
    const float impulse_ratio = floating ? 1 : water ? 0.5f : 1;    // half speed in water; units for maxspeed are cubes/sec

    vmul(pl->vel, friction);    // slowly apply friction and direction to velocity, gives a smooth movement
    vmul(d, impulse*impulse_ratio);
    vadd(pl->vel, d);
    d = pl->vel;
    vmul(d, speed);             // d is now frametime based velocity vector

    pl->blocked = false;
    pl->moving = true;

    if(floating)                // just apply velocity
    {
        vadd(pl->o, d);
        if(pl->jumpnext) { pl->jumpnext = false; pl->vel.z = 2;    }
    }
    else                        // apply velocity with collision
    {
        int lasttimeinair;      // needed later for gravity
        if(pl->onfloor || water)
        {
            if(pl->jumpnext)
            {
                moveplayer_timeinair_end(pl);
                pl->jumpnext = false;
                applyjumpimpulse(pl, 1.7f, impulse_ratio, water, submerged);
                if(water) { pl->vel.x /= 8; pl->vel.y /= 8; };      // dampen velocity change even harder, gives correct water feel
                if(local) playsoundc(S_JUMP);
                else if(pl->monsterstate) playsound(S_JUMP, &pl->o);
            }
            else
            {
                if(estimatedownwardspeed(pl) >= 30.0f && !water)    // if we land after long time must have been a high jump, make thud sound
                {
                    if(local) playsoundc(S_LAND);
                    else if(pl->monsterstate) playsound(S_LAND, &pl->o);
                    if(debugmovement) if(pl == player1) conoutf("moveplayer: (hard landing!) downward speed = %.1f", estimatedownwardspeed(pl), pl->timeinair);
                };
                moveplayer_timeinair_end(pl);
                if(pl->onfloor) pl->vel.z = max(0.0f, pl->vel.z);   // cancel any remaining downward velocity because dynent is on the floor
                else
                {
                    // landed in water; limit vel to 1.7x dynent's maxspeed for water landings
                    float speed = sqrt(dotprod(pl->vel, pl->vel));
                    if(speed > 1.7f)
                    {
                        vmul(pl->vel, 1.7f/speed);

                        // create some bubbles (like teleportEffect)
                        const float effectRadius = max(1.0f, pl->radius);
                        const int particleCount = int((speed-1.7f)*pl->maxspeed*max(1.0f, pow(4*pl->radius*pl->radius*(pl->eyeheight+pl->aboveeye), 1/3.0f)/2));
                        particle_cloud(PART_SEAFOAM, particleCount, 400, 600, pl->o, effectRadius, 0);
                    };
                };
            };
            lasttimeinair = 0;
        }
        else
        {
            lasttimeinair = pl->timeinair;
            pl->timeinair += curtime;
        };

        const float f = 1.0f/moveres;
        const int lastgz = lasttimeinair*lasttimeinair;         // position and acceleration are related as 1/2*a*t*t; constants will be applied in a later calculation step
        const int deltagz = pl->timeinair*pl->timeinair - lastgz;
        float dropd = 9.5f*curtime+deltagz/60.0f;               // accurate except for "tractor beam" component; constants applied here
        if(water) dropd = 2.5f*curtime;                         // float slowly down in water
        dropd *= (gravitypercent/100.0f);
        const float drop = dropd/1000/moveres;
        const float rise = speed*impulse_ratio/moveres/1.2f;    // extra smoothness when lifting up stairs

        // on every loop iteration, accumulate these counters, the reset the delta amount to 0
        #define zero_loop_values() if(false) {} else { thiscollideceilingcount = 0; thisrisedist = 0; };
        #define accumulate_counters() if(false) {} else { collideceilingcount += thiscollideceilingcount; totalrisedist += thisrisedist; zero_loop_values(); }
        // init/ensure loop values are zero
        zero_loop_values();

        loopi(moveres)                                          // discrete steps collision detection & sliding
        {
            // try move forward
            pl->o.x += f*d.x;
            pl->o.y += f*d.y;
            pl->o.z += f*d.z;
            if(collide(pl, false, drop, rise)) { accumulate_counters(); continue; };
            // player stuck, try slide along y axis
            pl->blocked = true;
            pl->o.x -= f*d.x;
            if(collide(pl, false, drop, rise)) { accumulate_counters(); continue; };
            pl->o.x += f*d.x;
            // still stuck, try x axis
            pl->o.y -= f*d.y;
            if(collide(pl, false, drop, rise)) { accumulate_counters(); continue; };
            pl->o.y += f*d.y;
            // try just dropping down
            pl->moving = false;
            pl->o.x -= f*d.x;
            pl->o.y -= f*d.y;
            if(collide(pl, false, drop, rise)) { accumulate_counters(); continue; };
            pl->o.z -= f*d.z;
            break;
        };

        // process, then reset these counters
        if(collideceilingcount)
            decelerate_dynent_ceiling_collision(pl, curtime*collideceilingcount/float(moveres));
        if(totalrisedist > 0)
        {
            velocity_loss_by_clip(pl, totalrisedist, water);
            stair_movement_add(pl, totalrisedist);
        };

        collideceilingcount = 0;
        totalrisedist = 0;
        stair_movement_smoothing(pl, curtime);
    };

    // detect whether player is outside map, used for skipping zbuffer clear mostly

    if(pl->o.x < 0 || pl->o.x >= ssize || pl->o.y <0 || pl->o.y > ssize)
    {
        pl->outsidemap = true;
    }
    else
    {
        sqr *s = S((int)pl->o.x, (int)pl->o.y);
        pl->outsidemap = SOLID(s)
           || pl->o.z < s->floor - (s->type==FHF ? s->vdelta/4 : 0)
           || pl->o.z > s->ceil  + (s->type==CHF ? s->vdelta/4 : 0);
    };

    // automatically apply smooth roll when strafing

    if(pl == player1) updateCameraMovement(pl, mergedMove, mergedStrafe);

    // play sounds on water transitions

    if(!pl->inwater && water && estimateupwardspeed(pl) <= -9.5f*gravitypercent/100.0f)
    {
        if(debugmovement) if(pl == player1) conoutf("water splash: entered water! z-speed %.2f", estimateupwardspeed(pl));
        playsound(S_SPLASH2, &pl->o);
    }
    else if(pl->inwater && !water && estimateupwardspeed(pl) >= 9.5f*gravitypercent/100.0f)
    {
        if(debugmovement) if(pl == player1) conoutf("water splash: exited water! z-speed %.2f", estimateupwardspeed(pl));
        playsound(S_SPLASH1, &pl->o);
    };
    pl->inwater = water;
};

void moveplayer(dynent *pl, int moveres, bool local)
{
    const int iterations = physicsrepeat;

    int completedtime = 0;
    loopi(iterations)
    {
        int timeslice = curtime*(i+1)/iterations - completedtime;
        if(timeslice > 0) moveplayer(pl, moveres, local, timeslice);
        completedtime += timeslice;
    };
};

int getphysicsrepeat()
{
    return physicsrepeat;
};
