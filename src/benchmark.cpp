/*
    Frametime and Performance Tracker
    Copyright (C) 2011, 2012, 2015 Willy Deng

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/

// Terminology
//
// Age: A ushort specifying the n-th most recent entry in the
//      ringbuffer. (n = age)
//      Note that the age corresponding to an entry in the
//      ringbuffer changes whenever new entries are recorded.
// Window (milliseconds):
//      A ushort specifying the maximum accumulated time of the
//      complete frames on which the analysis will be run.
//      Invariant: Wall time.
//      (e.g. "window = 3 sec" -> 3 sec avg fps, max fps in prev 3 sec, min fps in prev 3 sec)
// Window (frames):
//      A ushort specifying the maximum number of complete
//      frames on which the analysis will be run.
//      Invariant: # of frames rendered, but each frames may have different complexity.
//      (e.g. "window = 30 frames" -> 30 frame avg fps, max fps in prev 30 frames, min fps in prev 30 frames)

// Subscribers and Expiration
//
// Premise:
// Entries eventually will expire and be removed from perfcounter_rawdata.
//
// Solution: [TODO]
// When an entry expires, all subscribers will be notified with a message
// containing the expired entry's "rawindex" and value.
// Expiration occurs monotonically with age; i.e. only the oldest entry
// in a perfcounter_rawdata can expire.

#include "cube.h"
#include "benchmark.h"

// Enqueue one data element into the ringbuffer and write evicted value (if any)
// into a pointer when a pointer is provided.
//
// e - The element to be enqueued by copying.
// evicted - A pointer to storage for the evicted element
//           if there is one (default: NULL).
// tail - Must always be the index of the most recently
//        inserted element.
//
// Returns true if an eviction occurred.
bool ringbuffer::enqueue(const ushort e, ushort *evicted = NULL)
{
    // "head" and "tail" index the same element if and only if count is 0 or count is 1.
    // They must point to different elements otherwise.
    assert((head == tail && (count == 0 || count == 1)) ||
           (head != tail && (count > 1)));

    // If full, evict the oldest entry and redo the enqueue.
    if(count == MAX_RINGBUFFER_LENGTH)
    {
        ushort d = dequeue();
        enqueue(e);

        if(evicted) { *evicted = d; };
        return true;
    };

    // Perform the enqueue.
    // If empty, don't advance tail. It is already at the correct index.
    if(count > 0)
        if(++tail == MAX_RINGBUFFER_LENGTH) tail = 0;
    buf[tail] = e;

    ++count;

    return false;
};

ushort ringbuffer::dequeue()
{
    assert(count > 0);

    ushort v = buf[head];

    // If count == 1, don't advance head. It is already at the correct index.
    if(count > 1)
        if(++head == MAX_RINGBUFFER_LENGTH) head = 0;

    --count;

    return v;
};

// perfcounter_director
void pf_fsm_start(perfcounter_director &d, perfcounter_event ev, uint SDLmillis)
{ d.state.state = ev; };
void pf_fsm_seal(perfcounter_director &d, perfcounter_event ev, uint SDLmillis)
{ d.seal(ev,SDLmillis); };
void pf_fsm_open(perfcounter_director &d, perfcounter_event ev, uint SDLmillis)
{ d.open(ev,SDLmillis); };
void pf_fsm_bookkeep(perfcounter_director &d, perfcounter_event ev, uint SDLmillis)
{ d.bookkeep(ev,SDLmillis); };

static const struct pf_director_transitions
{
    int st;
    int ev;
    void (* const fn)(perfcounter_director &d, perfcounter_event ev, uint SDLmillis);
} pf_transitions[] =
{
    { FRAMEZERO_EV,     FRAMESTART_EV,  &pf_fsm_start    },
    { FRAMEDONE_EV,     FRAMESTART_EV,  &pf_fsm_seal     },
    { FRAMESTART_EV,    METRICS_STGRP,  &pf_fsm_open     },
    { FRAMESTART_EV,    NONE_ST,        &pf_fsm_open     },
    { METRICS_STGRP,    METRICS_STGRP,  &pf_fsm_bookkeep },
    { METRICS_STGRP,    NONE_ST,        &pf_fsm_bookkeep },
    { METRICS_STGRP,    FRAMEDONE_EV,   &pf_fsm_bookkeep },
    { NONE_ST,          METRICS_STGRP,  &pf_fsm_bookkeep },
    { NONE_ST,          NONE_ST,        &pf_fsm_bookkeep },
    { NONE_ST,          FRAMEDONE_EV,   &pf_fsm_bookkeep },
};

void perfcounter_director::init()
{
    memset(framedata, 0, sizeof(framedata));
    state.active = 0;
    state.state = FRAMEZERO_EV;
};

void perfcounter_director::log_event(perfcounter_event ev, uint SDLmillis)
{
    loopi(ARRLEN(pf_transitions))
    {
        const pf_director_transitions &t = pf_transitions[i];
        if((state.state == t.st || (state.state < MAX_METRICS && t.st == METRICS_STGRP)) &&
           (ev == t.ev || (ev < MAX_METRICS && t.ev == METRICS_STGRP)))
        {
            if(t.fn)
                t.fn(director, ev, SDLmillis);
            return;
        };
    };
    printf("perfcounter_director: Invalid transition: state %d, event %d, buffer %d\n", state.state, ev, state.active?1:0);
};

void perfcounter_director::open(perfcounter_event ev, uint SDLmillis)
{
    framedata[state.active].laststarted = SDLmillis;
    framedata[state.active].lasttouched = SDLmillis;
    state.state = ev;
};

void perfcounter_director::seal(perfcounter_event ev, uint SDLmillis)
{
    perfcounter_metrics &d = framedata[state.active];
    d.ready = 1;
    state.active++;
    state.state = ev;
};

void perfcounter_director::bookkeep(perfcounter_event ev, uint SDLmillis)
{
    perfcounter_metrics &d = framedata[state.active];
    if(state.state < MAX_METRICS)
        d.metrics[state.state] += SDLmillis - d.lasttouched;
    d.lasttouched = SDLmillis;
    state.state = ev;
};

VAR(logframedata, 0, 0, 1);
VARP(framedatawindowlength, 30, 300, 10000);
VARP(framedatawindowismillis, 0, 1, 1);
VARP(framedatawindowpartitionpercent, 0, 25, 100);

void perfcounter_director::commit()
{
    uint i = 0;
    ARRFIND(framedata[i].ready, i < ARRLEN(framedata), ++i);
    if(i == ARRLEN(framedata))
    {
        printf("perfcounter_director: Nothing to commit\n");
        return;
    };

    perfcounters_setparams();

    perfcounter_metrics &d = framedata[i];

    total_frametimes.log_val(d.lasttouched-d.laststarted);
    cpu_compute_frametimes.log_val(d.metrics[TABULATE_ST]+d.metrics[UPDATE_ST]);
    cpu_active_frametimes.log_val(d.lasttouched-d.laststarted-d.metrics[SWAPBUFFERS_ST]-d.metrics[SLEEP_ST]);

    construct_minmax_frametime_heap();

    // logframedata
    int sum = 0;
    loopi(MAX_METRICS)
        sum += d.metrics[i];
    if(logframedata)
    {
        // print to stdout (not the in-game console out)
        printf("framedata,etimestart:%d,sleep:%2d,update:%2d,render:%2d,ocull:%2d,light:%2d,worldop:%2d,particles:%2d,swapbuf:%2d,other:%2d\n",
            d.laststarted, d.metrics[SLEEP_ST], d.metrics[UPDATE_ST], d.metrics[RENDER_ST], d.metrics[OCULL_ST], d.metrics[CALCLIGHT_ST], d.metrics[WORLD_OP_ST], d.metrics[UPDATE_PARTICLES_ST], d.metrics[SWAPBUFFERS_ST], d.metrics[TABULATE_ST] + d.lasttouched-d.laststarted-sum);
    };

    memset(&d, 0, sizeof(d));
};

void construct_minmax_frametime_heap()
{
    reset_minmax_frametime_tracker();

    // construct heap
    int totalframes = 0;
    float totaltime = 0.0f;
    for(int i = 0; i < total_frametimes.count; i++)
    {
        if(framedatawindowismillis) // == 1
        {
            if(totaltime >= framedatawindowlength) break;
        }
        else // framedatawindowismillis == 0
        {
            if(totalframes >= framedatawindowlength) break;
        };

        totaltime += total_frametimes.history(i);
        push_minmax_frametime(total_frametimes.history(i));
        totalframes++;
    };
    set_minmax_totalframes_totaltime((float)totalframes, (float)totaltime);
};

// perfcounter_rawdata
int perfcounter_rawdata::subscribe(perfcounter *listener)
{
    listeners.add(listener);
    return 0;   // todo: return a subscriber id
};

bool perfcounter_rawdata::unsubscribe(int sub_id)
{
    // todo: function body
    return false;
};

void perfcounter_rawdata::log_val(const ushort v)
{
    ushort discarded_value;
    ushort discarded_index = head;
    bool discard_occurred = enqueue(v, &discarded_value);

    // Update listeners when information expires
    // pseudocode: listeners.foreach(e->update(discarded_value, discarded_index));
    //if(discard_occurred)
        //loopv(listeners)
            // Disable because this is a useless update that only causes unneeded recalculation
            // TODO: Something with the loop macros causes an ambiguous else with this code structure
            //listeners[i]->update(discarded_value, discarded_index);

    // Update listeners when new information available
    // pseudocode: listeners.foreach(e->update(v, tail));
    loopv(listeners)
        listeners[i]->update(v, age(tail));
};

ushort perfcounter_rawdata::rawindex(const ushort age)
{
    assert(age < count);

    ushort index;

    if(tail - age < 0)
        index = MAX_RINGBUFFER_LENGTH + tail - age;
    else
        index = tail - age;

    return index;
};

ushort perfcounter_rawdata::history(const ushort age)
{
    return buf[rawindex(age)];
};

ushort perfcounter_rawdata::age(const ushort rawindex)
{
    ushort age;

    if(tail - rawindex < 0)
        age = MAX_RINGBUFFER_LENGTH + tail - rawindex;
    else
        age = tail - rawindex;

    assert(age < count);

    return age;
};

ushort perfcounter_rawdata::find_maxima(const ushort search_count, ushort *age = NULL)
{
    assert(search_count > 0 && search_count <= count);

    // these two lines are the unrolled result of the initial iteration of loop
    // they also initialize variables
    ushort candidate_maxima = history(0);
    ushort age_of_maxima = 0;

    for(int i = 1; i < search_count; i++)
    {
        if(history(i) > candidate_maxima)
        {
            candidate_maxima = history(i);
            age_of_maxima = i;
        };
    };

    if(age)
        *age = age_of_maxima;

    return candidate_maxima;
};

ushort perfcounter_rawdata::find_minima(const ushort search_count, ushort *age = NULL)
{
    assert(search_count > 0 && search_count <= count);

    ushort candidate_minima = history(0);
    ushort age_of_minima = 0;
    for(int i = 1; i < search_count; i++)
    {
        if(history(i) < candidate_minima)
        {
            candidate_minima = history(i);
            age_of_minima = i;
        };
    };

    if(age)
        *age = age_of_minima;

    return candidate_minima;
};

// Abstract perfcounter constructor
perfcounter::perfcounter(ushort setwindow, perfcounter_windowtype::windowtype setwindowtype, bool makeactive, perfcounter_rawdata *dsrc, perfcounter * parent = NULL)
{
    window = setwindow;
    iswindowmillis = (setwindowtype == perfcounter_windowtype::milliseconds) ? true : false;
    active = makeactive;

    d.val1 = 0;
    d.val2 = 0;
    d.val3 = 0;
    data_ns = true;

    data = dsrc;
    limitdata = parent;

    subscription_id = data->subscribe(this);
};

// Zero a perfcounter.
void perfcounter::zero()
{
    d.val1 = 0;
    d.val2 = 0;
    d.val3 = 0;
    data_ns = true;
};

void perfcounter_avg::update(ushort value, ushort rawindex)
{
    // load state
    ushort totaltime = avgd.totaltime;
    ushort totalframes = avgd.framecount;
    ushort recent_rawindex = avgd.rawindex;

    // check whether to completely recalculate
    if(totalframes == 0)
        goto recalc_from_scratch;

    // fast update calculation
    //
    // data->age(recent_rawindex) is how many new frames have been added
    // to the perfcounter_rawdata since the last update.
    // Ensure that the oldest frame still exists by verifying there are
    // at least totalframes + data->age(raw_index) frames in the history.
    if(data->age(recent_rawindex) && data->age(recent_rawindex) + totalframes <= data->count)
    {
        // todo: there might be a bug here when data->count == 0, and/or data->age(recent_rawindex) == 0, and/or totalframes == 0
        const int range = data->age(recent_rawindex);
        ushort oldest = data->age(recent_rawindex) + totalframes - 1;   // conversion from totalframes to age needs -1 offset
        for(int i = 0; i < range; i++)
        {
            totaltime += data->history(i);
            ++totalframes;

            while(iswindowmillis ? (totaltime - data->history(oldest) > window) :
                                   (totalframes > window))
            {
                totaltime -= data->history(oldest);
                oldest--;
                totalframes--;
            };
        }
        recent_rawindex = data->rawindex(0);
    }

    else
    recalc_from_scratch:
    {
        totaltime = 0;
        totalframes = 0;
        for(int a = 0; a < data->count; a++)
        {
            totaltime += data->history(a);
            totalframes++;
            if(iswindowmillis ? (totaltime >= window) : (totalframes >= window))
                break;
        };
        recent_rawindex = data->rawindex(0);
    };

    // write state
    avgd.totaltime = totaltime;
    avgd.framecount = totalframes;
    avgd.rawindex = recent_rawindex;
    data_ns = iswindowmillis ? (totaltime < window) : (totalframes < window);
};

float perfcounter_avg::result()
{
    // initialize calculation variables
    const ushort totaltime = avgd.totaltime;
    ushort num_frames = avgd.framecount;
    const float msec_per_sec = 1000.0f;

    // unadjusted result
    float fps = (float)num_frames;

    // this block adjusts the contribution of a boundary data element to a partial weight
    if(totaltime > window)
    {
        const ushort oldest = num_frames - 1; // conversion from totalframes to age needs -1 offset
        const ushort overtime = totaltime - window;
        fps = fps - (float)(overtime) / (float)data->history(oldest);
    };

    return fps * msec_per_sec/(float)window;
};

void perfcounter_min::update(ushort value, ushort rawindex)
{
    ushort age = minmaxd.age;
    ushort limit = minmaxd.totaltime;

    if(iswindowmillis) // TODO: There's no code for the other case (where window is in frames)!
    {
        // calculate seach_count, then look in the data history over search_count frames for the extrema of interest
        ushort totaltime = 0;
        ushort search_count = 0;
        for(int i = 0; i < window && i < data->count; i++)
        {
            const ushort deficit = window - totaltime; // deficit from window
            totaltime += data->history(i);
            if(totaltime > window)
            {
                if(deficit >= data->history(i) - deficit) // if more of history(i) contributes to deficit than overflow
                    search_count++;
                break;
            };
            search_count++;
        };
        // search_count can be 0 if the only frametime analyzed is > 2*window.
        // Therefore, clamp search_count to [1, data->count].
        search_count = clamp(search_count, (ushort)1, data->count);
        data->find_maxima(search_count, &age);
        // totaltime > window even when that data point is discarded due to the 0.5f threshold.
        // This way, data_ns isn't true when totaltime < window because of a discard.
        limit = iswindowmillis ? totaltime : search_count;
    };

    minmaxd.age = age;
    minmaxd.totaltime = limit;
    data_ns = iswindowmillis ? (limit < window) : (limit < window);
};

void perfcounter_max::update(ushort value, ushort rawindex)
// TODO: Strategy
//  Rewrite update to check for special cases where a simple recalculation would not suffice.
//  If a special case isn't present, call recalculate.
//  Otherwise, recalculate from scratch.
//  This allows fast updates when data at the end of the ringbuffer expires because that data 
//  usually doesn't correspond to the most recent min/max measurement in the measurement window
//  so that update doesn't trigger recalculation.
{
    ushort age = minmaxd.age;
    ushort limit = minmaxd.totaltime;

    if(iswindowmillis) // TODO: There's no code for the other case (where window is in frames)!
    {
        // calculate seach_count, then look in the data history over search_count frames for the extrema of interest
        ushort totaltime = 0;
        ushort search_count = 0;
        for(int i = 0; i < window && i < data->count; i++)
        {
            const ushort deficit = window - totaltime; // deficit from window
            totaltime += data->history(i);
            if(totaltime > window)
            {
                if(deficit >= data->history(i) - deficit) // if more of history(i) contributes to deficit than overflow; or 
                    search_count++;
                break;
            };
            search_count++;
        };
        // search_count can be 0 if the only frametime analyzed is > 2*window.
        // Therefore, clamp search_count to [1, data->count].
        search_count = clamp(search_count, (ushort)1, data->count);
        data->find_minima(search_count, &age);
        // totaltime > window even when that data point is discarded due to the 0.5f threshold.
        // This way, data_ns isn't true when totaltime < window because of a discard.
        limit = iswindowmillis ? totaltime : search_count;
    };

    minmaxd.age = age;
    minmaxd.totaltime = limit;
    data_ns = iswindowmillis ? (limit < window) : (limit < window);
};

float perfcounter_min::result()
{
    return (data->count > 0) ? min(1000.0f, 1000.0f/(float)data->history(minmaxd.age)) : 0.0f;
};

float perfcounter_max::result()
{
    return (data->count > 0) ? min(1000.0f, 1000.0f/(float)data->history(minmaxd.age)) : 0.0f;
};


void perfcounter_util::update(ushort value, ushort rawindex)
{
};

float perfcounter_util::result()
{
    //return (data->count > 0) ? (float)data->history(val1)/(float)limitdata->data->history(val1) : 0.0f;
    return (data->count > 0) ? (float)data->history(0) : 0; // work in progress; for now, show the most recent data
};

perfcounter_director director;
perfcounter_rawdata cpu_active_frametimes, total_frametimes, cpu_compute_frametimes;

// perfcounters configuration details
// framedatawindowlength:
//     This determines how many frames to use in calculating the performance metric.
//     It specifies an amount of milliseconds or a number of frames.
// framedatawindowismillis:
//     Determines whether the value in framedatawindowlength is milliseconds or frames.
// TODO: Do VARP variables have a value at global var init time?
perfcounter_avg fps_average(framedatawindowlength, perfcounter_windowtype::milliseconds, true, &total_frametimes);
perfcounter_min fps_min(framedatawindowlength, perfcounter_windowtype::milliseconds, true, &total_frametimes);
perfcounter_max fps_max(framedatawindowlength, perfcounter_windowtype::milliseconds, true, &total_frametimes);
perfcounter_util cpu_active_average(framedatawindowlength, perfcounter_windowtype::milliseconds, true, &cpu_active_frametimes, &fps_average);
perfcounter_util cpu_compute_average(framedatawindowlength, perfcounter_windowtype::milliseconds, true, &cpu_compute_frametimes, &fps_average);

void perfcounters_setparams()
{
    const int wlen = framedatawindowlength;
    //const perfcounter_windowtype::windowtype wtype = framedatawindowismillis ? perfcounter_windowtype::milliseconds : perfcounter_windowtype::frames;
    const perfcounter_windowtype::windowtype wtype = perfcounter_windowtype::milliseconds; // perfcounter_windowtype::frames not implemented yet
    //fps_average.setparams(wlen, wtype);
    fps_average.window = wlen;
    fps_average.iswindowmillis = (wtype == perfcounter_windowtype::milliseconds);
    //fps_max.setparams(wlen, wtype);
    fps_max.window = wlen;
    fps_max.iswindowmillis = (wtype == perfcounter_windowtype::milliseconds);
    //fps_min.setparams(wlen, wtype);
    fps_min.window = wlen;
    fps_min.iswindowmillis = (wtype == perfcounter_windowtype::milliseconds);
    //cpu_active_average.setparams(wlen, wtype);
    cpu_active_average.window = wlen;
    cpu_active_average.iswindowmillis = (wtype == perfcounter_windowtype::milliseconds);
    //cpu_compute_average.setparams(wlen, wtype);
    cpu_compute_average.window = wlen;
    cpu_compute_average.iswindowmillis = (wtype == perfcounter_windowtype::milliseconds);
};

//integration with main
void perfcounters_init()
{
    director.init();

    cpu_active_frametimes.init();
    cpu_compute_frametimes.init();
    total_frametimes.init();

    // perfcounter_rawdata should remain under 1 kB for optimal performance.
    // todo: move this to test framework
    //printf("sizeof perfcounter_rawdata: %d\n", sizeof(perfcounter_rawdata));

    perfcounters_setparams();

    const ushort testdata[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    const size_t testdatalen = sizeof(testdata)/sizeof(testdata[0]);
};

void cleanbenchmark()
{
    while(cpu_compute_frametimes.count > 0) cpu_compute_frametimes.dequeue();
    cpu_compute_frametimes.enqueue(1);
    cpu_compute_frametimes.dequeue();
    while(cpu_active_frametimes.count > 0) cpu_active_frametimes.dequeue();
    cpu_active_frametimes.enqueue(1);
    cpu_active_frametimes.dequeue();
    while(total_frametimes.count > 0) total_frametimes.dequeue();
    total_frametimes.enqueue(1);
    total_frametimes.dequeue();

    director.init();
    fps_average.zero();
    fps_min.zero();
    fps_max.zero();
    cpu_active_average.zero();
    cpu_compute_average.zero();
};
