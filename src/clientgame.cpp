// clientgame.cpp: core game related stuff

#include "cube.h"
#include "gamestate.h"

// IceCube or Cube rule set?
bool classicRuleset = true;

void classic2IceCubeRuleset()
{
    accurateChainGun(false);
    //enableShotgunBlast(true);
    classicItems(false);
};

void iceCube2ClassicRuleset()
{
    accurateChainGun(true);
    //enableShotgunBlast(false);
    classicItems(true);
};

int nextmode = 0;         // nextmode becomes gamemode after next map load
VAR(gamemode, 1, 0, 0);

void mode(int n) { addmsg(1, 2, SV_GAMEMODE, nextmode = n); };
COMMAND(mode, ARG_1INT);

void help()
{
    const char *desc = modeinfo(gamemode);
    if(desc) conouts(desc, 1);
};
COMMAND(help, ARG_NONE);

struct mutators currentmutator = { 0, 0, 0, 0, 0 }, nextmutator = { 0, 0, 0, 0, 0 };

void resetmutators()
{
    currentmutator = nextmutator;
};

void nightmareinfomsg();
void applymutators()
{
    // only show nightmare mutator status message if nightmare mode is active
    if(currentmutator.nightmare)
        nightmareinfomsg();

    // basic regen is restricted to single-player modes
    // players with special.regenhealth can't receive basicregen
    if(m_sp && currentmutator.basicregen && !player1->special.regenhealth)
    {
        RegenData r = { 1.0f, 0.0f };
        player1->regen.add(r);
        player1->special.basicregen = 1;
    };

    // slowmo
    if(currentmutator.slowmo)
    {
        startslowmo();
    };
};

void mutatorinfo(const char *name)
{
    if(strcmp(name, "nightmare") == 0)
    {
        conoutf("nightmare = %d, next = %d", currentmutator.nightmare, nextmutator.nightmare);
    }
    else if(strcmp(name, "slowmo") == 0)
    {
        conoutf("slowmo = %d, next = %d", currentmutator.slowmo, nextmutator.slowmo);
    }
    else if(strcmp(name, "basicregen") == 0)
    {
        conoutf("basicregen = %d, next = %d", currentmutator.basicregen, nextmutator.basicregen);
    }
    else if(strcmp(name, "classic") == 0)
    {
        conoutf("classic = %d (set/unset at startup with -gc/-gi)", classicRuleset ? 1 : 0);
    }
    else if(strcmp(name, "strictmovement") == 0)
    {
        conoutf("strictmovement = %d, next = %d", currentmutator.strictmovement, nextmutator.strictmovement);
    }
    else if(strcmp(name, "parkour") == 0)
    {
        conoutf("parkour = %d, next = %d", currentmutator.parkour, nextmutator.parkour);
    }
    else conoutf("usage: /mutatorinfo MUTATORNAME");
};
COMMAND(mutatorinfo, ARG_1STR);

// Nightmare mode: Monsters deal much more damage to players in this mode
// "/nightmare VALUE" sets nightmare status
// "/mutatorinfo nightmare" reveals the nightmare status

// Command to set nightmare mode (/nightmare)
// The default state is not nightmare
// -1: nightmare temporarily on (until next mapstart)
//  0: nightmare permanently off
//  1: nightmare permanently on
void nightmare(int on)
{
    if(on < -1 || on > 1)
    {
        conoutf("usage: /nightmare VALUE (-1, 0, 1)");
        return;
    };

    if((on && !currentmutator.nightmare) || (!on && currentmutator.nightmare))
    {
        currentmutator.nightmare = on ? 1 : 0;
        nightmareinfomsg();
    };
    if(on == 1) nextmutator.nightmare = 1;
    if(on == 0) nextmutator.nightmare = 0;
};
COMMAND(nightmare, ARG_1INT);

// Show a message when entering Nightmare mode and leaving Nightmare mode
// Don't show a message when Nightmare is off.
void nightmareinfomsg()
{
    if(currentmutator.nightmare)
        conoutf("The monsters have become nightmarishly powerful!");
    else
        conoutf("Now the monsters seem no stronger than usual.");
};

void basicregen(int on)
{
    if(on > 0)
    {
        nextmutator.basicregen = 1;
        conoutf("basic regeneration mutator will be ACTIVE in the next round");
    } else
    {
        nextmutator.basicregen = 0;
        conoutf("basic regeneration mutator will NOT be active in the next round");
    };
    return;
};
COMMAND(basicregen, ARG_1INT);

// Float control and in-air maneuverability are zero!
void strictmovement(int on)
{
    if(on > 0)
    {
        currentmutator.strictmovement = 1;
        nextmutator.strictmovement = 1;
    } else
    {
        currentmutator.strictmovement = 0;
        nextmutator.strictmovement = 0;
    };
    return;
};
COMMAND(strictmovement, ARG_1INT);

// Parkour
// TODO: Movement into the blocking wall is converted into upward movement
void parkour(int on)
{
    if(on > 0)
    {
        currentmutator.parkour = 1;
        nextmutator.parkour = 1;
    } else
    {
        currentmutator.parkour = 0;
        nextmutator.parkour = 0;
    };
    return;
};
COMMAND(parkour, ARG_1INT);

VARF(gamespeed, 10, 100, 1000, if(multiplayer()) gamespeed = 100);
int getgamespeed() { return gamespeed; };

bool intermission = false;
const uint MPTimeToRespawn = 10000;
const uint SPTimeToRespawn = 6000;
Uint32 nextRespawn = 0;
// if time is after reference, returns positive value
// if time is before reference, returns negative value
int timeDiff(Uint32 reference, Uint32 time)
{
    return (int)(time-reference);
};

dynent *player1 = new dynent;           // our client
dvector players;                        // other clients

int nextclass = 0;
VAR(playerclass, 1, 0, 0);
void setnextclass(const int classID) { nextclass = classID; };
void resetclass(dynent *d)
{
    d->special.big = 0;
    d->special.regenhealth = 0;
    d->special.regenarmour = 0;
    d->special.scientist = 0;
    if(d==player1) playerclass = 0;
};
void assignclass(dynent *d)
{
    if(nextclass >= 0 && nextclass <= 4)
    {
        switch(nextclass)
        {
        case 0: break;
        case 1: d->special.big = 1; break;
        case 2: d->special.regenhealth = 1; break;
        case 3: d->special.regenarmour = 1; break;
        case 4: d->special.scientist = 1; break;
        };
        if(d==player1) playerclass = nextclass;
    }
    else
    {
        conoutf("Invalid player class: %d", nextclass);
        if(d==player1) playerclass = 0;
    };
};

const char *classnames[] = { "normal player", "titan", "mutant", "specter", "scientist" };
void chooseclass(const int classID)
{
    if(classID < 0 || classID > 4)
    {
        conoutf("invalid range: class is 0..4");
        return;
    };
    setnextclass(classID);
    conoutf("you will respawn as a %s in the next round", classnames[classID]);
};
COMMAND(chooseclass, ARG_1INT);

VARP(sensitivity, 0, 10, 10000);
VARP(sensitivityscale, 1, 1, 10000);
VARP(invmouse, 0, 0, 1);

int lastmillis = 0;
float totalmillis = 0.0f;
int curtime = 10;
string clientmap = { };

char *getclientmap() { return clientmap; };

void resetmovement(dynent *d)
{
    d->k_left = false;
    d->k_right = false;
    d->k_up = false;
    d->k_down = false;  
    d->jumpnext = false;
    d->strafe = 0;
    d->move = 0;
};

int dynent::getplayerclass()
{
    if(player1->special.big) return 1;
    else if(player1->special.regenhealth) return 2;
    else if(player1->special.regenarmour) return 3;
    else if(player1->special.scientist) return 4;

    return 0;
};

// Damage received counter
// For now, don't put this in dynent class, so as to avoid bumping dynent version
int damagereceived = 0;
int damagedealt = 0;

void spawnstate(dynent *d)              // reset player state not persistent accross spawns
{
    // movement
    resetmovement(d);
    d->vel.x = d->vel.y = d->vel.z = 0; 
    d->onfloor = false;
    d->timeinair = 0;

    // update class
    resetclass(d);
    // disallow special classes in instagib, tactics arena, insta arena (256 health?), efficiency, instafist
    // implies special classes allowed in sp, standard mp, standard ctf/protect/hold, standard collect
    if(d == player1 && (!m_insta && !m_tactics && !m_arena && !m_efficiency && !(gamemode==12))) assignclass(d);

    // health, armour, buffs
    d->health = d->basehealth();
    loopi(NUMARMOUR) d->armour[i] = 0;
    d->armour[A_BLUE] = 50;
    updatearmourtype(d);
    loopi(NUMBUFFS) d->buffs[i] = 0;
    d->regen.setsize(0);
    d->armourRegen.setsize(0);
    resetexpregen(d);
    if(d == player1) { damagereceived = 0; damagedealt = 0; };

    // weapons, ammo
    d->lastattackgun = d->gunselect = GUN_SG;
    d->gunwait = 0;
    d->attacking = false;
    d->lastaction = 0;
    loopi(NUMGUNS) d->ammo[i] = 0;
    d->ammo[GUN_FIST] = 1;

    // apply initial values for special class features that need it
    if(d->special.big)
    {
        loopi(NUMARMOUR) d->armour[i] = 0;
        updatearmourtype(d);
    };
    if(d->special.regenarmour || d->special.scientist)
    {
        d->armour[A_BLUE] = d->maxarmour();
        updatearmourtype(d);
    };
    if(d->special.scientist)
    {
        RegenData r = { 2.5f, 0.0f };
        d->armourRegen.add(r);
    };

    // gameplay formats
    if(gamemode==12) // eihrul's secret "instafist" mode
    {
        d->gunselect = GUN_FIST;
        loopi(NUMARMOUR) d->armour[i] = 0;
        updatearmourtype(d);
        return;
    };
    if(m_tactics || m_efficiency)
    {
        if(m_tactics)
        {
            // Random selection without replacement
            // Rather than swap gun entries chosen in armory to the end of array,
            // we use the fact that if gun1 == gun2,
            // the last gun in the armory wasn't selected for gun1.
            const int armory[] = { GUN_SG, GUN_CG, GUN_RL, GUN_RIFLE };
            const int count = ARRLEN(armory), last = ARRLEN(armory)-1; // list count, list last index
            int gun1 = armory[rnd(count)];
            int gun2 = armory[rnd(count-1)];
            if(gun1==gun2) gun2 = armory[last];
            baseammo(gun1);
            baseammo(gun2);
            d->gunselect = gun1;
        }
        else // efficiency
        {
            givearmour(d, A_GREEN, 100);
            loopi(4) baseammo(i+1);
            d->gunselect = GUN_CG;
        };
        // divide chaingun ammo by 2 because baseammo function gave 2x ammo pickup for chaingun, but we want to the player to have 1x ammo
        d->ammo[GUN_CG] /= 2;
    }
    else // the default weapon for all other gamemodes is the shotgun
    {
        if(classicRuleset || m_classicsp) d->ammo[GUN_SG] = 5; // for classic cube and for sp map balance, start with 5 shotgun shells;
        else d->ammo[GUN_SG] = 10;                             // otherwise, for m_dmsp and IceCube multiplayer modes with normal ammo pickup, start with 10 shells (1x shotgun ammo).
    };

    // gameplay mods
    if(m_insta)
    {
        d->health = 1;
        loopi(NUMARMOUR) d->armour[i] = 0;
        updatearmourtype(d);
    };
    if(m_arena)
    {
        d->health = 256;
        loopi(NUMARMOUR) d->armour[i] = 0;
        updatearmourtype(d);
    };
    if(m_rifleonly)
    {
        loopi(NUMGUNS) d->ammo[i] = 0;
        d->ammo[GUN_RIFLE] = 100;
        d->gunselect = GUN_RIFLE;
    };
};

dynent *newdynent(dynent *const d)      // initialize a blank player or monster created by constructor
{
    d->o.x = 0;
    d->o.y = 0;
    d->o.z = 0;
    d->yaw = 270;
    d->pitch = 0;
    d->roll = 0;
    d->maxspeed = 22;
    d->outsidemap = false;
    d->inwater = false;
    d->radius = 1.1f;
    d->eyeheight = 3.2f;
    d->aboveeye = 0.7f;
    d->frags = 0;
    d->plag = 0;
    d->ping = 0;
    d->lastupdate = lastmillis;
    d->enemy = NULL;
    d->monsterstate = 0;
    zerostringmemory(d->name);
    zerostringmemory(d->team);
    d->blocked = false;
    d->lifesequence = 0;
    d->state = CS_ALIVE;
    spawnstate(d);
    return d;
};

void respawnself()
{
    spawnplayer(player1);
    showscores(false);
};

void arenacount(dynent *d, int &alive, int &dead, char *&lastteam, bool &oneteam)
{
    if(d->state!=CS_DEAD)
    {
        alive++;
        if(lastteam && strcmp(lastteam, d->team)) oneteam = false;
        lastteam = d->team;
    }
    else
    {
        dead++;
    };
};

const uint ArenaDetectInterval = 1000/5;
const uint ArenaNoDetectInterval = 10000;
const uint ArenaResetInterval = 5000;
// Arena round/respawn messages
// 0: wait for the round to finish
// 1: new round will start in %.1f
// 2: wait for the new round to start... %.1f
int arenaRespawnMessageType = 0;

struct ArenaController
{
    bool roundInProgress;
    Uint32 nextDetectTime;
    void init(Uint32 realtime) { nextDetectTime = realtime; roundInProgress = true; };
    void startRound() { roundInProgress = true; nextDetectTime = SDL_GetTicks() + ArenaDetectInterval; };
    void endRound() { roundInProgress = false; };
} arenaController;

void checkarena()
{
    if(!arenaController.roundInProgress)
    {
        if(timeDiff(nextRespawn, SDL_GetTicks()) >= 0)
        {
            conoutf("new round starting... fight!");
            respawnself();
            arenaRespawnMessageType = 0;
            arenaController.startRound();
        };
    }
    else if(timeDiff(arenaController.nextDetectTime, SDL_GetTicks()) >= 0)
    {
        int alive = 0, dead = 0;
        char *lastteam = NULL;
        bool oneteam = true;
        loopv(players) if(players[i]) arenacount(players[i], alive, dead, lastteam, oneteam);
        arenacount(player1, alive, dead, lastteam, oneteam);
        if(dead>0 && (alive<=1 || (m_teammode && oneteam)))
        {
            conoutf("arena round is over! next round in 5 seconds...");
            if(alive) conoutf("team %s is last man standing", lastteam);
            else conoutf("everyone died!");
            nextRespawn = SDL_GetTicks() + ArenaResetInterval;
            arenaRespawnMessageType = 1;
            arenaController.nextDetectTime += ArenaNoDetectInterval;
            arenaController.endRound();
        }
        else
        {
            arenaController.nextDetectTime += ArenaDetectInterval;
        }
    };
};

void zapdynent(dynent *&d)
{
    if(d) delete d;
    d = NULL;
};

extern int democlientnum;

void otherplayers()
{
    loopv(players) if(players[i])
    {
        const int lagtime = lastmillis-players[i]->lastupdate;
        if(lagtime>1000 && players[i]->state==CS_ALIVE)
        {
            players[i]->state = CS_LAGGED;
            continue;
        };
        if(lagtime && players[i]->state != CS_DEAD && (!demoplayback || i!=democlientnum)) moveplayer(players[i], 3, false);   // use physics to extrapolate player position
                                                                                                                               // don't need to move this player when their lagtime is 0
    };
};

void respawn()
{
    if(player1->state==CS_DEAD)
    {
        player1->attacking = false;
        player1->jumpnext = false;
        if(m_sp && timeDiff(nextRespawn,SDL_GetTicks())<0) { nextRespawn -= 1000; return; };
        if(m_arena) { if(!arenaController.roundInProgress) arenaRespawnMessageType = 2; return; };
        if(m_sp) { spawnstate(player1); nextmode = gamemode; changemap(clientmap); return; };    // if we die in SP we try the same map again
        respawnself();
    };
};

int sleepwait = 0;
string sleepcmd;
void sleepf(char *msec, char *cmd) { sleepwait = atoi(msec)+PhysicsClockPtr->getMillis(); copystring(sleepcmd, cmd); };
COMMANDN(sleep, sleepf, ARG_2STR);

void checksleepcmd()
{
    if(sleepwait && PhysicsClockPtr->getMillis()>sleepwait) { sleepwait = 0; execute(sleepcmd); };
};

void checkslowmo();
void applyslowmo();
void cancelslowmo();
VARF(slowmo, 0, 0, 1, checkslowmo());

void startslowmo()
{
    slowmo = 1;
    checkslowmo();
};

void checkslowmo()
{
    bool on = slowmo ? true : false;

    if(on && multiplayer())
    {
        on = false;
        cancelslowmo();
    };

    if(on)
    {
        currentmutator.slowmo = nextmutator.slowmo = 1;
        applyslowmo();
    }
    else
    {
        if(currentmutator.slowmo) cancelslowmo();
        currentmutator.slowmo = nextmutator.slowmo = 0;
    };
};

void applyslowmo()
{
    int speed = 100;
    int clarity = (int)(100.0f * player1->basicregen.tank / 100.0f);
    int endurance = (int)min(100.0f * (float)player1->health/(float)player1->basehealth(), 100.0f * (float)player1->maxhealth()/(float)player1->basehealth());

    speed = max(clarity, endurance);
    speed = clamp(speed, 10, 200);

    gamespeed = speed;
};

void cancelslowmo()
{
    slowmo = 0;
    if(gamespeed != 100)
    {
        gamespeed = 100;
        conoutf("gamespeed reset to normal");
    };
};

void updateworld(int millis)        // main game update loop
{
    if(lastmillis)
    {
        curtime = millis - lastmillis;
        physicsframe(curtime);

        // "freeze" player1 at intermision by sending 0 as curtime here
        // Note: checkbuff needs to run on every frame in order to apply "quad glow"
        checkbuffs(intermission ? 0 : curtime);
        checkregen(intermission ? 0 : curtime);

        checkslowmo();

        if(m_arena) checkarena();
        updateprojectiles(curtime, getphysicsrepeat());
        demoplaybackstep();
        if(!demoplayback)
        {
            if(getclientnum()>=0) shoot(player1, worldpos);     // only shoot when connected to server
            gets2c();           // do this first, so we have most accurate information when our player moves
        };
        otherplayers();
        if(!demoplayback)
        {
            monsterthink();
            if(player1->state==CS_DEAD)
            {
                player1->move = player1->strafe = 0;
                moveplayer(player1, 10, false);
                if(!m_arena && !m_sp && timeDiff(nextRespawn, SDL_GetTicks()) >= 0) respawn();
            }
            else if(!intermission)
            {
                // TODO: enable autofloat during intermission?
                moveplayer(player1, 20, true);
                checkitems();
            };
            c2sinfo(player1);   // do this last, to reduce the effective frame lag
        };
    };
    lastmillis = millis;
};

void entinmap(dynent *d)    // brute force but effective way to find a free spawn spot in the map
{
    resetcamera(d);
    loopi(100)              // try max 100 times
    {
        float dx = (rnd(21)-10)/10.0f*i;  // increasing distance
        float dy = (rnd(21)-10)/10.0f*i;
        d->o.x += dx;
        d->o.y += dy;
        if(collide(d, true, 0, 0)) return;
        d->o.x -= dx;
        d->o.y -= dy;
    };
    conoutf("can't find entity spawn spot! (%d, %d)", (int)d->o.x, (int)d->o.y);
    // leave ent at original pos, possibly stuck
};

int spawncycle = -1;
int fixspawn = 2;

void spawnplayer(dynent *d)   // place at random spawn. also used by monsters!
{
    int r = fixspawn-->0 ? 4 : rnd(10)+1;
    loopi(r) spawncycle = findentity(PLAYERSTART, spawncycle+1);
    if(spawncycle!=-1)
    {
        d->o.x = ents[spawncycle].x;
        d->o.y = ents[spawncycle].y;
        d->o.z = ents[spawncycle].z;
        d->yaw = ents[spawncycle].attr1;
        d->pitch = 0;
        d->roll = 0;
    }
    else
    {
        d->o.x = d->o.y = (float)ssize/2;
        d->o.z = 4;
    };
    entinmap(d);
    spawnstate(d);
    d->state = CS_ALIVE;
    if(d == player1)
    {
        if(player1->getplayerclass() != 0)
        {
            // Playing a special class in MP results in a 1 frag handicap on spawn
            if(!m_sp) addmsg(1, 2, SV_FRAGS, --player1->frags);
        };
    };
};

// movement input code

#define dir(name,v,d,s,os) void name(bool isdown) { player1->s = isdown; player1->v = isdown ? d : (player1->os ? -(d) : 0); player1->lastmove = lastmillis; };

dir(backward, move,   -1, k_down,  k_up);
dir(forward,  move,    1, k_up,    k_down);
dir(left,     strafe,  1, k_left,  k_right); 
dir(right,    strafe, -1, k_right, k_left); 

void attack(bool on)
{
    if(intermission) return;
    if(editmode) editdrag(on);
    else if(player1->attacking = on) respawn();
};

void jumpn(bool on) { if(!intermission && (player1->jumpnext = on)) respawn(); };

COMMAND(backward, ARG_DOWN);
COMMAND(forward, ARG_DOWN);
COMMAND(left, ARG_DOWN);
COMMAND(right, ARG_DOWN);
COMMANDN(jump, jumpn, ARG_DOWN);
COMMAND(attack, ARG_DOWN);
COMMAND(showscores, ARG_DOWN);
COMMAND(showaccuracy, ARG_DOWN);

void fixplayer1range()
{
    const float MAXPITCH = 90.0f;
    if(player1->pitch>MAXPITCH) player1->pitch = MAXPITCH;
    if(player1->pitch<-MAXPITCH) player1->pitch = -MAXPITCH;
    while(player1->yaw<0.0f) player1->yaw += 360.0f;
    while(player1->yaw>=360.0f) player1->yaw -= 360.0f;
};

void freelook(float yawdelta, float pitchdelta)
{
    if(player1->state==CS_DEAD || intermission) return;
    float zoompower = getzoompower();
    if(zoompower != 1.0f)
    {
        // e.g. 2x zoom = scale by 1/2
        yawdelta /= zoompower;
        pitchdelta /= zoompower;
    }
    player1->yaw += yawdelta;
    player1->pitch -= pitchdelta;
    fixplayer1range();
};

void mousemove(int dx, int dy)
{
    const float SENSF = 33.0f;     // try match quake sens
    float yawdelta = (dx/SENSF)*(sensitivity/(float)sensitivityscale);
    float pitchdelta = (dy/SENSF)*(sensitivity/(float)sensitivityscale)*(invmouse ? -1 : 1);
    freelook(yawdelta, pitchdelta);
};

void freelook_periodic_update(int cur_realtime)
{
    const float SENSF = 360.0f; // 360 DEG / second?
    float joyyawdelta = getjoylookx()*SENSF*cur_realtime/1000.0f;
    float joypitchdelta = getjoylooky()*SENSF*cur_realtime/1000.0f;
    freelook(joyyawdelta, joypitchdelta);
};

// locally generated damagekick from a non-local recreated attack
void virtualpush(dynent *d, vec &damagekick, bool useEnergyPush)
{
    if(editmode || intermission) return;    // ignore damagekick
    if(useEnergyPush) energypush(d, damagekick);
    else vadd(d->vel, damagekick);
};

// damage arriving from the network, monsters, yourself, all ends up here.

// returns int ignoredamage
// ignoredamage = 0: regular damage
// ignoredamage = -1: damage ignored
int selfdamage(int damage, int actor, dynent *act)
{
    if(player1->state!=CS_ALIVE) return -1;
    else if(editmode || intermission) { playsound(S_PAIN6); return -1; };

    // give player a kick depending on amount of damage
    float droll = damage/0.5f;
    addCameraRoll(player1, droll, 180);

    // damage before armour reduction ("incoming damage")
    inhibitexpregen(damage, 0);
    damagereceived += damage;

    // let armour absorb when possible
    int adamage[NUMARMOUR];
    loopi(NUMARMOUR) adamage[i] = 0;
    int ad = armourdamagecalc(damage, adamage);   
    damage -= ad;

    // damage after armour reduction ("health damage")
    inhibitexpregen(0, damage);
    damageblend(damage);
    demoblend(damage);
    player1->health -= damage;

    // log damage and resulting health
    extern int logbattledata;
    if(logbattledata)
    {
        printf("damage,total:%d,healthdamage:%d,armourdamage:%d,bluelost:%d,greenlost:%d,yellowlost:%d\n", damage+ad, damage, ad, adamage[A_BLUE], adamage[A_GREEN], adamage[A_YELLOW]);
        loghealth();
    };

    // check if dead and setup respawn if so; if not play pain sound
    if(player1->health<=0)
    {
        if(actor==-2)
        {
            conoutf("you got killed by %s!", act->name);
        }
        else if(actor==-1)
        {
            actor = getclientnum();
            conoutf("you suicided!");
            if(!m_sp) --player1->frags; // lose a point for suicide unless single-player
            addmsg(1, 2, SV_FRAGS, player1->frags);
        }
        else
        {
            dynent *a = getclient(actor);
            if(a)
            {
                if(isteam(a->team, player1->team))
                {
                    conoutf("you got fragged by a teammate (%s)", a->name);
                }
                else
                {
                    conoutf("you got fragged by %s", a->name);
                };
            };
        };
        showscores(true);
        addmsg(1, 2, SV_DIED, actor);
        player1->lifesequence++;
        player1->attacking = false;
        player1->state = CS_DEAD;
        enterDeathCamera();
        playsound(S_DIE1+rnd(2));
        player1->lastaction = lastmillis;
        player1->buffs[BUFF_QUAD] = 0;
        if(!m_arena) nextRespawn = SDL_GetTicks() + (m_sp ? SPTimeToRespawn : MPTimeToRespawn);
    }
    else
    {
        playsound(S_PAIN6);
    };
    return 0;
};

void timeupdate(int timeremain)
{
    if(!timeremain)
    {
        intermission = true;
        player1->attacking = false;
        conoutf("intermission:");
        conoutf("game has ended!");
        showscores(true);
    }
    else
    {
        conoutf("time remaining: %d minutes", timeremain);
    };
};

dynent *getclient(int cn)   // ensure valid entity
{
    if(cn<0 || cn>=MAXCLIENTS)
    {
        neterr("clientnum");
        return NULL;
    };
    while(cn>=players.length()) players.add(NULL);
    return players[cn] ? players[cn] : (players[cn] = new dynent);
};

void initclient()
{
    clientmap[0] = 0;
    initclientnet();
};

void startmap(char *name)   // called just after a map load
{
    if(netmapstart() && m_sp) { gamemode = 0; conoutf("coop sp not supported yet"); };
    sleepwait = 0;
    monsterstartmap();
    monsterclear();
    clearprojectiles();
    reset_particles();
    clear_dynlightsources();
    resetspawns();
    player1->frags = 0;
    loopv(players) if(players[i]) players[i]->frags = 0;
    initAccuracy();
    spawncycle = -1;
    spawnplayer(player1);
    copystring(clientmap, name);
    if(editmode) toggleedit();
    gamespeed = 100;
    setvar("fog", 180);
    setvar("fogcolour", 0x8099B3);
    showscores(false);
    showaccuracy(false);
    intermission = false;
    if(m_arena) arenaController.init(SDL_GetTicks());
    if(classicRuleset) iceCube2ClassicRuleset();
    else classic2IceCubeRuleset();
    defformatstring(msg)("game mode is %s", modename(gamemode));
    if(!classicRuleset) concatstring(msg, " (IceCube)");
    conoutf(msg);
    resetmutators();
    applymutators();
    mainloop_mapchanged_nodelay();
};

COMMANDN(map, changemap, ARG_1STR);
