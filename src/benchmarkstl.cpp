#include <queue>
#include <cstdio>

// Defined as VARP variables in benchmark.cpp
extern int framedatawindowlength;
extern int framedatawindowismillis;
extern int framedatawindowpartitionpercent;

using std::priority_queue;
using std::deque;
using std::less;

typedef priority_queue<int, deque<int>, less<int> > MaxHeap; // less: whether first argument is less than second argument

class FrametimesHeap;
const float calculate_minmax_part_average_fps(FrametimesHeap *h);

class FrametimesHeap : public MaxHeap
{
public:
    float totaltime;
    float totalframes;
    void init() { while(!empty()) pop(); totaltime = totalframes = 0; };
    virtual void insert(int value) = 0;
    virtual int readHeap() = 0;
    const float result() { return calculate_minmax_part_average_fps(this); };
};

class MaxFrametimesHeap : public FrametimesHeap
{
public:
    void insert(int value) { push(value); };
    int readHeap() { return top(); };
};

class MinFrametimesHeap : public FrametimesHeap
{
public:
    void insert(int value) { push(-value); };   // push negated value to invert strict weak ordering comparision
    int readHeap() { return -top(); };          // return negated read value to retrieve original value
};

MaxFrametimesHeap max_frametimes;
MinFrametimesHeap min_frametimes;

const bool ShowPerfCounterHeapDebugLog = false;

const float calculate_minmax_part_average_fps(FrametimesHeap *h)
{
    const char* names[] = { "max_frametimes", "min_frametimes" };
    const char* myName = h == &max_frametimes ? names[0] : names[1];

    // process top elements of heap until reached partition limit
    int partitionframes = 0;
    float partitiontime = 0.0f;
    if(ShowPerfCounterHeapDebugLog) printf("%s:", myName);
    while(!h->empty())
    {
        if(framedatawindowismillis) // == 1
        {
            if(partitiontime >= h->totaltime*framedatawindowpartitionpercent/100) break;
        }
        else // framedatawindowismillis == 0
        {
            if(partitionframes >= h->totalframes*framedatawindowpartitionpercent/100) break;
        };

        partitiontime += h->readHeap();
        if(ShowPerfCounterHeapDebugLog) printf(" %d", h->readHeap());
        h->pop();
        partitionframes++;
    };
    if(ShowPerfCounterHeapDebugLog) printf("\n");
    if(ShowPerfCounterHeapDebugLog) printf("%s: partitiontime=%4.1f, partitionframes=%d\n", myName, partitiontime, partitionframes);

    const bool valid = (partitiontime > 0.0f && partitionframes > 0.0f) ? true : false;

    return valid ? 1000.0f/(partitiontime/partitionframes) : 0;
};

float maxpart_fpsmin()
{
    return max_frametimes.result();
};

float minpart_fpsmax()
{
    return min_frametimes.result();
};

void push_minmax_frametime(int ft)
{
    max_frametimes.insert(ft);
    min_frametimes.insert(ft);
};

void reset_minmax_frametime_tracker()
{
    max_frametimes.init();
    min_frametimes.init();
};

void set_minmax_totalframes_totaltime(float tframes, float ttime)
{
    max_frametimes.totalframes = tframes;
    max_frametimes.totaltime = ttime;
    min_frametimes.totalframes = tframes;
    min_frametimes.totaltime = ttime;
};
